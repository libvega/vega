﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Astrum.Maths;
using Astrum.Render;

namespace Astrum;


/// <summary>Fluent-style builder for configuring and launching Astrum applications.</summary>
/// <remarks>Instances cannot be modified after an application is started with <see cref="Run"/>.</remarks>
public sealed class AppConfig
{
	/// <summary>Returns a new default app builder with the given arguments and initial scene type.</summary>
	/// <param name="args">The command line + additional arguments for the application.</param>
	/// <typeparam name="SceneT">The type of the initial scene to run.</typeparam>
	public static AppConfig New<SceneT>(IReadOnlyList<string> args) where SceneT : Scene, new() =>
		new(args, typeof(SceneT), () => new SceneT());

	/// <summary>Returns a new default app builder with the given arguments and initial scene factory.</summary>
	/// <param name="args">The command line + additional arguments for the application.</param>
	/// <param name="factory">The factory callback to create the initial scene instance with.</param>
	/// <typeparam name="SceneT">The type of the initial scene to run.</typeparam>
	public static AppConfig New<SceneT>(IReadOnlyList<string> args, Func<SceneT> factory) where SceneT : Scene =>
		new(args, typeof(SceneT), factory);


	private AppConfig(IReadOnlyList<string> args, Type sceneType, Func<Scene> factory)
	{
		ApplicationArgs = args.ToArray(); // Defensive copy
		InitialSceneType = sceneType;
		InitialSceneFactory = factory;
		_ = Core.IsMainThread; // Force Core static init early
	}


	/// <summary>Uses the current builder configuration to start a new Astrum application.</summary>
	/// <remarks>
	/// This method does not return until the application exits. This method cannot be called more than once. This
	/// method must be called on the main thread.
	/// </remarks>
	public void Run()
	{
		if (IsStarted) throw new InvalidOperationException("Cannot start multiple applications from one AppBuilder");
		if (!Core.IsMainThread) {
			throw new InvalidOperationException("Astrum applications must be run on the main thread");
		}
		IsStarted = true;

		Core.Run(this);
	}


	#region General
	// If the builder has been used to run an application and is locked
	internal bool IsStarted { get; private set; }

	// The provided command line arguments
	internal readonly IReadOnlyList<string> ApplicationArgs;
	// The initial scene type
	internal readonly Type InitialSceneType;
	// The initial scene factory
	internal readonly Func<Scene> InitialSceneFactory;

	// Core event handlers
	internal IAppEventHandler? EventHandler;

	/// <summary>Sets the event handler instance for application events.</summary>
	public AppConfig WithEventHandler(IAppEventHandler handler)
	{
		assertNotStarted();
		EventHandler = handler;
		return this;
	}
	#endregion // General


	#region Rendering
	/// <summary>Set the graphics device to use for the application.</summary>
	/// <param name="device">The device to use.</param>
	public AppConfig WithGraphicsDevice(GraphicsDevice device)
	{
		assertNotStarted();
		GraphicsDevice = device;
		return this;
	}
	internal GraphicsDevice? GraphicsDevice;

	/// <summary>Sets the display to open the main window on in fullscreen mode.</summary>
	/// <remarks>
	/// By default, the window will open in non-fullscreen mode on whichever display is considered the primary.
	/// </remarks>
	public AppConfig SetMainWindowDisplay(Display display)
	{
		assertNotStarted();
		MainWindowDisplay = display;
		return this;
	}
	internal Display? MainWindowDisplay;

	/// <summary>Sets the initial size of the main window.</summary>
	/// <remarks>By default, a size of <c>1366x768</c> is used.</remarks>
	public AppConfig SetMainWindowSize(uint width, uint height)
	{
		assertNotStarted();
		MainWindowSize = new(width, height);
		return this;
	}
	internal Point2U? MainWindowSize;

	/// <summary>Sets the title of the main window.</summary>
	/// <remarks>By default, the title will be the application name.</remarks>
	public AppConfig SetMainWindowTitle(string title)
	{
		assertNotStarted();
		MainWindowTitle = title;
		return this;
	}
	internal string? MainWindowTitle;

	/// <summary>Tells the render device to use more space for the uniform and streaming buffers.</summary>
	/// <remarks>
	/// By default, the render device uses <c>1MB</c> for uniform space and <c>2MB</c> for streaming buffer space per
	/// application frame. The increased sizes are <c>4MB</c> and <c>8MB</c> per frame, respectively (a 4x increase).
	/// </remarks>
	/// <param name="uniform">If the uniform buffer space should be increased.</param>
	/// <param name="stream">If the streaming buffer space should be increased.</param>
	public AppConfig UseLargeBufferSpace(bool uniform, bool stream)
	{
		assertNotStarted();
		LargeBufferSpace = new(uniform, stream);
		return this;
	}
	internal LargeBufferSpaceConfig LargeBufferSpace;
	internal readonly record struct LargeBufferSpaceConfig(bool Uniform, bool Streaming);

	/// <summary>Tells the render devices to support larger limits for texture resources.</summary>
	/// <remarks>
	/// By default, the render device supports 32 writeable textures and 4,096 readonly textures. The increased limits
	/// are 128 and 16,384 respectively.
	/// </remarks>
	/// <param name="writeTextures">If the number of writeable textures should be increased.</param>
	/// <param name="textures">If the number of readonly textures should be increased.</param>
	public AppConfig UseLargeTextureTables(bool writeTextures, bool textures)
	{
		assertNotStarted();
		LargeTextureTables = new(writeTextures, textures);
		return this;
	}
	internal LargeTextureTableConfig LargeTextureTables;
	internal readonly record struct LargeTextureTableConfig(bool Write, bool Readonly);
	#endregion // Rendering


	private void assertNotStarted()
	{
		if (IsStarted) {
			throw new InvalidOperationException("Cannot modify an AppBuilder instance that is already started");
		}
	}
}
