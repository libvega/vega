﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Represents the runtime stages of an Astrum application.</summary>
/// <remarks>
/// The order of stages in normal execution is:
/// <list type="bullet">
/// <item><see cref="NotStarted"/></item>
/// <item><see cref="Initializing"/></item>
/// <item><em>Main application loop:</em>
/// <list type="bullet">
///		<item><see cref="Updating"/></item>
///		<item><see cref="Rendering"/></item>
///		<item><see cref="Waiting"/></item>
/// </list></item>
/// <item><see cref="Terminating"/></item>
/// <item><see cref="Ended"/></item>
/// </list>
/// <para>The value order of the enum values is in the order they appear in the application.</para>
/// </remarks>
public enum AppStage : uint
{
	/// <summary>The application has not been started.</summary>
	NotStarted = 0,
	/// <summary>The application has started and is currently initializing.</summary>
	Initializing,
	/// <summary>The application is in the main loop and is executing update logic.</summary>
	Updating,
	/// <summary>The application is in the main loop and is executing rendering logic.</summary>
	Rendering,
	/// <summary>The application is in the main loop and is in-between frames.</summary>
	Waiting,
	/// <summary>The application is terminating.</summary>
	Terminating,
	/// <summary>The application has terminated and is no longer running.</summary>
	Ended
}
