﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;

namespace Astrum;


// [Partial] Application timing
public static partial class Core
{
	/// <summary>Provides timing information about the application.</summary>
	public static class Time
	{
		private const uint FPS_HISTORY = 10; // The number of previous frames to use for average fps


		#region Fields
		/// <summary>The time at which the application started running.</summary>
		public static DateTime StartTime { get; private set; } = DateTime.MinValue;

		/// <summary>The current application frame number.</summary>
		public static ulong FrameCount { get; private set; }

		/// <summary>The total elapsed application time at the start of the current frame.</summary>
		public static TimeSpan Elapsed { get; private set; }
		/// <summary>The value of <see cref="Elapsed"/> in seconds.</summary>
		public static float ElapsedSec { get; private set; }
		/// <summary>The value of <see cref="Elapsed"/> at the start of the last frame.</summary>
		public static TimeSpan LastElapsed { get; private set; }
		/// <summary>The value of <see cref="LastElapsed"/> in seconds.</summary>
		public static float LastElapsedSec { get; private set; }

		/// <summary>The raw elapsed application runtime, not tied to application frames.</summary>
		public static TimeSpan RawElapsed => _Timer.Elapsed;
		/// <summary>The value of <see cref="RawElapsed"/> in seconds.</summary>
		public static float RawElapsedSec => (float)RawElapsed.TotalSeconds;

		/// <summary>The total amount of time elapsed between the current and last frames.</summary>
		/// <remarks>The value of this timing is affected by <see cref="TimeScale"/>.</remarks>
		public static TimeSpan Delta { get; private set; }
		/// <summary>The value of <see cref="Delta"/> in seconds.</summary>
		public static float DeltaSec { get; private set; }
		/// <summary>The real (unscaled) amount of time elapsed between the current and last app frames.</summary>
		/// <remarks>The value of this timing in <em>not</em> affected by <see cref="TimeScale"/>.</remarks>
		public static TimeSpan RealDelta { get; private set; }
		/// <summary>The value of <see cref="RealDelta"/> in seconds.</summary>
		public static float RealDeltaSec { get; private set; }

		/// <summary>The global scaling value for <see cref="Delta"/>, in <c>[0, inf)</c>.</summary>
		/// <remarks>Changes to this value will not take effect until the next application frame.</remarks>
		public static float TimeScale {
			get => _Scale;
			set => _NewScale = MathF.Max(value, 0);
		}
		private static float _Scale;
		private static float? _NewScale;

		/// <summary>The raw FPS of the last app frame.</summary>
		public static float RawFps => _FpsHistory[_FpsIndex];
		/// <summary>The application FPS averaged over the last few app frames.</summary>
		public static float Fps { get; private set; }
		private static readonly float[] _FpsHistory = new float[FPS_HISTORY];
		private static uint _FpsIndex;

		// Time tracking stopwatch
		private static readonly Stopwatch _Timer = new();
		#endregion // Fields


		/// <summary>Gets if the current app frame is the first frame at or after the given elapsed time.</summary>
		/// <param name="span">The elapsed time to check for.</param>
		public static bool IsElapsedTime(TimeSpan span) => LastElapsed < span && Elapsed >= span;

		/// <summary>
		/// Gets if the current app frame is the first frame ar or after a multiple of the given elapsed time.
		/// </summary>
		/// <param name="span">The elapsed time multiple to check for.</param>
		public static bool IsTimeMultiple(TimeSpan span) => Elapsed.Ticks % span.Ticks < LastElapsed.Ticks % span.Ticks;

		/// <summary>
		/// Gets if the current app frame is the first frame ar or after a multiple and offset of the given elapsed time.
		/// </summary>
		/// <param name="span">The elapsed time multiple to check for.</param>
		/// <param name="offset">The offset of the elapsed time.</param>
		public static bool IsTimeMultiple(TimeSpan span, TimeSpan offset) =>
			(Elapsed.Ticks + offset.Ticks) % span.Ticks < (LastElapsed.Ticks + offset.Ticks) % span.Ticks;


		// Sets up the initial time values and start time calculations
		internal static void Start()
		{
			FrameCount = 0;
			Elapsed = TimeSpan.Zero;     ElapsedSec = 0;
			LastElapsed = TimeSpan.Zero; LastElapsedSec = 0;
			Delta = TimeSpan.Zero;       DeltaSec = 0;
			RealDelta = TimeSpan.Zero;   RealDeltaSec = 0;
			_Scale = 1;
			_NewScale = null;

			Array.Fill(_FpsHistory, 0);
			_FpsIndex = 0;

			StartTime = DateTime.Now;
			_Timer.Restart();
		}

		// Updates the time at the start of a frame
		internal static void Update()
		{
			FrameCount += 1;

			// Update scale
			if (_NewScale.HasValue) {
				_Scale = _NewScale.Value;
				_NewScale = null;
			}

			// Update time values
			LastElapsed = Elapsed;
			LastElapsedSec = (float)LastElapsed.TotalSeconds;
			Elapsed = _Timer.Elapsed;
			ElapsedSec = (float)Elapsed.TotalSeconds;
			RealDelta = Elapsed - LastElapsed;
			RealDeltaSec = (float)RealDelta.TotalSeconds;
			Delta = RealDelta * _Scale;
			DeltaSec = (float)Delta.TotalSeconds;

			// Update fps
			_FpsIndex = (_FpsIndex + 1u) % (uint)_FpsHistory.Length;
			_FpsHistory[_FpsIndex] = (float)(1 / RealDelta.TotalSeconds);
			Fps = 0;
			for (uint fi = 0; fi < _FpsHistory.Length; ++fi) {
				Fps += _FpsHistory[fi];
			}
			Fps /= MathF.Min(FrameCount, _FpsHistory.Length);
		}
	}
}
