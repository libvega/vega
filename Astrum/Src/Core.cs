﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Astrum.Native;
using Astrum.Render;
using static Astrum.EngineLogger;

namespace Astrum;


/// <summary>Core interface for application configuration, resource management, and runtime logic.</summary>
public static partial class Core
{
	#region Fields
	/// <summary>The reflected assembly for the running application.</summary>
	public static readonly Assembly ApplicationAssembly;
	/// <summary>The name of the application provided by <see cref="AstrumApplicationAttribute"/>.</summary>
	public static readonly string ApplicationName;
	/// <summary>The path to the application cache directory in the system local app data.</summary>
	public static readonly string ApplicationCachePath;

	// Cache directory for Astrum
	internal static readonly string EngineCachePath;

	// Priority queue for process exit handling
	internal static readonly PriorityQueue<Action, int> ProcessExitQueue = new();

	/// <summary>If the current Astrum runtime is a Develop build.</summary>
	public static readonly bool IsDevelop;


	/// <summary>The current stage of the application.</summary>
	public static AppStage Stage { get; private set; } = AppStage.NotStarted;
	/// <summary>If the application is currently running (initializing, in the main loop, or terminating).</summary>
	public static bool IsRunning => Stage is not (AppStage.NotStarted or AppStage.Ended);

	// The cached AppBuilder instance for the running application
	internal static AppConfig? Builder { get; private set; }

	// Exit flag
	private static bool _ShouldExit;
	#endregion // Fields


	// Performs the actual runtime logic (initialize, main loop, terminate) for the application
	internal static void Run(AppConfig config)
	{
		if (Stage != AppStage.NotStarted) throw new InvalidOperationException("Cannot start app more than once");
		AssertMainThread();

		EngineInfo("Starting new Astrum graphical application");
		Builder = config;

		// Wrap the entire app body to handle exceptions
		try {
			Stage = AppStage.Initializing;

			// Startup core engines and services
			EngineInfo("Initializing core Astrum services");
			Time.Start();
			RenderCore.Initialize(config);

			// User initialization including initial scene
			config.EventHandler?.OnInitialize();

			// Cleanup from loading, and force all core long-lived objects into Gen 2
			GC.Collect(0, GCCollectionMode.Forced);
			GC.WaitForPendingFinalizers();
			GC.Collect(2, GCCollectionMode.Aggressive);

			// Main application loop
			EngineInfo("Entering main application loop");
			_ShouldExit = false;
			while (!_ShouldExit) {
				// Pre-update
				Stage = AppStage.Updating;
				Time.Update();
				RenderCore.StartFrame();
				Glfw.PollEvents(); // Main system event pump
				
				// User pre-updates
				Coroutine.TickAndUpdate();
				config.EventHandler?.OnPreUpdate();

				// Update

				// Post-update
				config.EventHandler?.OnPostUpdate();

				// Exit early if requested
				if (_ShouldExit) break;

				// Pre-render
				Stage = AppStage.Rendering;
				config.EventHandler?.OnPreRender();

				// Render

				// Post-render
				config.EventHandler?.OnPostRender();
				if (!RenderCore.EndFrame()) {  // Exit on main window closed
					EngineInfo("Main application window was closed");
					break;
				} 

				// End frame
				Stage = AppStage.Waiting;
			}
			EngineInfo("Exited main application loop normally");

			// User cleanup
			Stage = AppStage.Terminating;
			config.EventHandler?.OnTerminate();
			Coroutine.StopAll();

			// Engine cleanup
			EngineInfo("Shutting down core application systems");
			RenderCore.Shutdown();
		}
		catch (Exception ex) {
			EngineFatal("Unhandled exception at the top level", ex);
			config.EventHandler?.OnTopLevelException(ex);
			throw;
		}
		finally {
			Stage = AppStage.Ended;
			Builder = null;
		}
	}


	/// <summary>Requests that the application exit at the end of the current frame.</summary>
	/// <remarks>
	/// If called during the update phase, then rendering will be skipped. It is possible for the application to ignore
	/// the exit request if a custom exit request handler is installed.
	/// </remarks>
	public static void RequestExit()
	{
		if (!IsRunning) return;
		
		if (Builder?.EventHandler?.OnExitRequest() ?? true) {
			_ShouldExit = true;
			EngineInfo("Application exit has been requested");
		}
		else {
			EngineInfo("Application exit was requested but cancelled by the handler");
		}
	}


	#region Threading
	internal static readonly int MainThreadId; // Id of main application thread (detected through reflection)

	/// <summary>Gets if the calling thread is the main application thread.</summary>
	public static bool IsMainThread => Environment.CurrentManagedThreadId == MainThreadId;

	/// <summary>Throws an exception if the calling thread is not the main application thread.</summary>
	/// <param name="msg">An additional message to include with the exception if thrown.</param>
	[MethodImpl(MethodImplOptions.NoInlining)]
	public static void AssertMainThread(string? msg = null)
	{
		if (IsMainThread) return;
		var caller = new StackTrace().GetFrame(1)!.GetMethod()!;
		var name = $"{caller.DeclaringType?.Name ?? "<Unknown>"}.{caller.Name}";
		throw msg is null
			? new InvalidOperationException($"Operation must occur on main application thread ({name})")
			: new InvalidOperationException($"Operation must occur on main application thread ({name}) - {msg}");
	}
	#endregion // Threading


	static Core()
	{
#if DEVELOP
		IsDevelop = true;
#else
		IsDevelop = false;
#endif // DEVELOP

		// Threading (see https://stackoverflow.com/a/2374560)
		var entry = Assembly.GetEntryAssembly()?.EntryPoint ??
			throw new PlatformNotSupportedException("Cannot read the application entry point");
		if (new StackTrace().GetFrames().FirstOrDefault(sf => sf.GetMethod() == entry) is null) {
			throw new InvalidOperationException("Astrum must be initialized on the main application thread");
		}
		MainThreadId = Environment.CurrentManagedThreadId;

		// Get the assembly and app info
		ApplicationAssembly = Assembly.GetEntryAssembly() ??
			throw new PlatformNotSupportedException("Failed to reflect the application assembly");
		ApplicationName = AstrumApplicationAttribute.Get().ApplicationName;

		// Setup derived app info
		var localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
		ApplicationCachePath = Path.Combine(localAppData, ApplicationName, "Cache");
		EngineCachePath = Path.Combine(localAppData, "Astrum", "Cache");

		// Setup process exit handling
		AppDomain.CurrentDomain.ProcessExit += (_, _) => {
			while (ProcessExitQueue.TryDequeue(out var action, out _)) { // Lowest -> Highest
				action();
			}
		};
		ProcessExitQueue.Enqueue(Logger.ShutdownAll, Int32.MaxValue - 1); // Shutdown logger as the very last thing
	}
}
