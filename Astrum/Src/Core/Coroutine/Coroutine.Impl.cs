﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Astrum;


// [Partial] Implementations for Coroutine and Coroutine management
public abstract partial class Coroutine
{
	// Active coroutines
	private static readonly List<Coroutine> _Coroutines = new();
	// New coroutines to add after ticking
	private static readonly List<Coroutine> _NewCoroutines = new();
	// Flag if ticking is active
	private static volatile bool _Ticking;


	#region Start
	/// <summary>Starts the coroutine to be ticked.</summary>
	/// <param name="co">The coroutine to start. Must not have already been started.</param>
	/// <returns>The same coroutine instance.</returns>
	public static Coroutine Start(Coroutine co)
	{
		if (co.IsRunning || co.TickCount > 0) {
			throw new ArgumentException("Cannot start a coroutine more than once", nameof(co));
		}
		return StartCoroutine(co);
	}

	/// <summary>Starts a new coroutine.</summary>
	/// <typeparam name="T">The coroutine type to construct and start.</typeparam>
	/// <returns>The new coroutine instance.</returns>
	public static T Start<T>() where T : Coroutine, new() => (T)StartCoroutine(new T());

	/// <summary>Starts a coroutine that is driven by an enumerator.</summary>
	/// <param name="enumerator">The enumerator to call once per tick.</param>
	/// <param name="unscaled">See <see cref="UseUnscaledTime"/>.</param>
	/// <returns>The new coroutine instance.</returns>
	public static Coroutine Start(IEnumerator<object?> enumerator, bool unscaled = false) =>
		StartCoroutine(new EnumeratorCoroutine(enumerator, unscaled));
	#endregion // Start


	#region Scheduling
	/// <summary>Schedules an optionally-repeating action to occur after an initial delay.</summary>
	/// <param name="action">The action to call, returns the next wait time or <c>null</c> to exit.</param>
	/// <param name="delay">The initial delay.</param>
	/// <param name="unscaled">See <see cref="UseUnscaledTime"/>.</param>
	/// <returns>The new coroutine instance.</returns>
	public static Coroutine Schedule(Func<TimeSpan?> action, TimeSpan delay, bool unscaled = false) =>
		StartCoroutine(new DelayRepeatCoroutine(action, delay, unscaled));

	/// <summary>Schedules an optionally-repeating action to occur after an initial delay.</summary>
	/// <param name="action">The action to call, returns <c>true</c> to repeat or <c>false</c> to stop.</param>
	/// <param name="delay">The initial delay.</param>
	/// <param name="repeat">The time to wait between calls to the action.</param>
	/// <param name="unscaled">See <see cref="UseUnscaledTime"/>.</param>
	/// <returns>The new coroutine instance.</returns>
	public static Coroutine Schedule(Func<bool> action, TimeSpan delay, TimeSpan repeat, bool unscaled = false) =>
		StartCoroutine(new DelayRepeatCoroutine(() => action() ? repeat : null, delay, unscaled));

	/// <summary>Calls the given action once after a given delay.</summary>
	/// <param name="action">The action to call.</param>
	/// <param name="delay">The delay before calling the action.</param>
	/// <param name="unscaled">See <see cref="UseUnscaledTime"/>.</param>
	/// <returns>The new coroutine instance.</returns>
	public static Coroutine Delay(Action action, TimeSpan delay, bool unscaled = false) =>
		StartCoroutine(new DelayRepeatCoroutine(() => { action(); return null; }, delay, unscaled));

	/// <summary>Schedules an action to repeat a set number of times after an initial delay.</summary>
	/// <param name="action">The action to call.</param>
	/// <param name="count">The total number of times to call the action.</param>
	/// <param name="delay">The initial delay.</param>
	/// <param name="repeat">The time to wait between calls to the action.</param>
	/// <param name="unscaled">See <see cref="UseUnscaledTime"/>.</param>
	/// <returns>The new coroutine instance.</returns>
	public static Coroutine Repeat(Action action, uint count, TimeSpan delay, TimeSpan repeat, bool unscaled = false)
	{
		return StartCoroutine(new EnumeratorCoroutine(repeatEnumerator(action, count, repeat, delay), unscaled));

		static IEnumerator<object?> repeatEnumerator(Action action, uint count, TimeSpan delay, TimeSpan repeat)
		{
			if (delay > TimeSpan.Zero) yield return delay;
			while (count-- > 0) {
				action();
				yield return repeat;
			}
			yield return DONE;
		}
	}
	#endregion // Scheduling


	// Ticks all coroutines
	internal static void TickAndUpdate()
	{
		// Protect _Coroutines
		lock (_NewCoroutines) _Ticking = true;

		// Check and tick coroutines
		foreach (var co in _Coroutines) {
			// Not running (will be removed)
			if (!co.IsRunning) continue;

			// Update wait as needed
			if (co.WaitTime is { } waitTime) {
				var delta = co.UseUnscaledTime ? Core.Time.RealDelta : Core.Time.Delta;
				if ((co.WaitTime = waitTime - delta) <= TimeSpan.Zero) co.WaitTime = null;
				else continue;
			}
			else if (co.WaitCoroutine is { } waitCo) {
				if (!waitCo.IsRunning) co.WaitCoroutine = null;
				else continue;
			}
			else if (co.WaitFunction is { } waitFn) {
				try {
					if (waitFn()) co.WaitFunction = null;
					else continue;
				}
				catch (Exception ex) {
					co.IsRunning = false;
					try { co.OnCancel(ex); } catch { }
					continue;
				}
			}
			else if (co.WaitTask is { } waitTask) {
				if (waitTask.IsCompleted) co.WaitTask = null;
				else continue;
			}

			// Tick
			++co.TickCount;
			object? ret;
			try {
				ret = co.Tick();
			}
			catch (Exception ex) {
				co.IsRunning = false;
				try { co.OnCancel(ex); } catch { }
				continue;
			}

			// Update state as needed
			switch (ret)
			{
				case TimeSpan ts:     co.WaitTime = ts > TimeSpan.Zero ? ts : null; break;
				case Coroutine newCo: co.WaitCoroutine = newCo.IsRunning ? newCo : null; break;
				case Func<bool> fn:   co.WaitFunction = fn; break;
				case Task task:       co.WaitTask = task.IsCompleted ? null : task; break;
				case not null when ReferenceEquals(ret, DONE): co.IsRunning = false; break;
				case not null:        throw new InvalidTickReturnException(ret);
			}
		}

		// Remove finished ones and add pending ones
		_Coroutines.RemoveAll(static co => {
			if (co.IsRunning) return false;
			try { co.OnRemove(); } catch { }
			return true;
		});
		lock (_NewCoroutines) {
			foreach (var co in _NewCoroutines) _Coroutines.Add(co);
			_NewCoroutines.Clear();
		}

		// Unprotect _Coroutines
		lock (_NewCoroutines) _Ticking = false;
	}

	// Starts a new coroutine
	private static Coroutine StartCoroutine(Coroutine co)
	{
		Debug.Assert(!co.IsRunning);

		lock (_NewCoroutines) {
			(_Ticking ? _NewCoroutines : _Coroutines).Add(co);
		}
		co.IsRunning = true;
		return co;
	}

	// Stops all coroutines
	internal static void StopAll()
	{
		lock (_NewCoroutines) {
			foreach (var co in _Coroutines) try { co.OnCancel(null); } catch { }
			foreach (var co in _NewCoroutines) try { co.OnCancel(null); } catch { }
			_Coroutines.Clear();
			_NewCoroutines.Clear();
		}
	}


	// Coroutine for enumerator objects
	private sealed class EnumeratorCoroutine : Coroutine
	{
		private readonly IEnumerator<object?> _ticker;
		private readonly bool _unscaled;
		public override bool UseUnscaledTime => _unscaled;

		public EnumeratorCoroutine(IEnumerator<object?> ticker, bool unscaled) =>
			(_ticker, _unscaled) = (ticker, unscaled);

		protected override object? Tick() => _ticker.MoveNext() ? _ticker.Current : DONE;
	}


	// Delay/repeat coroutine that calls an action
	private sealed class DelayRepeatCoroutine : Coroutine
	{
		private readonly Func<TimeSpan?> _action;
		private readonly TimeSpan _initialDelay;
		private readonly bool _unscaled;
		public override bool UseUnscaledTime => _unscaled;

		public DelayRepeatCoroutine(Func<TimeSpan?> action, TimeSpan delay, bool unscaled) =>
			(_action, _initialDelay, _unscaled) = (action, delay >= TimeSpan.Zero ? delay : TimeSpan.Zero, unscaled);

		protected override object? Tick()
		{
			var repeat = _action();
			if (repeat.HasValue) {
				return repeat.Value <= TimeSpan.Zero ? null : repeat.Value;
			}
			return DONE;
		}
	}
}
