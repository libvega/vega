﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Threading.Tasks;

namespace Astrum;


/// <summary>Supports lightweight delayed and repeating task execution.</summary>
/// <remarks>All coroutines execute on the main thread, and their responsiveness is tied to the frame rate.</remarks>
public abstract partial class Coroutine
{
	/// <summary>Special return value for <see cref="Tick"/> indicating the coroutine has finished execution.</summary>
	// ReSharper disable once InconsistentNaming
	protected static readonly object DONE = new();


	#region Fields
	/// <summary>If the coroutine is actively running (being ticked).</summary>
	public bool IsRunning { get; internal set; }

	/// <summary>The number of times <see cref="Tick"/> has been called for the coroutine.</summary>
	public uint TickCount { get; internal set; }

	/// <summary>If the coroutine is currently waiting before continuing ("paused").</summary>
	public bool IsWaiting => IsRunning && (
		(WaitTime is { } wt && wt > TimeSpan.Zero) ||
		(WaitCoroutine is not null && WaitCoroutine.IsRunning) ||
		WaitFunction is not null ||
		(WaitTask is not null && WaitTask.IsCompleted)
	);

	/// <summary>If <c>false</c>, time-based waits for the coroutine ignore <see cref="Core.Time.TimeScale"/>.</summary>
	/// <remarks>Defaults to <c>false</c>.</remarks>
	public virtual bool UseUnscaledTime => false;

	// Tracking objects for waits
	internal TimeSpan? WaitTime;
	internal Coroutine? WaitCoroutine;
	internal Func<bool>? WaitFunction;
	internal Task? WaitTask;
	#endregion // Fields


	/// <summary>Cancels the coroutine execution, removing the coroutine at the next application frame.</summary>
	public void Cancel()
	{
		if (!IsRunning) return;
		OnCancel(null);
		IsRunning = false;
	}


	/// <summary>Perform the logic for a single coroutine tick.</summary>
	/// <remarks>
	/// The return value for this method dictates how the coroutine will continue executing, and must be one of:
	/// <list type="bullet">
	/// <item><c>null</c> - The coroutine will tick again on the next app frame.</item>
	/// <item><c>Coroutine</c> - The coroutine will pause until the returned coroutine is complete.</item>
	/// <item><c>TimeSpan</c> - The coroutine will pause for the given amount of time, must be zero or positive.</item>
	/// <item>
	/// <c>Func&lt;bool&gt;</c> - The coroutine will pause until the function (called every frame) returns true.
	/// </item>
	/// <item><c>Task</c> - The coroutine will pause until the task is finished.</item>
	/// <item><see cref="DONE"/> - The coroutine is complete and will not be ticked again.</item>
	/// <item>Any other type will generate a <see cref="InvalidTickReturnException"/> error.</item>
	/// </list>
	/// </remarks>
	/// <returns>The coroutine execution control object, see remarks for supported types.</returns>
	protected abstract object? Tick();

	/// <summary>Called when the coroutine is explicitly cancelled or throws an exception.</summary>
	/// <param name="ex">The exception that cancelled the coroutine, or <c>null</c> if manually cancelled.</param>
	protected virtual void OnCancel(Exception? ex) { }

	/// <summary>Called when the coroutine is removed after finishing execution or cancelled.</summary>
	protected virtual void OnRemove() { }


	/// <summary>Exception generated when <see cref="Coroutine.Tick"/> returns an invalid object.</summary>
	public sealed class InvalidTickReturnException : Exception
	{
		/// <summary>The invalid object returned by the Tick function.</summary>
		public readonly object Value;

		internal InvalidTickReturnException(object value) :
			base($"Coroutine.Tick() returned invalid object '{value}' ({value.GetType()})") => Value = value;
	}
}
