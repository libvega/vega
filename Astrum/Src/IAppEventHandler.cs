﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Interface for handling the lifecycle events associated with an Astrum application.</summary>
public interface IAppEventHandler
{
	/// <summary>Called when core initialization is complete.</summary>
	/// <remarks>All core services will be ready when this is called.</remarks>
	void OnInitialize() { }

	/// <summary>Called each application frame before updating the active scene.</summary>
	void OnPreUpdate() { }

	/// <summary>Called each application frame after updating the active scene.</summary>
	void OnPostUpdate() { }

	/// <summary>Called each application frame before executing the render graph.</summary>
	void OnPreRender() { }

	/// <summary>Called each application frame after executing the render graph.</summary>
	void OnPostRender() { }

	/// <summary>Called when the application is terminated.</summary>
	void OnTerminate() { }

	/// <summary>Called when an unhandled exception reaches the top level of the application.</summary>
	/// <remarks>This cannot be recovered from, the application will exit after this call.</remarks>
	/// <param name="ex">The exception that propagated to the top level.</param>
	void OnTopLevelException(Exception ex) { }

	/// <summary>Called to handle <see cref="Core.RequestExit"/>.</summary>
	/// <returns><c>true</c> to allow the application to exit, <c>false</c> to cancel the application exit.</returns>
	bool OnExitRequest() => true;
}
