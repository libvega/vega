﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using System.Text;
using Astrum.Maths;
using static LLVK.Vulkan;
using static LLVK.Vulkan.KHR;
// ReSharper disable IdentifierTypo InconsistentNaming

#pragma warning disable CS0649 // Field is never assigned to, and will always have its default value

namespace Astrum.Native;


// GLFW native API functions
internal static unsafe partial class Glfw
{
	#region Monitors
	// glfwGetMonitors
	public static ReadOnlySpan<MonitorHandle> GetMonitors()
	{
		int count;
		var monPtr = _Library.GetMonitors(&count);
		return new(monPtr, count);
	}

	// glfwGetPrimaryMonitor
	public static MonitorHandle GetPrimaryMonitor() => _Library.GetPrimaryMonitor();

	// glfwGetMonitorName
	public static string GetMonitorName(MonitorHandle handle) =>
		Marshal.PtrToStringAnsi((nint)_Library.GetMonitorName(handle)) ?? "Unknown";

	// glfwGetMonitorPhysicalSize
	public static Point2U GetMonitorPhysicalSize(MonitorHandle handle)
	{
		int w, h;
		_Library.GetMonitorPhysicalSize(handle, &w, &h);
		return new((uint)w, (uint)h);
	}

	// glfwGetVideoMode
	public static VidMode GetVideoMode(MonitorHandle handle) => *_Library.GetVideoMode(handle);

	// glfwGetMonitorPos
	public static Point2 GetMonitorPos(MonitorHandle handle)
	{
		int x, y;
		_Library.GetMonitorPos(handle, &x, &y);
		return new(x, y);
	}

	// glfwGetMonitorWorkarea
	public static Box2 GetMonitorWorkarea(MonitorHandle handle)
	{
		int x, y, w, h;
		_Library.GetMonitorWorkarea(handle, &x, &y, &w, &h);
		return new(x, y, x + w, y + h);
	}
	#endregion // Monitors


	#region Windows
	// glfwCreateWindow
	public static WindowHandle CreateWindow(uint width, uint height, string title, MonitorHandle? monitor)
	{
		var strPtr = stackalloc byte[title.Length * 4 + 1]; // Worst-case length
		var len = Encoding.UTF8.GetBytes(title, new(strPtr, title.Length * 4));
		strPtr[len] = 0;
		return _Library.CreateWindow((int)width, (int)height, strPtr, monitor ?? default, default);
	}

	// glfwDestroyWindow
	public static void DestroyWindow(WindowHandle handle) => _Library.DestroyWindow(handle);

	// glfwWindowShouldClose
	public static bool WindowShouldClose(WindowHandle handle) => _Library.WindowShouldClose(handle) == GLFW_TRUE;

	// glfwSetWindowShouldClose
	public static void SetWindowShouldClose(WindowHandle handle, bool flag) =>
		_Library.SetWindowShouldClose(handle, flag ? GLFW_TRUE : GLFW_FALSE);

	// glfwSetWindowAttrib
	public static void SetWindowAttrib(WindowHandle handle, WindowAttrib attrib, int value) =>
		_Library.SetWindowAttrib(handle, attrib, value);
	
	// glfwGetWindowAttrib
	public static int GetWindowAttrib(WindowHandle handle, WindowAttrib attrib) =>
		_Library.GetWindowAttrib(handle, attrib);
	
	// glfwGetWindowMonitor
	public static MonitorHandle GetWindowMonitor(WindowHandle handle) => _Library.GetWindowMonitor(handle);

	// glfwSetWindowMonitor
	public static void SetWindowMonitor(WindowHandle handle, MonitorHandle monitor, int x, int y, uint w, uint h, int rate) =>
		_Library.SetWindowMonitor(handle, monitor, x, y, (int)w, (int)h, rate);
	
	// glfwSetWindowTitle
	public static void SetWindowTitle(WindowHandle handle, string title)
	{
		var strPtr = stackalloc byte[title.Length * 4 + 1]; // Worst-case length
		var len = Encoding.UTF8.GetBytes(title, new(strPtr, title.Length * 4));
		strPtr[len] = 0;
		_Library.SetWindowTitle(handle, strPtr);
	}
	
	// glfwGetWindowPos
	public static (int X, int Y) GetWindowPos(WindowHandle handle)
	{
		int x, y;
		_Library.GetWindowPos(handle, &x, &y);
		return (x, y);
	}

	// glfwSetWindowPos
	public static void SetWindowPos(WindowHandle handle, int x, int y) => _Library.SetWindowPos(handle, x, y);

	// glfwGetWindowSize
	public static (uint W, uint H) GetWindowSize(WindowHandle handle)
	{
		int w, h;
		_Library.GetWindowSize(handle, &w, &h);
		return ((uint)w, (uint)h);
	}

	// glfwSetWindowSize
	public static void SetWindowSize(WindowHandle handle, uint x, uint y) => 
		_Library.SetWindowSize(handle, (int)x, (int)y);
	
	// glfwGetFramebufferSize
	public static (uint W, uint H) GetFramebufferSize(WindowHandle handle)
	{
		int w, h;
		_Library.GetFramebufferSize(handle, &w, &h);
		return ((uint)w, (uint)h);
	}
	
	// glfwFocusWindow
	public static void FocusWindow(WindowHandle handle) => _Library.FocusWindow(handle);

	// glfwRequestWindowAttention
	public static void RequestWindowAttention(WindowHandle handle) => _Library.RequestWindowAttention(handle);

	// glfwIconifyWindow
	public static void IconifyWindow(WindowHandle handle) => _Library.IconifyWindow(handle);

	// glfwRestoreWindow
	public static void RestoreWindow(WindowHandle handle) => _Library.RestoreWindow(handle);

	// glfwMaximizeWindow
	public static void MaximizeWindow(WindowHandle handle) => _Library.MaximizeWindow(handle);
	
	// glfwSetWindowSizeLimits
	public static void SetWindowSizeLimits(WindowHandle handle, int minW, int minH, int maxW, int maxH) =>
		_Library.SetWindowSizeLimits(handle, minW, minH, maxW, maxH);
	
	// glfwCreateWindowSurface
	public static VkResult CreateWindowSurface(VkInstance inst, WindowHandle handle, VkSurfaceKHR* surface) =>
		_Library.CreateWindowSurface(inst, handle, null, surface);
	#endregion // Windows
	
	
	// glfwPollEvents
	public static void PollEvents() => _Library.PollEvents();


	// Contains the native library handle and function symbols
	// ReSharper disable MemberHidesStaticFromOuterClass
	private sealed class Library
	{
		private readonly nint _handle;

		[NativeFunction("glfwInit")]
		public readonly delegate* unmanaged<int> Init;
		[NativeFunction("glfwTerminate")]
		public readonly delegate* unmanaged<void> Terminate;
		[NativeFunction("glfwPollEvents")]
		public readonly delegate* unmanaged<void> PollEvents;

		[NativeFunction("glfwSetErrorCallback")]
		public readonly delegate* unmanaged<void*, void*> SetErrorCallback;

		[NativeFunction("glfwGetMonitors")]
		public readonly delegate* unmanaged<int*, MonitorHandle*> GetMonitors;
		[NativeFunction("glfwGetPrimaryMonitor")]
		public readonly delegate* unmanaged<MonitorHandle> GetPrimaryMonitor;
		[NativeFunction("glfwGetMonitorName")]
		public readonly delegate* unmanaged<MonitorHandle, byte*> GetMonitorName;
		[NativeFunction("glfwGetMonitorPhysicalSize")]
		public readonly delegate* unmanaged<MonitorHandle, int*, int*, void> GetMonitorPhysicalSize;
		[NativeFunction("glfwGetVideoMode")]
		public readonly delegate* unmanaged<MonitorHandle, VidMode*> GetVideoMode;
		[NativeFunction("glfwGetMonitorPos")]
		public readonly delegate* unmanaged<MonitorHandle, int*, int*, void> GetMonitorPos;
		[NativeFunction("glfwGetMonitorWorkarea")]
		public readonly delegate* unmanaged<MonitorHandle, int*, int*, int*, int*, void> GetMonitorWorkarea;
		[NativeFunction("glfwSetMonitorCallback")]
		public readonly delegate* unmanaged<void*, void*> SetMonitorCallback;

		[NativeFunction("glfwWindowHint")]
		public readonly delegate* unmanaged<WindowAttrib, int, void> WindowHint;
		[NativeFunction("glfwCreateWindow")]
		public readonly delegate* unmanaged<int, int, byte*, MonitorHandle, WindowHandle, WindowHandle> CreateWindow;
		[NativeFunction("glfwDestroyWindow")]
		public readonly delegate* unmanaged<WindowHandle, void> DestroyWindow;
		[NativeFunction("glfwWindowShouldClose")]
		public readonly delegate* unmanaged<WindowHandle, int> WindowShouldClose;
		[NativeFunction("glfwSetWindowShouldClose")]
		public readonly delegate* unmanaged<WindowHandle, int, void> SetWindowShouldClose;
		[NativeFunction("glfwSetWindowAttrib")]
		public readonly delegate* unmanaged<WindowHandle, WindowAttrib, int, void> SetWindowAttrib;
		[NativeFunction("glfwGetWindowAttrib")]
		public readonly delegate* unmanaged<WindowHandle, WindowAttrib, int> GetWindowAttrib;
		[NativeFunction("glfwGetWindowMonitor")]
		public readonly delegate* unmanaged<WindowHandle, MonitorHandle> GetWindowMonitor;
		[NativeFunction("glfwSetWindowMonitor")]
		public readonly delegate* unmanaged<WindowHandle, MonitorHandle, int, int, int, int, int, void> SetWindowMonitor;
		[NativeFunction("glfwSetWindowTitle")]
		public readonly delegate* unmanaged<WindowHandle, byte*, void> SetWindowTitle;
		[NativeFunction("glfwGetWindowPos")]
		public readonly delegate* unmanaged<WindowHandle, int*, int*, void> GetWindowPos;
		[NativeFunction("glfwSetWindowPos")]
		public readonly delegate* unmanaged<WindowHandle, int, int, void> SetWindowPos;
		[NativeFunction("glfwGetWindowSize")]
		public readonly delegate* unmanaged<WindowHandle, int*, int*, void> GetWindowSize;
		[NativeFunction("glfwSetWindowSize")]
		public readonly delegate* unmanaged<WindowHandle, int, int, void> SetWindowSize;
		[NativeFunction("glfwGetFramebufferSize")]
		public readonly delegate* unmanaged<WindowHandle, int*, int*, void> GetFramebufferSize;
		[NativeFunction("glfwFocusWindow")]
		public readonly delegate* unmanaged<WindowHandle, void> FocusWindow;
		[NativeFunction("glfwRequestWindowAttention")]
		public readonly delegate* unmanaged<WindowHandle, void> RequestWindowAttention;
		[NativeFunction("glfwIconifyWindow")]
		public readonly delegate* unmanaged<WindowHandle, void> IconifyWindow;
		[NativeFunction("glfwRestoreWindow")]
		public readonly delegate* unmanaged<WindowHandle, void> RestoreWindow;
		[NativeFunction("glfwMaximizeWindow")]
		public readonly delegate* unmanaged<WindowHandle, void> MaximizeWindow;
		[NativeFunction("glfwSetWindowSizeLimits")]
		public readonly delegate* unmanaged<WindowHandle, int, int, int, int, void> SetWindowSizeLimits;
		
		[NativeFunction("glfwCreateWindowSurface")]
		public readonly delegate* unmanaged<VkInstance, WindowHandle, void*, VkSurfaceKHR*, VkResult> CreateWindowSurface;
		

		public Library()
		{
			// Load the library handle
			var libPath = OperatingSystem.IsWindows()
				? "runtimes/win-x64/native/glfw3.dll"
				: "runtimes/linux-x64/native/libglfw.so";
			if (!NativeLibrary.TryLoad(libPath, out var handle)) {
				throw new PlatformNotSupportedException("Could not find native GLFW library");
			}
			_handle = handle;

			// Queue the library free
			AppDomain.CurrentDomain.ProcessExit += (_, _) => {
				Terminate();
				NativeLibrary.Free(_handle);
			};

			// Resolve functions
			NativeFunctionAttribute.Resolve(_handle, this);
		}
	}


	// Library handle
	private static readonly Library _Library;

	// Error handling delegate
	private static readonly GLFWerrorfun _ErrorCallback = static (error_code, description) =>
		_LastError = (error_code, Marshal.PtrToStringAnsi((nint)description) ?? "Unknown error");

	// Last error (clears when read)
	public static (ErrorCode Code, string Description) LastError {
		get { var saved = _LastError; _LastError = (ErrorCode.NoError, String.Empty); return saved; }
	}
	private static (ErrorCode Code, string Description) _LastError = (ErrorCode.NoError, String.Empty);

	// Monitor handling delegate
	private static readonly GLFWmonitorfun _MonitorCallback = static (monitor, @event) =>
		OnMonitorEvent?.Invoke(monitor, @event);

	// Public monitor event callback
	public static event GLFWmonitorfun? OnMonitorEvent;

	// Static ctor
	static Glfw()
	{
		_Library = new();

		// Error callback
		_Library.SetErrorCallback(Marshal.GetFunctionPointerForDelegate(_ErrorCallback).ToPointer());

		// Init the library
		if (_Library.Init() != GLFW_TRUE) {
			var err = LastError;
			throw new PlatformNotSupportedException($"Failed to initialize GLFW ({err.Code}): {err.Description}");
		}
		_Library.WindowHint(WindowAttrib.ClientApi, 0 /* GLFW_NO_API */);

		// Monitor callback
		_Library.SetMonitorCallback(Marshal.GetFunctionPointerForDelegate(_MonitorCallback).ToPointer());
	}
}
