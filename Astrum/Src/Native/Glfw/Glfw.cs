﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
// ReSharper disable IdentifierTypo InconsistentNaming

namespace Astrum.Native;


// GLFW native API types and constants
internal static unsafe partial class Glfw
{
	// Constants
	public const int GLFW_FALSE = 0;
	public const int GLFW_TRUE = 1;
	public const int GLFW_DONT_CARE = -1;

	// Error codes
	public enum ErrorCode : int
	{
		NoError              = 0,
		NotInitialized       = 0x00010001,
		NoCurrentContext     = 0x00010002,
		InvalidEnum          = 0x00010003,
		InvalidValue         = 0x00010004,
		OutOfMemory          = 0x00010005,
		ApiUnavailable       = 0x00010006,
		VersionUnavailable   = 0x00010007,
		PlatformError        = 0x00010008,
		FormatUnavailable    = 0x00010009,
		NoWindowContext      = 0x0001000A,
		CursorUnavailable    = 0x0001000B,
		FeatureUnavailable   = 0x0001000C,
		FeatureUnimplemented = 0x0001000D,
		PlatformUnavailable  = 0x0001000E
	}

	// Monitor events
	public enum MonitorEvent : int
	{
		Connected    = 0x00040001,
		Disconnected = 0x00040002
	}

	// Window attribs and hints
	public enum WindowAttrib : int
	{
		Focused     = 0x00020001,
		Iconified   = 0x00020002,
		Resizeable  = 0x00020003,
		Visible     = 0x00020004,
		Decorated   = 0x00020005,
		AutoIconify = 0x00020006,
		Floating    = 0x00020007,
		Maximized   = 0x00020008,
		ClientApi   = 0x00022001
	}


	// Delegate types
	public delegate void GLFWerrorfun(ErrorCode error_code, void* description);
	public delegate void GLFWmonitorfun(MonitorHandle monitor, MonitorEvent @event);


	// Window handle type (blitable with GLFWwindow)
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public readonly struct WindowHandle(nuint handle) : IEquatable<WindowHandle>
	{
		[FieldOffset(0)] public readonly nuint Handle = handle;

		public override string ToString() => $"WindowHandle[0x{Handle:X16}]";
		public override bool Equals(object? obj) => obj is WindowHandle h && Handle == h.Handle;
		public override int GetHashCode() => Handle.GetHashCode();
		public bool Equals(WindowHandle h) => Handle == h.Handle;

		public static implicit operator bool (WindowHandle handle) => handle.Handle != 0;
		public static bool operator == (WindowHandle l, WindowHandle r) => l.Handle == r.Handle;
		public static bool operator != (WindowHandle l, WindowHandle r) => l.Handle != r.Handle;
	}

	// Monitor handle type (blitable with GLFWmonitor)
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public readonly struct MonitorHandle(nuint handle) : IEquatable<MonitorHandle>
	{
		[FieldOffset(0)] public readonly nuint Handle = handle;

		public override string ToString() => $"MonitorHandle[0x{Handle:X16}]";
		public override bool Equals(object? obj) => obj is MonitorHandle h && Handle == h.Handle;
		public override int GetHashCode() => Handle.GetHashCode();
		public bool Equals(MonitorHandle h) => Handle == h.Handle;

		public static implicit operator bool (MonitorHandle handle) => handle.Handle != 0;
		public static bool operator == (MonitorHandle l, MonitorHandle r) => l.Handle == r.Handle;
		public static bool operator != (MonitorHandle l, MonitorHandle r) => l.Handle != r.Handle;
	}

	// Monitor video mode (blitable with GLFWvidmode)
	[StructLayout(LayoutKind.Explicit, Size = 24)]
	public struct VidMode
	{
		[FieldOffset(0)]  public int Width;
		[FieldOffset(4)]  public int Height;
		[FieldOffset(8)]  public int RedBits;
		[FieldOffset(12)] public int GreenBits;
		[FieldOffset(16)] public int BlueBits;
		[FieldOffset(20)] public int RefreshRate;
	}
}
