﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using Astrum.Render.Vulkan;
using LLVK;
using static LLVK.Vulkan;

// ReSharper disable IdentifierTypo InconsistentNaming

#pragma warning disable CS0649 // Field is never assigned to, and will always have its default value

namespace Astrum.Native;


// AMD VMA native API functions
internal static unsafe partial class Vma
{
	//
	public static VkResult CreateAllocator(VmaAllocator* pAllocator) => _Library.CreateAllocator(
		VulkanCtx.Instance, VulkanCtx.PhysicalDevice, VulkanCtx.Device, 
		VulkanLoader.GetFunctionPointer("vkGetInstanceProcAddr"), VulkanLoader.GetFunctionPointer("vkGetDeviceProcAddr"), 
		pAllocator);
	
	//
	public static void DestroyAllocator(VmaAllocator allocator) => _Library.DestroyAllocator(allocator);

	//
	public static void FindMemoryTypes(MemoryTypes* types) =>
		_Library.FindMemoryTypes(VulkanCtx.VmaAllocator, types);

	//
	public static VkResult CreateImage(VkImageCreateInfo* pCreateInfo, uint memoryIndex, bool dedicated, 
		VkImage* pImage, VmaAllocation* pAllocation) => _Library.CreateImage(
			VulkanCtx.VmaAllocator, pCreateInfo, memoryIndex, dedicated ? VkBool32.True : VkBool32.False, 
			pImage, pAllocation);
	
	//
	public static void DestroyImage(VkImage image, VmaAllocation allocation) =>
		_Library.DestroyImage(VulkanCtx.VmaAllocator, image, allocation);
	
	//
	public static VkResult CreateBuffer(VkBufferCreateInfo* pCreateInfo, uint memoryIndex, bool dedicated, bool mapped,
		VkBuffer* pBuffer, VmaAllocation* pAllocation, void** ppMapping) => _Library.CreateBuffer(
			VulkanCtx.VmaAllocator, pCreateInfo, memoryIndex, dedicated ? VkBool32.True : VkBool32.False,
			mapped ? VkBool32.True : VkBool32.False, pBuffer, pAllocation, ppMapping);
	
	//
	public static void DestroyBuffer(VkBuffer buffer, VmaAllocation allocation) =>
		_Library.DestroyBuffer(VulkanCtx.VmaAllocator, buffer, allocation);
	
	
	// Contains the native library handle and function symbols
	// ReSharper disable MemberHidesStaticFromOuterClass
	private sealed class Library
	{
		private readonly nint _handle;

		[NativeFunction("CreateAllocator")]
		public readonly delegate* unmanaged<VkInstance, VkPhysicalDevice, VkDevice, void*, void*, VmaAllocator*,
			VkResult> CreateAllocator;
		[NativeFunction("DestroyAllocator")] 
		public readonly delegate* unmanaged<VmaAllocator, void> DestroyAllocator;

		[NativeFunction("FindMemoryTypes")]
		public readonly delegate* unmanaged<VmaAllocator, MemoryTypes*, void> FindMemoryTypes;

		[NativeFunction("CreateImage")]
		public readonly delegate* unmanaged<VmaAllocator, VkImageCreateInfo*, uint, VkBool32, VkImage*, VmaAllocation*,
			VkResult> CreateImage;
		[NativeFunction("DestroyImage")]
		public readonly delegate* unmanaged<VmaAllocator, VkImage, VmaAllocation, void> DestroyImage;

		[NativeFunction("CreateBuffer")]
		public readonly delegate* unmanaged<VmaAllocator, VkBufferCreateInfo*, uint, VkBool32, VkBool32, VkBuffer*,
			VmaAllocation*, void**, VkResult> CreateBuffer;
		[NativeFunction("DestroyBuffer")]
		public readonly delegate* unmanaged<VmaAllocator, VkBuffer, VmaAllocation, void> DestroyBuffer;
		

		public Library()
		{
			// Load the library handle
			var libPath = OperatingSystem.IsWindows()
				? "runtimes/win-x64/native/vma.dll"
				: "runtimes/linux-x64/native/libvma.so";
			if (!NativeLibrary.TryLoad(libPath, out var handle)) {
				throw new PlatformNotSupportedException("Could not find native VMA library");
			}
			_handle = handle;

			// Queue the library free
			AppDomain.CurrentDomain.ProcessExit += (_, _) => {
				NativeLibrary.Free(_handle);
			};

			// Resolve functions
			NativeFunctionAttribute.Resolve(_handle, this);
		}
	}
	
	
	// Library handle
	private static readonly Library _Library;
	
	// Static ctor
	static Vma() => _Library = new();
}
