﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;

// ReSharper disable IdentifierTypo InconsistentNaming

namespace Astrum.Native;


// AMD VMA native API types and constants
internal static unsafe partial class Vma
{
	// Memory types
	[StructLayout(LayoutKind.Explicit, Size = 20)]
	public struct MemoryTypes
	{
		[FieldOffset(0)] public uint Device;
		[FieldOffset(4)] public uint Upload;
		[FieldOffset(8)] public uint Readback;
		[FieldOffset(12)] public uint Dynamic;
		[FieldOffset(16)] public uint Lazy;
	}
	
	
	// VMA allocator type (blittable with VmaAllocator)
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public readonly struct VmaAllocator(nuint handle) : IEquatable<VmaAllocator>
	{
		[FieldOffset(0)] public readonly nuint Handle = handle;
		
		public override string ToString() => $"VmaAllocator[0x{Handle:X16}]";
		public override bool Equals(object? obj) => obj is VmaAllocator h && Handle == h.Handle;
		public override int GetHashCode() => Handle.GetHashCode();
		public bool Equals(VmaAllocator h) => Handle == h.Handle;

		public static implicit operator bool (VmaAllocator handle) => handle.Handle != 0;
		public static bool operator == (VmaAllocator l, VmaAllocator r) => l.Handle == r.Handle;
		public static bool operator != (VmaAllocator l, VmaAllocator r) => l.Handle != r.Handle;
	}
	
	// VMA allocation type (blittable with VmaAllocation)
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public readonly struct VmaAllocation(nuint handle) : IEquatable<VmaAllocation>
	{
		[FieldOffset(0)] public readonly nuint Handle = handle;
		
		public override string ToString() => $"VmaAllocation[0x{Handle:X16}]";
		public override bool Equals(object? obj) => obj is VmaAllocation h && Handle == h.Handle;
		public override int GetHashCode() => Handle.GetHashCode();
		public bool Equals(VmaAllocation h) => Handle == h.Handle;

		public static implicit operator bool (VmaAllocation handle) => handle.Handle != 0;
		public static bool operator == (VmaAllocation l, VmaAllocation r) => l.Handle == r.Handle;
		public static bool operator != (VmaAllocation l, VmaAllocation r) => l.Handle != r.Handle;
	}
}
