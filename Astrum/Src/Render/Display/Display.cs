﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Maths;
using Astrum.Native;

namespace Astrum.Render;


/// <summary>Represents a physical display output on the system. This maps to monitors on desktop systems.</summary>
/// <remarks>Unless otherwise noted, all fields must be accessed on the main thread only.</remarks>
public sealed class Display : IEquatable<Glfw.MonitorHandle>
{
	#region Fields
	// The monitor handle
	internal Glfw.MonitorHandle Handle { get; private set; }

	/// <summary>If the display is still connected. Re-connected displays will appear as new instances.</summary>
	/// <remarks>Safe to access from any thread.</remarks>
	public bool IsConnected => Handle;

	/// <summary>The name of the display reported by the system. May not be unique.</summary>
	/// <remarks>Safe to access from any thread.</remarks>
	public readonly string Name;

	/// <summary>The physical size of the display area, in millimeters.</summary>
	/// <remarks>Safe to access from any thread.</remarks>
	public readonly Point2U PhysicalSize;

	/// <summary>If this display is considered the primary display by the system.</summary>
	public bool IsPrimary {
		get {
			Core.AssertMainThread();
			return Handle == Glfw.GetPrimaryMonitor();
		}
	}

	/// <summary>Information about the display surface.</summary>
	/// <remarks>
	/// This can change if the display configuration is changed while the application is running. Astrum will not change
	/// this configuration itself explicitly.
	/// </remarks>
	public SurfaceInfo Surface {
		get {
			Core.AssertMainThread();
			return Handle ? new(Glfw.GetVideoMode(Handle)) : default;
		}
	}

	/// <summary>The position of the top-left of the display in global display space.</summary>
	public Point2 Position {
		get {
			Core.AssertMainThread();
			return Handle ? Glfw.GetMonitorPos(Handle) : default;
		}
	}

	/// <summary>The size of the display surface, in pixels.</summary>
	public Point2U Size => Surface.Size;

	/// <summary>The region that the display occupies in global display space.</summary>
	public Box2 DisplayArea {
		get {
			Core.AssertMainThread();
			return Handle ? new(Position, Position + (Point2)Size) : default;
		}
	}

	/// <summary>The "usable" area in the local display space (without the system taskbar).</summary>
	public Box2 WorkArea {
		get {
			Core.AssertMainThread();
			return Handle ? Glfw.GetMonitorWorkarea(Handle) : default;
		}
	}
	#endregion // Fields


	internal Display(Glfw.MonitorHandle handle)
	{
		Handle = handle;
		Name = Glfw.GetMonitorName(handle);
		PhysicalSize = Glfw.GetMonitorPhysicalSize(handle);
	}

	// Mark the display as disconnected
	internal void MarkDisconnected() => Handle = default;

	public override string ToString() => $"Display[\"{Name}\"]";

	bool IEquatable<Glfw.MonitorHandle>.Equals(Glfw.MonitorHandle other) => Handle == other;
	

	/// <summary>Provides information about a display surface.</summary>
	public readonly struct SurfaceInfo
	{
		/// <summary>The width of the display surface, in pixels.</summary>
		public readonly uint Width;
		/// <summary>The height of the display surface, in pixels.</summary>
		public readonly uint Height;
		/// <summary>The color data size/depth, in bits.</summary>
		public readonly uint ColorDepth;
		/// <summary>The surface refresh rate.</summary>
		public readonly uint Refresh;

		/// <summary>The dimensions of the display surface, in pixels.</summary>
		public Point2U Size => new(Width, Height);

		internal SurfaceInfo(Glfw.VidMode mode)
		{
			Width = (uint)mode.Width;
			Height = (uint)mode.Height;
			ColorDepth = (uint)(mode.RedBits + mode.GreenBits + mode.BlueBits);
			Refresh = (uint)mode.RefreshRate;
		}

		public override string ToString() => $"[{Width}x{Height}@{Refresh}Hz,{ColorDepth}bpp]";
	}
}
