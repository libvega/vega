﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Astrum.Maths;
using Astrum.Render.Vulkan;
using LLVK;
using static LLVK.Vulkan;
using static LLVK.Vulkan.KHR;
using static Astrum.EngineLogger;

namespace Astrum.Render;


// Manages the surface and swapchain objects for a specific window
internal sealed unsafe class Swapchain
{
	// Max supported per-swapchain images
	private const uint MAX_IMAGES = 4; // Nearly all platforms are 2 or 3, but use 4 just to be safe
	
	// Preferred surface formats
	public const VkFormat PREFERRED_FORMAT = VkFormat.B8g8r8a8Unorm; // Windows/Most Linux/macOS/iOS
	public const VkFormat FALLBACK_FORMAT = VkFormat.R8g8b8a8Unorm;  // Android/Some Linux
	
	
	#region Fields
	// The window
	public readonly Window Window;
	private readonly string _windowDebugName;
	
	// Main handles
	public readonly VkSurfaceKHR SurfaceHandle;
	public VkSwapchainKHR SwapchainHandle { get; private set; }
	
	// Current swapchain extent
	public Point2U Extent { get; private set; }
	// If the swapchain surface is minimized
	public bool IsMinimized => Extent.X == 0 || Extent.Y == 0;
	
	// Surface caps
	public readonly VkPresentModeKHR VsyncOnMode;
	public readonly VkPresentModeKHR VsyncOffMode;
	public readonly VkSurfaceFormatKHR SurfaceFormat;
	
	// Synchronization objects
	public VkSemaphore CurrentSemaphore => _semaphores[(int)_syncIndex];
	private SemaphoreArray _semaphores; // GPU->GPU, Acquire->Blit
	private uint _syncIndex;
	
	// Swapchain images
	public (uint Index, VkImage Image, VkImageView View) CurrentImage =>
		(_imageIndex, _images[(int)_imageIndex], _views[(int)_imageIndex]);
	private ImageArray _images;
	private ImageViewArray _views;
	private uint _imageCount;
	private uint _imageIndex;
	
	// If the swapchain needs to be rebuilt (caught when acquiring next swapchain image)
	public bool NeedsRebuild { get; private set; }
	#endregion // Fields


	public Swapchain(Window window)
	{
		Window = window;
		_windowDebugName = $"@{window}";

		// Create the surface
		SurfaceHandle = window.CreateSurface();
		
		// Check present support (not expected to ever fail, must must be called to fix validation checks)
		VkBool32 canPresent;
		vkGetPhysicalDeviceSurfaceSupportKHR(VulkanCtx.PhysicalDevice, RenderCore.Device.MainQueueFamily, SurfaceHandle, 
			&canPresent).ThrowIfError();
		if (!canPresent) throw new PlatformNotSupportedException("The selected device does not support presentation");
		
		// Get surface info
		uint formatCount, modeCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(VulkanCtx.PhysicalDevice, SurfaceHandle, &formatCount, null).ThrowIfError();
		vkGetPhysicalDeviceSurfacePresentModesKHR(VulkanCtx.PhysicalDevice, SurfaceHandle, &modeCount, null).ThrowIfError();
		var fmtPtr = stackalloc VkSurfaceFormatKHR[(int)formatCount];
		var modePtr = stackalloc VkPresentModeKHR[(int)modeCount];
		vkGetPhysicalDeviceSurfaceFormatsKHR(VulkanCtx.PhysicalDevice, SurfaceHandle, &formatCount, fmtPtr).ThrowIfError();
		vkGetPhysicalDeviceSurfacePresentModesKHR(VulkanCtx.PhysicalDevice, SurfaceHandle, &modeCount, modePtr).ThrowIfError();
		
		// Find modes
		bool hasImmediate = false, hasMailbox = false, hasRelaxed = false;
		foreach (var mode in new ReadOnlySpan<VkPresentModeKHR>(modePtr, (int)modeCount)) {
			hasImmediate |= mode is VkPresentModeKHR.ImmediateKHR;
			hasMailbox |= mode is VkPresentModeKHR.MailboxKHR;
			hasRelaxed |= mode is VkPresentModeKHR.FifoRelaxedKHR;
		}
		VsyncOnMode = hasRelaxed ? VkPresentModeKHR.FifoRelaxedKHR : VkPresentModeKHR.FifoKHR;
		VsyncOffMode = hasMailbox ? VkPresentModeKHR.MailboxKHR :
			hasImmediate ? VkPresentModeKHR.ImmediateKHR : VkPresentModeKHR.FifoKHR;
		
		// Select the format (all Astrum platforms support either the preferred or fallback format)
		foreach (var fmt in new ReadOnlySpan<VkSurfaceFormatKHR>(fmtPtr, (int)formatCount)) {
			if (fmt.ColorSpace != VkColorSpaceKHR.SrgbNonlinearKHR) continue;
			if (fmt.Format == PREFERRED_FORMAT) { SurfaceFormat = fmt; break; }
			if (fmt.Format == FALLBACK_FORMAT) { SurfaceFormat = fmt; break; }
		}
		Debug.Assert(SurfaceFormat.Format is PREFERRED_FORMAT or FALLBACK_FORMAT, "Unsupported window surface format");
		
		// Create sync objects
		for (uint si = 0; si < RenderCore.MAX_FRAMES; ++si) {
			VkSemaphore semaphore;
			VkSemaphoreCreateInfo create = new();
			vkCreateSemaphore(VulkanCtx.Device, &create, null, &semaphore).ThrowIfError();
			VulkanCtx.SetName(semaphore, _windowDebugName, "Semaphore", si);
			_semaphores[(int)si] = semaphore;
		}

		EngineInfo(
			$"Created swapchain for window '{window}' (F={SurfaceFormat.Format} VSync={VsyncOnMode}/{VsyncOffMode})");
		
		// Perform the initial build
		Rebuild();
	}

	public void Destroy()
	{
		vkDeviceWaitIdle(VulkanCtx.Device);
		
		// Destroy image objects
		foreach (var view in _views) vkDestroyImageView(VulkanCtx.Device, view, null);
		
		// Destroy swapchain objects
		if (SwapchainHandle) vkDestroySwapchainKHR(VulkanCtx.Device, SwapchainHandle, null);
		foreach (var sem in _semaphores) vkDestroySemaphore(VulkanCtx.Device, sem, null);
		vkDestroySurfaceKHR(VulkanCtx.Instance, SurfaceHandle, null);
		
		EngineInfo($"Destroyed swapchain for window '{Window}'");
	}
	
	// Marks the swapchain to be rebuilt
	public void MarkDirty() => NeedsRebuild = true;
	
	
	// Rebuilds the swapchain
	public void Rebuild()
	{
		// Get the surface info
		Point2U surfaceSize;
		VkSurfaceCapabilitiesKHR caps;
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(VulkanCtx.PhysicalDevice, SurfaceHandle, &caps).ThrowIfError();
		if (caps.CurrentExtent.Width != UInt32.MaxValue) {
			surfaceSize = new(caps.CurrentExtent.Width, caps.CurrentExtent.Height);
		}
		else {
			var winSize = Window.BackbufferSize;
			surfaceSize = new(
				Math.Clamp(winSize.X, caps.MinImageExtent.Width, caps.MaxImageExtent.Width),
				Math.Clamp(winSize.Y, caps.MinImageExtent.Height, caps.MaxImageExtent.Height)
			);
		}
		
		// Cancel the rebuild for minimized surfaces
		if (surfaceSize.X == 0 || surfaceSize.Y == 0) {
			Extent = surfaceSize;
			return;
		}
		
		// Describe the new swapchain
		VkSwapchainCreateInfoKHR create = new() {
			Surface = SurfaceHandle,
			MinImageCount = caps.MinImageCount,
			ImageFormat = SurfaceFormat.Format,
			ImageColorSpace = SurfaceFormat.ColorSpace,
			ImageExtent = new(surfaceSize.X, surfaceSize.Y),
			ImageArrayLayers = 1,
			ImageUsage = VkImageUsageFlags.TransferDst | VkImageUsageFlags.ColorAttachment,
			ImageSharingMode = VkSharingMode.Exclusive,
			PreTransform = caps.CurrentTransform,
			CompositeAlpha = VkCompositeAlphaFlagsKHR.OpaqueKHR,
			PresentMode = RenderCore.Vsync ? VsyncOnMode : VsyncOffMode,
			OldSwapchain = SwapchainHandle
		};
		
		// Device idle, then create new and destroy old
		if (SwapchainHandle) RenderCore.Driver.FlushAll();
		VkSwapchainKHR swapchain;
		vkCreateSwapchainKHR(VulkanCtx.Device, &create, null, &swapchain).ThrowIfError();
		VulkanCtx.SetName(swapchain, _windowDebugName, "Swapchain");
		if (SwapchainHandle) vkDestroySwapchainKHR(VulkanCtx.Device, SwapchainHandle, null);
		SwapchainHandle = swapchain;
		Extent = surfaceSize;
		
		// Free old image objects
		foreach (var view in _views) vkDestroyImageView(VulkanCtx.Device, view, null);
		
		// Get the new swapchain images
		uint imageCount;
		vkGetSwapchainImagesKHR(VulkanCtx.Device, swapchain, &imageCount, null).ThrowIfError();
		var imagePtr = stackalloc VkImage[(int)imageCount];
		vkGetSwapchainImagesKHR(VulkanCtx.Device, swapchain, &imageCount, imagePtr).ThrowIfError();
		_imageCount = imageCount;
		for (uint ii = 0; ii < imageCount; ++ii) {
			_images[(int)ii] = imagePtr[ii];
			VulkanCtx.SetName(imagePtr[ii], _windowDebugName, "Image", ii);
			VkImageViewCreateInfo viewCreate = new() {
				Image = imagePtr[ii],
				ViewType = VkImageViewType.E2D,
				Format = SurfaceFormat.Format,
				Components = new(), // Identity mapping
				SubresourceRange = new(VkImageAspectFlags.Color, 0, 1, 0, 1)
			};
			VkImageView view;
			vkCreateImageView(VulkanCtx.Device, &viewCreate, null, &view).ThrowIfError();
			_views[(int)ii] = view;
			VulkanCtx.SetName(view, _windowDebugName, "ImageView", ii);
		}
		
		// Setup swapchain state
		_imageIndex = 0;
		_syncIndex = 0;
		NeedsRebuild = false;
		
		EngineInfo($"Rebuilt swapchain for window '{Window.Name}' (E:{Extent}, M:{create.PresentMode}, I:{imageCount})");
		
		// Acquire the next image immediately
		if (Acquire(out var acquireResult)) return;
		EngineError($"Failed to reacquire swapchain after rebuild ({acquireResult})");
		throw new("Failed to reacquire swapchain");
	}
	
	// Acquires the next image in the swapchain
	public bool Acquire(out VkResult res)
	{
		_syncIndex = (_syncIndex + 1) % RenderCore.MAX_FRAMES;
		uint image;
		res = vkAcquireNextImageKHR(VulkanCtx.Device, SwapchainHandle, ~0ul, _semaphores[(int)_syncIndex],
			default, &image);
		if (res != VkResult.Success) return false;
		_imageIndex = image;
		return true;
	}
	
	
	// Inline handle arrays
	[InlineArray((int)RenderCore.MAX_FRAMES)] private struct SemaphoreArray { private VkSemaphore _e0; }
	[InlineArray((int)MAX_IMAGES)] private struct ImageArray { private VkImage _e0; }
	[InlineArray((int)MAX_IMAGES)] private struct ImageViewArray { private VkImageView _e0; }
}
