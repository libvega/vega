﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using Astrum.Maths;
using Astrum.Native;
using Astrum.Render.Vulkan;
using LLVK;
using static LLVK.Vulkan.KHR;
using static Astrum.EngineLogger;

namespace Astrum.Render;


// [Partial] Desktop window implementation for Win, Linux, and macOS using GLFW
public partial class Window
{
	/// <summary>The maximum number of windows that can be open at once.</summary>
	public const uint MAX_COUNT = 8;
	
	/// <summary>The minimum width of a window.</summary>
	public const uint MIN_WIDTH = 256;  // 16:9 144p
	/// <summary>The minimum height of a window.</summary>
	public const uint MIN_HEIGHT = 144;  // 16:9 144p
	/// <summary>The maximum width of a window.</summary>
	public const uint MAX_WIDTH = 10_240;  // maximum common width (ultra-wide "21:9", 8:3, 32:9)
	/// <summary>The maximum height of a window.</summary>
	public const uint MAX_HEIGHT = 4_320;  // maximum common height (8k 16:9)
	
	// Default window size (good minimum default based on Steam hardware survey)
	internal const uint DEFAULT_WIDTH = 1_366;
	internal const uint DEFAULT_HEIGHT = 768;
	
	
	// Desktop window implementation
	internal sealed class Desktop(string name) : Window(name)
	{
		#region Fields
		// The window handle
		public Glfw.WindowHandle Handle { get; private set; }
		
		// The window swapchain
		internal override Swapchain? Swapchain { get; private protected set; }

		// Cached properties
		private Box2 _cachedArea; // Store window location for fullscreen->windowed transitions
		private string _title = name; // GLFW cannot retrieve window titles

		#region State
		//
		public override bool IsOpen => Handle;

		//
		public override bool IsClosing {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return Glfw.WindowShouldClose(Handle);
			}
		}

		//
		public override Display? FullscreenDisplay {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				if (Glfw.GetWindowMonitor(Handle) is var mon && !mon) return null;
				foreach (var display in RenderCore.AllDisplays)
					if (display.Handle == mon)
						return display;
				return null;
			}
		}

		//
		public override Display? CurrentDisplay {
			get {
				if (FullscreenDisplay is { } fs) return fs; // Also performs main thread and open checks
				var winBox = ContentArea;
				Display? best = null;
				uint bestArea = 0;
				foreach (var d in RenderCore.AllDisplays) {
					if (d.DisplayArea.Intersect(winBox) is { Area: var overlap } && overlap <= bestArea) continue;
					best = d;
					bestArea = overlap;
				}
				return best;
			}
		}
		#endregion // State

		#region Properties
		//
		public override string Title {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return _title;
			}
			set {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				Glfw.SetWindowTitle(Handle, _title = value);
			}
		}

		//
		public override bool Resizeable {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return Glfw.GetWindowAttrib(Handle, Glfw.WindowAttrib.Resizeable) == Glfw.GLFW_TRUE;
			}
			set {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				Glfw.SetWindowAttrib(Handle, Glfw.WindowAttrib.Resizeable, value ? Glfw.GLFW_TRUE : Glfw.GLFW_FALSE);
			}
		}

		//
		public override bool Decorated {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return Glfw.GetWindowAttrib(Handle, Glfw.WindowAttrib.Decorated) == Glfw.GLFW_TRUE;
			}
			set {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				Glfw.SetWindowAttrib(Handle, Glfw.WindowAttrib.Decorated, value ? Glfw.GLFW_TRUE : Glfw.GLFW_FALSE);
			}
		}

		//
		public override bool Floating {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return Glfw.GetWindowAttrib(Handle, Glfw.WindowAttrib.Floating) == Glfw.GLFW_TRUE;
			}
			set {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				Glfw.SetWindowAttrib(Handle, Glfw.WindowAttrib.Floating, value ? Glfw.GLFW_TRUE : Glfw.GLFW_FALSE);
			}
		}

		//
		public override bool IsMinimized {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return Glfw.GetWindowAttrib(Handle, Glfw.WindowAttrib.Iconified) == Glfw.GLFW_TRUE;
			}
		}

		//
		public override bool IsMaximized {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				return Glfw.GetWindowAttrib(Handle, Glfw.WindowAttrib.Maximized) == Glfw.GLFW_TRUE;
			}
		}

		//
		public override Point2 Position {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				var (x, y) = Glfw.GetWindowPos(Handle);
				return new(x, y);
			}
			set {
				if (IsFullscreen) return; // Also performs main thread and open checks
				Glfw.SetWindowPos(Handle, value.X, value.Y);
			}
		}

		//
		public override Point2U Size {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				var (w, h) = Glfw.GetWindowSize(Handle);
				return new(w, h);
			}
			set {
				if (IsFullscreen) return; // Also performs main thread and open checks
				ClampWindowSize(ref value.X, ref value.Y);
				Glfw.SetWindowSize(Handle, value.X, value.Y);
			}
		}

		//
		public override Point2U BackbufferSize {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				var (w, h) = Glfw.GetFramebufferSize(Handle);
				return new(w, h);
			}
		}

		//
		public override Box2 ContentArea {
			get {
				Core.AssertMainThread();
				NotOpenException.CheckAndThrow(this);
				var (x, y) = Glfw.GetWindowPos(Handle);
				var (w, h) = Glfw.GetWindowSize(Handle);
				return new(x, y, x + (int)w, y + (int)h);
			}
			set {
				if (IsFullscreen) return; // Also performs main thread and open checks
				uint w = value.Width, h = value.Height;
				ClampWindowSize(ref w, ref h);
				Glfw.SetWindowPos(Handle, value.Min.X, value.Min.Y);
				Glfw.SetWindowSize(Handle, w, h);
			}
		}
		#endregion // Properties
		#endregion // Fields
		
		
		// Opens the window
		public void Open(uint w, uint h, string title, Display? display)
		{
			// Check state
			Core.AssertMainThread();
			Debug.Assert(!Handle && Swapchain is null && display is null or { IsConnected: true });
		
			// Create the window
			w = display?.Size.X ?? w;
			h = display?.Size.Y ?? h;
			ClampWindowSize(ref w, ref h);
			Handle = Glfw.CreateWindow(w, h, title, display?.Handle ?? default);
			if (!Handle) {
				var err = Glfw.LastError;
				throw new($"Failed to open window ({err.Code} - {err.Description})");
			}
		
			// Set the properties
			Glfw.SetWindowAttrib(Handle, Glfw.WindowAttrib.Resizeable, Glfw.GLFW_TRUE);
			Glfw.SetWindowAttrib(Handle, Glfw.WindowAttrib.Decorated, Glfw.GLFW_TRUE);
			Glfw.SetWindowAttrib(Handle, Glfw.WindowAttrib.Floating, Glfw.GLFW_FALSE);
			Glfw.SetWindowSizeLimits(Handle, (int)MIN_WIDTH, (int)MIN_HEIGHT, (int)MAX_WIDTH, (int)MAX_HEIGHT);
			_title = title;
		
			// Create swapchain
			Swapchain = new(this);
		
			EngineInfo($"Opened window '{Name}' with W={w}, H={h}");
		}
	
		// Destroys the window
		public void Destroy()
		{
			Core.AssertMainThread();
			if (!Handle) return;
		
			// Destroy swapchain and window
			Swapchain?.Destroy();
			Glfw.DestroyWindow(Handle);

			// Reset state
			Handle = default;
			Swapchain = null;
			_cachedArea = default;
			_title = Name;
		
			EngineInfo($"Destroyed window '{Name}'");
		}


		//
		internal override unsafe VkSurfaceKHR CreateSurface()
		{
			VkSurfaceKHR surface;
			Glfw.CreateWindowSurface(VulkanCtx.Instance, Handle, &surface).ThrowIfError();
			VulkanCtx.SetName(surface, ToString(), "Surface");
			return surface;
		}

		
		#region Actions
		//
		public override void RequestClose()
		{
			Core.AssertMainThread();
			if (!Handle) return;
			Glfw.SetWindowShouldClose(Handle, true);
			EngineInfo($"Requested window '{Name}' to close");
		}
		
		//
		public override bool SetFullscreen(Display? display)
		{
			// Cancel if no-op (also performs main thread and open checks)
			if (ReferenceEquals(FullscreenDisplay, display)) {
				return false;
			}

			// Update the state
			if (display is not null) {
				if (!display.IsConnected) {
					throw new ArgumentException("Cannot use a disconnected display to set a screen to fullscreen");
				}
				if (_cachedArea.IsEmpty) _cachedArea = ContentArea; // Dont override for fs -> fs transition
				var area = display.DisplayArea;
				Glfw.SetWindowMonitor( // GLFW uses a "borderless fullscreen" mode in this case
					Handle, display.Handle, 0, 0, area.Width, area.Height, Glfw.GLFW_DONT_CARE);
				EngineInfo($"Switched window '{Name}' to fullscreen on display '{display.Name}'");
			}
			else {
				var area = _cachedArea;
				if (area.IsEmpty) area = new(0, 0, (int)DEFAULT_WIDTH, (int)DEFAULT_HEIGHT);
				Glfw.SetWindowMonitor(Handle, default, area.Min.X, area.Min.Y, area.Width, area.Height, 0);
				_cachedArea = default;
				EngineInfo($"Switched window '{Name}' to windowed");
			}
			return true;
		}
		
		//
		public override void Minimize()
		{
			Core.AssertMainThread();
			NotOpenException.CheckAndThrow(this);
			Glfw.IconifyWindow(Handle);
		}

		//
		public override void Restore()
		{
			Core.AssertMainThread();
			NotOpenException.CheckAndThrow(this);
			Glfw.RestoreWindow(Handle);
		}

		//
		public override void Maximize()
		{
			Core.AssertMainThread();
			NotOpenException.CheckAndThrow(this);
			if (IsFullscreen) return;
			Glfw.MaximizeWindow(Handle);
		}

		//
		public override void Focus()
		{
			Core.AssertMainThread();
			NotOpenException.CheckAndThrow(this);
			Glfw.FocusWindow(Handle);
		}

		//
		public override void RequestAttention()
		{
			Core.AssertMainThread();
			NotOpenException.CheckAndThrow(this);
			Glfw.RequestWindowAttention(Handle);
		}
		#endregion // Actions
		
		
		// Clamps to the valid window size range
		public static void ClampWindowSize(ref uint width, ref uint height) =>
			(width, height) = (Math.Clamp(width, MIN_WIDTH, MAX_WIDTH), Math.Clamp(height, MIN_HEIGHT, MAX_HEIGHT));
	}
}
