﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
using Astrum.Maths;
using static LLVK.Vulkan.KHR;

namespace Astrum.Render;


/// <summary>Provides status and control for a specific application window.</summary>
/// <remarks>
/// Unless otherwise noted, all methods and properties must be accessed on the main thread only. Properties or methods
/// that modify a window will throw an exception if the window is not open.
/// </remarks>
public abstract partial class Window
{
	#region Fields
	// The internal name for the window (should match the Windows field name)
	internal readonly string Name;
	
	// The window swapchain
	internal abstract Swapchain? Swapchain { get; private protected set; }
	
	#region State
	/// <summary>If the window is currently open.</summary>
	public abstract bool IsOpen { get; }

	/// <summary>If the window is expected to close at the end of the current application frame.</summary>
	public abstract bool IsClosing { get; }

	/// <summary>The display that the window is fullscreen on, or <c>null</c> if windowed.</summary>
	public abstract Display? FullscreenDisplay { get; }

	/// <summary>The current display that the window center is in, or the display with the largest overlap.</summary>
	public abstract Display? CurrentDisplay { get; }

	/// <summary>If the window is currently in fullscreen mode.</summary>
	public bool IsFullscreen => FullscreenDisplay is not null;

	/// <summary>If the window is currently in windowed mode.</summary>
	public bool IsWindowed => FullscreenDisplay is null;
	#endregion // State

	#region Properties
	/// <summary>The title of the window.</summary>
	public abstract string Title { get; set; }

	/// <summary>If the window is user resizeable when not fullscreen.</summary>
	public abstract bool Resizeable { get; set; }

	/// <summary>If the window is decorated with a title bar and frame when not fullscreen.</summary>
	public abstract bool Decorated { get; set; }

	/// <summary>If the window is always on top when not fullscreen.</summary>
	public abstract bool Floating { get; set; }

	/// <summary>If the window has been minimized (iconified) to the system taskbar.</summary>
	public abstract bool IsMinimized { get; }

	/// <summary>If the window is maximized.</summary>
	public abstract bool IsMaximized { get; }

	/// <summary>The position of the top-left of the window in global display space.</summary>
	/// <remarks>Setting this value has no effect on fullscreen windows.</remarks>
	public abstract Point2 Position { get; set; }

	/// <summary>The size of the window in display coordinates.</summary>
	/// <remarks>
	/// Setting this value has no effect on fullscreen windows. For high-DPI displays, this value may not map 1:1 with
	/// pixels. See also <see cref="BackbufferSize"/>.
	/// </remarks>
	public abstract Point2U Size { get; set; }

	/// <summary>The size of the window render backbuffer in pixels.</summary>
	/// <remarks>
	/// For high-DPI displays, this value gives the actual size of the window in pixels instead of display units. Use
	/// this value for all rendering-related uses, otherwise use <see cref="Size"/>.
	/// </remarks>
	public abstract Point2U BackbufferSize { get; }

	/// <summary>The content area (window area without system decorations) in display units.</summary>
	/// <remarks>Setting this value has no effect on fullscreen windows.</remarks>
	public abstract Box2 ContentArea { get; set; }
	#endregion // Properties
	#endregion // Fields


	// Restrict Window subclassing to internal only
	private protected Window(string name) => Name = name;

	public override string ToString() => $"Window[{Name}]";
	
	
	// Creates the window surface needed by swapchain
	internal abstract VkSurfaceKHR CreateSurface();


	#region Actions
	/// <summary>Marks the window to close at the end of the current application frame.</summary>
	/// <remarks>Closing the main window will also close the application. No-op for windows that aren't open.</remarks>
	public abstract void RequestClose();

	/// <summary>Sets the windowed/fullscreen state of the window.</summary>
	/// <param name="display">The display to make the window fullscreen on, or <c>null</c> to make windowed.</param>
	/// <returns>If the windowed/fullscreen state was changed.</returns>
	public abstract bool SetFullscreen(Display? display);

	/// <summary>Minimizes the window to the system task bar.</summary>
	public abstract void Minimize();

	/// <summary>Restores the window from the system task bar.</summary>
	public abstract void Restore();

	/// <summary>Maximizes the window. Ignored for fullscreen screens.</summary>
	public abstract void Maximize();

	/// <summary>Brings the window to the top and gives it input focus.</summary>
	/// <remarks>Doing this can annoy the user and should not be done frivolously.</remarks>
	public abstract void Focus();

	/// <summary>Performs an OS-specific notification to bring attention to the window.</summary>
	/// <remarks>Doing this can annoy the user and should not be done frivolously.</remarks>
	public abstract void RequestAttention();
	#endregion // Actions
	

	/// <summary>Implicit bool cast to check if the window is open. Equivalent to <see cref="IsOpen"/>.</summary>
	public static implicit operator bool (Window window) => window.IsOpen;


	/// <summary>Indicates that the application attempted to modify a closed window.</summary>
	public sealed class NotOpenException : Exception
	{
		/// <summary>The window that was modified while closed.</summary>
		public readonly Window Window;

		internal NotOpenException(Window window)
			: base($"Cannot modify window '{window.Name}' if it is closed") => Window = window;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal static void CheckAndThrow(Window window)
		{
			if (!window) throw new NotOpenException(window);
		}
	}
}
