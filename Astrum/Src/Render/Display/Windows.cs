﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;

namespace Astrum.Render;


/// <summary>Provides the set of supported windows for the application.</summary>
public sealed class Windows
{
	/// <summary>The main application window. This is the only window on non-desktop platforms.</summary>
	public static readonly Window Main   = new Window.Desktop("Main");
	/// <summary>Extra window 1.</summary>
	public static readonly Window Extra1 = new Window.Desktop("Extra1");
	/// <summary>Extra window 2.</summary>
	public static readonly Window Extra2 = new Window.Desktop("Extra2");
	/// <summary>Extra window 3.</summary>
	public static readonly Window Extra3 = new Window.Desktop("Extra3");
	/// <summary>Extra window 4.</summary>
	public static readonly Window Extra4 = new Window.Desktop("Extra4");
	/// <summary>Extra window 5.</summary>
	public static readonly Window Extra5 = new Window.Desktop("Extra5");
	/// <summary>Extra window 6.</summary>
	public static readonly Window Extra6 = new Window.Desktop("Extra6");
	/// <summary>Extra window 7.</summary>
	public static readonly Window Extra7 = new Window.Desktop("Extra7");
	
	/// <summary>Array of all window instances, starting with <see cref="Main"/>, then any extras in order.</summary>
	public static readonly IReadOnlyList<Window> AllWindows;

	static Windows() => AllWindows = new[] { Main, Extra1, Extra2, Extra3, Extra4, Extra5, Extra6, Extra7 };
}
