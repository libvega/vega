﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Astrum.Maths;
using Astrum.Render.Vulkan;
using static LLVK.Vulkan;
using static LLVK.Vulkan.EXT;

namespace Astrum.Render;


/// <summary>Describes a graphics device available on the current system.</summary>
public sealed unsafe class GraphicsDevice
{
	#region Fields
	// Vulkan info
	internal readonly VkPhysicalDevice PhysicalDevice;
	internal readonly DeviceExtensionSet Extensions;

	/// <summary>The device name.</summary>
	public readonly string Name;
	/// <summary>The device hardware implementation kind.</summary>
	public readonly DeviceKind Kind;
	/// <summary>The device manufacturer or hardware vendor.</summary>
	public readonly DeviceVendor Vendor;
	/// <summary>The device driver version.</summary>
	public readonly Version DriverVersion;
	/// <summary>The device implementation API version.</summary>
	public readonly Version ApiVersion;

	/// <summary>The amount of VRAM local to the device (in bytes).</summary>
	public readonly ulong DeviceLocalMemory;

	/// <summary>The maximum supported dimensions for render targets, in pixels.</summary>
	public readonly Point2U RenderTargetMaxSize;
	/// <summary>Maximum texel count for 1D textures.</summary>
	public readonly uint Texture1DMaxSize;
	/// <summary>Maximum size for non-cubemap 2D textures along any dimension.</summary>
	public readonly uint Texture2DMaxSize;
	/// <summary>Maximum size for 3D textures along any dimension.</summary>
	public readonly uint Texture3DMaxSize;
	/// <summary>Maximum layer count for layered textures.</summary>
	public readonly uint TextureMaxLayers;
	/// <summary>Maximum size for cubemap 2D textures along the sides of any cubemap layer.</summary>
	public readonly uint TextureCubeMaxSize;

	/// <summary>Optional Feature Support: line primitive widths other than <c>1</c>.</summary>
	public readonly bool WideLines;
	/// <summary>Optional Feature Support: point primitive sizes other than <c>1</c>.</summary>
	public readonly bool LargePoints;
	/// <summary>Optional Feature Support: non-solid fill modes (line and point).</summary>
	public readonly bool FillModeNonSolid;

	// Vulkan-specific values
	internal readonly Int128 PipelineCacheUuid;
	internal readonly Int128 DeviceUuid;
	internal readonly Int128 DriverUuid;
	internal readonly uint MainQueueFamily;
	internal readonly uint? DmaQueueFamily;
	internal readonly uint? AsyncComputeQueueFamily;
	internal readonly uint UniformBufferAlignment;
	internal readonly uint StorageBufferAlignment;

	// Feature checks
	internal readonly string[] MissingFeatures;

	// The report string for the device
	internal string Report =>
		$"'{Name}' ({Vendor}/{Kind}): Driver={DriverVersion}, Api={ApiVersion}, DMA={DmaQueueFamily.HasValue}, " +
		$"AsC={AsyncComputeQueueFamily.HasValue}, VRAM={DeviceLocalMemory/1_048_576}MB";
	#endregion // Fields


	internal GraphicsDevice(VkPhysicalDevice device)
	{
		// Load the info
		PhysicalDevice = device;
		VulkanDeviceFeatures feats = new();
		VulkanDeviceProperties props = new();
		feats.Invoke(ptr => vkGetPhysicalDeviceFeatures2(device, ptr));
		props.Invoke(ptr => vkGetPhysicalDeviceProperties2(device, ptr));
		Extensions = DeviceExtensionSet.Load(device);
		ref readonly var f10 = ref feats.Features;
		ref readonly var f11 = ref feats.Vulkan11;
		ref readonly var fdi = ref feats.DescriptorIndexing;
		ref readonly var p10 = ref props.Properties;
		ref readonly var p11 = ref props.Vulkan11;

		// Get the device info
		fixed (byte* namePtr = &p10.DeviceName[0]) {
			Name = Marshal.PtrToStringUTF8((nint)namePtr) ?? $"Device[{p10.DeviceID:X8}]";
		}
		Kind = p10.DeviceType switch {
			VkPhysicalDeviceType.DiscreteGpu   => DeviceKind.Discrete,
			VkPhysicalDeviceType.IntegratedGpu => DeviceKind.Integrated,
			VkPhysicalDeviceType.VirtualGpu    => DeviceKind.Virtual,
			VkPhysicalDeviceType.Cpu           => DeviceKind.Cpu,
			_                                  => DeviceKind.Unknown
		};
		Vendor = (DeviceVendor)p10.VendorID;
		DriverVersion = new((int)VK_API_VERSION_MAJOR(p10.DriverVersion), (int)VK_API_VERSION_MINOR(p10.DriverVersion),
			(int)VK_API_VERSION_PATCH(p10.DriverVersion));
		ApiVersion = new((int)VK_API_VERSION_MAJOR(p10.ApiVersion), (int)VK_API_VERSION_MINOR(p10.ApiVersion),
			(int)VK_API_VERSION_PATCH(p10.ApiVersion));

		// Memory
		DeviceLocalMemory = 0;
		VkPhysicalDeviceMemoryProperties memProps;
		vkGetPhysicalDeviceMemoryProperties(device, &memProps);
		for (uint mi = 0; mi < memProps.MemoryHeapCount; ++mi) {
			var heap = memProps.MemoryHeaps[mi];
			if (heap.Flags.HasFlag(VkMemoryHeapFlags.DeviceLocal) && heap.Size > DeviceLocalMemory) {
				DeviceLocalMemory = heap.Size;
			}
		}

		// Limits
		RenderTargetMaxSize = new(
			Math.Min(p10.Limits.MaxFramebufferWidth, UInt16.MaxValue), 
			Math.Min(p10.Limits.MaxFramebufferHeight, UInt16.MaxValue)
		);
		Texture1DMaxSize = Math.Min(p10.Limits.MaxImageDimension1D, UInt16.MaxValue);
		Texture2DMaxSize = Math.Min(p10.Limits.MaxImageDimension2D, UInt16.MaxValue);
		Texture3DMaxSize = Math.Min(p10.Limits.MaxImageDimension3D, UInt16.MaxValue);
		TextureMaxLayers = Math.Min(p10.Limits.MaxImageArrayLayers, UInt16.MaxValue);
		TextureCubeMaxSize = Math.Min(p10.Limits.MaxImageDimensionCube, UInt16.MaxValue);

		// Optional features
		WideLines = f10.WideLines;
		LargePoints = f10.LargePoints;
		FillModeNonSolid = f10.FillModeNonSolid;

		// Queues
		uint queueCount;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, null);
		var queuePtr = stackalloc VkQueueFamilyProperties[(int)queueCount];
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, queuePtr);
		uint? mainQueueFamily = null;
		for (uint qi = 0; qi < queueCount; ++qi) {
			var flags = queuePtr[qi].QueueFlags;
			if      (flags.HasFlag(VkQueueFlags.Graphics)) mainQueueFamily ??= qi;
			else if (flags.HasFlag(VkQueueFlags.Compute))  AsyncComputeQueueFamily ??= qi;
			else if (flags.HasFlag(VkQueueFlags.Transfer)) DmaQueueFamily ??= qi;
		}
		MainQueueFamily = mainQueueFamily!.Value;

		// Other Vulkan values
		PipelineCacheUuid = Unsafe.As<byte, Int128>(ref p10.PipelineCacheUUID[0]);
		DeviceUuid = Unsafe.As<byte, Int128>(ref p11.DeviceUUID[0]);
		DriverUuid = Unsafe.As<byte, Int128>(ref p11.DriverUUID[0]);
		UniformBufferAlignment = (uint)p10.Limits.MinUniformBufferOffsetAlignment;
		StorageBufferAlignment = (uint)p10.Limits.MinStorageBufferOffsetAlignment;

		// Check validity
		List<string>? mFeats = null;
		checkFeature(ApiVersion.Minor >= 1, ref mFeats, "Vulkan1.1");
		checkFeature(DeviceLocalMemory != 0, ref mFeats, "VRAM");
		checkFeature(Extensions.KHR_swapchain, ref mFeats, "KHR_swapchain");
		checkFeature(f10.IndependentBlend, ref mFeats, "IndependentBlend");
		checkFeature(f11.ShaderDrawParameters, ref mFeats, "ShaderDrawParameters");
		checkFeature(Extensions.KHR_timeline_semaphore, ref mFeats, "TimelineSemaphore");
		checkFeature(Extensions.EXT_scalar_block_layout, ref mFeats, "ScalarBlockLayout");
		checkFeature(Extensions.EXT_buffer_device_address, ref mFeats, "BufferDeviceAddress");
		checkFeature(Extensions.EXT_descriptor_indexing, ref mFeats, "DescriptorIndexing");
		checkFeature(fdi.ShaderStorageImageArrayNonUniformIndexing, ref mFeats, "StorageImageArrayNonUniformIndexing");
		MissingFeatures = mFeats?.ToArray() ?? Array.Empty<string>();

		return;

		static void checkFeature(bool flag, ref List<string>? mFeats, string feature)
		{
			if (!flag) (mFeats ??= []).Add(feature);
		}
	}


	/// <summary>Enumeration of <see cref="GraphicsDevice"/> hardware implementation kinds.</summary>
	public enum DeviceKind
	{
		/// <summary>The device has an unknown or uncategorized implementation.</summary>
		Unknown,
		/// <summary>Dedicated hardware processor tightly coupled with the host processor.</summary>
		Integrated,
		/// <summary>Dedicated hardware processor uncoupled from the host processor.</summary>
		Discrete,
		/// <summary>Device is a virtual or otherwise abstracted from a physical device.</summary>
		Virtual,
		/// <summary>The host processor is acting as the GPU.</summary>
		Cpu
	}

	/// <summary>Common device vendors.</summary>
	/// <remarks>
	/// This enum generally has numeric values equal to the 16-bit PCI vendor ID, if one is available for the vendor. It
	/// is safe to cast enum values to an integer to check for specific vendors not explicitly included in the enum.
	/// </remarks>
	public enum DeviceVendor : uint
	{
		/// <summary>Unknown or unrecognized vendor.</summary>
		Unknown  = 0,
		/// <summary>AMD - Advanced Micro Devices</summary>
		Amd      = 0x1022,
		/// <summary>Apple</summary>
		Apple    = 0x106B,
		/// <summary>ARM</summary>
		Arm      = 0x13B5,
		/// <summary>Broadcom Inc.</summary>
		Broadcom = 0x14E4,
		/// <summary>Google</summary>
		Google   = 0x1AE0,
		/// <summary>Imagination Technologies</summary>
		ImgTec   = 0x1010,
		/// <summary>Intel</summary>
		Intel    = 0x8086,
		/// <summary>Mesa drivers</summary>
		Mesa     = 0x10005,
		/// <summary>Nvidia</summary>
		Nvidia   = 0x10DE,
		/// <summary>Qualcomm</summary>
		Qualcomm = 0x17CB,
		/// <summary>Samsung</summary>
		Samsung  = 0x144D
	}
}
