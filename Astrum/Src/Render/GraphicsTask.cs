﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Astrum.Render;


/// <summary>Represents an awaitable graphics operation which does not produce a host value.</summary>
public readonly struct GraphicsTask : INotifyCompletion
{
	#region Fields
	private readonly RenderDriver.WorkUnit _work;

	/// <summary>If the task is complete.</summary>
	public bool IsComplete => _work.IsComplete;
	#endregion // Fields

	internal GraphicsTask(RenderDriver.WorkUnit work) => _work = work;

	
	/// <summary>Waits for the task to complete with an optional timeout.</summary>
	/// <param name="nsTimeout">The wait timeout in nanoseconds.</param>
	/// <returns><c>true</c> if the task completed, <c>false</c> if the timeout was reached.</returns>
	public bool Wait(ulong nsTimeout = UInt64.MaxValue) => _work.WaitComplete(nsTimeout);

	/// <summary>Blocks the current thread until the task is complete.</summary>
	public void GetResult() => _work.WaitComplete(UInt64.MaxValue);

	/// <summary>Converts the task into a result-providing one.</summary>
	/// <param name="result">The result to provide from the task.</param>
	public GraphicsTask<T> With<T>(T result) => new(_work, result);

	
	/// <summary>Gets the notify awaiter for the task. For advanced use only.</summary>
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public GraphicsTask GetAwaiter() => this;

	/// <inheritdoc cref="INotifyCompletion.OnCompleted"/>
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public void OnCompleted(Action action)
	{
		if (_work.IsComplete) action();
		else if (SynchronizationContext.Current is { } ctx) ctx.Post(AwaiterCallback, (_work, action));
		else ThreadPool.QueueUserWorkItem(WorkItemCallback, (_work, action), true);
	}
	
	// Synchronization context awaiter callback
	internal static void AwaiterCallback(object? o)
	{
		var (work, action) = ((RenderDriver.WorkUnit, Action))o!;
		work.WaitComplete(UInt64.MaxValue);
		action();
	}
	
	// Thread pool work item callback
	internal static void WorkItemCallback((RenderDriver.WorkUnit Work, Action Action) args)
	{
		args.Work.WaitComplete(UInt64.MaxValue);
		args.Action();
	}
}


/// <summary>Represents an awaitable graphics operation which produces a host value.</summary>
/// <typeparam name="T">The type that is produced by the task.</typeparam>
public readonly struct GraphicsTask<T> : INotifyCompletion
{
	#region Fields
	private readonly RenderDriver.WorkUnit _work;
	private readonly T _value;
	
	/// <summary>If the task is complete.</summary>
	public bool IsComplete => _work.IsComplete;

	/// <summary>Performs a blocking wait until the task result becomes available.</summary>
	public T Result => GetResult();
	#endregion // Fields

	internal GraphicsTask(RenderDriver.WorkUnit work, T value) => (_work, _value) = (work, value);
	
	
	/// <summary>Waits for the task to complete with an optional timeout.</summary>
	/// <param name="nsTimeout">The wait timeout in nanoseconds.</param>
	/// <returns><c>true</c> if the task completed, <c>false</c> if the timeout was reached.</returns>
	public bool Wait(ulong nsTimeout = UInt64.MaxValue) => _work.WaitComplete(nsTimeout);

	/// <summary>Blocks the current thread until the task is complete.</summary>
	/// <returns>The value produced by the task completion.</returns>
	public T GetResult()
	{
		_work.WaitComplete(UInt64.MaxValue);
		return _value;
	}
	
	
	/// <summary>Gets the notify awaiter for the task. For advanced use only.</summary>
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public GraphicsTask<T> GetAwaiter() => this;
	
	/// <inheritdoc cref="INotifyCompletion.OnCompleted"/>
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public void OnCompleted(Action action)
	{
		if (_work.IsComplete) action();
		else if (SynchronizationContext.Current is { } ctx) ctx.Post(GraphicsTask.AwaiterCallback, (_work, action));
		else ThreadPool.QueueUserWorkItem(GraphicsTask.WorkItemCallback, (_work, action), true);
	}
}
