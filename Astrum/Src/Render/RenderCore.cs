/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Astrum.Native;
using static Astrum.EngineLogger;

namespace Astrum.Render;


/// <summary>Core API for the rendering subsystem.</summary>
/// <remarks>
/// Unless otherwise noted, fields and methods in this type must not be accessed before <see cref="AppConfig.Run"/>.
/// </remarks>
public static class RenderCore
{
	// Maximum number of concurrently processed frames
	internal const uint MAX_FRAMES = 2;

	/// <summary>The maximum size of any uniform update, in bytes.</summary>
	public const uint MAX_UNIFORM_SIZE = 1024;
	
	/// <summary>Delegate for <see cref="Display"/> connect/disconnect events.</summary>
	/// <param name="display">The display that changed.</param>
	/// <param name="connected"><c>true</c> if the display connected, <c>false</c> for disconnected.</param>
	public delegate void DisplayChangedCallback(Display display, bool connected);
	/// <summary>Delegate for <see cref="Window"/> open/close events.</summary>
	/// <param name="window">The window that opened or closed.</param>
	/// <param name="opened"><c>true</c> if the window opened, <c>false</c> if it closed.</param>
	public delegate void WindowCallback(Window window, bool opened);


	#region Fields
	/// <summary>If the rendering subsystem has been initialized.</summary>
	/// <remarks>Safe to access before the application starts.</remarks>
	public static bool IsInitialized { get; private set; }

	/// <summary>All supported graphics devices on the system.</summary>
	/// <remarks>Safe to access before the application starts.</remarks>
	public static IReadOnlyList<GraphicsDevice> AllDevices => RenderDriver.AllDevices;
	/// <summary>The default graphics device on the system.</summary>
	/// <remarks>
	/// This is chosen using a simple heuristic to find the device with the largest local memory pool, preferring
	/// discrete devices. Safe to access before the application starts.
	/// </remarks>
	public static GraphicsDevice DefaultDevice => RenderDriver.DefaultDevice;

	/// <summary>The graphics device in use by the rendering driver.</summary>
	public static GraphicsDevice Device => NotInitializedException.CheckAndThrow(_Driver?.Device)!;
	
	/// <summary>All displays on the current system.</summary>
	/// <remarks>
	/// The returned list will not be modified at any time, but may go out of date. Safe to access on any thread.
	/// </remarks>
	public static IReadOnlyList<Display> AllDisplays => _AllDisplays;
	private static Display[] _AllDisplays;

	/// <summary>The display considered to be the primary one by the system.</summary>
	public static Display PrimaryDisplay => _AllDisplays.FirstOrDefault(static d => d.IsPrimary) ??
		throw new PlatformNotSupportedException("No displays available on system");
	
	/// <summary>Event that is raised when a <see cref="Display"/> connects or disconnects.</summary>
	/// <remarks>Will only ever be raised on the main thread.</remarks>
	public static event DisplayChangedCallback? OnDisplayChanged;
	
	/// <summary>If the windows currently have VSync enabled. See also <see cref="SetVsync"/>.</summary>
	public static bool Vsync { get; private set; }

	/// <summary>The set of all windows that are currently opened by the application.</summary>
	public static IReadOnlyList<Window> OpenWindows => NotInitializedException.CheckAndThrow(_OpenWindows);
	private static readonly List<Window> _OpenWindows = [];
	
	/// <summary>Event that is raised when a window is opened.</summary>
	/// <remarks>Will only ever be raised on the main thread.</remarks>
	public static event WindowCallback? OnWindowOpened;
	/// <summary>Event that is raised when a window is closed.</summary>
	/// <remarks>Will only ever be raised on the main thread.</remarks>
	public static event WindowCallback? OnWindowClosed;

	/// <summary>The max size of uniform updates across all render commands per frame, in bytes.</summary>
	public static uint MaxPerFrameUniformSize => NotInitializedException.CheckAndThrow(_Driver?.UniformSpace ?? 0);
	/// <summary>The max size of all streaming buffer data across all render commands per frame, in bytes.</summary>
	public static uint MaxPerFrameStreamingSize => NotInitializedException.CheckAndThrow(_Driver?.StreamingSpace ?? 0);
	/// <summary>The remaining space for uniform updates in the current frame, in bytes.</summary>
	public static uint RemainingUniformSpace => NotInitializedException.CheckAndThrow(_Driver?.RemainingUniformSpace ?? 0);
	/// <summary>The remaining space for streaming data in the current frame, in bytes.</summary>
	public static uint RemainingStreamingSpace => NotInitializedException.CheckAndThrow(_Driver?.RemainingStreamingSpace ?? 0);

	/// <summary>The maximum number of active writeable textures.</summary>
	public static uint MaxWriteableTextures => NotInitializedException.CheckAndThrow(_Driver?.WriteTextureLimit ?? 0);
	/// <summary>The maximum number of active readonly textures.</summary>
	public static uint MaxTextures => NotInitializedException.CheckAndThrow(_Driver?.TextureLimit ?? 0);
	
	// The render driver implementation
	internal static RenderDriver Driver => NotInitializedException.CheckAndThrow(_Driver)!;
	private static RenderDriver? _Driver;
	#endregion


	#region Lifecycle
	// Initialization after device selection
	internal static void Initialize(AppConfig config)
	{
		Debug.Assert(!IsInitialized);

		// Create the render driver from the device
		var device = config.GraphicsDevice ?? RenderDriver.DefaultDevice;
		_Driver = new(device, config);
		
		IsInitialized = true;
		
		// Open the main window
		var winSize = config.MainWindowSize ?? new(Window.DEFAULT_WIDTH, Window.DEFAULT_HEIGHT);
		var winTitle = config.MainWindowTitle ?? Core.ApplicationName;
		OpenWindow(Windows.Main, winSize.X, winSize.Y, winTitle, config.MainWindowDisplay);
	}

	// Shutdown/terminate on application terminate
	internal static void Shutdown()
	{
		Debug.Assert(IsInitialized);
		
		// Close all open windows
		foreach (var win in _OpenWindows) {
			((Window.Desktop)win).Destroy();
			OnWindowClosed?.Invoke(win, false);
		}
		_OpenWindows.Clear();

		// Terminate the render driver
		_Driver?.Destroy();
		_Driver = null;

		IsInitialized = false;
	}

	// Handles the start of an application frame
	internal static void StartFrame()
	{
		_Driver!.StartFrame();
	}

	// Handles the end of an application frame (return false when main window closes to quit application)
	internal static bool EndFrame()
	{
		// Check for Closed Windows
		_OpenWindows.RemoveAll(static w => {
			if (!w.IsClosing) return false;
			((Window.Desktop)w).Destroy();
			OnWindowClosed?.Invoke(w, false);
			return true;
		});
		if (!Windows.Main.IsOpen) return false;

		// End frame
		_Driver!.EndFrame();
		return true;
	}
	#endregion // Lifecycle


	#region Windows
	/// <summary>Enables (<c>true</c>) or disables (<c>false</c>) window vsync.</summary>
	/// <remarks>Must be called on the main thread only.</remarks>
	public static void SetVsync(bool enabled)
	{
		Core.AssertMainThread();
		NotInitializedException.CheckAndThrow();
		if (Vsync == enabled) return;
		
		// Dirty all swapchains and update state
		foreach (var win in _OpenWindows) win.Swapchain!.MarkDirty();
		Vsync = enabled;
	}

	/// <summary>Opens a new application window.</summary>
	/// <remarks>The application must not be rendering or terminating. Must be called on the main thread only.</remarks>
	/// <param name="window">The identifier of the window to open. Must not already be open.</param>
	/// <param name="width">The initial width of the window. Will be clamped to valid window size range.</param>
	/// <param name="height">The initial height of the window. Will be clamped to valid window size range.</param>
	/// <param name="title">The initial title of the window.</param>
	/// <param name="display">The display to immediately make the window fullscreen on, or <c>null</c>.</param>
	public static void OpenWindow(Window window, uint width, uint height, string title, Display? display = null)
	{
		// Validate state
		Core.AssertMainThread();
		NotInitializedException.CheckAndThrow();
		if (Core.Stage is not (AppStage.Updating or AppStage.Initializing or AppStage.Waiting)) {
			throw new InvalidOperationException("Cannot open a window during rendering or application shutdown");
		}
		
		// Validate arguments
		if (window.IsOpen) throw new InvalidOperationException($"The window '{window.Name}' is already open");
		if (display is { IsConnected: false }) {
			throw new ArgumentException("Cannot open window on disconnected display");
		}
		
		// Open the window
		((Window.Desktop)window).Open(width, height, title, display);
		_OpenWindows.Add(window);
		
		// TODO: input
		
		// Raise event
		OnWindowOpened?.Invoke(window, true);
	}
	#endregion // Windows
	
	
	// Static Ctor
	static RenderCore()
	{
		// Populate initial displays
		_AllDisplays = Glfw.GetMonitors().ToArray(static m => new Display(m));

		// Handle monitor events
		Glfw.OnMonitorEvent += static (monitor, @event) => {
			if (@event == Glfw.MonitorEvent.Connected) {
				Display display = new(monitor);
				EngineInfo($"Display connected: {display.Handle}, '{display.Name}'");
				Array.Resize(ref _AllDisplays, _AllDisplays.Length + 1);
				_AllDisplays[^1] = display;
				OnDisplayChanged?.Invoke(display, true);
			}
			else if (@event == Glfw.MonitorEvent.Disconnected) {
				var idx = Array.FindIndex(_AllDisplays, d => d.Handle == monitor);
				var display = _AllDisplays[idx];
				EngineWarn($"Display disconnected: {display.Handle}, '{display.Name}'");
				display.MarkDisconnected();

				var newDisplays = new Display[_AllDisplays.Length - 1];
				for (int si = 0, di = 0; si < _AllDisplays.Length; ++si) {
					if (si != idx) newDisplays[di++] = _AllDisplays[si];
				}
				_AllDisplays = newDisplays;
				OnDisplayChanged?.Invoke(display, false);
			}
		};
	}
	
	
	/// <summary>
	/// Indicates that <see cref="RenderCore"/> or other rendering functionality was used before the application was
	/// initialized.
	/// </summary>
	public sealed class NotInitializedException : Exception
	{
		internal NotInitializedException() : base("Cannot access RenderCore before initialization") { }

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal static void CheckAndThrow() { if (!IsInitialized) throw new NotInitializedException(); }

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal static T CheckAndThrow<T>(T value)
		{
			if (!IsInitialized) throw new NotInitializedException();
			return value;
		}
	}

	/// <summary>Indicates that the application has exceeded its per-frame uniform or streaming buffer space.</summary>
	public sealed class BufferSpaceLimitException : Exception
	{
		private BufferSpaceLimitException(string buffer) : base($"Per-frame {buffer} data exceeded") { }

		internal static BufferSpaceLimitException Uniform() => new("uniform");
		internal static BufferSpaceLimitException Streaming() => new("streaming");
	}

	/// <summary>Indicates that the application has exceeded the supported number of some resource type.</summary>
	public sealed class ResourceLimitException : Exception
	{
		private ResourceLimitException(string typeName) : base($"Exceeded max number of {typeName}") { }

		internal static ResourceLimitException WriteTextures() => new("write texture table spaces");
		internal static ResourceLimitException ReadonlyTextures() => new("readonly texture table spaces");
		internal static ResourceLimitException Buffers() => new("buffers");
		internal static ResourceLimitException Textures() => new("textures");
	}
}
