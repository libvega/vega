﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using Astrum.Render.Vulkan;
using LLVK;
using static LLVK.Vulkan;

namespace Astrum.Render;


// [Partial] RenderDriver components related to resource binding
//   In the normal binding mode (Win/Linux/Most Android), the bind-less approach is used with the following layout:
//     - (0:0)    Shader Globals     - Dynamic Uniform Buffer 
//     - (0:1)    Shader Uniforms    - Dynamic Uniform Buffer
//     - (1:0)[M] Writeable Textures - Storage Images (fixed size array size MAX_WRITEABLE_TEXTURES)
//     - (1:1)[N] Textures           - Combined Image/Sampler (unbound array size MAX_TEXTURES)
//     - (N/A)    Buffers            - Buffers are passed around using their device addresses as uvec2 references
internal sealed unsafe partial class RenderDriver
{
	// All resource stages
	private const VkShaderStageFlags ALL_STAGES =
		VkShaderStageFlags.Vertex | VkShaderStageFlags.Fragment | VkShaderStageFlags.Compute;
	// The flags needed for bind-less support in layouts
	private const VkDescriptorBindingFlags BIND_LESS_FLAGS =
		VkDescriptorBindingFlags.PartiallyBound | VkDescriptorBindingFlags.UpdateUnusedWhilePending;
	
	// Binding table limits
	private const uint DEFAULT_WRITE_TEXTURE_COUNT = 32;
	private const uint DEFAULT_TEXTURE_COUNT = 4_096;
	private const uint LARGE_WRITE_TEXTURE_COUNT = DEFAULT_WRITE_TEXTURE_COUNT * 4;
	private const uint LARGE_TEXTURE_COUNT = DEFAULT_TEXTURE_COUNT * 4;
	
	// Global buffer sizes
	private const uint DEFAULT_UNIFORM_SIZE = 1_024 * 1_024;
	private const uint DEFAULT_STREAMING_SIZE = 2 * 1_024 * 1_024;
	private const uint LARGE_UNIFORM_SIZE = DEFAULT_UNIFORM_SIZE * 4;
	private const uint LARGE_STREAMING_SIZE = DEFAULT_STREAMING_SIZE * 4;


	// Sets the uniform buffer bindings
	private void setUniformBufferBinding(in BufferRef buffer)
	{
		VkDescriptorBufferInfo bufferInfo = new() {
			Buffer = buffer.Descriptor->Handle, Offset = 0, Range = RenderCore.MAX_UNIFORM_SIZE
		};
		VkWriteDescriptorSet write = new() {
			DstSet = _bindingTables.BufferSet, DstBinding = 0, DstArrayElement = 0,
			DescriptorCount = 1, DescriptorType = VkDescriptorType.UniformBufferDynamic, PBufferInfo = &bufferInfo
		};
		vkUpdateDescriptorSets(VulkanCtx.Device, 1, &write, 0, null);
		write.DstBinding = 1;
		vkUpdateDescriptorSets(VulkanCtx.Device, 1, &write, 0, null);
	}


	#region Write Textures
	// Reserve writeable texture
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public uint ReserveWriteableTextureBinding(ImageRef image)
	{
		var desc = image.Descriptor;
		Debug.Assert(desc != null, "Attempted to reserve binding for invalid image");
		
		// Check for old slot
		if (desc->WriteBindingSlot != 0) return desc->WriteBindingSlot;
		
		// Reserve new slot
		uint slot;
		lock (_bindingTables.WriteTextureMask) {
			var clear = _bindingTables.WriteTextureMask.FirstClear(); 
			if (!clear.HasValue) throw RenderCore.ResourceLimitException.WriteTextures();
			_bindingTables.WriteTextureMask.Set(clear.Value);
			slot = clear.Value;
		}
		setWriteableTextureBinding(slot, image);
		return slot;
	}
	
	// Free writeable texture
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void FreeWriteableTextureBinding(ImageRef image)
	{
		ImageDescriptor* desc;
		if (Core.Stage >= AppStage.Terminating || (desc = image.Descriptor) == null) return;
		if (desc->WriteBindingSlot == 0) return;
		lock (_bindingTables.WriteTextureMask) {
			_bindingTables.WriteTextureMask.Clear(image.Descriptor->RealWriteBindingSlot);
		}
		image.Descriptor->WriteBindingSlot = 0;
	}

	// Set a writeable texture slot (use with extreme care, has no delay and slot allocation is not changed)
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private void setWriteableTextureBinding(uint slot, ImageRef image)
	{
		VkDescriptorImageInfo info = new() {
			ImageView = image.Descriptor->View, ImageLayout = VkImageLayout.General
		};
		VkWriteDescriptorSet write = new() {
			DstSet = _bindingTables.TexelSet, DstBinding = 0, DstArrayElement = slot,
			DescriptorCount = 1, DescriptorType = VkDescriptorType.StorageImage, PImageInfo = &info
		};
		vkUpdateDescriptorSets(VulkanCtx.Device, 1, &write, 0, null);
		image.Descriptor->WriteBindingSlot = (ushort)(slot + 1);
	}
	#endregion // Write Textures


	#region Readonly Textures
	// Reserve readonly texture
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public uint ReserveTextureBinding(ImageRef image, VkImageLayout layout, Sampler sampler)
	{
		var desc = image.Descriptor;
		Debug.Assert(desc != null, "Attempted to reserve binding for invalid image");
		
		// Check for old slot
		if (desc->ReadonlyBindingSlots[(int)sampler.Index] != 0) {
			return desc->ReadonlyBindingSlots[(int)sampler.Index];
		}
		
		// Reserve new slot
		uint slot;
		lock (_bindingTables.TextureMask) {
			var clear = _bindingTables.TextureMask.FirstClear(); 
			if (!clear.HasValue) throw RenderCore.ResourceLimitException.ReadonlyTextures();
			_bindingTables.TextureMask.Set(clear.Value);
			slot = clear.Value;
		}
		setTextureBinding(slot, image, layout, sampler);
		return slot;
	}
	
	// Free readonly texture
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void FreeTextureBinding(ImageRef image, Sampler sampler)
	{
		ImageDescriptor* desc;
		if (Core.Stage >= AppStage.Terminating || (desc = image.Descriptor) == null) return;
		if (desc->ReadonlyBindingSlots[(int)sampler.Index] == 0) return;
		lock (_bindingTables.TextureMask) {
			_bindingTables.TextureMask.Clear(desc->ReadonlyBindingSlots[(int)sampler.Index]);
		}
		desc->ReadonlyBindingSlots[(int)sampler.Index] = 0;
	}
	
	// Free all readonly bindings in the texture
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void FreeAllTextureBindings(ImageRef image)
	{
		ImageDescriptor* desc;
		if (Core.Stage >= AppStage.Terminating || (desc = image.Descriptor) == null) return;
		lock (_bindingTables.TextureMask) {
			foreach (ref var slot in desc->ReadonlyBindingSlots) {
				if (slot == 0) continue;
				_bindingTables.TextureMask.Clear(slot);
				slot = 0;
			}
		}
	}

	// Set a readonly texture slot (use with extreme care, has no delay and slot allocation is not changed)
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void setTextureBinding(uint slot, ImageRef image, VkImageLayout layout, Sampler sampler)
	{
		Debug.Assert(_samplerTable[(int)sampler.Index], "Attempted to bind an un-created sampler");
		
		VkDescriptorImageInfo info = new() {
			Sampler = _samplerTable[(int)sampler.Index], ImageView = image.Descriptor->View, ImageLayout = layout
		};
		VkWriteDescriptorSet write = new() {
			DstSet = _bindingTables.TexelSet, DstBinding = 1, DstArrayElement = slot,
			DescriptorCount = 1, DescriptorType = VkDescriptorType.CombinedImageSampler, PImageInfo = &info
		};
		vkUpdateDescriptorSets(VulkanCtx.Device, 1, &write, 0, null);
		image.Descriptor->ReadonlyBindingSlots[(int)sampler.Index] = (ushort)slot;
	}
	#endregion // Readonly Textures
	

	#region Global Buffers
	// Allocate new space in the uniform buffer, returns the real aligned size
	[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
	public uint AllocateUniformSpace(uint size, out uint offset, out void* dataPtr)
	{
		var align = Device.UniformBufferAlignment;
		size = (size % align != 0) ? ((size + align) / align) : size;
		
		var allocEnd = Interlocked.Add(ref _globalBuffers.UniformOffset, size);
		if (allocEnd > _globalBuffers.PerFrameUniformSpace) throw RenderCore.BufferSpaceLimitException.Uniform();
		offset = (_globalBuffers.PerFrameUniformSpace * _globalBuffers.FrameIndex) + (allocEnd - size);
		dataPtr = (byte*)_globalBuffers.UniformBuffer.Descriptor->MemoryMap + offset;
		return size;
	}
	
	// Allocate new space in the streaming buffer, returns the real aligned size
	[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
	public uint AllocateStreamingSpace(uint size, out uint offset, out void* dataPtr)
	{
		var align = Device.StorageBufferAlignment;
		size = (size % align != 0) ? ((size + align) / align) : size;
		
		var allocEnd = Interlocked.Add(ref _globalBuffers.StreamingOffset, size);
		if (allocEnd > _globalBuffers.PerFrameStreamingSpace) throw RenderCore.BufferSpaceLimitException.Streaming();
		offset = (_globalBuffers.PerFrameStreamingSpace * _globalBuffers.FrameIndex) + (allocEnd - size);
		dataPtr = (byte*)_globalBuffers.StreamingBuffer.Descriptor->MemoryMap + offset;
		return size;
	}
	#endregion // Global Buffers


	#region Create/Destroy
	// Initializes the engine global descriptor pool and tables
	private static void CreateBindingTables(out BindingTables tables, AppConfig.LargeTextureTableConfig config)
	{
		// Texel table sizes
		var writeTexSize = config.Write ? LARGE_WRITE_TEXTURE_COUNT : DEFAULT_WRITE_TEXTURE_COUNT;
		var texSize = config.Readonly ? LARGE_TEXTURE_COUNT : DEFAULT_TEXTURE_COUNT;
		
		// Create layouts
		VkDescriptorSetLayout bufferLayout, texelLayout;
		{
			var bindingPtr = stackalloc VkDescriptorSetLayoutBinding[2] {
				new() { Binding = 0, DescriptorType = VkDescriptorType.UniformBufferDynamic, DescriptorCount = 1,
					StageFlags = ALL_STAGES }, // Globals
				new() { Binding = 1, DescriptorType = VkDescriptorType.UniformBufferDynamic, DescriptorCount = 1,
					StageFlags = ALL_STAGES }  // Uniforms
			};
			VkDescriptorSetLayoutCreateInfo create = new() {
				Flags = VkDescriptorSetLayoutCreateFlags.UpdateAfterBindPool, BindingCount = 2, PBindings = bindingPtr
			};
			vkCreateDescriptorSetLayout(VulkanCtx.Device, &create, null, &bufferLayout).ThrowIfError();
			VulkanCtx.SetName(bufferLayout, "@BufferTableLayout");
		}
		{
			var bindingPtr = stackalloc VkDescriptorSetLayoutBinding[2] {
				new() { Binding = 0, DescriptorType = VkDescriptorType.StorageImage, 
					DescriptorCount = writeTexSize, StageFlags = ALL_STAGES }, // Writeable Textures
				new() { Binding = 1, DescriptorType = VkDescriptorType.CombinedImageSampler, 
					DescriptorCount = texSize, StageFlags = ALL_STAGES } // Textures
			};
			var flagsPtr = stackalloc VkDescriptorBindingFlags[2] { BIND_LESS_FLAGS, BIND_LESS_FLAGS };
			VkDescriptorSetLayoutBindingFlagsCreateInfo flagInfo = new() { BindingCount = 2, PBindingFlags = flagsPtr };
			VkDescriptorSetLayoutCreateInfo create = new() {
				Flags = VkDescriptorSetLayoutCreateFlags.UpdateAfterBindPool, BindingCount = 2, PBindings = bindingPtr,
				PNext = &flagInfo
			};
			vkCreateDescriptorSetLayout(VulkanCtx.Device, &create, null, &texelLayout).ThrowIfError();
			VulkanCtx.SetName(texelLayout, "@TexelTableLayout");
		}
		
		// Create the pool
		VkDescriptorPool pool;
		{
			var sizePtr = stackalloc VkDescriptorPoolSize[3] {
				new() { Type = VkDescriptorType.UniformBufferDynamic, DescriptorCount = 2 },
				new() { Type = VkDescriptorType.StorageImage,         DescriptorCount = writeTexSize },
				new() { Type = VkDescriptorType.CombinedImageSampler, DescriptorCount = texSize }
			};
			VkDescriptorPoolCreateInfo create = new() {
				Flags = VkDescriptorPoolCreateFlags.UpdateAfterBind,
				MaxSets = 2, PoolSizeCount = 3, PPoolSizes = sizePtr
			};
			vkCreateDescriptorPool(VulkanCtx.Device, &create, null, &pool).ThrowIfError();
			VulkanCtx.SetName(pool, "@DescriptorPool");
		}
		
		// Create the sets
		var setPtr = stackalloc VkDescriptorSet[2];
		{
			var layoutPtr = stackalloc VkDescriptorSetLayout[2] { bufferLayout, texelLayout };
			VkDescriptorSetAllocateInfo alloc = new() {
				DescriptorPool = pool, DescriptorSetCount = 2, PSetLayouts = layoutPtr
			};
			vkAllocateDescriptorSets(VulkanCtx.Device, &alloc, setPtr).ThrowIfError();
			VulkanCtx.SetName(setPtr[0], "@BufferTable");
			VulkanCtx.SetName(setPtr[1], "@TexelTable");
		}
		
		// Populate tables
		tables = new() {
			Pool = pool,
			BufferLayout = bufferLayout, TexelLayout = texelLayout,
			BufferSet = setPtr[0], TexelSet = setPtr[1],
			WriteTextureMask = new(writeTexSize), TextureMask = new(texSize)
		};
	}
	
	// Destroy a set of binding tables
	private static void DestroyBindingTables(in BindingTables tables)
	{
		if (tables.Pool) vkDestroyDescriptorPool(VulkanCtx.Device, tables.Pool, null);
		if (tables.TexelLayout) vkDestroyDescriptorSetLayout(VulkanCtx.Device, tables.TexelLayout, null);
		if (tables.BufferLayout) vkDestroyDescriptorSetLayout(VulkanCtx.Device, tables.BufferLayout, null);
	}
	
	// Initializes the global data buffers
	private void createGlobalBuffers(out GlobalBuffers buffers, AppConfig.LargeBufferSpaceConfig config)
	{
		// Buffer sizes
		var uniformSize = config.Uniform ? LARGE_UNIFORM_SIZE : DEFAULT_UNIFORM_SIZE;
		var streamingSize = config.Streaming ? LARGE_STREAMING_SIZE : DEFAULT_STREAMING_SIZE;
		
		// Create uniform buffer
		BufferCreate create = new() {
			Size = uniformSize * RenderCore.MAX_FRAMES, Usage = VkBufferUsageFlags.UniformBuffer, 
			Shared = true, Mapped = true, DeviceAddress = false
		}; 	
		var uniformBuffer = createBuffer(in create, VulkanCtx.DynamicMemory);
		
		// Create streaming buffer
		create = new() {
			Size = streamingSize * RenderCore.MAX_FRAMES,
			Usage = VkBufferUsageFlags.VertexBuffer | VkBufferUsageFlags.IndexBuffer | VkBufferUsageFlags.StorageBuffer,
			Shared = true, Mapped = true, DeviceAddress = true
		};
		var streamingBuffer = createBuffer(in create, VulkanCtx.DynamicMemory);
		
		// Populate buffers
		buffers = new() {
			UniformBuffer = uniformBuffer, StreamingBuffer = streamingBuffer,
			PerFrameUniformSpace = uniformSize, PerFrameStreamingSpace = streamingSize
		};
	}
	
	// Destroy the global data buffers
	private void destroyGlobalBuffers(in GlobalBuffers buffers)
	{
		if (buffers.StreamingBuffer) DestroyBuffer(buffers.StreamingBuffer);
		if (buffers.UniformBuffer) DestroyBuffer(buffers.UniformBuffer);
	}
	#endregion // Create/Destroy
	
	
	// Values and objects for the binding tables
	private struct BindingTables
	{
		public required VkDescriptorPool Pool;
		public required VkDescriptorSetLayout BufferLayout;
		public required VkDescriptorSetLayout TexelLayout;
		public required VkDescriptorSet BufferSet;
		public required VkDescriptorSet TexelSet;
		public required Bitmask WriteTextureMask;
		public required Bitmask TextureMask;
	}
	
	// Global data buffers
	private struct GlobalBuffers
	{
		public required BufferRef UniformBuffer;
		public required BufferRef StreamingBuffer;
		public required uint PerFrameUniformSpace;
		public required uint PerFrameStreamingSpace;
		public uint UniformOffset;
		public uint StreamingOffset;
		public uint FrameIndex;
	}
}
