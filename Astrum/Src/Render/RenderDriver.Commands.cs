﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Astrum.Render.Vulkan;
using JetBrains.Annotations;
using LLVK;
using static Astrum.EngineLogger;
using static LLVK.Vulkan;
using static LLVK.Vulkan.KHR;

namespace Astrum.Render;


// [Partial] RenderDriver components related to command pools and command buffers
internal sealed unsafe partial class RenderDriver
{
	// Submit to the main queue
	public VkResult SubmitMainQueue(uint count, VkSubmitInfo* pInfo, VkFence fence)
	{
		lock (VulkanCtx.MainQueue.Lock) return vkQueueSubmit(VulkanCtx.MainQueue.Handle, count, pInfo, fence);
	}
	
	// Submit to the DMA queue
	public VkResult SubmitDmaQueue(uint count, VkSubmitInfo* pInfo, VkFence fence)
	{
		Debug.Assert(VulkanCtx.DmaQueue.HasValue, "Attempted to use unsupported DMA queue");
		var dmaQueue = VulkanCtx.DmaQueue!.Value;
		lock (dmaQueue.Lock) return vkQueueSubmit(dmaQueue.Handle, count, pInfo, fence);
	}
	
	// Present to the main queue
	public VkResult PresentMainQueue(VkPresentInfoKHR* pInfo)
	{
		lock (VulkanCtx.MainQueue.Lock) return vkQueuePresentKHR(VulkanCtx.MainQueue.Handle, pInfo);
	}
	
	
	// Start a new CommandList on the current thread for the main queue
	//   ALWAYS USE `using` WITH THIS RETURN VALUE
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	[MustDisposeResource]
	public CommandList StartMainCommandList(uint bufferCount) =>
		getOrCreateMainCommandPool().StartCommandList(bufferCount);
	
	// Start a new CommandList on the current thread for the DMA queue
	//   ALWAYS USE `using` WITH THIS RETURN VALUE
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	[MustDisposeResource]
	public CommandList StartDmaCommandList(uint bufferCount) =>
		getOrCreateDmaCommandPool().StartCommandList(bufferCount);
	
	
	// Get or create the main command pool for the current thread
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private CommandPool getOrCreateMainCommandPool()
	{
		// Lookup existing pool
		var thisThread = Thread.CurrentThread;
		lock (_commandPools) {
			foreach (ref var pools in CollectionsMarshal.AsSpan(_commandPools)) {
				if (!ReferenceEquals(pools.Thread, thisThread)) continue;
				return pools.MainPool ??= new(thisThread, true);
			}
		}
		
		// Create new entry
		CommandPool newPool = new(thisThread, true);
		lock (_commandPools) _commandPools.Add((thisThread, newPool, null));
		return newPool;
	}
	
	// Get or create the DMA command pool for the current thread
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private CommandPool getOrCreateDmaCommandPool()
	{
		Debug.Assert(HasDmaQueue, "Attempted to get or create unsupported DMA command pool");
		
		// Lookup existing pool
		var thisThread = Thread.CurrentThread;
		lock (_commandPools) {
			foreach (ref var pools in CollectionsMarshal.AsSpan(_commandPools)) {
				if (!ReferenceEquals(pools.Thread, thisThread)) continue;
				return pools.DmaPool ??= new(thisThread, false);
			}
		}
		
		// Create new entry
		CommandPool newPool = new(thisThread, false);
		lock (_commandPools) _commandPools.Add((thisThread, null, newPool));
		return newPool;
	}
	
	// Periodically cleans up the command pools from dead threads
	private void cleanupDeadCommandPools()
	{
		if ((Core.Time.Elapsed - _lastCommandPoolCleanup).TotalSeconds >= 10) {
			lock (_commandPools) {
				_commandPools.RemoveAll(static pools => {
					if (pools.Thread.IsAlive) return false;
					pools.MainPool?.Destroy();
					pools.DmaPool?.Destroy();
					EngineInfo($"Cleanup CommandPools for thread {pools.Thread.ManagedThreadId}");
					return true;
				});
			}
			_lastCommandPoolCleanup = Core.Time.Elapsed;
		}
	}
	
	
	// Manages a pool of command buffers, and the state of command submissions under construction
	//   Each pool can have a single submission under construction at once (each can have multiple buffers)
	private sealed class CommandPool
	{
		// Maximum per-submission objects
		private const uint MAX_WAITS = Window.MAX_COUNT + 4;
		private const uint MAX_BUFFERS = 4;
		
		// Max semaphores in a single pool before waits are used (also implicitly limits the max number of buffers)
		private const uint MAX_POOL_SEMAPHORES = 8;
		
		
		#region Fields
		// The thread that uses the pool
		private readonly Thread _thread;
		// The parent queue flag (true == main, false == dma)
		private readonly bool _isMainQueue;
		// Debug name
		private readonly string _debugName;
		
		// Pool handle
		private readonly VkCommandPool _pool;
		// Queue of timeline semaphores to use for submissions (stored as their last WorkUnits)
		private readonly Queue<WorkUnit> _semaphores = new((int)MAX_POOL_SEMAPHORES);
		// Queue of command buffers with last work units for each (oldest is generally first)
		private readonly Queue<(VkCommandBuffer Buffer, WorkUnit LastWork)> _buffers = new();
		private uint _totalBufferCount;
		
		// Boxed valid flag for safe WorkUnit queries
		private readonly Boxed<bool> _isValid = new(true);
		
		// Submission construction state
		public ulong Version { get; private set; } = 1; // List versioning (increments every start and submit)
		private uint _waitCount;
		private uint _bufferCount;
		private bool _hasCommandList;
		private SubmissionObjects _submit;
		#endregion // Fields

		
		public CommandPool(Thread thread, bool isMainQueue)
		{
			_thread = thread;
			_isMainQueue = isMainQueue;
			Debug.Assert(isMainQueue || VulkanCtx.DmaQueue.HasValue, "Cannot create pool for invalid DMA queue");
			
			// Create pool
			VkCommandPoolCreateInfo create = new() {
				Flags = VkCommandPoolCreateFlags.ResetCommandBuffer, 
				QueueFamilyIndex = isMainQueue ? VulkanCtx.MainQueue.Family : VulkanCtx.DmaQueue!.Value.Family 
			};
			VkCommandPool pool;
			vkCreateCommandPool(VulkanCtx.Device, &create, null, &pool).ThrowIfError();
			_debugName = $"{(isMainQueue ? "@MainCommandPool" : "@DmaCommandPool")}/{_thread.ManagedThreadId}";
			VulkanCtx.SetName(pool, _debugName);
			_pool = pool;
			
			EngineInfo($"Created command pool {_debugName}");
		}

		// Destroys the pool resources with safe waits
		public void Destroy()
		{
			CancelCommandList(Version);

			foreach (var work in _semaphores) work.WaitComplete(UInt64.MaxValue);
			_isValid.Value = false;
			foreach (var work in _semaphores) vkDestroySemaphore(VulkanCtx.Device, work.Semaphore, null);
			_semaphores.Clear();
			_buffers.Clear();
			vkDestroyCommandPool(VulkanCtx.Device, _pool, null);
			
			EngineInfo($"Destroyed command pool {_debugName}");
		}
		
		
		// Reserves a command buffer from the pool, growing if needed
		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		private VkCommandBuffer reserveCommandBuffer()
		{
			// Use oldest if idle
			if (_buffers.TryPeek(out var oldest) && oldest.LastWork.IsComplete) return _buffers.Dequeue().Buffer;
			
			// Create new buffer otherwise
			VkCommandBufferAllocateInfo alloc = new() {
				CommandPool = _pool, Level = VkCommandBufferLevel.Primary, CommandBufferCount = 1
			};
			VkCommandBuffer buffer;
			vkAllocateCommandBuffers(VulkanCtx.Device, &alloc, &buffer).ThrowIfError();
			VulkanCtx.SetName(buffer, _debugName, "Buffer", _totalBufferCount);
			_totalBufferCount += 1;
			EngineDebug($"Added command buffer to pool {_debugName}");
			return buffer;
		}
		
		// Reserves a timeline semaphore to use for submission
		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		private WorkUnit reserveSemaphore()
		{
			// Use oldest if already complete
			if (_semaphores.TryPeek(out var work) && work.IsComplete) return _semaphores.Dequeue();
			
			// Wait on oldest if limit reached
			if (_semaphores.Count == MAX_POOL_SEMAPHORES) {
				// TODO: this might become an issue on long-running background compute tasks, check in later
				if (!work.WaitComplete(10_000_000_000ul /* 10 sec */)) {
					Trace.Fail($"Render operations appear to be hung on thread {_thread.ManagedThreadId}");
				}
				return _semaphores.Dequeue();
			}
			
			// Create new semaphore
			VkSemaphoreTypeCreateInfo typeInfo = new() { SemaphoreType = VkSemaphoreType.Timeline, InitialValue = 0 };
			VkSemaphoreCreateInfo create = new() { PNext = &typeInfo };
			VkSemaphore semaphore;
			vkCreateSemaphore(VulkanCtx.Device, &create, null, &semaphore).ThrowIfError();
			VulkanCtx.SetName(semaphore, _debugName, "Semaphore", (uint)_semaphores.Count);
			EngineDebug($"Added semaphore to pool {_debugName}");
			return new(_isValid, semaphore, 0);
		}
		
		
		// Start a new command list
		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		public CommandList StartCommandList(uint bufferCount)
		{
			Debug.Assert(!_hasCommandList, "Cannot record multiple CommandLists at once");
			Trace.Assert(bufferCount is > 0 and <= MAX_BUFFERS, "Bad buffer count for CommandList"); // Keep as trace
				
			// Reserve and begin buffers with error handling to return them to the pool
			try {
				VkCommandBufferBeginInfo begin = new() { Flags = VkCommandBufferUsageFlags.OneTimeSubmit };
				for (_bufferCount = 0; _bufferCount < bufferCount; ) {
					var buffer = reserveCommandBuffer();
					_submit.Buffers[(int)_bufferCount++] = buffer;
					vkBeginCommandBuffer(buffer, &begin).ThrowIfError();
				}
			}
			catch {
				for (var bi = 0; bi < _bufferCount; ++bi) {
					_ = vkEndCommandBuffer(_submit.Buffers[bi]);
					_buffers.Enqueue((_submit.Buffers[bi], WorkUnit.Completed));
				} 
				throw;
			}
			
			// Set remaining state
			Version += 1;
			_waitCount = 0;
			_hasCommandList = true;
			_submit.BinarySignalSemaphore = default;

			// Return operational list
			return new(this, Version, MemoryMarshal.CreateReadOnlySpan(ref _submit.Buffers[0], (int)_bufferCount));
		}
		
		// Submits the current command list to the queue
		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		[SkipLocalsInit]
		public WorkUnit SubmitCommandList(VkFence fence)
		{
			Debug.Assert(_hasCommandList, "Attempted to submit in-active CommandList for execution");

			var thisWork = WorkUnit.Completed;
			var oldWork = WorkUnit.Completed;
			try {
				// End the buffers
				for (var bi = 0; bi < _bufferCount; ++bi) vkEndCommandBuffer(_submit.Buffers[bi]).ThrowIfError();

				// Setup the signal objects
				oldWork = reserveSemaphore();
				var signalCount = _submit.BinarySignalSemaphore ? 2u : 1;
				var signalSemPtr = stackalloc VkSemaphore[2] { oldWork.Semaphore, _submit.BinarySignalSemaphore };
				var signalValPtr = stackalloc ulong[2] { oldWork.WaitValue + 1, 0 };

				// Build the submission
				fixed (SubmissionObjects* objPtr = &_submit) {
					VkTimelineSemaphoreSubmitInfo timeline = new() {
						WaitSemaphoreValueCount = _waitCount, PWaitSemaphoreValues = &objPtr->WaitValues[0],
						SignalSemaphoreValueCount = signalCount, PSignalSemaphoreValues = signalValPtr
					};
					VkSubmitInfo submit = new() {
						WaitSemaphoreCount = _waitCount, PWaitSemaphores = &objPtr->WaitSemaphores[0],
						PWaitDstStageMask = &objPtr->WaitStages[0],
						SignalSemaphoreCount = signalCount, PSignalSemaphores = signalSemPtr,
						CommandBufferCount = _bufferCount, PCommandBuffers = &objPtr->Buffers[0],
						PNext = &timeline
					};

					// Submit
					if (_isMainQueue) Instance.SubmitMainQueue(1, &submit, fence).ThrowIfError();
					else Instance.SubmitDmaQueue(1, &submit, fence).ThrowIfError();

					// Re-queue and return the new work unit
					_semaphores.Enqueue(thisWork = new(_isValid, oldWork.Semaphore, oldWork.WaitValue + 1));
					return thisWork;
				}
			}
			catch {
				if (oldWork.IsValid) _semaphores.Enqueue(oldWork); // Re-pool the unchanged reserved semaphore
				throw;
			}
			finally {
				// Put the buffers back into the pool and reset state
				for (var bi = 0; bi < _bufferCount; ++bi) _buffers.Enqueue((_submit.Buffers[bi], thisWork));
				Version += 1;
				_hasCommandList = false;
			}
		}
		
		// Cancels the current submission
		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		public void CancelCommandList(ulong version)
		{
			if (version != Version || !_hasCommandList) return;
			for (var bi = 0; bi < _bufferCount; ++bi) {
				_ = vkEndCommandBuffer(_submit.Buffers[bi]);
				_buffers.Enqueue((_submit.Buffers[bi], WorkUnit.Completed));
			}
			Version += 1;
			_hasCommandList = false;
		}
		
		
		// Adds a timeline wait to the command list
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void AddTimelineWait(WorkUnit unit, VkPipelineStageFlags stages)
		{
			Debug.Assert(_hasCommandList, "Attempted to modify invalid CommandList");
			Debug.Assert(_waitCount < MAX_WAITS, "Too many waits in CommandList");
			if (!unit.IsValid) return;
			_submit.WaitSemaphores[(int)_waitCount] = unit.Semaphore;
			_submit.WaitValues[(int)_waitCount] = unit.WaitValue;
			_submit.WaitStages[(int)_waitCount] = stages;
			_waitCount += 1;
		}
		
		// Adds a binary wait to the command list
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void AddBinaryWait(VkSemaphore semaphore, VkPipelineStageFlags stages)
		{
			Debug.Assert(_hasCommandList, "Attempted to modify invalid CommandList");
			Debug.Assert(_waitCount < MAX_WAITS, "Too many waits in CommandList");
			_submit.WaitSemaphores[(int)_waitCount] = semaphore;
			_submit.WaitStages[(int)_waitCount] = stages;
			_waitCount += 1;
		}
		
		// Adds a binary signal to the command list
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void AddBinarySignal(VkSemaphore semaphore)
		{
			Debug.Assert(_hasCommandList, "Attempted to modify invalid CommandList");
			Debug.Assert(!_submit.BinarySignalSemaphore, "CommandList already has binary signal semaphore");
			_submit.BinarySignalSemaphore = semaphore;
		}
		
		
		// Inline arrays for submission values (2 signal semaphores: work unit timeline + optional binary)
		[InlineArray((int)MAX_WAITS)] private struct WaitSemaphoreArray { private VkSemaphore _e0; }
		[InlineArray((int)MAX_WAITS)] private struct WaitValueArray { private ulong _e0; }
		[InlineArray((int)MAX_WAITS)] private struct WaitStagesArray { private VkPipelineStageFlags _e0; }
		[InlineArray((int)MAX_BUFFERS)] private struct BufferArray { private VkCommandBuffer _e0; }
		private struct SubmissionObjects
		{
			public WaitSemaphoreArray WaitSemaphores;
			public WaitValueArray WaitValues;
			public WaitStagesArray WaitStages;
			public BufferArray Buffers;
			public VkSemaphore BinarySignalSemaphore;
		}
	}
	
	
	// Supports state-safe construction of command buffer submissions
	//   NOTE: ALWAYS USE THIS TYPE WITH `using` TO ENSURE CORRECT STATE TRACKING
	public readonly ref struct CommandList
	{
		#region Fields
		private readonly CommandPool _pool;
		private readonly ulong _version;
		private readonly ReadOnlySpan<VkCommandBuffer> _buffers;

		// Buffers in the list, in submission order (DO NOT begin OR end THESE)
		public ReadOnlySpan<VkCommandBuffer> Buffers {
			get {
				Debug.Assert(_version == _pool.Version, "Out-of-date CommandList buffer access");
				return _buffers;
			}
		}
		#endregion // Fields

		public CommandList(object commandPool, ulong poolVersion, ReadOnlySpan<VkCommandBuffer> buffers)
		{
			Debug.Assert(commandPool is CommandPool, "Do not construct CommandList manually (bad pool type)");
			_pool = (CommandPool)commandPool;
			_version = poolVersion;
			_buffers = buffers;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void AddWait(WorkUnit work, VkPipelineStageFlags stages)
		{
			Debug.Assert(_version == _pool.Version, "Attempted to modify out-of-date CommandList");
			_pool.AddTimelineWait(work, stages);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void AddWait(VkSemaphore semaphore, VkPipelineStageFlags stages)
		{
			Debug.Assert(_version == _pool.Version, "Attempted to modify out-of-date CommandList");
			_pool.AddBinaryWait(semaphore, stages);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void AddSignal(VkSemaphore semaphore)
		{
			Debug.Assert(_version == _pool.Version, "Attempted to modify out-of-date CommandList");
			_pool.AddBinarySignal(semaphore);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public WorkUnit Submit(VkFence fence)
		{
			Debug.Assert(_version == _pool.Version, "Attempted to submit out-of-date CommandList");
			return _pool.SubmitCommandList(fence);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Dispose() => _pool.CancelCommandList(_version);
	}
	
	
	// Encapsulates the queryable state of a specific command buffer submission as a unit of work
	public readonly struct WorkUnit(Boxed<bool>? isValid, VkSemaphore semaphore, ulong waitValue)
	{
		// A completed unit of work
		public static WorkUnit Completed => default;

		// The parent queue, semaphore index, and wait value
		public readonly VkSemaphore Semaphore = semaphore;
		public readonly ulong WaitValue = waitValue;
		
		// If the unit represents a real work unit that is still valid
		public bool IsValid {
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => isValid is { Value: true } && Semaphore;
		}
		
		// If the submission has completed
		public bool IsComplete {
			[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
			[SkipLocalsInit]
			get {
				if (!IsValid) return true;
				ulong value;
				vkGetSemaphoreCounterValue(VulkanCtx.Device, Semaphore, &value).ThrowIfError();
				return value >= WaitValue;
			}
		}

		// Waits for the work unit to complete (returns true), or for the wait timeout (returns false)
		[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
		[SkipLocalsInit]
		public bool WaitComplete(ulong nsTimeout)
		{
			if (!IsValid) return true;
			var handle = Semaphore;
			var waitValue = WaitValue;
			VkSemaphoreWaitInfo wait = new() { SemaphoreCount = 1, PSemaphores = &handle, PValues = &waitValue };
			return vkWaitSemaphores(VulkanCtx.Device, &wait, nsTimeout) != VkResult.Timeout;
		}
	}
}
