﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Astrum.Native;
using Astrum.Render.Vulkan;
using JetBrains.Annotations;
using LLVK;
using static LLVK.Vulkan;
// ReSharper disable InconsistentNaming

namespace Astrum.Render;


// [Partial] RenderDriver components related to memory management and allocation tracking
//   Graphics memory is split into four types:
//     1. Device - Device Local, not Host Visible on discrete devices, usually dedicated VRAM
//          - Used for static or mostly static buffers and images
//     2. Host - Host Local and Cached, DRAM + cache
//          - Used for host-only data, and read-back buffers, can be mapped on the host
//     3. Upload - Host Local and not Cached, DRAM + no cache (bypassed)
//          - Used for staging host data for upload to Device memory
//          - Falls back to Host
//     4. Dynamic - Device Local and Host Visible
//          - Used for dynamic buffer memory (uniforms, streaming data, ect...)
//          - Falls back to Upload
//   Memory allocations are not pooled by the root allocator, but other pooling types in the public API are available.
//   Bulk uploads are handled using host-size staging buffers (in Upload memory), either pooled or temporary
//     The pooled stager has 24MB capacity, split into 8 SMALL (1MB) and 4 MID (4MB) partitions.
//       The pooled stager will stay open for 20 seconds after the last use.
//       All uploads under MID size will simply wait for a pooled partition to become available
//     There are also single temporary buffers for BIG (8MB) and HUGE (16MB), which both also stay open for 5 seconds
//       All uploads over MID size will try to create/use one of the big stagers, otherwise create a one-use temp one
internal sealed unsafe partial class RenderDriver
{
	// Shared staging buffer partition sizes
	public const uint SMALL_STAGE_SIZE = 1 * 1_024 * 1_024;  // 1MB
	public const uint MID_STAGE_SIZE = 4 * 1_024 * 1_024; // 4MB
	public const uint BIG_STAGE_SIZE = 8 * 1_024 * 1_024; // 8MB
	public const uint HUGE_STAGE_SIZE = 16 * 1_024 * 1_024; // 16MB
	
	// Pooled staging partition counts
	private const uint SMALL_STAGE_COUNT = 8;
	private const uint MID_STAGE_COUNT = 4;
	
	// Destroy times for pooled staging buffers
	private static readonly TimeSpan _NormalStageCleanup = TimeSpan.FromSeconds(20);
	private static readonly TimeSpan _BigStageCleanup = TimeSpan.FromSeconds(5);
	private static readonly TimeSpan _HugeStageCleanup = TimeSpan.FromSeconds(5);
	
	// Periodic staging buffer cleanup time
	private static readonly TimeSpan _StageCleanupTime = TimeSpan.FromSeconds(2);


	#region Data Staging
	// Initializes the pooled staging buffers
	private void initializePooledStagingBuffers()
	{
		MemoryMarshal.CreateSpan(ref _staging.SmallWork[0], (int)SMALL_STAGE_COUNT).Fill(WorkUnit.Completed);
		MemoryMarshal.CreateSpan(ref _staging.MidWork[0], (int)MID_STAGE_COUNT).Fill(WorkUnit.Completed);
		_staging.BigWork = WorkUnit.Completed;
		_staging.HugeWork = WorkUnit.Completed;
		
		_staging.BufferLock = new();
		_staging.SmallWorkSemaphore = new((int)SMALL_STAGE_COUNT);
		_staging.MidWorkSemaphore = new((int)MID_STAGE_COUNT);
	}
	
	// Cleans up the pooled staging buffers
	private void cleanupPooledStagingBuffers()
	{
		_staging.MidWorkSemaphore.Dispose();
		_staging.SmallWorkSemaphore.Dispose();
		_staging.BufferLock.Dispose();
		if (_staging.HugeBuffer) Instance.DestroyBuffer(_staging.HugeBuffer);
		if (_staging.BigBuffer) Instance.DestroyBuffer(_staging.BigBuffer);
		if (_staging.PoolBuffer) Instance.DestroyBuffer(_staging.PoolBuffer);
	}
	
	// Periodically cleans up pooled staging buffers that have not been used in a while
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private void checkAndReleaseOldStagingBuffers()
	{
		// Check for periodic cleanup
		var now = DateTime.Now;
		if (now - _staging.LastCheckTime < _StageCleanupTime) return;
			
		// Perform cleanup
		_staging.BufferLock.EnterWriteLock();
		try {
			if (now - _staging.LastPoolUseTime >= _NormalStageCleanup) {
				Instance.DestroyBuffer(_staging.PoolBuffer);
				_staging.PoolBuffer = default;
			}
			if (now - _staging.LastBigUseTime >= _BigStageCleanup) {
				Instance.DestroyBuffer(_staging.BigBuffer);
				_staging.BigBuffer = default;
			}
			if (now - _staging.LastHugeUseTime >= _HugeStageCleanup) {
				Instance.DestroyBuffer(_staging.HugeBuffer);
				_staging.HugeBuffer = default;
			}
		}
		finally { _staging.BufferLock.ExitWriteLock(); }
	}
	
	// Starts a memory upload operation - should be protected with a `using` block
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	[MustDisposeResource]
	public UploadOperation StartUploadOperation(uint size)
	{
		const uint MID_POOL_OFFSET = SMALL_STAGE_SIZE * SMALL_STAGE_COUNT;
		const uint POOL_SIZE = MID_POOL_OFFSET + MID_STAGE_SIZE * MID_STAGE_COUNT;
		
		// Try to reserve from the pooled staging buffers first
		// Small stagers
		if (size <= SMALL_STAGE_SIZE) {
			ensureBuffer(_staging.BufferLock, POOL_SIZE, ref _staging.PoolBuffer);
			_staging.SmallWorkSemaphore.Wait(_shutdownToken.Token);
			_staging.BufferLock.EnterWriteLock();
			try {
				for (var i = 0; i < SMALL_STAGE_COUNT; ++i) {
					if (_staging.SmallWork[i] is not { IsComplete: true }) continue;
					_staging.SmallWork[i] = null;
					_staging.LastPoolUseTime = DateTime.Now;
					var off = (uint)i * SMALL_STAGE_SIZE;
					return new(_staging.PoolBuffer, off, SMALL_STAGE_SIZE);
				}
			}
			finally { _staging.BufferLock.ExitWriteLock(); }
			Debug.Fail("Unexpected fallthrough of SMALL staging buffers");
		}
		
		// Mid stagers
		if (size <= MID_STAGE_SIZE) {
			ensureBuffer(_staging.BufferLock, POOL_SIZE, ref _staging.PoolBuffer);
			_staging.MidWorkSemaphore.Wait(_shutdownToken.Token);
			_staging.BufferLock.EnterWriteLock();
			try {
				for (var i = 0; i < MID_STAGE_COUNT; ++i) {
					if (_staging.MidWork[i] is not { IsComplete: true }) continue;
					_staging.MidWork[i] = null;
					_staging.LastPoolUseTime = DateTime.Now;
					var off = (uint)i * MID_STAGE_SIZE + MID_POOL_OFFSET;
					return new(_staging.PoolBuffer, off, MID_STAGE_SIZE);
				}
			}
			finally { _staging.BufferLock.ExitWriteLock(); }
			Debug.Fail("Unexpected fallthrough of MID staging buffers");
		}

		// Big and huge stagers
		switch (size) {
			case <= BIG_STAGE_SIZE: {
				ensureBuffer(_staging.BufferLock, BIG_STAGE_SIZE, ref _staging.BigBuffer);
				_staging.BufferLock.EnterWriteLock();
				try {
					if (_staging.BigWork is not { IsComplete: true }) break;
					_staging.BigWork = null;
					_staging.LastBigUseTime = DateTime.Now;
					return new(_staging.BigBuffer, 0, BIG_STAGE_SIZE);
				} 
				finally { _staging.BufferLock.ExitWriteLock(); }
			}
			case <= HUGE_STAGE_SIZE: {
				ensureBuffer(_staging.BufferLock, HUGE_STAGE_SIZE, ref _staging.HugeBuffer);
				_staging.BufferLock.EnterWriteLock();
				try {
					if (_staging.HugeWork is not { IsComplete: true }) break;
					_staging.HugeWork = null;
					_staging.LastHugeUseTime = DateTime.Now;
					return new(_staging.HugeBuffer, 0, HUGE_STAGE_SIZE);
				} 
				finally { _staging.BufferLock.ExitWriteLock(); }
			}
		}

		// Otherwise, create a temporary staging buffer
		createStagingBuffer(size, out var tempBuffer);
		return new(tempBuffer, 0, size);

		// Ensures the buffer is created
		static void ensureBuffer(ReaderWriterLockSlim @lock, uint size, ref BufferRef buffer)
		{
			@lock.EnterWriteLock();
			try { if (!buffer) createStagingBuffer(size, out buffer); }
			finally { @lock.ExitWriteLock(); }
		}
		
		// Creates a staging buffer of the given size
		static void createStagingBuffer(uint size, out BufferRef buffer)
		{
			BufferCreate create = new() {
				Size = size, Usage = VkBufferUsageFlags.TransferSrc, 
				Shared = true, Mapped = true, DeviceAddress = false
			};
			buffer = Instance.createBuffer(in create, VulkanCtx.UploadMemory);
		}
	}

	// Handles returning the upload operation region to the pool or destroying temp stagers
	private void completeUploadOperation(in UploadOperation op, out bool waited)
	{
		const uint MID_POOL_OFFSET = SMALL_STAGE_SIZE * SMALL_STAGE_COUNT;
		
		// Check op validity
		if (!op.Buffer || !op.Work.HasValue) {
			waited = true;
			return;
		}
		
		// Try return normal
		if (op.Buffer == _staging.PoolBuffer) {
			lock (this) {
				if (op.Offset < MID_POOL_OFFSET) {
					var idx = op.Offset / SMALL_STAGE_SIZE;
					Debug.Assert(op.Offset % SMALL_STAGE_SIZE == 0);
					Debug.Assert(!_staging.SmallWork[(int)idx].HasValue);
					_staging.SmallWork[(int)idx] = op.Work;
					_staging.SmallWorkSemaphore.Release();
					_staging.LastPoolUseTime = DateTime.Now;
				}
				else {
					var idx = (op.Offset - MID_POOL_OFFSET) / MID_STAGE_SIZE;
					Debug.Assert((op.Offset - MID_POOL_OFFSET) % MID_STAGE_SIZE == 0);
					Debug.Assert(!_staging.MidWork[(int)idx].HasValue);
					_staging.MidWork[(int)idx] = op.Work;
					_staging.MidWorkSemaphore.Release();
					_staging.LastPoolUseTime = DateTime.Now;
				}
			}
			waited = false;
			return;
		}
			
		// Try return big
		if (op.Buffer == _staging.BigBuffer) {
			lock (this) {
				Debug.Assert(!_staging.BigWork.HasValue);
				_staging.BigWork = op.Work;
				_staging.LastBigUseTime = DateTime.Now;
			}
			waited = false;
			return;
		}
			
		// Try return huge
		if (op.Buffer == _staging.HugeBuffer) {
			lock (this) {
				Debug.Assert(!_staging.HugeWork.HasValue);
				_staging.BigWork = op.Work;
				_staging.LastHugeUseTime = DateTime.Now;
			}
			waited = false;
			return;
		}
		
		// Must be temporary - wait and destroy
		op.Work.Value.WaitComplete(UInt64.MaxValue);
		DestroyBuffer(op.Buffer);
		waited = true;
	}
	#endregion // Data Staging
	
	
	#region SetData
	// Copies data from the host buffer to the destination buffer described in the copy
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private WorkUnit writeBufferDataFromHost(BufferRef hostBuf, uint hostOff, in BufferCopy copy)
	{
		// Validate
		var hDesc = hostBuf.Descriptor;
		var dDesc = copy.Buffer.Descriptor;
		Debug.Assert(hDesc != null && dDesc != null, "Invalid buffer references for data initialization");
		Debug.Assert(hDesc->Memory.Flags.HasFlag(VkMemoryPropertyFlags.HostVisible), "Src buffer is not on host");
		Debug.Assert(hDesc->Usage.HasFlag(VkBufferUsageFlags.TransferSrc), "Host buffer has invalid usage");
		Debug.Assert(dDesc->Usage.HasFlag(VkBufferUsageFlags.TransferDst), "Dest. buffer has invalid usage");
		
		// Get the real size
		var copySize = Math.Min(copy.Size, dDesc->Size - copy.Offset);
		Debug.Assert((hDesc->Size - hostOff) >= copySize, "Host buffer not large enough for transfer");
		
		// Get the barriers
		var useDma = hDesc->Shared && copy.Initial && HasDmaQueue;
		dDesc->Usage.GetWriteDstBarrier(out var dstStages, out var dstAccess);

		// Record the copy
		WorkUnit copyWork;
		VkBufferMemoryBarrier barrier;
		using (var copyList = useDma ? StartDmaCommandList(1) : StartMainCommandList(1)) {
			// Source barrier if not initializing
			if (!copy.Initial) {
				dDesc->Usage.GetWriteSrcBarrier(out var srcStages, out var srcAccess);
				barrier = new() {
					SrcAccessMask = srcAccess, DstAccessMask = VkAccessFlags.TransferWrite,
					SrcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED, DstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
					Buffer = dDesc->Handle, Offset = copy.Offset, Size = copySize
				};
				vkCmdPipelineBarrier(copyList.Buffers[0], 
					srcStages, VkPipelineStageFlags.Transfer,
					default,
					0, null,
					1, &barrier,
					0, null);
			}
			
			// Buffer copy
			VkBufferCopy vkCopy = new() { SrcOffset = hostOff, DstOffset = copy.Offset, Size = copySize };
			vkCmdCopyBuffer(copyList.Buffers[0], hDesc->Handle, dDesc->Handle, 1, &vkCopy);
			
			// Dst barrier - if DMA, start ownership transfer, else just normal barrier
			barrier.SrcAccessMask = VkAccessFlags.TransferWrite;
			barrier.DstAccessMask = useDma ? default : dstAccess;
			barrier.SrcQueueFamilyIndex = useDma ? VulkanCtx.DmaQueue!.Value.Family : VK_QUEUE_FAMILY_IGNORED;
			barrier.DstQueueFamilyIndex = useDma ? VulkanCtx.MainQueue.Family : VK_QUEUE_FAMILY_IGNORED;
			vkCmdPipelineBarrier(copyList.Buffers[0],
				VkPipelineStageFlags.Transfer, useDma ? VkPipelineStageFlags.Transfer : dstStages,
				default,
				0, null,
				1, &barrier,
				0, null);
			
			// Submit
			copyWork = copyList.Submit(default);
		}
		
		// Done if not using DMA
		if (!useDma) return copyWork;
		
		// Otherwise, complete the ownership transfer for the buffer
		using var ownershipList = StartMainCommandList(1);
		barrier.SrcAccessMask = default;
		barrier.DstAccessMask = dstAccess;
		vkCmdPipelineBarrier(ownershipList.Buffers[0],
			VkPipelineStageFlags.Transfer, dstStages,
			default,
			0, null,
			1, &barrier,
			0, null);
		ownershipList.AddWait(copyWork, VkPipelineStageFlags.Transfer);
		return ownershipList.Submit(default);
	}
	
	// Copies data from the host buffer to the destination image described in the copy
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private WorkUnit writeImageDataFromHost(BufferRef hostBuf, uint hostOff, in ImageCopy copy)
	{
		// Validate
		var hDesc = hostBuf.Descriptor;
		var dDesc = copy.Image.Descriptor;
		Debug.Assert(hDesc != null && dDesc != null, "Invalid references for data initialization");
		Debug.Assert(hostOff < hDesc->Size, "Invalid host buffer offset");
		Debug.Assert(hDesc->Memory.Flags.HasFlag(VkMemoryPropertyFlags.HostVisible), "Src buffer is not on host");
		Debug.Assert(hDesc->Usage.HasFlag(VkBufferUsageFlags.TransferSrc), "Host buffer has invalid usage");
		Debug.Assert(dDesc->Usage.HasFlag(VkImageUsageFlags.TransferDst), "Dest. image has invalid usage");
		Debug.Assert(copy.Offset.X + copy.Extent.Width <= dDesc->Width, "Image copy has bad target region width");
		Debug.Assert(copy.Offset.Y + copy.Extent.Height <= dDesc->Height, "Image copy has bad target region height");
		Debug.Assert(copy.Offset.Z + copy.Extent.Depth <= dDesc->Depth, "Image copy has bad target region depth");
		Debug.Assert(copy.LayerStart + copy.LayerCount <= dDesc->ArrayLayers, "Image copy has bad target layers");
		Debug.Assert(copy.MipLevel < dDesc->MipLevels, "Image copy has bad target mip level");
		Debug.Assert(
			!dDesc->Usage.HasFlag(VkImageUsageFlags.ColorAttachment) &&
			!dDesc->Usage.HasFlag(VkImageUsageFlags.DepthStencilAttachment), "Image copy cannot use attachment images");
		// Unfortunately, checking that the host has enough data is hard and error prone at this low level
		// Do not check that initialization covers the whole image, this is not strictly required
		
		// Get the barriers
		var useDma = hDesc->Shared && copy.Initial && HasDmaQueue;
		var aspect = dDesc->Format.GetAspectFlags();
		dDesc->Usage.GetWriteSrcBarrier(out var srcStages, out var srcAccess);
		dDesc->Usage.GetWriteDstBarrier(out var dstStages, out var dstAccess);
		
		// Record the copy
		WorkUnit copyWork;
		VkImageMemoryBarrier barrier;
		using (var copyList = useDma ? StartDmaCommandList(1) : StartMainCommandList(1)) {
			// Source barrier
			barrier = new() {
				SrcAccessMask = copy.Initial ? default : srcAccess, 
				DstAccessMask = VkAccessFlags.TransferWrite,
				OldLayout = copy.Initial ? VkImageLayout.Undefined : copy.InitialLayout, 
				NewLayout = VkImageLayout.TransferDstOptimal,
				SrcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED, DstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
				Image = dDesc->Handle, 
				SubresourceRange = new(aspect, copy.MipLevel, 1, copy.LayerStart, copy.LayerCount)
			};
			vkCmdPipelineBarrier(copyList.Buffers[0],
				copy.Initial ? VkPipelineStageFlags.TopOfPipe : srcStages, VkPipelineStageFlags.Transfer,
				default,
				0, null,
				0, null,
				1, &barrier);
			
			// Image copy
			VkBufferImageCopy vkCopy = new() {
				BufferOffset = hostOff,
				BufferImageHeight = 0, BufferRowLength = 0,
				ImageSubresource = new(aspect, copy.MipLevel, copy.LayerStart, copy.LayerCount),
				ImageOffset = copy.Offset, ImageExtent = copy.Extent
			};
			vkCmdCopyBufferToImage(copyList.Buffers[0], hDesc->Handle, dDesc->Handle, VkImageLayout.TransferDstOptimal,
				1, &vkCopy);
			
			// Dst barrier - if DMA, start ownership transfer, else just normal barrier
			barrier.SrcAccessMask = VkAccessFlags.TransferWrite;
			barrier.DstAccessMask = useDma ? default : dstAccess;
			barrier.OldLayout = VkImageLayout.TransferDstOptimal;
			barrier.NewLayout = copy.FinalLayout;
			barrier.SrcQueueFamilyIndex = useDma ? VulkanCtx.DmaQueue!.Value.Family : VK_QUEUE_FAMILY_IGNORED;
			barrier.DstQueueFamilyIndex = useDma ? VulkanCtx.MainQueue.Family : VK_QUEUE_FAMILY_IGNORED;
			vkCmdPipelineBarrier(copyList.Buffers[0],
				VkPipelineStageFlags.Transfer, useDma ? VkPipelineStageFlags.Transfer : dstStages,
				default,
				0, null,
				0, null,
				1, &barrier);
			
			// Submit
			copyWork = copyList.Submit(default);
		}
		
		// Done if not using DMA
		if (!useDma) return copyWork;
		
		// Otherwise, complete the ownership transfer
		using var ownershipList = StartMainCommandList(1);
		barrier.SrcAccessMask = default;
		barrier.DstAccessMask = dstAccess;
		vkCmdPipelineBarrier(ownershipList.Buffers[0],
			VkPipelineStageFlags.Transfer, dstStages,
			default,
			0, null,
			0, null,
			1, &barrier);
		ownershipList.AddWait(copyWork, VkPipelineStageFlags.Transfer);
		return ownershipList.Submit(default);
	}
	#endregion // SetData
	
	
	// Describes a buffer copy operation
	public struct BufferCopy
	{
		public required BufferRef Buffer; // The target of the copy
		public required uint Offset;      // The offset into the target to copy from
		public required uint Size;        // The size of the region to copy
		public required bool Initial;     // If the copy is initializing the buffer (allows optimizations)
	}
	
	// Describes an image copy operation
	public struct ImageCopy
	{
		public required ImageRef Image;              // The target of the copy
		public required VkOffset3D Offset;           // The offset of the target region
		public required VkExtent3D Extent;           // The extent of the target region
		public required uint LayerStart;             // The target starting array layer
		public required uint LayerCount;             // The target array layer count
		public required uint MipLevel;               // The target mip level
		public required VkImageLayout InitialLayout; // The target starting layout (UNDEFINED == allows optimizations)
		public required VkImageLayout FinalLayout;   // The target final layout
		public required bool Initial;                // If the copy is initializing the image (allows optimizations)
	}
	
	
	// The pooled staging buffer objects
	private struct StagingBufferObjects()
	{
		#region Fields
		// Lock for create/destroy buffers
		public ReaderWriterLockSlim BufferLock = new();
		
		// Last pool cleanup time
		public DateTime LastCheckTime = DateTime.Now;
		
		// Normal stager
		public BufferRef PoolBuffer;
		public SmallWorkUnitArray SmallWork;
		public MidWorkUnitArray MidWork;
		public DateTime LastPoolUseTime;
		public SemaphoreSlim SmallWorkSemaphore = new((int)SMALL_STAGE_COUNT);
		public SemaphoreSlim MidWorkSemaphore = new((int)MID_STAGE_COUNT);

		// Big stager
		public BufferRef BigBuffer;
		public WorkUnit? BigWork;
		public DateTime LastBigUseTime;
		
		// Huge stager
		public BufferRef HugeBuffer;
		public WorkUnit? HugeWork;
		public DateTime LastHugeUseTime;
		#endregion // Fields
		
		// Work unit arrays
		[InlineArray((int)SMALL_STAGE_COUNT)] public struct SmallWorkUnitArray { private WorkUnit? _e0; }
		[InlineArray((int)MID_STAGE_COUNT)] public struct MidWorkUnitArray { private WorkUnit? _e0; }
	}
	
	// Upload operation wrapping a staging buffer region - can perform a single upload task per operation
	public ref struct UploadOperation(BufferRef buffer, uint offset, uint size)
	{
		public BufferRef Buffer { get; } = buffer;
		public uint Offset { get; } = offset;
		public uint Size { get; } = size;
		public WorkUnit? Work { get; private set; } = null;
		
		public void* DataPtr => (byte*)Buffer.Descriptor->MemoryMap + Offset;

		public void Dispose()
		{
			if (Work.HasValue) return;
			Work = WorkUnit.Completed;
			Instance.completeUploadOperation(in this, out _);
		}

		public WorkUnit SetBufferData(in BufferCopy copy)
		{
			Debug.Assert(!Work.HasValue, "Cannot re-use upload operation");
			Work = Instance.writeBufferDataFromHost(Buffer, Offset, in copy);
			Instance.completeUploadOperation(in this, out var waited);
			return waited ? (Work = WorkUnit.Completed).Value : Work.Value;
		}

		public WorkUnit SetImageData(in ImageCopy copy)
		{
			Debug.Assert(!Work.HasValue, "Cannot re-use upload operation");
			Work = Instance.writeImageDataFromHost(Buffer, Offset, in copy);
			Instance.completeUploadOperation(in this, out var waited);
			return waited ? (Work = WorkUnit.Completed).Value : Work.Value;
		}
	}
}
