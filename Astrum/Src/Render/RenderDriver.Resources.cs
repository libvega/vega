﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Astrum.Native;
using Astrum.Render.Vulkan;
using LLVK;
using static Astrum.EngineLogger;
using static LLVK.Vulkan;

namespace Astrum.Render;


// [Partial] RenderDriver components related to managing and tracking buffer and image resources
internal sealed unsafe partial class RenderDriver
{
	#region Buffer
	// Create buffer
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	[SkipLocalsInit]
	private BufferRef createBuffer(in BufferCreate create, VulkanCtx.MemoryType memoryType)
	{
		// Validate
		Debug.Assert(create.Size <= Int32.MaxValue, "Requested buffer size is too large (2GB max)");
		Debug.Assert(create.Usage != default, "Requested buffer usage flags are empty");
		Debug.Assert(!create.Mapped || memoryType.Type.PropertyFlags.HasFlag(VkMemoryPropertyFlags.HostVisible),
			"Attempted to create a non-host-visible mapped buffer");
		
		// Allocate a buffer table slot
		var bufferHandle = _bufferTable.Allocate();
		if (bufferHandle.IsNull) throw RenderCore.ResourceLimitException.Buffers();
		
		// Describe buffer
		VkBufferCreateInfo createInfo = new() {
			Size = create.Size, Usage = create.Usage, SharingMode = VkSharingMode.Exclusive
		};
		var famPtr = stackalloc uint[2] { 0, 0 }; // TODO: account for async compute
		if (create.Shared && HasDmaQueue) {
			createInfo.SharingMode = VkSharingMode.Concurrent;
			createInfo.QueueFamilyIndexCount = 2;
			createInfo.PQueueFamilyIndices = famPtr;
			famPtr[0] = VulkanCtx.MainQueue.Family;
			famPtr[1] = VulkanCtx.DmaQueue!.Value.Family;
		}
		
		// Create
		VkBuffer buffer = default;
		Vma.VmaAllocation allocation = default;
		nuint deviceAddr = 0;
		void* pMapping;
		try {
			// Create buffer
			var dedicated = memoryType.Index != VulkanCtx.DeviceMemory.Index ||
				create.Usage == VkBufferUsageFlags.StorageBuffer;
			Vma.CreateBuffer(&createInfo, memoryType.Index, dedicated, create.Mapped, &buffer, &allocation, &pMapping)
				.ThrowIfError();
			
			// Get address if needed
			if (create.DeviceAddress) {
				VkBufferDeviceAddressInfo addrInfo = new() { Buffer = buffer };
				deviceAddr = (nuint)vkGetBufferDeviceAddress(VulkanCtx.Device, &addrInfo);
				Debug.Assert(deviceAddr != 0, "Failed to get buffer device address");
			}
		}
		catch {
			if (buffer) Vma.DestroyBuffer(buffer, allocation);
			_bufferTable.Free(bufferHandle);
			throw;
		}
		
		// Setup table entry and return ref
		*bufferHandle.Data = new() {
			Handle = buffer,
			Memory = new(allocation, memoryType.Type.PropertyFlags),
			Usage = create.Usage,
			Size = create.Size,
			DeviceAddress = deviceAddr,
			MemoryMap = pMapping,
			Shared = create.Shared && HasDmaQueue
		};
		BufferRef bufferRef = new(bufferHandle);
		EngineDebug($"Created {bufferRef}");
		return bufferRef;
	}
	
	// Destroy buffer (immediately)
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void DestroyBuffer(BufferRef buffer)
	{
		if (!buffer) return;
		try {
			Vma.DestroyBuffer(buffer.Descriptor->Handle, buffer.Descriptor->Memory.Handle);
			EngineDebug($"Destroyed {buffer}");
		}
		finally { _bufferTable.Free(Unsafe.As<BufferRef, SparseTable<BufferDescriptor>.Handle>(ref buffer)); }
	}
	
	// Buffer create info
	public struct BufferCreate
	{
		public required uint Size;
		public required VkBufferUsageFlags Usage;
		public required bool Shared;
		public required bool Mapped;
		public required bool DeviceAddress;
	}
	#endregion // Buffer


	#region Image
	// Create image
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	[SkipLocalsInit]
	public ImageRef CreateImage(in ImageCreate create)
	{
		// Validate
		Debug.Assert(TryValidateImageDims(create.ViewType, create.Width, create.Height, create.Depth, create.Layers, 
			create.MipLevels, out var dimError), dimError);
		Debug.Assert(create.Usage != default, "Requested image usage flags are empty");
		Debug.Assert(!create.Msaa || create.ViewType == VkImageViewType.E2D, "Non-2D MSAA image requested");
		
		// Allocate an image table slot
		var imageHandle = _imageTable.Allocate();
		if (imageHandle.IsNull) throw RenderCore.ResourceLimitException.Textures();

		// Describe the image
		VkImageCreateInfo createInfo = new() {
			Usage = create.Usage,
			Format = create.Format,
			ImageType = create.ViewType switch {
				VkImageViewType.E1D      => VkImageType.E1D,
				VkImageViewType.E2D      => VkImageType.E2D,
				VkImageViewType.E3D      => VkImageType.E3D,
				VkImageViewType.E2DArray => VkImageType.E2D,
				VkImageViewType.Cube     => VkImageType.E2D,
				_                        => throw new("Bad image view type") // Should be caught before here
			},
			Extent = new(create.Width, create.Height, create.Depth),
			ArrayLayers = create.Layers,
			MipLevels = create.MipLevels,
			Samples = create.Msaa ? VkSampleCountFlags.E4 : VkSampleCountFlags.E1,
			InitialLayout = VkImageLayout.Undefined,
			SharingMode = VkSharingMode.Exclusive,
			Tiling = VkImageTiling.Optimal,
			Flags = create.ViewType == VkImageViewType.Cube ? VkImageCreateFlags.CubeCompatible : default
		};
		
		// Create
		VkImage image = default;
		Vma.VmaAllocation allocation = default;
		VkImageView view = default;
		try {
			// Create image
			var dedicated =
				create.Usage.HasFlag(VkImageUsageFlags.ColorAttachment) ||
				create.Usage.HasFlag(VkImageUsageFlags.DepthStencilAttachment);
			Vma.CreateImage(&createInfo, VulkanCtx.DeviceMemory.Index, dedicated, &image, &allocation).ThrowIfError();
			
			// Create view
			VkImageViewCreateInfo viewInfo = new() {
				Image = image,
				ViewType = create.ViewType,
				Format = create.Format,
				Components = create.Components,
				SubresourceRange = new(create.Format.GetAspectFlags(), 0, create.MipLevels, 0, create.Layers)
			};
			vkCreateImageView(VulkanCtx.Device, &viewInfo, null, &view).ThrowIfError();
		}
		catch {
			if (view) vkDestroyImageView(VulkanCtx.Device, view, null);
			if (image) Vma.DestroyImage(image, allocation);
			_imageTable.Free(imageHandle);
			throw;
		}
		
		// Setup table entry and return ref
		*imageHandle.Data = new() {
			Handle = image,
			View = view,
			Memory = new(allocation, VulkanCtx.DeviceMemory.Type.PropertyFlags),
			Usage = create.Usage,
			Type = createInfo.ImageType,
			ViewType = create.ViewType,
			Format = create.Format,
			Width = (ushort)create.Width,
			Height = (ushort)create.Height,
			Depth = (ushort)create.Depth,
			ArrayLayers = (ushort)create.Layers,
			MipLevels = (ushort)create.MipLevels,
			WriteBindingSlot = 0,
			ReadonlyBindingSlots = default
		};
		ImageRef imageRef = new(imageHandle);
		EngineDebug($"Created {imageRef}");
		return imageRef;
	}
	
	// Destroy image (immediately), also releases any bindings the image may have
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void DestroyImage(ImageRef image)
	{
		if (!image) return;
		try {
			FreeWriteableTextureBinding(image);
			FreeAllTextureBindings(image);
			vkDestroyImageView(VulkanCtx.Device, image.Descriptor->View, null);
			Vma.DestroyImage(image.Descriptor->Handle, image.Descriptor->Memory.Handle);
			EngineDebug($"Destroyed {image}");
		}
		finally { _imageTable.Free(Unsafe.As<ImageRef, SparseTable<ImageDescriptor>.Handle>(ref image)); }
	}
	
	// Validates the given image dimensions against the current limits
	public bool TryValidateImageDims(VkImageViewType type, uint width, uint height, uint depth, uint layers, uint mips, 
		[NotNullWhen(false)] out string? err)
	{
		// Zero checks
		if (width == 0) return setErr(out err, "Cannot have a width of zero");
		if (height == 0) return setErr(out err, "Cannot have a height of zero");
		if (depth == 0) return setErr(out err, "Cannot have a depth of zero");
		if (layers == 0) return setErr(out err, "Cannot have a layer count of zero");
		if (mips == 0) return setErr(out err, "Cannot have a mip levels of zero");
		
		// Type checks
		if (type == VkImageViewType.E1D) {
			if (width >= Device.Texture1DMaxSize) return setErr(out err, "Texture 1D width too large");
			if (layers != 1 || height != 1 || depth != 1) return setErr(out err, "Invalid other 1D dimensions");
		}
		else if (type == VkImageViewType.E2D) {
			if (width >= Device.Texture2DMaxSize) return setErr(out err, "Texture 2D width too large");
			if (height >= Device.Texture2DMaxSize) return setErr(out err, "Texture 2D height too large");
			if (layers != 1 || depth != 1) return setErr(out err, "Invalid other 2D dimensions");
		}
		else if (type == VkImageViewType.E3D) {
			if (width >= Device.Texture3DMaxSize) return setErr(out err, "Texture 3D width too large");
			if (height >= Device.Texture3DMaxSize) return setErr(out err, "Texture 3D height too large");
			if (depth >= Device.Texture3DMaxSize) return setErr(out err, "Texture 3D depth too large");
			if (layers != 1) return setErr(out err, "Invalid other 3D dimensions");
		}
		else if (type == VkImageViewType.E2DArray) {
			if (width >= Device.Texture2DMaxSize) return setErr(out err, "Texture 2DArray width too large");
			if (height >= Device.Texture2DMaxSize) return setErr(out err, "Texture 2DArray height too large");
			if (layers >= Device.TextureMaxLayers) return setErr(out err, "Texture 2DArray layers too large");
			if (depth != 1) return setErr(out err, "Invalid other 2DArray dimensions");
		}
		else if (type == VkImageViewType.Cube) {
			if (width >= Device.TextureCubeMaxSize) return setErr(out err, "Texture cube width too large");
			if (width != height) return setErr(out err, "Texture cube width does not equal height");
			if (layers != 6) return setErr(out err, "Texture cube layers not 6");
			if (depth != 1) return setErr(out err, "Invalid other cube dimensions");
		}
		else Debug.Fail($"Invalid VkImageView type given to texture dims validation: {type}");

		// Good
		err = null;
		return true;
		
		static bool setErr(out string err, string msg) { err = msg; return false; }
	}
	
	// Image create info
	public struct ImageCreate
	{
		public required uint Width;
		public required uint Height;
		public required uint Depth;
		public required uint Layers;
		public required uint MipLevels;
		public required VkImageUsageFlags Usage;
		public required VkFormat Format;
		public required VkImageViewType ViewType;
		public required VkComponentMapping Components;
		public required bool Msaa;
	}
	#endregion // Image
	
	
	// Handle to a buffer resource
	public readonly struct BufferRef(SparseTable<BufferDescriptor>.Handle handle) : IEquatable<BufferRef>
	{
		private readonly SparseTable<BufferDescriptor>.Handle _handle = handle; // Must stay as only field
		
		// If the handle is still valid
		public bool IsValid => _handle.IsValid;
		// Gets the buffer descriptor, or null if the handle is no longer valid
		public BufferDescriptor* Descriptor => _handle.Data;

		public override string ToString()
		{
			if (_handle.IsNull) return "Buffer[NULL]";
			return _handle.Data == null ? "Buffer[INV]" : $"Buffer[0x{_handle.Data->Handle.Handle:X16}]";
		}

		public bool Equals(BufferRef other) => _handle.Equals(other._handle);
		public override bool Equals(object? obj) => obj is BufferRef other && _handle.Equals(other._handle);
		public override int GetHashCode() => _handle.GetHashCode();
		
		public static implicit operator bool (BufferRef buffer) => buffer.IsValid;

		public static bool operator == (BufferRef l, BufferRef r) => l._handle == r._handle;
		public static bool operator != (BufferRef l, BufferRef r) => l._handle != r._handle;
	}
	
	// Handle to an image resource
	public readonly struct ImageRef(SparseTable<ImageDescriptor>.Handle handle) : IEquatable<ImageRef>
	{
		private readonly SparseTable<ImageDescriptor>.Handle _handle = handle; // Must stay as only field
		
		// If the handle is still valid
		public bool IsValid => _handle.IsValid;
		// Gets the buffer descriptor, or null if the handle is no longer valid
		public ImageDescriptor* Descriptor => _handle.Data;
		
		public override string ToString()
		{
			if (_handle.IsNull) return "Image[NULL]";
			return _handle.Data == null ? "Image[INV]" : $"Image[0x{_handle.Data->Handle.Handle:X16}]";
		}
		
		public bool Equals(ImageRef other) => _handle.Equals(other._handle);
		public override bool Equals(object? obj) => obj is ImageRef other && _handle.Equals(other._handle);
		public override int GetHashCode() => _handle.GetHashCode();
		
		public static implicit operator bool (ImageRef image) => image.IsValid;
		
		public static bool operator == (ImageRef l, ImageRef r) => l._handle == r._handle;
		public static bool operator != (ImageRef l, ImageRef r) => l._handle != r._handle;
	}
	
	
	// Versioned resource table descriptor for a buffer that lives in a SparseTable
	public struct BufferDescriptor
	{
		public required VkBuffer Handle;
		public required MemoryAllocation Memory;
		public required VkBufferUsageFlags Usage;
		public required uint Size;
		public required nuint DeviceAddress;
		public required void* MemoryMap;
		public required bool Shared; // False if no DMA queue
	}
	
	// Versioned resource table descriptor for a texture that lives in a SparseTable
	public struct ImageDescriptor
	{
		public required VkImage Handle;
		public required VkImageView View;
		public required MemoryAllocation Memory;
		public required VkImageUsageFlags Usage;
		public required VkImageType Type;
		public required VkImageViewType ViewType;
		public required VkFormat Format;
		public required ushort Width;
		public required ushort Height;
		public required ushort Depth;
		public required ushort ArrayLayers;
		public required ushort MipLevels;
		public required ushort WriteBindingSlot; // -1 for real slot to differentiate slot 0
		public required BindingSlotArray ReadonlyBindingSlots;

		public readonly ushort RealWriteBindingSlot => (ushort)(WriteBindingSlot > 0 ? WriteBindingSlot - 1 : 0);
		
		[InlineArray((int)MAX_SAMPLERS)] public struct BindingSlotArray { private ushort _e0; }
	}
	
	// Memory allocation result
	public readonly struct MemoryAllocation(Vma.VmaAllocation handle, VkMemoryPropertyFlags flags)
	{
		public readonly Vma.VmaAllocation Handle = handle;
		public readonly VkMemoryPropertyFlags Flags = flags;
	}
	
	
	// Manages a sparse two-level LUT of native memory blocks of a specific unmanaged type
	//   The LUT is limited to 16,384 entries - 64 blocks in the table, and 256 entries in each block
	//   The LUT manages versioning of the entries, so validity of handles against the entries can be maintained
	//   The blocks live in unmanaged memory, and are not freed if all entries are released
	public sealed class SparseTable<T> : IDisposable
		where T : unmanaged
	{
		private const uint TABLE_SIZE = 64;  // Number of blocks in the table
		private const uint BLOCK_SIZE = 256; // Number of entries in a block
		public const uint MAX_ENTRIES = TABLE_SIZE * BLOCK_SIZE;
		
		
		#region Fields
		// The block table
		private BlockArray _table;
		// The mask of block full states to quickly find allocation points
		private Bitmask64 _blockFullMask = default;
		
		// The number of allocated entries in the table
		public uint AllocationCount { get; private set; }
		
		// The version tag counter
		private uint _tagCounter;
		
		// The read/write lock for the table
		private readonly ReaderWriterLockSlim _lock = new();
		
		// Disposed flag
		private bool _isDisposed;
		#endregion // Fields


		~SparseTable() => cleanup(false);
		
		// Disposes of the table and all associated memory (all Entry pointers become invalid at this point)
		public void Dispose()
		{
			cleanup(true);
			GC.SuppressFinalize(this);
		}
		
		// Dispose implementation
		private void cleanup(bool disposing)
		{
			if (_isDisposed) return;
			_lock.EnterWriteLock();
			try {
				for (var ti = 0; ti < TABLE_SIZE; ++ti) {
					if (_table[ti] != 0) NativeMemory.Free((void*)_table[ti]);
				}
			}
			finally { _lock.ExitWriteLock(); }
			if (disposing) _lock.Dispose();
			AllocationCount = 0;
			_isDisposed = true;
		}
		
		
		// Allocate a new table entry, returns null if no space left in table
		//   The returned entry has the tag and index fields setup properly, and the data field untouched
		[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
		public Handle Allocate()
		{
			Debug.Assert(!_isDisposed);
			
			_lock.EnterWriteLock();
			try {
				// Get the block (allocate a new one if needed)
				if (_blockFullMask.FirstClear is not { } tableIndex) return default;
				var blockPtr = (Block*)((_table[(int)tableIndex] == 0) 
					? _table[(int)tableIndex] = (nuint)NativeMemory.AllocZeroed((nuint)sizeof(Block)) 
					: _table[(int)tableIndex]);
				
				// Get the block index of the new entry
				uint blockIndex = 0;
				if (blockPtr->Masks[0].FirstClear is { } clear0) blockIndex = clear0;
				else if (blockPtr->Masks[1].FirstClear is { } clear1) blockIndex = clear1 + 64;
				else if (blockPtr->Masks[2].FirstClear is { } clear2) blockIndex = clear2 + 128;
				else if (blockPtr->Masks[3].FirstClear is { } clear3) blockIndex = clear3 + 192;
				else Trace.Fail("Bad resource map state - block is full when it was advertised as not");
				
				// Setup the new entry (pointer trickery to bypass readonly fields)
				var entryPtr = &blockPtr->Data[(int)blockIndex];
				*(uint*)&entryPtr->Tag = ++_tagCounter;
				*(byte*)&entryPtr->TableIndex = (byte)tableIndex;
				*(byte*)&entryPtr->BlockIndex = (byte)blockIndex;

				// Update block and table state
				var maskNumber = blockIndex / 64;
				var maskOffset = blockIndex - (maskNumber * 64);
				blockPtr->Masks[(int)maskNumber][maskOffset] = true;
				blockPtr->AllocCount += 1;
				_blockFullMask[tableIndex] = blockPtr->AllocCount == BLOCK_SIZE;
				AllocationCount += 1;

				return CreateHandle(entryPtr);
			}
			finally { _lock.ExitWriteLock(); }
		}
		
		// Frees the given table entry, only if the indices are valid and the tag matches, returns if freed
		[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
		public bool Free(Handle handle)
		{
			Debug.Assert(!_isDisposed);
			var entryPtr = *(Entry**)&handle;
			if (!handle || entryPtr->TableIndex >= TABLE_SIZE) return false;
			
			_lock.EnterWriteLock();
			try {
				// Lookup the entry and check the version
				var blockPtr = (Block*)_table[entryPtr->TableIndex];
				if (blockPtr == null) return false;
				var maskNumber = entryPtr->BlockIndex / 64;
				var maskOffset = entryPtr->BlockIndex - (maskNumber * 64);
				if (!blockPtr->Masks[maskNumber][(uint)maskOffset]) return false;
				var lookupEntry = &blockPtr->Data[entryPtr->BlockIndex];
				if (entryPtr != lookupEntry) return false;
				
				// Free the entry and update block/table state
				*(uint*)&entryPtr->Tag = ++_tagCounter; // Immediately invalidate the tag
				blockPtr->Masks[maskNumber][(uint)maskOffset] = false;
				blockPtr->AllocCount -= 1;
				_blockFullMask[entryPtr->TableIndex] = false;
				AllocationCount -= 1;

				return true;
			}
			finally { _lock.ExitWriteLock(); }
		}
		
		
		// Evil pointer hacking to create Handle
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static Handle CreateHandle(Entry* entryPtr)
		{
			var data = stackalloc ulong[2] { (ulong)entryPtr, entryPtr->Tag | ((ulong)entryPtr->Tag << 32) };
			return Unsafe.As<ulong, Handle>(ref data[0]);
		}
		
		
		// Public versioned handle to a sparse table entry
		//   NOTE: the byte layout of this type is very important - do not change
		[StructLayout(LayoutKind.Sequential)]
		public readonly struct Handle : IEquatable<Handle>
		{
			#region Fields
			private readonly Entry* _entry;
			private readonly uint _tag;
			
			// If the handle is a null entry
			public bool IsNull => _entry == null;
			// If the handle is still valid
			public bool IsValid => _entry != null && _entry->Tag == _tag;
			// Gets the buffer descriptor, or null if the handle is no longer valid
			public T* Data => IsValid ? &_entry->Data : null;
			#endregion // Fields

			public override int GetHashCode() => HashCode.Combine((ulong)_entry, _tag);
			public override bool Equals(object? obj) => base.Equals(obj);
			public bool Equals(Handle handle) => handle._entry == _entry && handle._tag == _tag;
			
			public static implicit operator bool (Handle handle) => handle.IsValid;

			public static bool operator == (Handle l, Handle r) => l._entry == r._entry && l._tag == r._tag;
			public static bool operator != (Handle l, Handle r) => l._entry != r._entry || l._tag != r._tag;
		}
		
		
		// The inline array of blocks
		[InlineArray((int)TABLE_SIZE)] private struct BlockArray { private nuint /* Block* */ _e0; }
		
		// The block type - lives in unmanaged memory
		private struct Block()
		{
			public uint AllocCount = 0;
			public MaskArray Masks = default;
			public EntryArray Data = default;
		}
		
		// The inline array of masks for a block
		[InlineArray(4)] private struct MaskArray { private Bitmask64 _e0; }
		
		// The inline array of entries
		[InlineArray((int)BLOCK_SIZE)] private struct EntryArray { private Entry _e0; }
		
		// The entry type with extra header information
		[StructLayout(LayoutKind.Sequential)]
		public struct Entry
		{
			public readonly uint Tag;
			public readonly byte TableIndex;
			public readonly byte BlockIndex;
			// 2 unused bytes
			public T Data;
		}
	}
}
