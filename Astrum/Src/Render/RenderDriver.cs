﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Astrum.Render.Vulkan;
using LLVK;
using static Astrum.EngineLogger;
using static LLVK.Vulkan;
using static LLVK.Vulkan.KHR;

namespace Astrum.Render;


// "Private" rendering API implementation built on Vulkan that powers RenderCore
//   In general, if it is not part of the public RenderCore API, it goes here
internal sealed unsafe partial class RenderDriver
{
	// Maximum internal resource limits
	public const uint MAX_SAMPLERS = 10;
	
	// The driver instance (bypasses the initialization check in RenderCore)
	public static RenderDriver Instance { get; private set; } = null!;
	
	
	#region Fields
	// The graphics devices
	public static readonly IReadOnlyList<GraphicsDevice> AllDevices;
	public static readonly GraphicsDevice DefaultDevice;
	
	
	// The selected graphics device
	public readonly GraphicsDevice Device;
	
	// General-purpose cancellation token source for when the driver is shut down
	private readonly CancellationTokenSource _shutdownToken = new();
	
	// Command management
	private readonly List<(Thread Thread, CommandPool? MainPool, CommandPool? DmaPool)> _commandPools; // Thread->pool
	private TimeSpan _lastCommandPoolCleanup; // Last dead thread command pool cleanup time
	
	// Command values
	public bool HasDmaQueue => VulkanCtx.DmaQueue.HasValue;
	
	// Binding management
	private readonly BindingTables _bindingTables;
	public uint WriteTextureLimit => _bindingTables.WriteTextureMask.Size;
	public uint TextureLimit => _bindingTables.TextureMask.Size;
	
	// Global buffers
	private GlobalBuffers _globalBuffers;
	public uint UniformSpace => _globalBuffers.PerFrameUniformSpace;
	public uint StreamingSpace => _globalBuffers.PerFrameStreamingSpace;
	public uint RemainingUniformSpace => _globalBuffers.PerFrameUniformSpace - _globalBuffers.UniformOffset;
	public uint RemainingStreamingSpace => _globalBuffers.PerFrameStreamingSpace - _globalBuffers.StreamingOffset;
	
	// Resource allocation tables
	private readonly SparseTable<BufferDescriptor> _bufferTable = new();
	private readonly SparseTable<ImageDescriptor> _imageTable = new();
	
	// Data staging objects
	private StagingBufferObjects _staging;
	
	// Blit/presentation objects
	private PresentObjects _present;
	
	// Sampler table
	private SamplerArray _samplerTable = default; // TODO
	#endregion // Fields

	
	public RenderDriver(GraphicsDevice device, AppConfig config)
	{
		Debug.Assert(Instance is null, "Attempted to create multiple RenderDriver instances");
		Instance = this;
		
		// Open the context device
		VulkanCtx.OpenDevice(device);
		Device = device;
		
		// Setup command objects
		_commandPools = new(1);
		_lastCommandPoolCleanup = TimeSpan.Zero;
		
		// Setup binding objects
		CreateBindingTables(out _bindingTables, config.LargeTextureTables);
		createGlobalBuffers(out _globalBuffers, config.LargeBufferSpace);
		setUniformBufferBinding(_globalBuffers.UniformBuffer);
		
		// Initialize staging buffers
		initializePooledStagingBuffers();
		
		// Create present objects
		for (var bi = 0; bi < RenderCore.MAX_FRAMES; ++bi) {
			VkSemaphoreCreateInfo create = new();
			VkSemaphore semaphore;
			vkCreateSemaphore(VulkanCtx.Device, &create, null, &semaphore).ThrowIfError();
			VulkanCtx.SetName(semaphore, "@BlitSemaphore", (uint)bi);
			_present.BlitSemaphores[bi] = semaphore;
		}
		
		EngineInfo($"Initialized RenderDriver using device '{device.Name}'");
	}
	
	// Called at application shutdown to cleanup the driver
	public void Destroy()
	{
		Debug.Assert(ReferenceEquals(Instance, this), "Attempted to destroy RenderDriver more than once");
		
		// Wait idle
		_shutdownToken.Cancel();
		vkDeviceWaitIdle(VulkanCtx.Device);
		
		// Destroy presentation objects
		foreach (var semaphore in _present.BlitSemaphores) vkDestroySemaphore(VulkanCtx.Device, semaphore, null);
		
		// Destroy the staging resources
		cleanupPooledStagingBuffers();
		
		// Destroy binding objects
		destroyGlobalBuffers(_globalBuffers);
		DestroyBindingTables(_bindingTables);
		
		// Dispose resource tables - TODO: Release all still-active resources
		_imageTable.Dispose();
		_bufferTable.Dispose();
		
		// Destroy all command pools
		lock (_commandPools) {
			foreach (var (_, mainPool, dmaPool) in _commandPools) {
				mainPool?.Destroy();
				dmaPool?.Destroy();
			}
			_commandPools.Clear();
		}
		
		// Close the device in the context
		VulkanCtx.CloseDevice();

		Instance = null!;
		EngineInfo("Terminated RenderDriver");
	}
	
	
	// Start a new application frame
	public void StartFrame()
	{
		// Periodically cleanup dead or old objects
		cleanupDeadCommandPools();
		checkAndReleaseOldStagingBuffers();
		
		// Reset global buffers
		_globalBuffers.FrameIndex = (_globalBuffers.FrameIndex + 1) % RenderCore.MAX_FRAMES;
		_globalBuffers.UniformOffset = 0;
		_globalBuffers.StreamingOffset = 0;
		
		// Advance to the next present frame and wait (MAIN GPU->CPU SYNC POINT)
		_present.BlitIndex = (_present.BlitIndex + 1) % RenderCore.MAX_FRAMES;
		_present.BlitWork[(int)_present.BlitIndex].WaitComplete(UInt64.MaxValue);
	}
	
	// Ends the current application frame by presenting the latest render results
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public void EndFrame()
	{
		// Get the list of swapchains to present
		uint presentCount = 0;
		using var swapchains = new RentedArray<Swapchain>(Window.MAX_COUNT);
		var swapchainHandles = stackalloc VkSwapchainKHR[(int)Window.MAX_COUNT];
		var swapchainIndices = stackalloc uint[(int)Window.MAX_COUNT];
		foreach (var window in RenderCore.OpenWindows) {
			// Skip minimized windows
			if (window.IsMinimized) continue;
			
			// If the window is newly restored, rebuild now and skip present
			if (window is { IsMinimized: false, Swapchain.IsMinimized: true }) {
				window.Swapchain.Rebuild();
				continue;
			}
			
			swapchains[presentCount] = window.Swapchain!;
			swapchainHandles[presentCount] = window.Swapchain!.SwapchainHandle;
			swapchainIndices[presentCount] = window.Swapchain!.CurrentImage.Index;
			presentCount += 1;
		}
		
		// If nothing is presenting, wait on a full-flush barrier and return
		if (presentCount == 0) {
			_ = vkQueueWaitIdle(VulkanCtx.MainQueue.Handle);
			return;
		}

		// Simulated render work - TODO: real render work
		WorkUnit renderWork;
		using (var renderList = StartMainCommandList(1)) {
			vkCmdPipelineBarrier(renderList.Buffers[0],
				VkPipelineStageFlags.BottomOfPipe, VkPipelineStageFlags.TopOfPipe,
				default, 0, null, 0, null, 0, null);
			renderWork = renderList.Submit(default);
		}
		
		// Start the blit operation
		var blitWork = submitBlitOperation(swapchains.Array.AsSpan(0, (int)presentCount), renderWork);
		_present.BlitWork[(int)_present.BlitIndex] = blitWork;
		
		// Submit the present
		var blitSem = _present.BlitSemaphores[(int)_present.BlitIndex];
		var presentResults = stackalloc VkResult[(int)presentCount];
		VkPresentInfoKHR present = new() {
			WaitSemaphoreCount = 1, PWaitSemaphores = &blitSem,
			SwapchainCount = presentCount, PSwapchains = swapchainHandles, PImageIndices = swapchainIndices,
			PResults = presentResults
		};
		PresentMainQueue(&present);
		
		// Handle the present results
		for (uint pi = 0; pi < presentCount; ++pi) {
			var sc = swapchains[pi];
			var result = presentResults[pi];
			if (sc.NeedsRebuild || result is VkResult.ErrorOutOfDateKHR or VkResult.SuboptimalKHR) {
				sc.Rebuild(); // Also re-acquires
			}
			else if (result != VkResult.Success) throw new($"Failed to present window {sc.Window}");
			else if (!sc.Acquire(out result)) {
				throw new($"Failed to acquire window {sc.Window} after present ({result})");
			}
		}
	}
	
	// Performs a full pipeline flush and internal state clean
	public void FlushAll()
	{
		// Wait idle on the main queue
		_ = vkQueueWaitIdle(VulkanCtx.MainQueue.Handle);
	}
	
	
	// Records the blit operations for all current windows
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	[SkipLocalsInit]
	private WorkUnit submitBlitOperation(ReadOnlySpan<Swapchain> swapchains, WorkUnit renderWork)
	{
		// TODO: For now, this just clears the swapchain images instead of blitting - actually blit render results
		
		using var blitList = StartMainCommandList(1);
		var cmd = blitList.Buffers[0];
		
		VkImageSubresourceRange colorRange = new(VkImageAspectFlags.Color, 0, 1, 0, 1);

		// Record initial transitions
		var barriers = stackalloc VkImageMemoryBarrier[swapchains.Length];
		for (uint si = 0; si < swapchains.Length; ++si) {
			barriers[si] = new() {
				SrcAccessMask = default, DstAccessMask = VkAccessFlags.TransferWrite,
				OldLayout = VkImageLayout.Undefined, NewLayout = VkImageLayout.TransferDstOptimal,
				SrcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED, DstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
				Image = swapchains[(int)si].CurrentImage.Image, SubresourceRange = colorRange
			};
		}
		vkCmdPipelineBarrier(cmd,
			VkPipelineStageFlags.TopOfPipe, VkPipelineStageFlags.Transfer,
			default,
			0, null,
			0, null,
			(uint)swapchains.Length, barriers);
		
		// Blit images (clear for now)
		VkClearColorValue clearColor = new();
		clearColor.Float32[0] = 1; clearColor.Float32[1] = 1; clearColor.Float32[2] = 1; clearColor.Float32[3] = 1;
		for (uint si = 0; si < swapchains.Length; ++si) {
			vkCmdClearColorImage(cmd, 
				swapchains[(int)si].CurrentImage.Image, VkImageLayout.TransferDstOptimal, &clearColor,
				1, &colorRange);
		}
		
		// Transfer back to present
		for (uint si = 0; si < swapchains.Length; ++si) {
			barriers[(int)si].SrcAccessMask = VkAccessFlags.TransferWrite;
			barriers[(int)si].DstAccessMask = default;
			barriers[(int)si].OldLayout = VkImageLayout.TransferDstOptimal;
			barriers[(int)si].NewLayout = VkImageLayout.PresentSrcKHR;
		}
		vkCmdPipelineBarrier(cmd,
			VkPipelineStageFlags.Transfer, VkPipelineStageFlags.BottomOfPipe,
			default,
			0, null,
			0, null,
			(uint)swapchains.Length, barriers);
		
		// Add acquire and render waits
		foreach (var sc in swapchains) blitList.AddWait(sc.CurrentSemaphore, VkPipelineStageFlags.Transfer);
		blitList.AddWait(renderWork, VkPipelineStageFlags.Transfer);
		
		// Submit
		blitList.AddSignal(_present.BlitSemaphores[(int)_present.BlitIndex]);
		return blitList.Submit(default);
	}
	
	
	// Static ctor
	static RenderDriver()
	{
		// Enumerate the graphics devices
		uint devCount;
		vkEnumeratePhysicalDevices(VulkanCtx.Instance, &devCount, null).ThrowIfError();
		var devices = stackalloc VkPhysicalDevice[(int)devCount];
		vkEnumeratePhysicalDevices(VulkanCtx.Instance, &devCount, devices).ThrowIfError();
		AllDevices = Enumerable.Range(0, (int)devCount)
			.Select(i => new GraphicsDevice(devices[i]))
			.Where(static d => d.MissingFeatures.Length == 0)
			.OrderByDescending(static d => d.DeviceLocalMemory)
			.ToArray();
		if (AllDevices.Count == 0) {
			throw new PlatformNotSupportedException("No devices on the system support the required Vulkan features");
		}
		DefaultDevice =
			AllDevices.FirstOrDefault(static d => d.Kind == GraphicsDevice.DeviceKind.Discrete, AllDevices[0]);
		
		// Report devices
		EngineInfo($"Found {AllDevices.Count} graphics devices:");
		foreach (var dev in AllDevices) EngineInfo("  " + dev.Report);
	}
	
	
	// Inline array of samplers
	[InlineArray((int)MAX_SAMPLERS)] private struct SamplerArray { private VkSampler _e0; }
	
	// Objects for final window blits and presentation
	private struct PresentObjects
	{
		public uint BlitIndex;
		public SemaphoreArray BlitSemaphores;
		public WorkUnitArray BlitWork;
		
		[InlineArray((int)RenderCore.MAX_FRAMES)] public struct SemaphoreArray { private VkSemaphore _e0; }
		[InlineArray((int)RenderCore.MAX_FRAMES)] public struct WorkUnitArray { private WorkUnit _e0; }
	}
}
