﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;

namespace Astrum.Render;


/// <summary>Describes a sampler configuration for reading texture data in a shader.</summary>
public readonly struct Sampler
{
	// The internal sampler index
	internal readonly uint Index;

	internal Sampler(uint index)
	{
		Debug.Assert(index < RenderDriver.MAX_SAMPLERS);
		Index = index;
	}
}
