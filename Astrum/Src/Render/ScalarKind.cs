﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Render;


/// <summary>The type categories for primitive scalar data.</summary>
public enum ScalarKind
{
	/// <summary>Signed integer data.</summary>
	SignedInt,
	/// <summary>Unsigned integer data.</summary>
	UnsignedInt,
	/// <summary>Signed normalized float data.</summary>
	SignedNorm,
	/// <summary>Unsigned normalized float data.</summary>
	UnsignedNorm,
	/// <summary>Standard floating point data.</summary>
	Float
}


/// <summary>Extension functionality for <see cref="ScalarKind"/> values.</summary>
public static class ScalarKindExtensions
{
	/// <summary>If the scalar kind is an integer type.</summary>
	public static bool IsInteger(this ScalarKind kind) => kind is ScalarKind.SignedInt or ScalarKind.UnsignedInt;

	/// <summary>If the scalar kind is a float type (includes normalized kinds).</summary>
	public static bool IsFloat(this ScalarKind kind) => kind is not (ScalarKind.SignedInt or ScalarKind.UnsignedInt);
}
