﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Render;


/// <summary>Describes a four-component RGBA color, stored as a 4-byte packed integer with Red at the LSB.</summary>
/// <remarks>This type is blittable with the layout expected by <see cref="TexelFormat.Color"/>.</remarks>
[StructLayout(LayoutKind.Explicit, Size = 4)]
public unsafe struct Color : IEquatable<Color>, ITexelType
{
	// The default generator for random colors
	private static readonly Random _Random = new((int)(DateTime.Now.Ticks % Int32.MaxValue));
	
	
	#region Fields
	/// <inheritdoc/>
	public static TexelFormat TexelFormat => TexelFormat.Color;
	
	
	/// <summary>The value of the red channel (in [0, 255]).</summary>
	public byte R {
		readonly get => _comps[0];
		init => _comps[0] = value;
	}
	/// <summary>The value of the green channel (in [0, 255]).</summary>
	public byte G {
		readonly get => _comps[1];
		init => _comps[1] = value;
	}
	/// <summary>The value of the blue channel (in [0, 255]).</summary>
	public byte B {
		readonly get => _comps[2];
		init => _comps[2] = value;
	}
	/// <summary>The value of the alpha channel (in [0, 255]).</summary>
	public byte A {
		readonly get => _comps[3];
		init => _comps[3] = value;
	}

	/// <summary>The normalized value of the red channel (in [0, 1]).</summary>
	public float RNorm {
		readonly get => _comps[0] / (float)Byte.MaxValue;
		init => _comps[0] = (byte)Math.Clamp(value * Byte.MaxValue, 0, Byte.MaxValue);
	}
	/// <summary>The normalized value of the green channel (in [0, 1]).</summary>
	public float GNorm {
		readonly get => _comps[1] / (float)Byte.MaxValue;
		init => _comps[1] = (byte)Math.Clamp(value * Byte.MaxValue, 0, Byte.MaxValue);
	}
	/// <summary>The normalized value of the blue channel (in [0, 1]).</summary>
	public float BNorm {
		readonly get => _comps[2] / (float)Byte.MaxValue;
		init => _comps[2] = (byte)Math.Clamp(value * Byte.MaxValue, 0, Byte.MaxValue);
	}
	/// <summary>The normalized value of the alpha channel (in [0, 1]).</summary>
	public float ANorm {
		readonly get => _comps[3] / (float)Byte.MaxValue;
		init => _comps[3] = (byte)Math.Clamp(value * Byte.MaxValue, 0, Byte.MaxValue);
	}

	/// <summary>Gets the opaque version of the color (with max alpha channel)</summary>
	public readonly Color Opaque => new(_comps[0], _comps[1], _comps[2]);

	/// <summary>Gets the grayscale version of the color (with unchanged alpha).</summary>
	public readonly Color Grayscale {
		get { 
			var gray = (RNorm * 0.2125f) + (GNorm * 0.7154f) + (BNorm * 0.0721f); // Approx. response of the human eye
			var grayInt = (byte)Math.Clamp(gray * Byte.MaxValue, 0, Byte.MaxValue);
			return new(grayInt, grayInt, grayInt, A);
		}
	}
	
	// Packed color value
	[FieldOffset(0)]
	private uint _packed;
	// Packed components
	[FieldOffset(0)]
	private fixed byte _comps[4];
	#endregion // Fields
	
	
	/// <summary>Default color is transparent black.</summary>
	public Color()
	{
		Unsafe.SkipInit(out this);
		_packed = 0;
	}

	/// <summary>Construct a color from a packed RGBA value.</summary>
	/// <param name="packed">The packed RGBA values (Red at the LSB).</param>
	public Color(uint packed)
	{
		Unsafe.SkipInit(out this);
		_packed = packed;
	}

	/// <summary>Construct a color from a set of channel values.</summary>
	public Color(byte r, byte g, byte b, byte a = 255)
	{
		Unsafe.SkipInit(out this);
		_comps[0] = r; _comps[1] = g; _comps[2] = b; _comps[3] = a;
	}

	/// <summary>Construct a color from an existing color with a new alpha value.</summary>
	/// <param name="c">The existing color.</param>
	/// <param name="a">The new alpha value.</param>
	public Color(Color c, byte a)
	{
		Unsafe.SkipInit(out this);
		_packed = c._packed;
		_comps[3] = a;
	}
	
	
	#region Object
	public readonly bool Equals(Color other) => _packed == other._packed;

	public readonly override bool Equals([NotNullWhen(true)] object? obj) => (obj is Color c) && c._packed == _packed;

	public readonly override int GetHashCode() => _packed.GetHashCode();

	public readonly override string ToString() => $"#{_packed:X8}";

	public readonly void Deconstruct(out byte r, out byte g, out byte b, out byte a)
	{
		r = _comps[0]; g = _comps[1]; b = _comps[2]; a = _comps[3];
	}

	public static bool operator == (Color l, Color r) => l._packed == r._packed;
	public static bool operator != (Color l, Color r) => l._packed != r._packed;
	#endregion // Object
	
	
	/// <summary>Construct a color from a set of normalized ([0, 1]) channel values.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static Color FromNorm(float r, float g, float b, float a = 1) => new(
		(byte)Math.Clamp(r * Byte.MaxValue, 0, Byte.MaxValue),
		(byte)Math.Clamp(g * Byte.MaxValue, 0, Byte.MaxValue),
		(byte)Math.Clamp(b * Byte.MaxValue, 0, Byte.MaxValue),
		(byte)Math.Clamp(a * Byte.MaxValue, 0, Byte.MaxValue)
	);

	/// <summary>Get a random color.</summary>
	/// <param name="opaque">If the random color should be opaque.</param>
	public static Color Random(bool opaque = true)
	{
		var color = new Color((uint)_Random.NextInt64(UInt32.MaxValue));
		return opaque ? color.Opaque : color;
	}

	/// <summary>Get a random color using an external random state.</summary>
	/// <param name="random">The random state to generate the color with.</param>
	/// <param name="opaque">If the random color should be opaque.</param>
	public static Color Random(Random random, bool opaque = true)
	{
		var color = new Color((uint)random.NextInt64(UInt32.MaxValue));
		return opaque ? color.Opaque : color;
	}
	
	/// <summary>Converts the normalized channels into a vector.</summary>
	public readonly Vec4 ToVector() => new(RNorm, GNorm, BNorm, ANorm);

	/// <summary>Converts the integer channels into a point.</summary>
	public readonly Point4U ToPoint() => new(R, G, B, A);
}
