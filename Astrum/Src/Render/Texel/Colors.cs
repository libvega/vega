﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Render;


/// <summary>Predefined color values.</summary>
public static class Colors
{
	/// <summary><c>#000000FF</c></summary>
	public static readonly Color Black = new(0xFF000000);
	/// <summary><c>#00000000</c></summary>
	public static readonly Color TransparentBlack = new(0x00000000);
	/// <summary><c>#FFFFFFFF</c></summary>
	public static readonly Color White = new(0xFFFFFFFF);
	/// <summary><c>#FFFFFF00</c></summary>
	public static readonly Color TransparentWhite = new(0x00FFFFFF);

	/// <summary><c>#222222FF</c></summary>
	public static readonly Color DarkGray = new(0xFF222222);
	/// <summary><c>#555555FF</c></summary>
	public static readonly Color Gray = new(0xFF555555);
	/// <summary><c>#999999FF</c></summary>
	public static readonly Color LightGray = new(0xFF999999);

	/// <summary><c>#FF0000FF</c></summary>
	public static readonly Color Red = new(0xFF0000FF);
	/// <summary><c>#00FF00FF</c></summary>
	public static readonly Color Green = new(0xFF00FF00);
	/// <summary><c>#0000FFFF</c></summary>
	public static readonly Color Blue = new(0xFFFF0000);
	/// <summary><c>#FFFF00FF</c></summary>
	public static readonly Color Yellow = new(0xFF00FFFF);
	/// <summary><c>#FF00FFFF</c></summary>
	public static readonly Color Magenta = new(0xFFFF00FF);
	/// <summary><c>#00FFFFFF</c></summary>
	public static readonly Color Cyan = new(0xFFFFFF00);

	/// <summary><c>#E73C00FF</c></summary>
	public static readonly Color MonogameOrange = new(0xFF003CE7);
	/// <summary><c>#6495EDFF</c></summary>
	public static readonly Color CornflowerBlue = new(0xFFED9564);
}
