﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using static LLVK.Vulkan;

namespace Astrum.Render;


/// <summary>Supported layout formats for depth and stencil data.</summary>
public enum DepthFormat
{
	/// <summary>Low-precision 16-bit unsigned normalized depth data.</summary>
	LowDepth = 0,
	/// <summary>High-precision 32-bit signed float depth data.</summary>
	Depth = 1,
	/// <summary>Combined high-precision depth with 8-bit stencil data, exact layout is platform dependent.</summary>
	DepthStencil = 2
}


/// <summary>Extension functionality for <see cref="DepthFormat"/> values.</summary>
public static class DepthFormatExtensions
{
	/// <summary>If the format contains a stencil component.</summary>
	public static bool HasStencil(this DepthFormat format) => format is DepthFormat.DepthStencil;

	// Gets the VkFormat value
	internal static VkFormat ToVk(this DepthFormat format) => format switch {
		DepthFormat.LowDepth     => VkFormat.D16Unorm,
		DepthFormat.Depth        => VkFormat.D32Sfloat,
		DepthFormat.DepthStencil => OperatingSystem.IsAndroid() ? VkFormat.D24UnormS8Uint : VkFormat.D32SfloatS8Uint,
		_                        => throw new NotImplementedException()
	};
}
