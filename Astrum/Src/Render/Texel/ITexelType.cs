﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Render;


/// <summary>Descriptive interface for <c>unmanaged</c> types which represent raw texel data.</summary>
public interface ITexelType
{
	/// <summary>The format of the texel data supplied by the type.</summary>
	static abstract TexelFormat TexelFormat { get; }
}
