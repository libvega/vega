﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using static LLVK.Vulkan;

namespace Astrum.Render;


/// <summary>Supported layout formats for texel data.</summary>
public enum TexelFormat
{
	/// <summary>Special value representing an uninitialized, invalid, or unknown format. Channels: N/A.</summary>
	Undefined = 0,

	/// <summary>An unsigned 8-bit integer representing a normalized value. Channels: R.</summary>
	UNorm      = 1,
	/// <summary>Two unsigned 8-bit integers representing normalized values. Channels: RG.</summary>
	UNorm2     = 2,
	/// <summary>Four unsigned 8-bit integers representing normalized values. Channels: RGBA.</summary>
	UNorm4     = 3,
	/// <summary>Identical to <see cref="UNorm4"/>, compatible with <see cref="Render.Color"/>.</summary>
	Color      = UNorm4,
	/// <summary>Four unsigned 8-bit integers representing normalized values, in reversed color order. Channels: BGRA.</summary>
	Unorm4Bgra = 4,
	/// <summary>A signed 8-bit integer representing a normalized value. Channels: R.</summary>
	SNorm      = 5,
	/// <summary>Two signed 8-bit integers representing normalized values. Channels: RG.</summary>
	SNorm2     = 6,
	/// <summary>Four signed 8-bit integers representing normalized values. Channels: RGBA.</summary>
	SNorm4     = 7,

	/// <summary>An unsigned 16-bit integer representing a normalized value. Channels: R.</summary>
	U16Norm  = 8,
	/// <summary>Two unsigned 16-bit integers representing normalized values. Channels: RG.</summary>
	U16Norm2 = 9,
	/// <summary>Four unsigned 16-bit integers representing normalized values. Channels: RGBA.</summary>
	U16Norm4 = 10,
	/// <summary>A signed 16-bit integer representing a normalized value. Channels: R.</summary>
	S16Norm  = 11,
	/// <summary>Two signed 16-bit integers representing normalized values. Channels: RG.</summary>
	S16Norm2 = 12,
	/// <summary>Four signed 16-bit integers representing normalized values. Channels: RGBA.</summary>
	S16Norm4 = 13,

	/// <summary>An unsigned 8-bit integer. Channels: R.</summary>
	UByte  = 14,
	/// <summary>Two unsigned 8-bit integers. Channels: RG.</summary>
	UByte2 = 15,
	/// <summary>Four unsigned 8-bit integers. Channels: RGBA.</summary>
	UByte4 = 16,
	/// <summary>A signed 8-bit integer. Channels: R.</summary>
	SByte  = 17,
	/// <summary>Two signed 8-bit integers. Channels: RG.</summary>
	SByte2 = 18,
	/// <summary>Four signed 8-bit integers. Channels: RGBA.</summary>
	SByte4 = 19,

	/// <summary>An unsigned 16-bit integer. Channels: R.</summary>
	UShort  = 20,
	/// <summary>Two unsigned 16-bit integers. Channels: RG.</summary>
	UShort2 = 21,
	/// <summary>Four unsigned 16-bit integers. Channels: RGBA.</summary>
	UShort4 = 22,
	/// <summary>A signed 16-bit integer. Channels: R.</summary>
	SShort  = 23,
	/// <summary>Two signed 16-bit integers. Channels: RG.</summary>
	SShort2 = 24,
	/// <summary>Four signed 16-bit integers. Channels: RGBA.</summary>
	SShort4 = 25,

	/// <summary>An unsigned 32-bit integer. Channels: R.</summary>
	UInt  = 26,
	/// <summary>Two unsigned 32-bit integers. Channels: RG.</summary>
	UInt2 = 27,
	/// <summary>Four unsigned 32-bit integers. Channels: RGBA.</summary>
	UInt4 = 28,
	/// <summary>A signed 32-bit integer. Channels: R.</summary>
	SInt  = 29,
	/// <summary>Two signed 32-bit integers. Channels: RG.</summary>
	SInt2 = 30,
	/// <summary>Four signed 32-bit integers. Channels: RGBA.</summary>
	SInt4 = 31,

	/// <summary>A 16-bit float. Channels: R.</summary>
	Half = 32,
	/// <summary>Two 16-bit floats. Channels: RG.</summary>
	Half2 = 33,
	/// <summary>Four 16-bit floats. Channels: RGBA.</summary>
	Half4 = 34,
	/// <summary>A 32-bit float. Channels: R.</summary>
	Float  = 35,
	/// <summary>Two 32-bit floats. Channels: RG.</summary>
	Float2 = 36,
	/// <summary>Four 32-bit floats. Channels: RGBA.</summary>
	Float4 = 37,

	/// <summary>Unsigned normalized values, 2 bits for alpha, 10 bits for each color channel. Channels: ABGR.</summary>
	/// <remarks>Cannot be used as the format of a render target.</remarks>
	A2Bgr10 = 38,
	/// <summary>Unsigned normalized values, 1 bit for alpha, 5 bits for each color channel. Channels: ARGB.</summary>
	/// <remarks>Cannot be used as the format of a render target.</remarks>
	A1Rgb5  = 39,
	/// <summary>Unsigned normalized values, 5 bits for red and blue, 6 bits for green. Channels: RGB.</summary>
	/// <remarks>Cannot be used as the format of a render target.</remarks>
	R5G6B5  = 40
}


/// <summary>Extension functionality for <see cref="TexelFormat"/> values.</summary>
public static class TexelFormatExtensions
{
	/// <summary>The number of color channels in the format.</summary>
	public static uint GetChannelCount(this TexelFormat format) => _FormatInfo[(int)format].Channels;

	/// <summary>The size of a single texel of the format, in bytes.</summary>
	public static uint GetSize(this TexelFormat format) => _FormatInfo[(int)format].Size;

	/// <summary>The scalar data kind of the format.</summary>
	public static ScalarKind GetScalarKind(this TexelFormat format) => _FormatInfo[(int)format].Scalar;

	// Gets the VkFormat value
	internal static VkFormat ToVk(this TexelFormat format) => _FormatInfo[(int)format].Format;
	
	// Info for the formats
	private static readonly (VkFormat Format, uint Channels, uint Size, ScalarKind Scalar)[] _FormatInfo = [
		(VkFormat.Undefined, 0, 0, default),

		(VkFormat.R8Unorm, 1, 1, ScalarKind.UnsignedNorm),
		(VkFormat.R8g8Unorm, 2, 2, ScalarKind.UnsignedNorm),
		(VkFormat.R8g8b8a8Unorm, 4, 4, ScalarKind.UnsignedNorm),
		(VkFormat.B8g8r8a8Unorm, 4, 4, ScalarKind.UnsignedNorm),
		(VkFormat.R8Snorm, 1, 1, ScalarKind.SignedNorm),
		(VkFormat.R8g8Snorm, 2, 2, ScalarKind.SignedNorm),
		(VkFormat.R8g8b8a8Snorm, 4, 4, ScalarKind.SignedNorm),

		(VkFormat.R16Unorm, 1, 2, ScalarKind.UnsignedNorm),
		(VkFormat.R16g16Unorm, 2, 4, ScalarKind.UnsignedNorm),
		(VkFormat.R16g16b16a16Unorm, 4, 8, ScalarKind.UnsignedNorm),
		(VkFormat.R16Snorm, 1, 2, ScalarKind.SignedNorm),
		(VkFormat.R16g16Snorm, 2, 4, ScalarKind.SignedNorm),
		(VkFormat.R16g16b16a16Snorm, 4, 8, ScalarKind.SignedNorm),

		(VkFormat.R8Uint, 1, 1, ScalarKind.UnsignedInt),
		(VkFormat.R8g8Uint, 2, 2, ScalarKind.UnsignedInt),
		(VkFormat.R8g8b8a8Uint, 4, 4, ScalarKind.UnsignedInt),
		(VkFormat.R8Sint, 1, 1, ScalarKind.SignedInt),
		(VkFormat.R8g8Sint, 2, 2, ScalarKind.SignedInt),
		(VkFormat.R8g8b8a8Sint, 4, 4, ScalarKind.SignedInt),

		(VkFormat.R16Uint, 1, 2, ScalarKind.UnsignedInt),
		(VkFormat.R16g16Uint, 2, 4, ScalarKind.UnsignedInt),
		(VkFormat.R16g16b16a16Uint, 4, 8, ScalarKind.UnsignedInt),
		(VkFormat.R16Sint, 1, 2, ScalarKind.SignedInt),
		(VkFormat.R16g16Sint, 2, 4, ScalarKind.SignedInt),
		(VkFormat.R16g16b16a16Sint, 4, 8, ScalarKind.SignedInt),

		(VkFormat.R32Uint, 1, 4, ScalarKind.UnsignedInt),
		(VkFormat.R32g32Uint, 2, 8, ScalarKind.UnsignedInt),
		(VkFormat.R32g32b32a32Uint, 4, 16, ScalarKind.UnsignedInt),
		(VkFormat.R32Sint, 1, 4, ScalarKind.SignedInt),
		(VkFormat.R32g32Sint, 2, 8, ScalarKind.SignedInt),
		(VkFormat.R32g32b32a32Sint, 4, 16, ScalarKind.SignedInt),

		(VkFormat.R16Sfloat, 1, 2, ScalarKind.Float),
		(VkFormat.R16g16Sfloat, 2, 4, ScalarKind.Float),
		(VkFormat.R16g16b16a16Sfloat, 4, 8, ScalarKind.Float),
		(VkFormat.R32Sfloat, 1, 4, ScalarKind.Float),
		(VkFormat.R32g32Sfloat, 2, 8, ScalarKind.Float),
		(VkFormat.R32g32b32a32Sfloat, 4, 16, ScalarKind.Float),

		(VkFormat.A2b10g10r10UnormPack32, 4, 4, ScalarKind.UnsignedNorm),
		(VkFormat.A1r5g5b5UnormPack16, 4, 2, ScalarKind.UnsignedNorm),
		(VkFormat.R5g6b5UnormPack16, 3, 4, ScalarKind.UnsignedNorm)
	];
}
