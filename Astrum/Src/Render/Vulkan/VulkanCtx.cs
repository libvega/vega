﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Astrum.Native;
using LLVK;
using static LLVK.Vulkan;
using static LLVK.Vulkan.KHR;
using static LLVK.Vulkan.EXT;
using static Astrum.EngineLogger;
// ReSharper disable InconsistentNaming

namespace Astrum.Render.Vulkan;


// Manages the core Vulkan runtime objects
internal static unsafe class VulkanCtx
{
	// The default log file path
	private const string LOG_PATH = "./vulkan_debug.log";


	#region Fields
	// Core Vulkan instance objects
	public static readonly VkInstance Instance;
	public static readonly InstanceExtensionSet InstanceExtensions;

	// Support flags
	public static readonly bool HasDebug;
	public static readonly bool HasValidation;
	
	// Core Vulkan device objects
	public static VkPhysicalDevice PhysicalDevice { get; private set; }
	public static VkDevice Device { get; private set; }
	public static Vma.VmaAllocator VmaAllocator { get; private set; }
	
	// Vulkan queues
	public static DeviceQueue MainQueue { get; private set; }
	public static DeviceQueue? DmaQueue { get; private set; }
	
	// Memory heap types
	public static MemoryType DeviceMemory { get; private set; }
	public static MemoryType UploadMemory { get; private set; }
	public static MemoryType ReadbackMemory { get; private set; }
	public static MemoryType DynamicMemory { get; private set; }
	public static MemoryType? LazyMemory { get; private set; }

	// Debug logging objects
	private static readonly StreamWriter? _LogWriter; // Log file
	private static readonly object _LogLock = new();  // Log write lock
	private static readonly ThreadLocal<StringBuilder> _MessageBuilder = new(() => new(512)); // Thread-local builder
	private static readonly VkDebugUtilsMessengerEXT _DebugMessenger; // Debug messenger object
	private static readonly Delegate _DebugMessageCallbackInst = HandleDebugMessage; // Debug callback instance

	// The surface extension name for the platform
	public static readonly string SurfaceExtensionName =
		OperatingSystem.IsWindows() ? VK_KHR_WIN32_SURFACE_EXTENSION_NAME :
		OperatingSystem.IsLinux() ? VK_KHR_XCB_SURFACE_EXTENSION_NAME :
		throw new PlatformNotSupportedException("Unsupported Vulkan display platform");
	#endregion // Fields


	#region Device
	// Creates the device from the given GraphicsDevice
	public static void OpenDevice(GraphicsDevice gd)
	{
		// Setup extensions and features
		using NativeStringListUtf8 extList = new(5);
		extList.Add(VK_KHR_SWAPCHAIN_EXTENSION_NAME);             // swapchain
		extList.Add(VK_EXT_SCALAR_BLOCK_LAYOUT_EXTENSION_NAME);   // scalar_block_layout
		extList.Add(VK_KHR_TIMELINE_SEMAPHORE_EXTENSION_NAME);    // timeline_semaphore
		extList.Add(VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME);   // descriptor_indexing
		extList.Add(VK_EXT_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME); // buffer_device_address
		VulkanDeviceFeatures feats = new();
		feats.Features.IndependentBlend = true;
		feats.Features.WideLines = gd.WideLines;
		feats.Features.LargePoints = gd.LargePoints;
		feats.Features.FillModeNonSolid = gd.FillModeNonSolid;
		feats.Vulkan11.ShaderDrawParameters = true;
		feats.ScalarBlockLayout.ScalarBlockLayout = true;
		feats.TimelineSemaphore.TimelineSemaphore = true;
		feats.DescriptorIndexing.RuntimeDescriptorArray = true;
		feats.DescriptorIndexing.DescriptorBindingPartiallyBound = true;
		feats.DescriptorIndexing.DescriptorBindingUpdateUnusedWhilePending = true;
		feats.DescriptorIndexing.DescriptorBindingSampledImageUpdateAfterBind = true;
		feats.DescriptorIndexing.DescriptorBindingStorageImageUpdateAfterBind = true;
		feats.DescriptorIndexing.DescriptorBindingStorageBufferUpdateAfterBind = true;
		feats.DescriptorIndexing.ShaderSampledImageArrayNonUniformIndexing = true;
		feats.DescriptorIndexing.ShaderStorageImageArrayNonUniformIndexing = true;
		feats.DescriptorIndexing.ShaderStorageBufferArrayNonUniformIndexing = true;
		feats.BufferDeviceAddress.BufferDeviceAddress = true;

		// Setup queues - TODO: Async Compute when we figure out how to use it
		float priority = 1;
		var queuePtr = stackalloc VkDeviceQueueCreateInfo[2];
		uint queueCount = 1;
		queuePtr[0] = new() { PQueuePriorities = &priority, QueueCount = 1, QueueFamilyIndex = gd.MainQueueFamily };
		if (gd.DmaQueueFamily is { } dmaFamily) {
			queuePtr[queueCount++] = new() { PQueuePriorities = &priority, QueueCount = 1, QueueFamilyIndex = dmaFamily };
		}

		// Create the device
		VkDeviceCreateInfo devInfo = new() {
			QueueCreateInfoCount = queueCount, PQueueCreateInfos = queuePtr,
			EnabledExtensionCount = extList.Count, PPEnabledExtensionNames = extList.Data
		};
		var devInfoPtr = &devInfo;
		VkDevice handle;
		var handlePtr = &handle;
		feats.Invoke(ptr => {
			devInfoPtr->PNext = ptr;
			vkCreateDevice(gd.PhysicalDevice, devInfoPtr, null, handlePtr).ThrowIfError();
		});
		
		// Save device
		PhysicalDevice = gd.PhysicalDevice;
		Device = handle;
		VulkanLoader.LoadDeviceFunctions(Device);
		SetName(PhysicalDevice, "@PhysicalDevice");
		SetName(Device, "@Device"); 
		
		// Create the memory allocator
		Vma.VmaAllocator allocator;
		Vma.CreateAllocator(&allocator).ThrowIfError();
		VmaAllocator = allocator;
		
		// Get the queues
		{
			VkQueue queue;
			vkGetDeviceQueue(Device, gd.MainQueueFamily, 0, &queue);
			SetName(queue, "@MainQueue");
			MainQueue = new(queue, gd.MainQueueFamily, new());
			if (gd.DmaQueueFamily is { } dmaFam) {
				vkGetDeviceQueue(Device, dmaFam, 0, &queue);
				SetName(queue, "@DmaQueue");
				DmaQueue = new(queue, dmaFam, new());
			}
		}
		
		// Get the memory types
		{
			VkPhysicalDeviceMemoryProperties props;
			vkGetPhysicalDeviceMemoryProperties(PhysicalDevice, &props);
			
			// Use VMA to get the types
			Vma.MemoryTypes memTypes;
			Vma.FindMemoryTypes(&memTypes);
			DeviceMemory = new(memTypes.Device, props.MemoryTypes[memTypes.Device]);
			UploadMemory = new(memTypes.Upload, props.MemoryTypes[memTypes.Upload]);
			ReadbackMemory = new(memTypes.Readback, props.MemoryTypes[memTypes.Readback]);
			DynamicMemory = new(memTypes.Dynamic, props.MemoryTypes[memTypes.Dynamic]);
			if (memTypes.Lazy != UInt32.MaxValue) LazyMemory = new(memTypes.Lazy, props.MemoryTypes[memTypes.Lazy]);
			else LazyMemory = null;
		}
		
		EngineInfo($"Opened Vulkan render device '{gd.Name}' (API {gd.ApiVersion}) " +
			$"(Mem: D={DeviceMemory.Index} U={UploadMemory.Index} R={ReadbackMemory.Index} Dy={DynamicMemory.Index})");
	}
	
	// Destroy the device
	public static void CloseDevice()
	{
		if (!Device) return;
		
		// Destroy the allocator
		Vma.DestroyAllocator(VmaAllocator);
		
		// Destroy the device
		vkDestroyDevice(Device, null);

		// Clear state
		MainQueue = default;
		DmaQueue = default;
		VmaAllocator = default;
		Device = default;
		PhysicalDevice = default;

		EngineInfo("Closed Vulkan render device");
	}
	#endregion // Device
	
	
	#region Debug
	// Sets the name for an object
	[Conditional("DEBUG")]
	public static void SetName<T>(T handle, ReadOnlySpan<char> name)
		where T : unmanaged, IVulkanHandle =>
		SetName(handle, name, ReadOnlySpan<char>.Empty, null);
	
	// Sets the name for an object
	[Conditional("DEBUG")]
	public static void SetName<T>(T handle, ReadOnlySpan<char> name, uint index)
		where T : unmanaged, IVulkanHandle =>
		SetName(handle, name, ReadOnlySpan<char>.Empty, index);
	
	// Sets the name for an object
	[Conditional("DEBUG")]
	public static void SetName<T>(T handle, ReadOnlySpan<char> name, ReadOnlySpan<char> subName)
		where T : unmanaged, IVulkanHandle =>
		SetName(handle, name, subName, null);

	// Sets the name for an object
	[Conditional("DEBUG")]
	public static void SetName<T>(T handle, ReadOnlySpan<char> name, ReadOnlySpan<char> subName, uint? index)
		where T : unmanaged, IVulkanHandle
	{
		if (handle.Handle == 0 || !InstanceExtensions.EXT_debug_utils) return;
		if (name.Length > 32) name = name[..32];
		if (subName.Length > 32) subName = subName[..32];

		// Build name
		var len = (name.Length + subName.Length + 10) * 4; // Approx. worst-case length
		var nativeName = stackalloc byte[len];
		var off = Encoding.UTF8.GetBytes(name, new(nativeName, len));
		if (subName.Length > 0) {
			nativeName[off++] = (byte)'.';
			off += Encoding.UTF8.GetBytes(subName, new(nativeName + off, len - off));
		}
		if (index.HasValue) {
			nativeName[off++] = (byte)'/';
			_ = index.Value.TryFormat(new Span<byte>(nativeName + off, len - off), out var writeCount);
			off += writeCount;
		}
		nativeName[off] = 0;

		// Set name
		VkDebugUtilsObjectNameInfoEXT info = new() {
			ObjectHandle = handle.Handle,
			ObjectType = handle.ObjectType,
			PObjectName = nativeName
		};
		vkSetDebugUtilsObjectNameEXT(Device, &info);
	}
	
	// Debug message callback
	private static VkBool32 HandleDebugMessage(
		VkDebugUtilsMessageSeverityFlagsEXT severity,
		VkDebugUtilsMessageTypeFlagsEXT type,
		VkDebugUtilsMessengerCallbackDataEXT* data,
		void* userData
	)
	{
		// Get severity/type chars
		var sevChar = severity switch {
			VkDebugUtilsMessageSeverityFlagsEXT.VerboseEXT => 'V',
			VkDebugUtilsMessageSeverityFlagsEXT.InfoEXT    => 'I',
			VkDebugUtilsMessageSeverityFlagsEXT.WarningEXT => 'W',
			VkDebugUtilsMessageSeverityFlagsEXT.ErrorEXT   => 'E',
			_                                              => '?'
		};
		var typChar = type switch {
			VkDebugUtilsMessageTypeFlagsEXT.GeneralEXT     => 'G',
			VkDebugUtilsMessageTypeFlagsEXT.ValidationEXT  => 'V',
			VkDebugUtilsMessageTypeFlagsEXT.PerformanceEXT => 'P',
			_                                              => '?'
		};

		// Get the primary object name if available
		string? objName = null;
		VkObjectType objType = default;
		if ((data->ObjectCount > 0) && (data->PObjects[0].PObjectName != null)) {
			objName = Marshal.PtrToStringUTF8((nint)data->PObjects[0].PObjectName);
			objType = data->PObjects[0].ObjectType;
		}

		// Build the message
		var sb = _MessageBuilder.Value!;
		sb.Clear();
		sb.Append('[').Append(typChar).Append('|').Append(sevChar).Append("]: ");
		if (objName is not null) {
			sb.Append("('").Append(objName).Append("': ").Append(objType).Append(") ");
		}
		sb.Append(Marshal.PtrToStringUTF8((nint)data->PMessage));

		// Write the message
		lock (_LogLock) {
			_LogWriter?.WriteLine(sb);
			_LogWriter?.Flush();
		}

		return VK_TRUE;
	}
	#endregion // Debug

	
	// Static ctor performs instance initialization, debug setup, and device population
	static VulkanCtx()
	{
		const string VALIDATION_LAYER_NAME = "VK_LAYER_KHRONOS_validation";

		// Check and report runtime
		uint instVer;
		if (vkEnumerateInstanceVersion(&instVer) != VkResult.Success) {
			throw new PlatformNotSupportedException("Vulkan runtime not installed (failed to enumerate version)");
		}
		if (instVer < VK_API_VERSION_1_1) {
			throw new PlatformNotSupportedException("Vulkan runtime is out of date (1.1+ not found)");
		}
		EngineInfo($"Found Vulkan runtime 1.{VK_API_VERSION_MINOR(instVer)}.{VK_API_VERSION_PATCH(instVer)}");

		// Check instance extensions
		InstanceExtensions = InstanceExtensionSet.Load();
		if (!InstanceExtensions.KHR_surface) {
			throw new PlatformNotSupportedException("KHR_surface not supported (headless system?)");
		}
		if (!InstanceExtensions.Get(SurfaceExtensionName)) {
			throw new PlatformNotSupportedException($"{SurfaceExtensionName} not supported (headless system?)");
		}

		// List of instance extensions
		using var extList = new NativeStringListUtf8(3);
		extList.Add(VK_KHR_SURFACE_EXTENSION_NAME);
		extList.Add(SurfaceExtensionName);

		// Check for debug and validation
		HasDebug = HasValidation = false;
#if DEBUG
		const string VALIDATION_DISABLE_ENV = "ASTRUM_DISABLE_VALIDATION";
		if (InstanceExtensions.EXT_debug_utils) {
			HasDebug = true;
			extList.Add(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
			if (Environment.GetEnvironmentVariable(VALIDATION_DISABLE_ENV) is null or "0") {
				HasValidation = InstanceLayerSet.Load().Has(VALIDATION_LAYER_NAME);
			}
		}
#endif // DEBUG

		// Populate app info
		using var appNameNative = new NativeStringUtf8(Core.ApplicationName);
		using var engNameNative = new NativeStringUtf8("Astrum");
		var appVer = Core.ApplicationAssembly.GetName().Version ?? new();
		var engVer = typeof(VulkanCtx).Assembly.GetName().Version!;
		VkApplicationInfo appInfo = new() {
			PApplicationName = appNameNative.DataPtr,
			ApplicationVersion = VK_MAKE_API_VERSION(0, (uint)appVer.Major, (uint)appVer.Minor, (uint)appVer.Revision),
			PEngineName = engNameNative.DataPtr,
			EngineVersion = VK_MAKE_API_VERSION(0, (uint)engVer.Major, (uint)engVer.Major, (uint)engVer.Revision),
			ApiVersion = VK_API_VERSION_1_1
		};

		// Create the instance
		using NativeStringListUtf8 layerList = new(HasValidation ? 1u : 0);
		if (HasValidation) layerList.Add(VALIDATION_LAYER_NAME);
		VkInstanceCreateInfo instanceInfo = new() {
			PApplicationInfo = &appInfo,
			PPEnabledExtensionNames = extList.Data,
			EnabledExtensionCount = extList.Count,
			PPEnabledLayerNames = layerList.Data,
			EnabledLayerCount = layerList.Count
		};
		VkInstance instance;
		vkCreateInstance(&instanceInfo, null, &instance).ThrowIfError();
		Instance = instance;
		VulkanLoader.LoadInstanceFunctions(Instance);
		EngineInfo("Created global Vulkan instance");

		// Setup the debug logger if present
#if DEBUG
		if (HasDebug) {
			// Try to open the log file
			try {
				_LogWriter = new(File.Open(LOG_PATH, FileMode.Create, FileAccess.Write, FileShare.Read));
				EngineInfo("Opened Vulkan debug log file");
			}
			catch (Exception ex) {
				_LogWriter = null;
				EngineError("Could not open Vulkan debug log file", ex);
				HasDebug = false;
			}

			// Create debug messenger
			if (HasDebug) {
				VkDebugUtilsMessengerCreateInfoEXT create = new() {
					MessageSeverity = VkDebugUtilsMessageSeverityFlagsEXT.InfoEXT |
						VkDebugUtilsMessageSeverityFlagsEXT.WarningEXT | VkDebugUtilsMessageSeverityFlagsEXT.ErrorEXT,
					MessageType = VkDebugUtilsMessageTypeFlagsEXT.GeneralEXT |
						VkDebugUtilsMessageTypeFlagsEXT.PerformanceEXT | VkDebugUtilsMessageTypeFlagsEXT.ValidationEXT,
					PfnUserCallback =
						(delegate* unmanaged<VkDebugUtilsMessageSeverityFlagsEXT, VkDebugUtilsMessageTypeFlagsEXT,
							VkDebugUtilsMessengerCallbackDataEXT*, void*, VkBool32>)
						Marshal.GetFunctionPointerForDelegate(_DebugMessageCallbackInst).ToPointer(),
					PUserData = null
				};
				VkDebugUtilsMessengerEXT messenger;
				if (vkCreateDebugUtilsMessengerEXT(instance, &create, null, &messenger) == VkResult.Success) {
					_DebugMessenger = messenger;
				}
				else {
					EngineError("Could not create debug messenger, debug logging is disabled");
					_LogWriter!.WriteLine("FAILED TO CREATE DEBUG LOGGER");
					_LogWriter.Dispose();
					_LogWriter = null;
				}
			}
		}
#endif // DEBUG

		// Setup the shutdown callback
		Core.ProcessExitQueue.Enqueue(static () => {
			// Shutdown logger if needed
			if (_DebugMessenger) vkDestroyDebugUtilsMessengerEXT(Instance, _DebugMessenger, null);
			lock (_LogLock) _LogWriter?.Dispose();

			// Destroy instance
			vkDestroyInstance(Instance, null);

			EngineInfo("Destroyed global Vulkan instance");
		}, 1_000);

		// Report
		EngineInfo($"Created Vulkan render driver (validation={HasValidation})");
	}
	
	
	// Device queue descriptor
	public readonly struct DeviceQueue(VkQueue handle, uint family, object @lock) : IEquatable<DeviceQueue>
	{
		public readonly VkQueue Handle = handle;
		public readonly uint Family = family;
		public readonly object Lock = @lock;

		public override int GetHashCode() => Handle.GetHashCode();
		public override bool Equals(object? obj) => obj is DeviceQueue q && Handle == q.Handle;
		public bool Equals(DeviceQueue q) => Handle == q.Handle;
		public static bool operator == (DeviceQueue l, DeviceQueue r) => l.Handle == r.Handle;
		public static bool operator != (DeviceQueue l, DeviceQueue r) => l.Handle != r.Handle;
	}
	
	// Device memory type descriptor
	public readonly struct MemoryType(uint index, VkMemoryType type) : IEquatable<MemoryType>
	{
		public readonly uint Index = index;
		public readonly VkMemoryType Type = type;
		
		public override int GetHashCode() => Index.GetHashCode();
		public override bool Equals(object? obj) => obj is MemoryType q && Index == q.Index;
		public bool Equals(MemoryType q) => Index == q.Index;
		public static bool operator == (MemoryType l, MemoryType r) => l.Index == r.Index;
		public static bool operator != (MemoryType l, MemoryType r) => l.Index != r.Index;
	}
}
