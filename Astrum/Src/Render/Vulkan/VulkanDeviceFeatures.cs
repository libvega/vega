﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using static LLVK.Vulkan;
using static LLVK.Vulkan.KHR;
using static LLVK.Vulkan.EXT;

namespace Astrum.Render.Vulkan;


// The structure chain for device features required by Astrum
internal sealed class VulkanDeviceFeatures : VulkanStructureChain<
	VkPhysicalDeviceFeatures2, VkPhysicalDeviceVulkan11Features, 
	VkPhysicalDeviceScalarBlockLayoutFeaturesEXT, VkPhysicalDeviceTimelineSemaphoreFeaturesKHR, 
	VkPhysicalDeviceDescriptorIndexingFeaturesEXT, VkPhysicalDeviceBufferDeviceAddressFeaturesEXT> 
{
	public ref VkPhysicalDeviceFeatures Features => ref Value0.Features;
	public ref VkPhysicalDeviceVulkan11Features Vulkan11 => ref Value1;
	public ref VkPhysicalDeviceScalarBlockLayoutFeaturesEXT ScalarBlockLayout => ref Value2;
	public ref VkPhysicalDeviceTimelineSemaphoreFeaturesKHR TimelineSemaphore => ref Value3;
	public ref VkPhysicalDeviceDescriptorIndexingFeaturesEXT DescriptorIndexing => ref Value4;
	public ref VkPhysicalDeviceBufferDeviceAddressFeaturesEXT BufferDeviceAddress => ref Value5;
}
