﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using static LLVK.Vulkan;
using static LLVK.Vulkan.EXT;

namespace Astrum.Render.Vulkan;


// The structure chain for device properties required by Astrum
internal sealed class VulkanDeviceProperties : VulkanStructureChain<
	VkPhysicalDeviceProperties2, VkPhysicalDeviceVulkan11Properties, 
	VkPhysicalDeviceDescriptorIndexingPropertiesEXT>
{
	public ref VkPhysicalDeviceProperties Properties => ref Value0.Properties;
	public ref VkPhysicalDeviceVulkan11Properties Vulkan11 => ref Value1;
	public ref VkPhysicalDeviceDescriptorIndexingPropertiesEXT DescriptorIndexing => ref Value2;
}
