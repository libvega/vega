﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
using static LLVK.Vulkan;

namespace Astrum.Render.Vulkan;


// Contains various static utilities for working with Vulkan values
internal static class VulkanUtils
{
	// Gets the aspect flags for the given format
	//   Note: only works for depth/stencil/color, planes are not checked (not an issue for Astrum)
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static VkImageAspectFlags GetAspectFlags(this VkFormat format) => format switch {
		VkFormat.D16Unorm or VkFormat.D32Sfloat  => VkImageAspectFlags.Depth,
		VkFormat.D16UnormS8Uint or VkFormat.D24UnormS8Uint or VkFormat.D32SfloatS8Uint => 
			VkImageAspectFlags.Depth | VkImageAspectFlags.Stencil,
		_ => VkImageAspectFlags.Color
	};
	
	// Gets the source barrier flags for a write to a buffer
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void GetWriteSrcBarrier(
		this VkBufferUsageFlags usage, out VkPipelineStageFlags stages, out VkAccessFlags access
	)
	{
		stages = default;
		access = default;
		if (usage.HasFlag(VkBufferUsageFlags.StorageBuffer)) {
			stages = VkPipelineStageFlags.ComputeShader | VkPipelineStageFlags.FragmentShader;
			access = VkAccessFlags.MemoryRead;
		}
		else if (usage.HasFlag(VkBufferUsageFlags.VertexBuffer) || usage.HasFlag(VkBufferUsageFlags.IndexBuffer)) {
			stages = VkPipelineStageFlags.VertexInput;
			access = VkAccessFlags.VertexAttributeRead | VkAccessFlags.IndexRead;
		}
	}
	
	// Gets the destination barrier flags for a write to a buffer
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void GetWriteDstBarrier(
		this VkBufferUsageFlags usage, out VkPipelineStageFlags stages, out VkAccessFlags access
	)
	{
		stages = default;
		access = default;
		if (usage.HasFlag(VkBufferUsageFlags.StorageBuffer)) {
			stages = VkPipelineStageFlags.ComputeShader | VkPipelineStageFlags.VertexShader;
			access = VkAccessFlags.MemoryRead;
		}
		if (usage.HasFlag(VkBufferUsageFlags.VertexBuffer) || usage.HasFlag(VkBufferUsageFlags.IndexBuffer)) {
			stages |= VkPipelineStageFlags.VertexInput;
			if (access == default) access = VkAccessFlags.VertexAttributeRead | VkAccessFlags.IndexRead;
		}
	}
	
	// Gets the source barrier flags for a write to an image
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void GetWriteSrcBarrier(
		this VkImageUsageFlags usage, out VkPipelineStageFlags stages, out VkAccessFlags access
	)
	{
		stages = VkPipelineStageFlags.ComputeShader | VkPipelineStageFlags.FragmentShader;
		access = usage.HasFlag(VkImageUsageFlags.Storage) 
			? VkAccessFlags.MemoryRead | VkAccessFlags.MemoryWrite 
			: VkAccessFlags.ShaderRead;
	}
	
	// Gets the destination barrier flags for a write to an image
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void GetWriteDstBarrier(
		this VkImageUsageFlags usage, out VkPipelineStageFlags stages, out VkAccessFlags access
	)
	{
		stages = VkPipelineStageFlags.ComputeShader | VkPipelineStageFlags.VertexShader;
		access = usage.HasFlag(VkImageUsageFlags.Storage) 
			? VkAccessFlags.MemoryRead | VkAccessFlags.MemoryWrite
			: VkAccessFlags.ShaderRead;
	}
}
