﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>
/// Encapsulates logic and resources for specific application stages/phases into type-safe controllers.
/// </summary>
/// <remarks>Scenes are always loaded asynchronously, and can be staged and unloaded during transitions.</remarks>
public abstract class Scene
{
	
}
