/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

global using NUnit.Framework;


// The global setup and teardown for tests
[SetUpFixture]
internal class GlobalFixture
{
	[OneTimeSetUp]
	public void SetupTests()
	{

	}

	[OneTimeTearDown]
	public void TeardownTests()
	{

	}
}
