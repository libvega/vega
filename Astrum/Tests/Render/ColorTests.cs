﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Maths;
using Astrum.Render;

namespace Astrum.Tests.Render;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Color struct
internal class ColorTests
{
	[Test]
	public void Color_TexelFormat() => Assert.That(Color.TexelFormat, Is.EqualTo(TexelFormat.Color));
	
	[Test]
	public void Color_Components()
	{
		Color tblack = default;
		Assert.That(tblack.R,     Is.EqualTo(0));
		Assert.That(tblack.G,     Is.EqualTo(0));
		Assert.That(tblack.B,     Is.EqualTo(0));
		Assert.That(tblack.A,     Is.EqualTo(0));
		Assert.That(tblack.RNorm, Is.EqualTo(0f));
		Assert.That(tblack.GNorm, Is.EqualTo(0f));
		Assert.That(tblack.BNorm, Is.EqualTo(0f));
		Assert.That(tblack.ANorm, Is.EqualTo(0f));

		Color owhite = new() { R = 255, G = 255, B = 255, A = 255 };
		Assert.That(owhite.R,     Is.EqualTo(255));
		Assert.That(owhite.G,     Is.EqualTo(255));
		Assert.That(owhite.B,     Is.EqualTo(255));
		Assert.That(owhite.A,     Is.EqualTo(255));
		Assert.That(owhite.RNorm, Is.EqualTo(1f));
		Assert.That(owhite.GNorm, Is.EqualTo(1f));
		Assert.That(owhite.BNorm, Is.EqualTo(1f));
		Assert.That(owhite.ANorm, Is.EqualTo(1f));

		Color fcolor = new() { RNorm = 0.25f, GNorm = 0.5f, BNorm = 0.75f, ANorm = 1f };
		Assert.That(fcolor.R,     Is.EqualTo(63));
		Assert.That(fcolor.G,     Is.EqualTo(127));
		Assert.That(fcolor.B,     Is.EqualTo(191));
		Assert.That(fcolor.A,     Is.EqualTo(255));
		Assert.That(fcolor.RNorm, Is.EqualTo(0.25f).Within(1f / 255));
		Assert.That(fcolor.GNorm, Is.EqualTo(0.5f).Within(1f / 255));
		Assert.That(fcolor.BNorm, Is.EqualTo(0.75f).Within(1f / 255));
		Assert.That(fcolor.ANorm, Is.EqualTo(1f).Within(1f / 255));

		Color normColor = Color.FromNorm(0f, 0.25f, 0.5f, 0.75f);
		Assert.That(normColor.R,     Is.EqualTo(0));
		Assert.That(normColor.G,     Is.EqualTo(63));
		Assert.That(normColor.B,     Is.EqualTo(127));
		Assert.That(normColor.A,     Is.EqualTo(191));
		Assert.That(normColor.RNorm, Is.EqualTo(0f).Within(1f / 255));
		Assert.That(normColor.GNorm, Is.EqualTo(0.25f).Within(1f / 255));
		Assert.That(normColor.BNorm, Is.EqualTo(0.5f).Within(1f / 255));
		Assert.That(normColor.ANorm, Is.EqualTo(0.75f).Within(1f / 255));
	}
	
	[Test]
	public void Color_Fields()
	{
		Assert.That(Colors.TransparentBlack.Opaque, Is.EqualTo(Colors.Black));
		Assert.That(Colors.TransparentWhite.Opaque, Is.EqualTo(Colors.White));

		var gray = (32f / 255 * 0.2125f) + (75f / 255 * 0.7154f) + (180f / 255 * 0.0721f);
		Assert.That(new Color(32, 75, 180).Grayscale, Is.EqualTo(Color.FromNorm(gray, gray, gray)));
	}

	[Test]
	public void Color_Equality()
	{
		Assert.True(Colors.White.Equals(Colors.White));
		Assert.True(Colors.White.Equals((object)Colors.White));
		Assert.False(Colors.White.Equals(Colors.Black));
		Assert.False(Colors.White.Equals((object)Colors.Black));
		
		Assert.True(Colors.White == Colors.White);
		Assert.True(Colors.White != Colors.Black);
		Assert.False(Colors.White != Colors.White);
		Assert.False(Colors.White == Colors.Black);
	}

	[Test]
	public void Color_Deconstruct()
	{
		byte r, g, b, a;
		(r, g, b, a) = new Color(20, 40, 60, 80);
		Assert.That((r, g, b, a), Is.EqualTo((20, 40, 60, 80)));
	}

	[Test]
	public void Color_MathCasting()
	{
		Color c = new(20, 40, 60, 80);
		Assert.That(c.ToVector(), Is.EqualTo(new Vec4(20f / 255, 40f / 255, 60f / 255, 80f / 255)));
		Assert.That(c.ToPoint(), Is.EqualTo(new Point4U(20, 40, 60, 80)));
	}
}
