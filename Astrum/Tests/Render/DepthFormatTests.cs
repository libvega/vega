﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Render;
using static LLVK.Vulkan;

namespace Astrum.Tests.Render;


// Tests for DepthFormat enum
internal class DepthFormatTests
{
	[Test]
	public void DepthFormat_Count() => Assert.That(Enum.GetValues<DepthFormat>().Length, Is.EqualTo(3));

	[Test]
	public void DepthFormat_HasStencil()
	{
		Assert.False(DepthFormat.LowDepth.HasStencil());
		Assert.False(DepthFormat.Depth.HasStencil());
		Assert.True(DepthFormat.DepthStencil.HasStencil());
	}

	[Test]
	public void DepthFormat_ToVk()
	{
		Assert.That(DepthFormat.LowDepth.ToVk(),     Is.EqualTo(VkFormat.D16Unorm));
		Assert.That(DepthFormat.Depth.ToVk(),        Is.EqualTo(VkFormat.D32Sfloat));
		Assert.That(DepthFormat.DepthStencil.ToVk(), Is.EqualTo(OperatingSystem.IsAndroid() ? VkFormat.D24UnormS8Uint : VkFormat.D32SfloatS8Uint));
	}
}
