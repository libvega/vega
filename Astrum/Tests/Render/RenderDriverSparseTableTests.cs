﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Render;

#pragma warning disable NUnit2009
#pragma warning disable NUnit2010

namespace Astrum.Tests.Render;

using IntTable = RenderDriver.SparseTable<int>;


// Tests for the SparseTable<T> type in RenderDriver
internal unsafe class RenderDriverSparseTableTests
{
	[Test]
	public void RenderDriver_SparseTable_DefaultState()
	{
		using var table = new IntTable();
		Assert.That(table.AllocationCount, Is.EqualTo(0));
	}

	[Test]
	public void RenderDriver_SparseTable_DefaultHandle()
	{
		IntTable.Handle handle = default;
		Assert.That(handle.IsNull);
		Assert.That(!handle.IsValid);
		Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
	}

	[Test]
	[TestCase(1u), TestCase(255u), TestCase(256u), TestCase(512u)]
	public void RenderDriver_SparseTable_AllocateFree(uint size)
	{
		using var table = new IntTable();

		for (var i = 0; i < size; ++i) {
			var handle = table.Allocate();
			Assert.That(!handle.IsNull);
			Assert.That(handle.IsValid);
			Assert.That((nuint)handle.Data, Is.Not.EqualTo((nuint)0));
			*handle.Data = i;
			_Handles[i] = handle;
		}
		Assert.That(table.AllocationCount, Is.EqualTo(size));

		for (var i = 0; i < size; ++i) Assert.That(*_Handles[i].Data, Is.EqualTo(i));

		for (var i = 0; i < size; ++i) {
			var handle = _Handles[i];
			table.Free(handle);
			Assert.That(!handle.IsNull);
			Assert.That(!handle.IsValid);
			Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
		}
		Assert.That(table.AllocationCount, Is.EqualTo(0));
	}
	private static readonly IntTable.Handle[] _Handles = new IntTable.Handle[512];

	[Test]
	public void RenderDriver_SparseTable_FailAllocateWhenFull()
	{
		using var table = new IntTable();
		for (var i = 0; i < IntTable.MAX_ENTRIES; ++i) _ = table.Allocate();

		Assert.That(table.AllocationCount, Is.EqualTo(IntTable.MAX_ENTRIES));
		Assert.That(table.Allocate().IsNull);
	}

	[Test]
	public void RenderDriver_SparseTable_NoDoubleFree()
	{
		using var table = new IntTable();
		var handle = table.Allocate();
		Assert.That(table.Free(handle));
		Assert.That(!table.Free(handle));
	}
}
