﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Render;

namespace Astrum.Tests.Render;


// Tests for ScalarKind enum
internal class ScalarKindTests
{
	[Test]
	public void ScalarKind_Count() => Assert.That(Enum.GetValues<ScalarKind>().Length, Is.EqualTo(5));

	[Test]
	public void ScalarKind_IsMethods()
	{
		Assert.True(ScalarKind.SignedInt.IsInteger());
		Assert.True(ScalarKind.UnsignedInt.IsInteger());
		Assert.False(ScalarKind.SignedNorm.IsInteger());
		Assert.False(ScalarKind.UnsignedNorm.IsInteger());
		Assert.False(ScalarKind.Float.IsInteger());
		
		Assert.False(ScalarKind.SignedInt.IsFloat());
		Assert.False(ScalarKind.UnsignedInt.IsFloat());
		Assert.True(ScalarKind.SignedNorm.IsFloat());
		Assert.True(ScalarKind.UnsignedNorm.IsFloat());
		Assert.True(ScalarKind.Float.IsFloat());
	}
}
