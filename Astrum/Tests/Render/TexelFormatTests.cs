﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Render;
using static LLVK.Vulkan;

namespace Astrum.Tests.Render;


// Tests for TexelFormat enum
internal class TexelFormatTests
{
	[Test]
	public void TexelFormat_Count() => Assert.That(Enum.GetValues<TexelFormat>().Length, Is.EqualTo(42));

	[Test]
	public void TexelFormat_ColorEquality() => Assert.That(TexelFormat.Color, Is.EqualTo(TexelFormat.UNorm4));
	
	[Test]
	public void TexelFormat_GetChannelCount()
	{
		Assert.That(TexelFormat.Undefined.GetChannelCount(),  Is.EqualTo(0));
		Assert.That(TexelFormat.UNorm.GetChannelCount(),      Is.EqualTo(1));
		Assert.That(TexelFormat.UNorm2.GetChannelCount(),     Is.EqualTo(2));
		Assert.That(TexelFormat.UNorm4.GetChannelCount(),     Is.EqualTo(4));
		Assert.That(TexelFormat.Unorm4Bgra.GetChannelCount(), Is.EqualTo(4));
		Assert.That(TexelFormat.SNorm.GetChannelCount(),      Is.EqualTo(1));
		Assert.That(TexelFormat.SNorm2.GetChannelCount(),     Is.EqualTo(2));
		Assert.That(TexelFormat.SNorm4.GetChannelCount(),     Is.EqualTo(4));
		Assert.That(TexelFormat.U16Norm.GetChannelCount(),    Is.EqualTo(1));
		Assert.That(TexelFormat.U16Norm2.GetChannelCount(),   Is.EqualTo(2));
		Assert.That(TexelFormat.U16Norm4.GetChannelCount(),   Is.EqualTo(4));
		Assert.That(TexelFormat.S16Norm.GetChannelCount(),    Is.EqualTo(1));
		Assert.That(TexelFormat.S16Norm2.GetChannelCount(),   Is.EqualTo(2));
		Assert.That(TexelFormat.S16Norm4.GetChannelCount(),   Is.EqualTo(4));
		Assert.That(TexelFormat.UByte.GetChannelCount(),      Is.EqualTo(1));
		Assert.That(TexelFormat.UByte2.GetChannelCount(),     Is.EqualTo(2));
		Assert.That(TexelFormat.UByte4.GetChannelCount(),     Is.EqualTo(4));
		Assert.That(TexelFormat.SByte.GetChannelCount(),      Is.EqualTo(1));
		Assert.That(TexelFormat.SByte2.GetChannelCount(),     Is.EqualTo(2));
		Assert.That(TexelFormat.SByte4.GetChannelCount(),     Is.EqualTo(4));
		Assert.That(TexelFormat.UShort.GetChannelCount(),     Is.EqualTo(1));
		Assert.That(TexelFormat.UShort2.GetChannelCount(),    Is.EqualTo(2));
		Assert.That(TexelFormat.UShort4.GetChannelCount(),    Is.EqualTo(4));
		Assert.That(TexelFormat.SShort.GetChannelCount(),     Is.EqualTo(1));
		Assert.That(TexelFormat.SShort2.GetChannelCount(),    Is.EqualTo(2));
		Assert.That(TexelFormat.SShort4.GetChannelCount(),    Is.EqualTo(4));
		Assert.That(TexelFormat.UInt.GetChannelCount(),       Is.EqualTo(1));
		Assert.That(TexelFormat.UInt2.GetChannelCount(),      Is.EqualTo(2));
		Assert.That(TexelFormat.UInt4.GetChannelCount(),      Is.EqualTo(4));
		Assert.That(TexelFormat.SInt.GetChannelCount(),       Is.EqualTo(1));
		Assert.That(TexelFormat.SInt2.GetChannelCount(),      Is.EqualTo(2));
		Assert.That(TexelFormat.SInt4.GetChannelCount(),      Is.EqualTo(4));
		Assert.That(TexelFormat.Half.GetChannelCount(),       Is.EqualTo(1));
		Assert.That(TexelFormat.Half2.GetChannelCount(),      Is.EqualTo(2));
		Assert.That(TexelFormat.Half4.GetChannelCount(),      Is.EqualTo(4));
		Assert.That(TexelFormat.Float.GetChannelCount(),      Is.EqualTo(1));
		Assert.That(TexelFormat.Float2.GetChannelCount(),     Is.EqualTo(2));
		Assert.That(TexelFormat.Float4.GetChannelCount(),     Is.EqualTo(4));
		Assert.That(TexelFormat.A2Bgr10.GetChannelCount(),    Is.EqualTo(4));
		Assert.That(TexelFormat.A1Rgb5.GetChannelCount(),     Is.EqualTo(4));
		Assert.That(TexelFormat.R5G6B5.GetChannelCount(),     Is.EqualTo(3));
	}
	
	[Test]
	public void TexelFormat_GetSize()
	{
		Assert.That(TexelFormat.Undefined.GetSize(),  Is.EqualTo(0));
		Assert.That(TexelFormat.UNorm.GetSize(),      Is.EqualTo(1));
		Assert.That(TexelFormat.UNorm2.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.UNorm4.GetSize(),     Is.EqualTo(4));
		Assert.That(TexelFormat.Unorm4Bgra.GetSize(), Is.EqualTo(4));
		Assert.That(TexelFormat.SNorm.GetSize(),      Is.EqualTo(1));
		Assert.That(TexelFormat.SNorm2.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.SNorm4.GetSize(),     Is.EqualTo(4));
		Assert.That(TexelFormat.U16Norm.GetSize(),    Is.EqualTo(2));
		Assert.That(TexelFormat.U16Norm2.GetSize(),   Is.EqualTo(4));
		Assert.That(TexelFormat.U16Norm4.GetSize(),   Is.EqualTo(8));
		Assert.That(TexelFormat.S16Norm.GetSize(),    Is.EqualTo(2));
		Assert.That(TexelFormat.S16Norm2.GetSize(),   Is.EqualTo(4));
		Assert.That(TexelFormat.S16Norm4.GetSize(),   Is.EqualTo(8));
		Assert.That(TexelFormat.UByte.GetSize(),      Is.EqualTo(1));
		Assert.That(TexelFormat.UByte2.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.UByte4.GetSize(),     Is.EqualTo(4));
		Assert.That(TexelFormat.SByte.GetSize(),      Is.EqualTo(1));
		Assert.That(TexelFormat.SByte2.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.SByte4.GetSize(),     Is.EqualTo(4));
		Assert.That(TexelFormat.UShort.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.UShort2.GetSize(),    Is.EqualTo(4));
		Assert.That(TexelFormat.UShort4.GetSize(),    Is.EqualTo(8));
		Assert.That(TexelFormat.SShort.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.SShort2.GetSize(),    Is.EqualTo(4));
		Assert.That(TexelFormat.SShort4.GetSize(),    Is.EqualTo(8));
		Assert.That(TexelFormat.UInt.GetSize(),       Is.EqualTo(4));
		Assert.That(TexelFormat.UInt2.GetSize(),      Is.EqualTo(8));
		Assert.That(TexelFormat.UInt4.GetSize(),      Is.EqualTo(16));
		Assert.That(TexelFormat.SInt.GetSize(),       Is.EqualTo(4));
		Assert.That(TexelFormat.SInt2.GetSize(),      Is.EqualTo(8));
		Assert.That(TexelFormat.SInt4.GetSize(),      Is.EqualTo(16));
		Assert.That(TexelFormat.Half.GetSize(),       Is.EqualTo(2));
		Assert.That(TexelFormat.Half2.GetSize(),      Is.EqualTo(4));
		Assert.That(TexelFormat.Half4.GetSize(),      Is.EqualTo(8));
		Assert.That(TexelFormat.Float.GetSize(),      Is.EqualTo(4));
		Assert.That(TexelFormat.Float2.GetSize(),     Is.EqualTo(8));
		Assert.That(TexelFormat.Float4.GetSize(),     Is.EqualTo(16));
		Assert.That(TexelFormat.A2Bgr10.GetSize(),    Is.EqualTo(4));
		Assert.That(TexelFormat.A1Rgb5.GetSize(),     Is.EqualTo(2));
		Assert.That(TexelFormat.R5G6B5.GetSize(),     Is.EqualTo(4));
	}
	
	[Test]
	public void TexelFormat_GetScalarKind()
	{
		Assert.That(TexelFormat.Undefined.GetScalarKind(),  Is.EqualTo(default(ScalarKind)));
		Assert.That(TexelFormat.UNorm.GetScalarKind(),      Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.UNorm2.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.UNorm4.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.Unorm4Bgra.GetScalarKind(), Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.SNorm.GetScalarKind(),      Is.EqualTo(ScalarKind.SignedNorm));
		Assert.That(TexelFormat.SNorm2.GetScalarKind(),     Is.EqualTo(ScalarKind.SignedNorm));
		Assert.That(TexelFormat.SNorm4.GetScalarKind(),     Is.EqualTo(ScalarKind.SignedNorm));
		Assert.That(TexelFormat.U16Norm.GetScalarKind(),    Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.U16Norm2.GetScalarKind(),   Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.U16Norm4.GetScalarKind(),   Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.S16Norm.GetScalarKind(),    Is.EqualTo(ScalarKind.SignedNorm));
		Assert.That(TexelFormat.S16Norm2.GetScalarKind(),   Is.EqualTo(ScalarKind.SignedNorm));
		Assert.That(TexelFormat.S16Norm4.GetScalarKind(),   Is.EqualTo(ScalarKind.SignedNorm));
		Assert.That(TexelFormat.UByte.GetScalarKind(),      Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.UByte2.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.UByte4.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.SByte.GetScalarKind(),      Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.SByte2.GetScalarKind(),     Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.SByte4.GetScalarKind(),     Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.UShort.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.UShort2.GetScalarKind(),    Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.UShort4.GetScalarKind(),    Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.SShort.GetScalarKind(),     Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.SShort2.GetScalarKind(),    Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.SShort4.GetScalarKind(),    Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.UInt.GetScalarKind(),       Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.UInt2.GetScalarKind(),      Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.UInt4.GetScalarKind(),      Is.EqualTo(ScalarKind.UnsignedInt));
		Assert.That(TexelFormat.SInt.GetScalarKind(),       Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.SInt2.GetScalarKind(),      Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.SInt4.GetScalarKind(),      Is.EqualTo(ScalarKind.SignedInt));
		Assert.That(TexelFormat.Half.GetScalarKind(),       Is.EqualTo(ScalarKind.Float));
		Assert.That(TexelFormat.Half2.GetScalarKind(),      Is.EqualTo(ScalarKind.Float));
		Assert.That(TexelFormat.Half4.GetScalarKind(),      Is.EqualTo(ScalarKind.Float));
		Assert.That(TexelFormat.Float.GetScalarKind(),      Is.EqualTo(ScalarKind.Float));
		Assert.That(TexelFormat.Float2.GetScalarKind(),     Is.EqualTo(ScalarKind.Float));
		Assert.That(TexelFormat.Float4.GetScalarKind(),     Is.EqualTo(ScalarKind.Float));
		Assert.That(TexelFormat.A2Bgr10.GetScalarKind(),    Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.A1Rgb5.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedNorm));
		Assert.That(TexelFormat.R5G6B5.GetScalarKind(),     Is.EqualTo(ScalarKind.UnsignedNorm));
	}
	
	[Test]
	public void TexelFormat_ToVk()
	{
		Assert.That(TexelFormat.Undefined.ToVk(),  Is.EqualTo(VkFormat.Undefined));
		Assert.That(TexelFormat.UNorm.ToVk(),      Is.EqualTo(VkFormat.R8Unorm));
		Assert.That(TexelFormat.UNorm2.ToVk(),     Is.EqualTo(VkFormat.R8g8Unorm));
		Assert.That(TexelFormat.UNorm4.ToVk(),     Is.EqualTo(VkFormat.R8g8b8a8Unorm));
		Assert.That(TexelFormat.Unorm4Bgra.ToVk(), Is.EqualTo(VkFormat.B8g8r8a8Unorm));
		Assert.That(TexelFormat.SNorm.ToVk(),      Is.EqualTo(VkFormat.R8Snorm));
		Assert.That(TexelFormat.SNorm2.ToVk(),     Is.EqualTo(VkFormat.R8g8Snorm));
		Assert.That(TexelFormat.SNorm4.ToVk(),     Is.EqualTo(VkFormat.R8g8b8a8Snorm));
		Assert.That(TexelFormat.U16Norm.ToVk(),    Is.EqualTo(VkFormat.R16Unorm));
		Assert.That(TexelFormat.U16Norm2.ToVk(),   Is.EqualTo(VkFormat.R16g16Unorm));
		Assert.That(TexelFormat.U16Norm4.ToVk(),   Is.EqualTo(VkFormat.R16g16b16a16Unorm));
		Assert.That(TexelFormat.S16Norm.ToVk(),    Is.EqualTo(VkFormat.R16Snorm));
		Assert.That(TexelFormat.S16Norm2.ToVk(),   Is.EqualTo(VkFormat.R16g16Snorm));
		Assert.That(TexelFormat.S16Norm4.ToVk(),   Is.EqualTo(VkFormat.R16g16b16a16Snorm));
		Assert.That(TexelFormat.UByte.ToVk(),      Is.EqualTo(VkFormat.R8Uint));
		Assert.That(TexelFormat.UByte2.ToVk(),     Is.EqualTo(VkFormat.R8g8Uint));
		Assert.That(TexelFormat.UByte4.ToVk(),     Is.EqualTo(VkFormat.R8g8b8a8Uint));
		Assert.That(TexelFormat.SByte.ToVk(),      Is.EqualTo(VkFormat.R8Sint));
		Assert.That(TexelFormat.SByte2.ToVk(),     Is.EqualTo(VkFormat.R8g8Sint));
		Assert.That(TexelFormat.SByte4.ToVk(),     Is.EqualTo(VkFormat.R8g8b8a8Sint));
		Assert.That(TexelFormat.UShort.ToVk(),     Is.EqualTo(VkFormat.R16Uint));
		Assert.That(TexelFormat.UShort2.ToVk(),    Is.EqualTo(VkFormat.R16g16Uint));
		Assert.That(TexelFormat.UShort4.ToVk(),    Is.EqualTo(VkFormat.R16g16b16a16Uint));
		Assert.That(TexelFormat.SShort.ToVk(),     Is.EqualTo(VkFormat.R16Sint));
		Assert.That(TexelFormat.SShort2.ToVk(),    Is.EqualTo(VkFormat.R16g16Sint));
		Assert.That(TexelFormat.SShort4.ToVk(),    Is.EqualTo(VkFormat.R16g16b16a16Sint));
		Assert.That(TexelFormat.UInt.ToVk(),       Is.EqualTo(VkFormat.R32Uint));
		Assert.That(TexelFormat.UInt2.ToVk(),      Is.EqualTo(VkFormat.R32g32Uint));
		Assert.That(TexelFormat.UInt4.ToVk(),      Is.EqualTo(VkFormat.R32g32b32a32Uint));
		Assert.That(TexelFormat.SInt.ToVk(),       Is.EqualTo(VkFormat.R32Sint));
		Assert.That(TexelFormat.SInt2.ToVk(),      Is.EqualTo(VkFormat.R32g32Sint));
		Assert.That(TexelFormat.SInt4.ToVk(),      Is.EqualTo(VkFormat.R32g32b32a32Sint));
		Assert.That(TexelFormat.Half.ToVk(),       Is.EqualTo(VkFormat.R16Sfloat));
		Assert.That(TexelFormat.Half2.ToVk(),      Is.EqualTo(VkFormat.R16g16Sfloat));
		Assert.That(TexelFormat.Half4.ToVk(),      Is.EqualTo(VkFormat.R16g16b16a16Sfloat));
		Assert.That(TexelFormat.Float.ToVk(),      Is.EqualTo(VkFormat.R32Sfloat));
		Assert.That(TexelFormat.Float2.ToVk(),     Is.EqualTo(VkFormat.R32g32Sfloat));
		Assert.That(TexelFormat.Float4.ToVk(),     Is.EqualTo(VkFormat.R32g32b32a32Sfloat));
		Assert.That(TexelFormat.A2Bgr10.ToVk(),    Is.EqualTo(VkFormat.A2b10g10r10UnormPack32));
		Assert.That(TexelFormat.A1Rgb5.ToVk(),     Is.EqualTo(VkFormat.A1r5g5b5UnormPack16));
		Assert.That(TexelFormat.R5G6B5.ToVk(),     Is.EqualTo(VkFormat.R5g6b5UnormPack16));
	}
}
