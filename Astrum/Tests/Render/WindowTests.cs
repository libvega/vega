﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Render;

namespace Astrum.Tests.Render;


// Tests for functionality relating to Window and Windows
internal class WindowTests
{
	[Test]
	public void Window_MaxCount() => Assert.That(Windows.AllWindows, Has.Count.EqualTo(Window.MAX_COUNT));
}
