## Contribution Guidelines

**TODO**

## Code Style

While we aren't super strict about code style, the `.editorconfig` file in the repo should be respected. Most IDEs will automatically detect this file and configure code style guidelines to match it. Please don't change this file unless you have a good reason.

### Comments

Comments (even simple ones) and whitespace do wonders to improve code readability. Hard to read code will likely not be accepted in pull requests. XML-style documentation should be placed on types, methods, fields, ect... that appear in the public API of the projects. Internal and private API symbols can have somewhat more lax commenting.

## Local Development

While application developers can just use the Astrum NuGet packages, if you are developing Astrum itself, and have a local application with which you are testing the Astrum changes, you can't rely on NuGet.

In order to set up local development with an application external to the Astrum solution, you must reference the build artifacts in your application. Add the following to your application `.csproj`:

```xml
<ItemGroup Condition=" '$(Configuration)' == 'Debug' ">
  <Reference Include="Astrum">
    <HintPath>\path\to\Astrum\Astrum\Src\bin\x64\Develop\net8.0\Astrum.dll</HintPath>
  </Reference>
  <Reference Include="Astrum.Common">
    <HintPath>\path\to\Astrum\Astrum\Src\bin\x64\Develop\net8.0\Astrum.Common.dll</HintPath>
  </Reference>
  <Content Include="\path\to\astrum\Astrum\Src\bin\x64\Develop\net8.0\runtimes\**"
           LinkBase="runtimes"
           CopyToOutputDirectory="Always"/>
</ItemGroup>

<ItemGroup Condition=" '$(Configuration)' == 'Release' ">
  <Reference Include="Astrum">
    <HintPath>\path\to\Astrum\Astrum\Src\bin\x64\Release\net8.0\Astrum.dll</HintPath>
  </Reference>
  <Reference Include="Astrum.Common">
    <HintPath>\path\to\Astrum\Astrum\Src\bin\x64\Release\net8.0\Astrum.Common.dll</HintPath>
  </Reference>
  <Content Include="\path\to\Astrum\Astrum\Src\bin\x64\Release\net8.0\runtimes\**"
           LinkBase="runtimes"
           CopyToOutputDirectory="Always"/>
</ItemGroup>
```

This will ensure that the latest build of Astrum and Astrum.Common are always in use by your application, which ensures an accurate and fast development and testing loop.
