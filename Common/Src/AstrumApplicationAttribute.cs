﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Linq;
using System.Reflection;

namespace Astrum;


/// <summary>Assembly-targeted attribute that provides static application configuration.</summary>
/// <remarks>Each Astrum application must have exactly one of these attributes defined on the assembly.</remarks>
[AttributeUsage(AttributeTargets.Assembly)]
public sealed class AstrumApplicationAttribute : Attribute
{
	#region Fields
	/// <summary>
	/// The human-readable name of the application, must contain only alphanumeric, '_', and '-' characters.
	/// </summary>
	public readonly string ApplicationName;

	/// <summary>The optionally specified tag to use for the default application logger.</summary>
	public string? DefaultLoggerTag { get; init; }

	// The attribute
	private static AstrumApplicationAttribute? _Instance;
	#endregion // Fields

	/// <summary>Define a new application attribute.</summary>
	/// <param name="appName">
	/// The human-readable name of the application, must contain only alphanumeric, '_', and '-' characters.
	/// </param>
	public AstrumApplicationAttribute(string appName) => ApplicationName = String.IsNullOrWhiteSpace(appName)
		? throw new ArgumentException("Cannot use a null or empty application name")
		: appName.Any(static ch => !Char.IsAsciiLetterOrDigit(ch) && ch != '_' && ch != '-')
			? throw new ArgumentException("Application names must only contain alphanumeric, '_', or '-' characters")
			: appName;


	// Gets the application attribute, throws if not defined
	internal static AstrumApplicationAttribute Get()
	{
		if (_Instance is not null) return _Instance;
		var entry = Assembly.GetEntryAssembly()
			?? throw new PlatformNotSupportedException("Cannot reflect the application entry assembly");
		if (entry.GetCustomAttribute<AstrumApplicationAttribute>() is not { } attr) {
			throw new InvalidOperationException("Astrum applications must contain an AstrumApplicationAttribute");
		}
		return _Instance = attr;
	}
}
