﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Astrum;


/// <summary>
/// The default <see cref="LogHandler"/> implementation, which can be configured for both console and file logging.
/// </summary>
public sealed unsafe class DefaultLogHandler : LogHandler
{
	/// <summary>The default value for keeping historical log files.</summary>
	public const uint DEFAULT_HISTORY = 10;

	// The maximum header length
	private const uint HEADER_LENGTH = 18 + Logger.TAG_LENGTH; // Format: "[HH:MM:SS.sss|TTTTTTTTTTTT|L] "

	// Default file names
	internal const string LOG_FILE_NAME = "latest.log";
	internal const string ENGINE_LOG_FILE_NAME = "engine.log";

	// Historical file names
	internal const string HISTORY_FILE_DATETIME_FORMAT = "dd-MM-yy.HH-mm-ss";
	internal const string HISTORY_FILE_DATETIME_REGEX = /*language=regex*/ @"^\d\d-\d\d-\d\d\.\d\d-\d\d-\d\d";
	internal const string LOG_FILE_HISTORY_SUFFIX = ".log";
	internal const string ENGINE_LOG_FILE_HISTORY_SUFFIX = ".engine.log";

	// Regex for historical files
	internal static readonly Regex LogFileHistoryRegex =
		new(HISTORY_FILE_DATETIME_REGEX + LOG_FILE_HISTORY_SUFFIX.Replace(".", @"\.") + '^');
	internal static readonly Regex EngineLogFileHistoryRegex =
		new(HISTORY_FILE_DATETIME_REGEX + ENGINE_LOG_FILE_HISTORY_SUFFIX.Replace(".", @"\.") + '^');


	#region Fields
	// Thread-safe StringBuilder for formatting exceptions
	private static readonly ThreadLocal<StringBuilder> _ExceptionFormatter = new(() => new(4_096), false);


	/// <summary>If the handler is logging to the console.</summary>
	public readonly bool UseConsole;
	/// <summary>If the handler is logging to a file.</summary>
	public readonly bool UseFile;
	/// <summary>If the handler is logging engine messages.</summary>
	public bool LogEngine => EngineLevelMask != LogLevel.None;

	// The file history size
	private readonly uint _history;

	// Log files, if present
	private StreamWriter? _logFile;
	private StreamWriter? _engineLogFile;
	#endregion // Fields


	/// <summary>Create a new default log handler.</summary>
	/// <param name="useConsole">If the handler should write to the console.</param>
	/// <param name="useFile">If the handler should write to a log file.</param>
	/// <param name="logEngine">If the handler should log internal engine messages.</param>
	/// <param name="history">The number of old log files to keep before deleting the oldest.</param>
	public DefaultLogHandler(bool useConsole, bool useFile, bool logEngine, uint history)
	{
		UseConsole = useConsole;
		UseFile = useFile;
		if (!logEngine) EngineLevelMask = LogLevel.None;
		_history = history;
	}


	protected override void OnOpen()
	{
		// Create or cleanup log dir
		var logDir = new DirectoryInfo(Path.Combine(
			Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location ?? Environment.CurrentDirectory)!, "logs"));
		if (UseFile || LogEngine) {
			if (!logDir.Exists) logDir.Create();
			else CleanupLogDir(logDir, _history);
		}

		var now = DateTime.Now;
		var timeStr = now.ToString("HH:mm:ss");
		var dateStr = now.ToString("dd-MMM-yyyy");

		// Create the log file
		if (UseFile) {
			var logPath = Path.Combine(logDir.FullName, LOG_FILE_NAME);
			_logFile = new(
				File.Open(logPath, FileMode.Create, FileAccess.Write, FileShare.Read),
				Encoding.UTF8, 16_384, false);
			_logFile.WriteLine($"Log file opened at {timeStr} on {dateStr}");
			_logFile.WriteLine();
			_logFile.Flush();
		}

		// Create the engine log file
		if (LogEngine) {
			var logPath = Path.Combine(logDir.FullName, ENGINE_LOG_FILE_NAME);
			_engineLogFile = new(
				File.Open(logPath, FileMode.Create, FileAccess.Write, FileShare.Read),
				Encoding.UTF8, 16_384, false);
			_engineLogFile.WriteLine($"Log file opened at {timeStr} on {dateStr}");
			_engineLogFile.WriteLine();
			_engineLogFile.Flush();
		}
	}

	protected override void OnFlush()
	{
		_logFile?.Flush();
		_engineLogFile?.Flush();
	}

	protected override void OnClose()
	{
		_logFile?.Flush();
		_logFile?.Close();
		_engineLogFile?.Flush();
		_engineLogFile?.Close();
	}

	protected override void HandleMessage(Logger logger, TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message,
		Exception? ex)
	{
		if (!UseConsole && !UseFile) return;

		var header = stackalloc char[(int)HEADER_LENGTH];
		var headerSpan = FillHeader(header, level, timeStamp, logger);

		// Write to console
		if (UseConsole) {
			var stream = level >= LogLevel.Warn ? Console.Error : Console.Out;
			stream.Write(headerSpan);
			stream.WriteLine(message);
			if (ex is not null) WriteException(stream, headerSpan, ex);
		}

		// Write to the file
		if (_logFile is not null) {
			_logFile.Write(headerSpan);
			_logFile.WriteLine(message);
			if (ex is not null) WriteException(_logFile, headerSpan, ex);
		}
	}

	protected override void HandleEngineMessage(TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message,
		Exception? ex)
	{
		if (_engineLogFile is not null) {
			var header = stackalloc char[(int)HEADER_LENGTH];
			var headerSpan = FillHeader(header, level, timeStamp, null);
			_engineLogFile.Write(headerSpan);
			_engineLogFile.WriteLine(message);
			if (ex is not null) WriteException(_engineLogFile, headerSpan, ex);
		}
	}


	// Writes a formatted exception to the given stream
	private static void WriteException(TextWriter stream, ReadOnlySpan<char> header, Exception ex)
	{
		var sb = _ExceptionFormatter.Value!;
		sb.Clear();

		// Write the exception
		sb.Append(header).Append("Exception: (").Append(ex.GetType().Name).Append(") '").Append(ex.Message)
			.AppendLine("'");
		if (ex.StackTrace is { } trace) appendStackTrace(sb, header, trace);

		// Write the inner exception
		if (ex.InnerException is { } inner) {
			sb.Append(header).Append("Inner Exception: (").Append(inner.GetType().Name).Append(") '")
				.Append(inner.Message).AppendLine("'");
			if (inner.StackTrace is { } innerTrace) appendStackTrace(sb, header, innerTrace);
		}

		// Write to stream
		stream.Write(sb);
		return;

		static void appendStackTrace(StringBuilder sb, ReadOnlySpan<char> header, string trace)
		{
			const int MAX_TRACE_LINES = 12;

			var lines = trace.Split('\n', MAX_TRACE_LINES + 1, StringSplitOptions.RemoveEmptyEntries);
			for (uint li = 0; li < Math.Min(lines.Length, MAX_TRACE_LINES); ++li) {
				sb.Append(header).Append("   ").AppendLine(lines[li]);
			}
			if (lines.Length > MAX_TRACE_LINES) {
				sb.Append(header).Append("   ... and ").Append(lines.Length - MAX_TRACE_LINES + 1).AppendLine(" more");
			}
		}
	}

	// Populates the log message header (logger == null is for engine message logging)
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	private static ReadOnlySpan<char> FillHeader(char* header, LogLevel level, TimeSpan timeStamp, Logger? logger)
	{
		uint off = 0;

		// Fill the header time
		var hrs = (uint)timeStamp.Hours;
		var min = (uint)timeStamp.Minutes;
		var sec = (uint)timeStamp.Seconds;
		var msc = (uint)timeStamp.Milliseconds;
		header[off++] = '[';
		header[off++] = (char)('0' + (hrs / 10));
		header[off++] = (char)('0' + (hrs % 10));
		header[off++] = ':';
		header[off++] = (char)('0' + (min / 10));
		header[off++] = (char)('0' + (min % 10));
		header[off++] = ':';
		header[off++] = (char)('0' + (sec / 10));
		header[off++] = (char)('0' + (sec % 10));
		header[off++] = '.';
		header[off++] = (char)('0' + (msc / 100));
		header[off++] = (char)('0' + ((msc % 100) / 10));
		header[off++] = (char)('0' + (msc % 10));
		header[off++] = '|';

		// Fill the header logger (if present)
		if (logger is not null) {
			var tag = logger.Tag;
			for (int ti = 0; ti < Logger.TAG_LENGTH; ++ti) {
				header[off++] = tag[ti];
			}
			header[off++] = '|';
		}

		// Fill the level char
		header[off++] = level switch {
			LogLevel.Trace => 'T',
			LogLevel.Debug => 'D',
			LogLevel.Info  => 'I',
			LogLevel.Warn  => 'W',
			LogLevel.Error => 'E',
			LogLevel.Fatal => 'F',
			_              => '?'
		};
		header[off++] = ']';
		header[off++] = ' ';

		// Return the span around the header
		return new(header, (int)off);
	}

	// Performs the historical cleanup of the given log directory
	private static void CleanupLogDir(DirectoryInfo dir, uint history)
	{
		// Rename latest log file
		var latest = new FileInfo(Path.Combine(dir.FullName, LOG_FILE_NAME));
		if (latest.Exists) {
			var newName = $"{latest.CreationTime.ToString(HISTORY_FILE_DATETIME_FORMAT)}{LOG_FILE_HISTORY_SUFFIX}";
			latest.MoveTo(Path.Combine(dir.FullName, newName), true);
		}

		// Rename latest engine log file
		var latestEngine = new FileInfo(Path.Combine(dir.FullName, ENGINE_LOG_FILE_NAME));
		if (latestEngine.Exists) {
			var newName =
				$"{latestEngine.CreationTime.ToString(HISTORY_FILE_DATETIME_FORMAT)}{ENGINE_LOG_FILE_HISTORY_SUFFIX}";
			latestEngine.MoveTo(Path.Combine(dir.FullName, newName), true);
		}

		// Cleanup log history if needed
		var allFiles = dir.GetFiles();
		var histFiles = allFiles
			.Where(static f => LogFileHistoryRegex.IsMatch(f.Name))
			.OrderByDescending(static f => f.CreationTime);
		var engHistFiles = allFiles
			.Where(static f => EngineLogFileHistoryRegex.IsMatch(f.Name))
			.OrderByDescending(static f => f.CreationTime);
		uint histCount = 0;
		foreach (var file in histFiles) {
			if (++histCount > history) {
				file.Delete();
			}
		}
		histCount = 0;
		foreach (var file in engHistFiles) {
			if (++histCount > history) {
				file.Delete();
			}
		}
	}
}
