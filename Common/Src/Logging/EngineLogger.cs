﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Astrum;


// Provides logging for engine messages, goes through a separate route than normal logging messages
internal static class EngineLogger
{
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void EngineTrace(string msg, Exception? ex = null)
	{
		if (LogHandler.Current.EngineLevelMask.HasFlag(LogLevel.Trace)) {
			LogHandler.Current.PostEngineMessage(LogLevel.Trace, msg, ex);
		}
	}

	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void EngineDebug(string msg, Exception? ex = null)
	{
		if (LogHandler.Current.EngineLevelMask.HasFlag(LogLevel.Debug)) {
			LogHandler.Current.PostEngineMessage(LogLevel.Debug, msg, ex);
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void EngineInfo(string msg, Exception? ex = null)
	{
		if (LogHandler.Current.EngineLevelMask.HasFlag(LogLevel.Info)) {
			LogHandler.Current.PostEngineMessage(LogLevel.Info, msg, ex);
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void EngineWarn(string msg, Exception? ex = null)
	{
		if (LogHandler.Current.EngineLevelMask.HasFlag(LogLevel.Warn)) {
			LogHandler.Current.PostEngineMessage(LogLevel.Warn, msg, ex);
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void EngineError(string msg, Exception? ex = null)
	{
		if (LogHandler.Current.EngineLevelMask.HasFlag(LogLevel.Error)) {
			LogHandler.Current.PostEngineMessage(LogLevel.Error, msg, ex);
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void EngineFatal(string msg, Exception? ex = null)
	{
		if (LogHandler.Current.EngineLevelMask.HasFlag(LogLevel.Fatal)) {
			LogHandler.Current.PostEngineMessage(LogLevel.Fatal, msg, ex);
		}
	}
}
