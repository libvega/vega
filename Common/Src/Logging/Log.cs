﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Astrum;


/// <summary>Provides quick access the default application logger (see <see cref="Logger.Default"/>).</summary>
/// <remarks>Can be used as <c>static using Astrum.Log;</c>.</remarks>
public static class Log
{
	/// <summary>Logs a <see cref="LogLevel.Trace"/> level message. Develop builds only.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogTrace(string msg, Exception? ex = null) => Logger.Default.Trace(msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Trace"/> level message. Develop builds only.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogTrace(ref LoggerStringHandler.Trace msg, Exception? ex = null) => Logger.Default.Trace(ref msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Debug"/> level message. Develop builds only.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogDebug(string msg, Exception? ex = null) => Logger.Default.Debug(msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Debug"/> level message. Develop builds only.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogDebug(ref LoggerStringHandler.Debug msg, Exception? ex = null) => Logger.Default.Debug(ref msg, ex);

	/// <summary>Logs an <see cref="LogLevel.Info"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogInfo(string msg, Exception? ex = null) => Logger.Default.Info(msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Info"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogInfo(ref LoggerStringHandler.Info msg, Exception? ex = null) => Logger.Default.Info(ref msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Warn"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogWarn(string msg, Exception? ex = null) => Logger.Default.Warn(msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Warn"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogWarn(ref LoggerStringHandler.Warn msg, Exception? ex = null) => Logger.Default.Warn(ref msg, ex);

	/// <summary>Logs an <see cref="LogLevel.Error"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogError(string msg, Exception? ex = null) => Logger.Default.Error(msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Error"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogError(ref LoggerStringHandler.Error msg, Exception? ex = null) => Logger.Default.Error(ref msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Fatal"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogFatal(string msg, Exception? ex = null) => Logger.Default.Fatal(msg, ex);

	/// <summary>Logs a <see cref="LogLevel.Fatal"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogFatal(ref LoggerStringHandler.Fatal msg, Exception? ex = null) => Logger.Default.Fatal(ref msg, ex);
}
