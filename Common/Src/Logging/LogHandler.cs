﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Astrum;


/// <summary>Base type for implementing logic to handle logged messages and configure handler instances.</summary>
/// <remarks>
/// The handler can be configured at startup time using a subclass of <see cref="UseLogHandlerBaseAttribute"/>. If one
/// is not provided, then a default instance of <see cref="DefaultLogHandler"/> will be used.
/// </remarks>
public abstract class LogHandler
{
	#region Fields
	/// <summary>The current handler instance for all logged messages.</summary>
	public static LogHandler Current { get; private set; }

	// Internal stopwatch for generating message timestamps
	private static readonly Stopwatch _Timer = Stopwatch.StartNew();


	/// <summary>The mask of logging levels to accept in the handler for normal messages.</summary>
	/// <remarks>Defaults to <see cref="LogLevel.NonVerboseMask"/>.</remarks>
	public LogLevel LevelMask { get; protected set; } = LogLevel.NonVerboseMask;

	/// <summary>The mask of logging levels to accept in the handler for engine messages.</summary>
	/// <remarks>Defaults to <see cref="LogLevel.NonVerboseMask"/>.</remarks>
	public LogLevel EngineLevelMask { get; protected set; } = LogLevel.NonVerboseMask;

	// Lock object for posting messages
	private readonly object _postLock = new();
	#endregion // Fields


	/// <summary>Called when the handler is set as active and needs to prepare for handling messages.</summary>
	protected abstract void OnOpen();

	/// <summary>Called to flush the handler, if needed.</summary>
	protected abstract void OnFlush();

	/// <summary>Called when the handler is no longer needed to handle messages.</summary>
	protected abstract void OnClose();


	// Opens the handler
	internal void Open() { lock (_postLock) { OnOpen(); } }

	// Flushes the handler
	internal void Flush() { lock (_postLock) { OnFlush(); } }

	// Closes the handler
	internal void Close() { lock (_postLock) { OnClose(); } }


	#region Messages
	// Posts a message to the handler
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	internal void PostMessage(Logger logger, LogLevel level, string message, Exception? ex)
	{
		lock (_postLock) {
			HandleMessage(logger, _Timer.Elapsed, level, message, ex);
		}
	}

	// Posts an engine message to the handler
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	internal void PostEngineMessage(LogLevel level, string message, Exception? ex)
	{
		lock (_postLock) {
			HandleEngineMessage(_Timer.Elapsed, level, message, ex);
		}
	}

	/// <summary>Called to handle a newly logged message from a <see cref="Logger"/> instance.</summary>
	/// <remarks>Calls to this method are thread-safe by default.</remarks>
	/// <param name="logger">The logger that generated the message.</param>
	/// <param name="timeStamp">The application runtime timestamp of the message.</param>
	/// <param name="level">The level of the message.</param>
	/// <param name="message">The message text.</param>
	/// <param name="ex">The optional exception associated with the message.</param>
	protected abstract void HandleMessage(Logger logger, TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message,
		Exception? ex);

	/// <summary>Called to handle a newly logged internal engine message.</summary>
	/// <remarks>Calls to this method are thread-safe by default.</remarks>
	/// <param name="timeStamp">The application runtime timestamp of the message.</param>
	/// <param name="level">The level of the message.</param>
	/// <param name="message">The message text.</param>
	/// <param name="ex">The optional exception associated with the message.</param>
	protected abstract void HandleEngineMessage(TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message,
		Exception? ex);
	#endregion // Messages


	/// <summary>Provides a pass-through log handler that performs no operations or logging.</summary>
	public sealed class NullHandler : LogHandler
	{
		public NullHandler() => LevelMask = EngineLevelMask = LogLevel.None;

		protected override void OnOpen() { }
		protected override void OnFlush() { }
		protected override void OnClose() { }
		protected override void HandleMessage(Logger logger, TimeSpan timeStamp, LogLevel level,
			ReadOnlySpan<char> message, Exception? ex) { }
		protected override void HandleEngineMessage(TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message,
			Exception? ex) { }
	}


	// Used for multi-dispatch of messages (used in testing)
	internal sealed class DispatchHandler : LogHandler
	{
		public readonly HashSet<LogHandler> Handlers = new();

		public DispatchHandler() => LevelMask = EngineLevelMask = LogLevel.AllMask;

		protected override void OnOpen() { }

		protected override void OnFlush()
		{
			foreach (var handler in Handlers) handler.Flush();
		}

		protected override void OnClose()
		{
			foreach (var handler in Handlers) handler.Close();
			Handlers.Clear();
		}

		protected override void HandleMessage(Logger logger, TimeSpan timeStamp, LogLevel level,
			ReadOnlySpan<char> message, Exception? ex)
		{
			foreach (var handler in Handlers) {
				if (handler.LevelMask.HasFlag(level)) handler.HandleMessage(logger, timeStamp, level, message, ex);
			}
		}

		protected override void HandleEngineMessage(TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message,
			Exception? ex)
		{
			foreach (var handler in Handlers) {
				if (handler.EngineLevelMask.HasFlag(level)) handler.HandleEngineMessage(timeStamp, level, message, ex);
			}
		}
	}


	static LogHandler()
	{
		// For testing, use a dispatch handler, also support disabling logging
		if (Environment.GetEnvironmentVariable("_ASTRUM_TESTS") is not null) {
			Current = new DispatchHandler();
			return;
		}
		if (Environment.GetEnvironmentVariable("_ASTRUM_NO_LOGGING") is not null) {
			Current = new NullHandler();
			return;
		}

		// Lookup the attribute first
		var entry = Assembly.GetEntryAssembly() ??
			throw new PlatformNotSupportedException("Cannot reflect the application entry assembly");
		var attr = entry.GetCustomAttribute<UseLogHandlerBaseAttribute>();

		// Create the handler
		Current = attr?.CreateHandler() ?? new DefaultLogHandler(true, true, true, DefaultLogHandler.DEFAULT_HISTORY);
		Current.Open();

		// Pass unhandled messages to the engine logger and flush to ensure info is available
		AppDomain.CurrentDomain.UnhandledException += static (_, args) => {
			try {
				EngineLogger.EngineError("Unhandled exception at the top level", (Exception)args.ExceptionObject);
				Current.Flush();
			}
			catch { }
		};

		EngineLogger.EngineInfo($"Initialized application logging (handler: {Current.GetType().Name})");
	}
}
