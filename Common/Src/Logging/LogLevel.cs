﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Mask-able levels of importance for logged messages.</summary>
[Flags]
public enum LogLevel : uint
{
	/// <summary>Mask of no logging levels.</summary>
	None = 0,

	/// <summary>Lowest level of debugging, used for fine control flow and logic tracing.</summary>
	Trace = 0x01,
	/// <summary>Debug logging for verbose output in Develop builds.</summary>
	Debug = 0x02,
	/// <summary>Standard informational level for normal execution messages.</summary>
	Info = 0x04,
	/// <summary>Indicates an unexpected but non-error state or event with elevated importance.</summary>
	Warn = 0x08,
	/// <summary>Indicates a recoverable error state or event, potentially with reduced functionality.</summary>
	Error = 0x10,
	/// <summary>Indicates an unrecoverable error state which will usually result in application termination.</summary>
	Fatal = 0x20,

	/// <summary>A mask of elevated-importance levels.</summary>
	ImportantMask = Warn | Error | Fatal,
	/// <summary>A mask of non-debugging levels.</summary>
	StandardMask = Info | Warn | Error | Fatal,
	/// <summary>A mask of all levels except for <see cref="Trace"/>.</summary>
	NonVerboseMask = Debug | Info | Warn | Error | Fatal,
	/// <summary>A mask of all levels.</summary>
	AllMask = Trace | Debug | Info | Warn | Error | Fatal
}
