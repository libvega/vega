﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Astrum;


/// <summary>Represents a named source of logging messages.</summary>
public sealed class Logger : IEquatable<Logger>
{
	/// <summary>The length of <see cref="Tag"/>, in characters.</summary>
	public const uint TAG_LENGTH = 12;


	#region Fields
	/// <summary>The default logger for the application.</summary>
	public static readonly Logger Default = Environment.GetEnvironmentVariable("_ASTRUM_TESTS") is not null
		? new("AstrumTestLogger", null, null)
		: new(AstrumApplicationAttribute.Get().ApplicationName, AstrumApplicationAttribute.Get().DefaultLoggerTag, null);

	// Objects for the logger cache
	private static readonly Dictionary<string, Logger> _Loggers = new();

	// Counter for assigning uids
	private static uint _UidCounter;


	/// <summary>A unique identifier for the logger instance.</summary>
	public readonly uint Uid;

	/// <summary>A globally unique name for the logger.</summary>
	public readonly string Name;

	/// <summary>The logger tag, which is not guaranteed to be unique across all loggers.</summary>
	/// <remarks>It will always have length <see cref="TAG_LENGTH"/>.</remarks>
	public ReadOnlySpan<char> Tag => _tag.AsSpan();
	private readonly string _tag;

	/// <summary>The associated type for the logger, if the logger is type-associated.</summary>
	public readonly Type? ClassType;

	/// <summary>The mask of logging messages that the logger will forward to the handler.</summary>
	/// <remarks>Defaults to <see cref="LogLevel.NonVerboseMask"/>.</remarks>
	public LogLevel LevelMask = LogLevel.NonVerboseMask;
	#endregion // Fields


	internal Logger(string name, string? tag, Type? classType)
	{
		Uid = Interlocked.Increment(ref _UidCounter);
		Name = name;
		_tag = (tag ?? name).TrimOrPadRight(TAG_LENGTH);
		ClassType = classType;
	}


	#region Base
	public bool Equals(Logger? o) => o?.Uid == Uid;

	public override bool Equals(object? obj) => obj is Logger l && l.Uid == Uid;

	public override int GetHashCode() => Name.GetHashCode();

	public override string ToString() => $"Logger[{Name}]";
	#endregion // Base


	#region Log
	/// <summary>Logs a <see cref="LogLevel.Trace"/> level message. Develop builds only.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Trace(string msg, Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Trace) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Trace)) {
			LogHandler.Current.PostMessage(this, LogLevel.Trace, msg, ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Trace"/> level message. Develop builds only.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Trace([InterpolatedStringHandlerArgument("")] ref LoggerStringHandler.Trace msg,
		Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Trace) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Trace)) {
			LogHandler.Current.PostMessage(this, LogLevel.Trace, msg.ToStringAndClear(), ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Debug"/> level message. Develop builds only.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Debug(string msg, Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Debug) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Debug)) {
			LogHandler.Current.PostMessage(this, LogLevel.Debug, msg, ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Debug"/> level message. Develop builds only.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[Conditional("DEBUG")]
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Debug([InterpolatedStringHandlerArgument("")] ref LoggerStringHandler.Debug msg,
		Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Debug) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Debug)) {
			LogHandler.Current.PostMessage(this, LogLevel.Debug, msg.ToStringAndClear(), ex);
		}
	}

	/// <summary>Logs an <see cref="LogLevel.Info"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Info(string msg, Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Info) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Info)) {
			LogHandler.Current.PostMessage(this, LogLevel.Info, msg, ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Info"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Info([InterpolatedStringHandlerArgument("")] ref LoggerStringHandler.Info msg,
		Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Info) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Info)) {
			LogHandler.Current.PostMessage(this, LogLevel.Info, msg.ToStringAndClear(), ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Warn"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Warn(string msg, Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Warn) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Warn)) {
			LogHandler.Current.PostMessage(this, LogLevel.Warn, msg, ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Warn"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Warn([InterpolatedStringHandlerArgument("")] ref LoggerStringHandler.Warn msg,
		Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Warn) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Warn)) {
			LogHandler.Current.PostMessage(this, LogLevel.Warn, msg.ToStringAndClear(), ex);
		}
	}

	/// <summary>Logs an <see cref="LogLevel.Error"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Error(string msg, Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Error) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Error)) {
			LogHandler.Current.PostMessage(this, LogLevel.Error, msg, ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Error"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Error([InterpolatedStringHandlerArgument("")] ref LoggerStringHandler.Error msg,
		Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Error) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Error)) {
			LogHandler.Current.PostMessage(this, LogLevel.Error, msg.ToStringAndClear(), ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Fatal"/> level message.</summary>
	/// <param name="msg">The message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Fatal(string msg, Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Fatal) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Fatal)) {
			LogHandler.Current.PostMessage(this, LogLevel.Fatal, msg, ex);
		}
	}

	/// <summary>Logs a <see cref="LogLevel.Fatal"/> level message.</summary>
	/// <param name="msg">The formatted message to log.</param>
	/// <param name="ex">The optional exception to associate with the message.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Fatal([InterpolatedStringHandlerArgument("")] ref LoggerStringHandler.Fatal msg,
		Exception? ex = null)
	{
		if (LevelMask.HasFlag(LogLevel.Fatal) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Fatal)) {
			LogHandler.Current.PostMessage(this, LogLevel.Fatal, msg.ToStringAndClear(), ex);
		}
	}
	#endregion // Log


	#region Loggers
	/// <summary>Gets the logger unique to the class that calls the function.</summary>
	/// <remarks>
	/// The logger tag defaults to the class type name. It is highly recommended to cache the return value of this
	/// function, instead of looking up the logger each time it is needed.
	/// </remarks>
	/// <param name="tag">
	/// An optional override for <see cref="Tag"/>, only used the first time the logger is accessed.
	/// </param>
	[MethodImpl(MethodImplOptions.NoInlining)]
	public static Logger GetClassLogger(string? tag = null)
	{
		// Get the calling class using a stack trace
		var method = new StackTrace().GetFrame(1)!.GetMethod();
		var classType = method!.ReflectedType;
		var fullName = classType!.FullName;

		// Lookup or add
		lock (_Loggers) {
			// Lookup the existing logger first
			if (_Loggers.TryGetValue(fullName!, out var cached)) {
				return cached;
			}

			// Create a new logger
			var logger = new Logger(fullName!, tag ?? classType.Name, classType);
			_Loggers.Add(logger.Name, logger);
			EngineLogger.EngineInfo($"Created new class logger for class '{classType.FullName}'");
			return logger;
		}
	}

	/// <summary>Gets an existing logger with the given name.</summary>
	/// <param name="name">The name of the logger to get.</param>
	/// <returns>The logger instance, or <c>null</c> if a logger with that name does not exist.</returns>
	public static Logger? GetLogger(string name)
	{
		lock (_Loggers) {
			return _Loggers.TryGetValue(name, out var cached) ? cached : null;
		}
	}

	/// <summary>Gets the logger with the given name, or creates a new logger.</summary>
	/// <param name="name">The name of the logger to get or create.</param>
	/// <param name="tag">The optional override for <see cref="Tag"/>, if a logger is created.</param>
	public static Logger GetOrCreateLogger(string name, string? tag = null)
	{
		lock (_Loggers) {
			// Lookup the logger first
			if (_Loggers.TryGetValue(name, out var cached)) {
				return cached;
			}

			// Create a new logger
			var logger = new Logger(name, tag, null);
			_Loggers.Add(logger.Name, logger);
			EngineLogger.EngineInfo($"Created new logger with name '{name}'");
			return logger;
		}
	}

	/// <summary>Immediately flushes and closes all open loggers.</summary>
	/// <remarks>Should be called as the very last thing that occurs in the application.</remarks>
	public static void ShutdownAll()
	{
		lock (_Loggers) {
			_Loggers.Clear();
		}
		LogHandler.Current.Flush();
		LogHandler.Current.Close();
	}
	#endregion // Loggers
}
