﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Astrum;


/// <summary>Interpolated string handlers for optimizing string formatting based on logging configuration.</summary>
public static class LoggerStringHandler
{
	[InterpolatedStringHandler]
	public ref struct Trace
	{
		private DefaultInterpolatedStringHandler _handler;
		public Trace(int literalLength, int formattedCount, Logger logger, out bool enabled)
		{
			enabled = logger.LevelMask.HasFlag(LogLevel.Trace) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Trace);
			_handler = enabled ? new(literalLength, formattedCount) : new(0, 0);
		}
		public Trace(int literalLength, int formattedCount, out bool enabled)
			: this(literalLength, formattedCount, Logger.Default, out enabled)
		{ }

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}

	[InterpolatedStringHandler]
	public ref struct Debug
	{
		private DefaultInterpolatedStringHandler _handler;
		public Debug(int literalLength, int formattedCount, Logger logger, out bool enabled)
		{
			enabled = logger.LevelMask.HasFlag(LogLevel.Debug) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Debug);
			_handler = enabled ? new(literalLength, formattedCount) : new(0, 0);
		}
		public Debug(int literalLength, int formattedCount, out bool enabled)
			: this(literalLength, formattedCount, Logger.Default, out enabled)
		{ }

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}

	[InterpolatedStringHandler]
	public ref struct Info
	{
		private DefaultInterpolatedStringHandler _handler;
		public Info(int literalLength, int formattedCount, Logger logger, out bool enabled)
		{
			enabled = logger.LevelMask.HasFlag(LogLevel.Info) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Info);
			_handler = enabled ? new(literalLength, formattedCount) : new(0, 0);
		}
		public Info(int literalLength, int formattedCount, out bool enabled)
			: this(literalLength, formattedCount, Logger.Default, out enabled)
		{ }

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}

	[InterpolatedStringHandler]
	public ref struct Warn
	{
		private DefaultInterpolatedStringHandler _handler;
		public Warn(int literalLength, int formattedCount, Logger logger, out bool enabled)
		{
			enabled = logger.LevelMask.HasFlag(LogLevel.Warn) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Warn);
			_handler = enabled ? new(literalLength, formattedCount) : new(0, 0);
		}
		public Warn(int literalLength, int formattedCount, out bool enabled)
			: this(literalLength, formattedCount, Logger.Default, out enabled)
		{ }

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}

	[InterpolatedStringHandler]
	public ref struct Error
	{
		private DefaultInterpolatedStringHandler _handler;
		public Error(int literalLength, int formattedCount, Logger logger, out bool enabled)
		{
			enabled = logger.LevelMask.HasFlag(LogLevel.Error) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Error);
			_handler = enabled ? new(literalLength, formattedCount) : new(0, 0);
		}
		public Error(int literalLength, int formattedCount, out bool enabled)
			: this(literalLength, formattedCount, Logger.Default, out enabled)
		{ }

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}

	[InterpolatedStringHandler]
	public ref struct Fatal
	{
		private DefaultInterpolatedStringHandler _handler;
		public Fatal(int literalLength, int formattedCount, Logger logger, out bool enabled)
		{
			enabled = logger.LevelMask.HasFlag(LogLevel.Fatal) && LogHandler.Current.LevelMask.HasFlag(LogLevel.Fatal);
			_handler = enabled ? new(literalLength, formattedCount) : new(0, 0);
		}
		public Fatal(int literalLength, int formattedCount, out bool enabled)
			: this(literalLength, formattedCount, Logger.Default, out enabled)
		{ }

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}
}
