﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Astrum;


/// <summary>Assembly-targeted attribute for configuring a <see cref="LogHandler"/> to use.</summary>
[AttributeUsage(AttributeTargets.Assembly)]
public abstract class UseLogHandlerBaseAttribute : Attribute
{
	/// <summary>Factory function for creating the <see cref="LogHandler"/> instance to use.</summary>
	public abstract LogHandler CreateHandler();
}
