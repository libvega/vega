﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

/* This file was generated from a template. Do not edit by hand. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// ReSharper disable RedundantCast IdentifierTypo

namespace Astrum.Maths;


/// <summary>Describes a rectangular axis-aligned region in 2D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Box2 : IEquatable<Box2>, IEqualityOperators<Box2, Box2, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box2 Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0)</c> and <see cref="Max"/> at <c>(1,1)</c>.
	/// </summary>
	public static readonly Box2 Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Point2 Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Point2 Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Point2U Size => new((uint)(_max.X - _min.X), (uint)(_max.Y - _min.Y));
	/// <summary>The width of the region.</summary>
	public readonly uint Width => (uint)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly uint Height => (uint)(_max.Y - _min.Y);

	/// <summary>The center point of the region.</summary>
	public readonly Vec2 Center => new((_min.X + _max.X) / (float)2, (_min.Y + _max.Y) / (float)2);

	/// <summary>The internal area of the region.</summary>
	public readonly uint Area => (uint)((_max.X - _min.X) * (_max.Y - _min.Y));
	/// <summary>If the region has zero internal area.</summary>
	public readonly bool IsEmpty => _min.X == _max.X || _min.Y == _max.Y;

	private Point2 _min;
	private Point2 _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2(int x1, int y1, int x2, int y2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box2(Point2 c1, Point2 c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public readonly bool Equals(Box2 o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box2 b && Equals(b);

	public readonly override int GetHashCode() => HashCode.Combine(_min.X, _min.Y, _max.X, _max.Y);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y}];[{_max.X},{_max.Y}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)}]]";

	public readonly void Deconstruct(out int minX, out int minY, out int maxX, out int maxY) =>
		(minX, minY, maxX, maxY) = (_min.X, _min.Y, _max.X, _max.Y);

	public readonly void Deconstruct(out Point2 min, out Point2 max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box2 Translated(Point2 delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Point2)"/>
	public readonly void Translated(Point2 delta, out Box2 o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box2 Inflated(Point2 delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Point2)"/>
	public readonly void Inflated(Point2 delta, out Box2 o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box2 Scaled(Point2 scale, Point2 anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Point2, Point2)"/>
	public readonly void Scaled(Point2 scale, Point2 anchor, out Box2 o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2 l, Box2 r) => l.Equals(r);
	public static bool operator != (Box2 l, Box2 r) => !l.Equals(r);

	public static explicit operator Box2 (Box2L o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Max.X, (int)o.Max.Y);
	public static explicit operator Box2 (Box2F o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Max.X, (int)o.Max.Y);
	public static explicit operator Box2 (Box2D o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Max.X, (int)o.Max.Y);
	#endregion // Operators
}


/// <summary>Describes a rectangular axis-aligned region in 2D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct Box2L : IEquatable<Box2L>, IEqualityOperators<Box2L, Box2L, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box2L Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0)</c> and <see cref="Max"/> at <c>(1,1)</c>.
	/// </summary>
	public static readonly Box2L Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Point2L Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Point2L Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Point2UL Size => new((ulong)(_max.X - _min.X), (ulong)(_max.Y - _min.Y));
	/// <summary>The width of the region.</summary>
	public readonly ulong Width => (ulong)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly ulong Height => (ulong)(_max.Y - _min.Y);

	/// <summary>The center point of the region.</summary>
	public readonly Vec2D Center => new((_min.X + _max.X) / (double)2, (_min.Y + _max.Y) / (double)2);

	/// <summary>The internal area of the region.</summary>
	public readonly ulong Area => (ulong)((_max.X - _min.X) * (_max.Y - _min.Y));
	/// <summary>If the region has zero internal area.</summary>
	public readonly bool IsEmpty => _min.X == _max.X || _min.Y == _max.Y;

	private Point2L _min;
	private Point2L _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2L(long x1, long y1, long x2, long y2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box2L(Point2L c1, Point2L c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public readonly bool Equals(Box2L o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box2L b && Equals(b);

	public readonly override int GetHashCode() => HashCode.Combine(_min.X, _min.Y, _max.X, _max.Y);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y}];[{_max.X},{_max.Y}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)}]]";

	public readonly void Deconstruct(out long minX, out long minY, out long maxX, out long maxY) =>
		(minX, minY, maxX, maxY) = (_min.X, _min.Y, _max.X, _max.Y);

	public readonly void Deconstruct(out Point2L min, out Point2L max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box2L Translated(Point2L delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Point2L)"/>
	public readonly void Translated(Point2L delta, out Box2L o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box2L Inflated(Point2L delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Point2L)"/>
	public readonly void Inflated(Point2L delta, out Box2L o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box2L Scaled(Point2L scale, Point2L anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Point2L, Point2L)"/>
	public readonly void Scaled(Point2L scale, Point2L anchor, out Box2L o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2L l, Box2L r) => l.Equals(r);
	public static bool operator != (Box2L l, Box2L r) => !l.Equals(r);

	public static implicit operator Box2L (Box2 o) => new(o.Min.X, o.Min.Y, o.Max.X, o.Max.Y);
	public static explicit operator Box2L (Box2F o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Max.X, (long)o.Max.Y);
	public static explicit operator Box2L (Box2D o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Max.X, (long)o.Max.Y);
	#endregion // Operators
}


/// <summary>Describes a rectangular axis-aligned region in 2D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Box2F : IEquatable<Box2F>, IEqualityOperators<Box2F, Box2F, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box2F Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0)</c> and <see cref="Max"/> at <c>(1,1)</c>.
	/// </summary>
	public static readonly Box2F Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Vec2 Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Vec2 Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Vec2 Size => new((float)(_max.X - _min.X), (float)(_max.Y - _min.Y));
	/// <summary>The width of the region.</summary>
	public readonly float Width => (float)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly float Height => (float)(_max.Y - _min.Y);

	/// <summary>The center point of the region.</summary>
	public readonly Vec2 Center => new((_min.X + _max.X) / (float)2, (_min.Y + _max.Y) / (float)2);

	/// <summary>The internal area of the region.</summary>
	public readonly float Area => (float)((_max.X - _min.X) * (_max.Y - _min.Y));
	/// <summary>If the region has zero internal area.</summary>
	public readonly bool IsEmpty => _min.X.ApproxEqual(_max.X) || _min.Y.ApproxEqual(_max.Y);

	private Vec2 _min;
	private Vec2 _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2F(float x1, float y1, float x2, float y2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box2F(Vec2 c1, Vec2 c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public readonly bool Equals(Box2F o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box2F b && Equals(b);

	public readonly override int GetHashCode() => HashCode.Combine(_min.X, _min.Y, _max.X, _max.Y);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y}];[{_max.X},{_max.Y}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)}]]";

	public readonly void Deconstruct(out float minX, out float minY, out float maxX, out float maxY) =>
		(minX, minY, maxX, maxY) = (_min.X, _min.Y, _max.X, _max.Y);

	public readonly void Deconstruct(out Vec2 min, out Vec2 max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box2F Translated(Vec2 delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Vec2)"/>
	public readonly void Translated(Vec2 delta, out Box2F o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box2F Inflated(Vec2 delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Vec2)"/>
	public readonly void Inflated(Vec2 delta, out Box2F o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box2F Scaled(Vec2 scale, Vec2 anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Vec2, Vec2)"/>
	public readonly void Scaled(Vec2 scale, Vec2 anchor, out Box2F o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2F l, Box2F r) => l.Equals(r);
	public static bool operator != (Box2F l, Box2F r) => !l.Equals(r);

	public static explicit operator Box2F (Box2 o) => new(o.Min.X, o.Min.Y, o.Max.X, o.Max.Y);
	public static explicit operator Box2F (Box2L o) => new(o.Min.X, o.Min.Y, o.Max.X, o.Max.Y);
	public static explicit operator Box2F (Box2D o) => new((float)o.Min.X, (float)o.Min.Y, (float)o.Max.X, (float)o.Max.Y);
	#endregion // Operators
}


/// <summary>Describes a rectangular axis-aligned region in 2D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct Box2D : IEquatable<Box2D>, IEqualityOperators<Box2D, Box2D, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box2D Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0)</c> and <see cref="Max"/> at <c>(1,1)</c>.
	/// </summary>
	public static readonly Box2D Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Vec2D Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Vec2D Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Vec2D Size => new((double)(_max.X - _min.X), (double)(_max.Y - _min.Y));
	/// <summary>The width of the region.</summary>
	public readonly double Width => (double)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly double Height => (double)(_max.Y - _min.Y);

	/// <summary>The center point of the region.</summary>
	public readonly Vec2D Center => new((_min.X + _max.X) / (double)2, (_min.Y + _max.Y) / (double)2);

	/// <summary>The internal area of the region.</summary>
	public readonly double Area => (double)((_max.X - _min.X) * (_max.Y - _min.Y));
	/// <summary>If the region has zero internal area.</summary>
	public readonly bool IsEmpty => _min.X.ApproxEqual(_max.X) || _min.Y.ApproxEqual(_max.Y);

	private Vec2D _min;
	private Vec2D _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2D(double x1, double y1, double x2, double y2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box2D(Vec2D c1, Vec2D c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public readonly bool Equals(Box2D o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box2D b && Equals(b);

	public readonly override int GetHashCode() => HashCode.Combine(_min.X, _min.Y, _max.X, _max.Y);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y}];[{_max.X},{_max.Y}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)}]]";

	public readonly void Deconstruct(out double minX, out double minY, out double maxX, out double maxY) =>
		(minX, minY, maxX, maxY) = (_min.X, _min.Y, _max.X, _max.Y);

	public readonly void Deconstruct(out Vec2D min, out Vec2D max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box2D Translated(Vec2D delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Vec2D)"/>
	public readonly void Translated(Vec2D delta, out Box2D o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box2D Inflated(Vec2D delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Vec2D)"/>
	public readonly void Inflated(Vec2D delta, out Box2D o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box2D Scaled(Vec2D scale, Vec2D anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Vec2D, Vec2D)"/>
	public readonly void Scaled(Vec2D scale, Vec2D anchor, out Box2D o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2D l, Box2D r) => l.Equals(r);
	public static bool operator != (Box2D l, Box2D r) => !l.Equals(r);

	public static explicit operator Box2D (Box2 o) => new(o.Min.X, o.Min.Y, o.Max.X, o.Max.Y);
	public static explicit operator Box2D (Box2L o) => new(o.Min.X, o.Min.Y, o.Max.X, o.Max.Y);
	public static implicit operator Box2D (Box2F o) => new(o.Min.X, o.Min.Y, o.Max.X, o.Max.Y);
	#endregion // Operators
}


// Space2D methods for Box2
public static partial class Space2D
{

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box2 Union(this Box2 l, Box2 r) => new(Point2.Min(l.Min, r.Min), Point2.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box2,Box2)"/>
	public static void Union(this Box2 l, Box2 r, out Box2 o) =>
		o = new(Point2.Min(l.Min, r.Min), Point2.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box2 Intersect(this Box2 l, Box2 r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box2,Box2)"/>
	public static void Intersect(this Box2 l, Box2 r, out Box2 o)
	{
		Point2 min = Point2.Max(l.Min, r.Min), max = Point2.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y) ? Box2.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Point2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Point2L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Point2U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Point2UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Vec2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Vec2H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Vec2D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box2L Union(this Box2L l, Box2L r) => new(Point2L.Min(l.Min, r.Min), Point2L.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box2L,Box2L)"/>
	public static void Union(this Box2L l, Box2L r, out Box2L o) =>
		o = new(Point2L.Min(l.Min, r.Min), Point2L.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box2L Intersect(this Box2L l, Box2L r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box2L,Box2L)"/>
	public static void Intersect(this Box2L l, Box2L r, out Box2L o)
	{
		Point2L min = Point2L.Max(l.Min, r.Min), max = Point2L.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y) ? Box2L.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Point2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Point2L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Point2U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Point2UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Vec2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Vec2H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Vec2D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box2F Union(this Box2F l, Box2F r) => new(Vec2.Min(l.Min, r.Min), Vec2.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box2F,Box2F)"/>
	public static void Union(this Box2F l, Box2F r, out Box2F o) =>
		o = new(Vec2.Min(l.Min, r.Min), Vec2.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box2F Intersect(this Box2F l, Box2F r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box2F,Box2F)"/>
	public static void Intersect(this Box2F l, Box2F r, out Box2F o)
	{
		Vec2 min = Vec2.Max(l.Min, r.Min), max = Vec2.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y) ? Box2F.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Point2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Point2L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Point2U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Point2UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Vec2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Vec2H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Vec2D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box2D Union(this Box2D l, Box2D r) => new(Vec2D.Min(l.Min, r.Min), Vec2D.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box2D,Box2D)"/>
	public static void Union(this Box2D l, Box2D r, out Box2D o) =>
		o = new(Vec2D.Min(l.Min, r.Min), Vec2D.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box2D Intersect(this Box2D l, Box2D r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box2D,Box2D)"/>
	public static void Intersect(this Box2D l, Box2D r, out Box2D o)
	{
		Vec2D min = Vec2D.Max(l.Min, r.Min), max = Vec2D.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y) ? Box2D.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Point2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Point2L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Point2U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Point2UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Vec2 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Vec2H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Vec2D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y);

}
