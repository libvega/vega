﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

/* This file was generated from a template. Do not edit by hand. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// ReSharper disable RedundantCast IdentifierTypo

namespace Astrum.Maths;


/// <summary>Describes a rectangular axis-aligned region in 3D space (also commonly known as an "AABB").</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public struct Box3 : IEquatable<Box3>, IEqualityOperators<Box3, Box3, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box3 Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0,0)</c> and <see cref="Max"/> at <c>(1,1,1)</c>.
	/// </summary>
	public static readonly Box3 Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Point3 Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
			(_min.Z, _max.Z) = (value.Z < _max.Z) ? (value.Z, _max.Z) : (_max.Z, value.Z);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Point3 Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
			(_min.Z, _max.Z) = (value.Z > _min.Z) ? (_min.Z, value.Z) : (value.Z, _min.Z);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Point3U Size =>
		new((uint)(_max.X - _min.X), (uint)(_max.Y - _min.Y), (uint)(_max.Z - _min.Z));
	/// <summary>The width of the region.</summary>
	public readonly uint Width => (uint)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly uint Height => (uint)(_max.Y - _min.Y);
	/// <summary>The depth of the region.</summary>
	public readonly uint Depth => (uint)(_max.Z - _min.Z);

	/// <summary>The center point of the region.</summary>
	public readonly Vec3 Center =>
		new((_min.X + _max.X) / (float)2, (_min.Y + _max.Y) / (float)2, (_min.Z + _max.Z) / (float)2);

	/// <summary>The internal volume of the area.</summary>
	public readonly uint Volume => (uint)((_max.X - _min.X) * (_max.Y - _min.Y) * (_max.Z - _min.Z));
	/// <summary>If the region has zero internal volume.</summary>
	public readonly bool IsEmpty => _min.X == _max.X || _min.Y == _max.Y || _min.Z == _max.Z;

	private Point3 _min;
	private Point3 _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3(int x1, int y1, int z1, int x2, int y2, int z2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1, swapZ = z2 < z1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1, swapZ ? z2 : z1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2, swapZ ? z1 : z2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box3(Point3 c1, Point3 c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public readonly bool Equals(Box3 o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box3 b && Equals(b);

	public readonly override int GetHashCode() =>
		HashCode.Combine(_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y},{_min.Z}];[{_max.X},{_max.Y},{_max.Z}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)},{_min.Z.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)},{_max.Z.ToString(fmt)}]]";

	public readonly void Deconstruct(
		out int minX, out int minY, out int minZ, out int maxX, out int maxY, out int maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly void Deconstruct(out Point3 min, out Point3 max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box3 Translated(Point3 delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Point3)"/>
	public readonly void Translated(Point3 delta, out Box3 o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box3 Inflated(Point3 delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Point3)"/>
	public readonly void Inflated(Point3 delta, out Box3 o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box3 Scaled(Point3 scale, Point3 anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Point3, Point3)"/>
	public readonly void Scaled(Point3 scale, Point3 anchor, out Box3 o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3 l, Box3 r) => l.Equals(r);
	public static bool operator != (Box3 l, Box3 r) => !l.Equals(r);

	public static explicit operator Box3 (Box3L o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Min.Z, (int)o.Max.X, (int)o.Max.Y, (int)o.Max.Z);
	public static explicit operator Box3 (Box3F o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Min.Z, (int)o.Max.X, (int)o.Max.Y, (int)o.Max.Z);
	public static explicit operator Box3 (Box3D o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Min.Z, (int)o.Max.X, (int)o.Max.Y, (int)o.Max.Z);
	#endregion // Operators
}


/// <summary>Describes a rectangular axis-aligned region in 3D space (also commonly known as an "AABB").</summary>
[StructLayout(LayoutKind.Sequential, Size = 48)]
public struct Box3L : IEquatable<Box3L>, IEqualityOperators<Box3L, Box3L, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box3L Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0,0)</c> and <see cref="Max"/> at <c>(1,1,1)</c>.
	/// </summary>
	public static readonly Box3L Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Point3L Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
			(_min.Z, _max.Z) = (value.Z < _max.Z) ? (value.Z, _max.Z) : (_max.Z, value.Z);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Point3L Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
			(_min.Z, _max.Z) = (value.Z > _min.Z) ? (_min.Z, value.Z) : (value.Z, _min.Z);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Point3UL Size =>
		new((ulong)(_max.X - _min.X), (ulong)(_max.Y - _min.Y), (ulong)(_max.Z - _min.Z));
	/// <summary>The width of the region.</summary>
	public readonly ulong Width => (ulong)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly ulong Height => (ulong)(_max.Y - _min.Y);
	/// <summary>The depth of the region.</summary>
	public readonly ulong Depth => (ulong)(_max.Z - _min.Z);

	/// <summary>The center point of the region.</summary>
	public readonly Vec3D Center =>
		new((_min.X + _max.X) / (double)2, (_min.Y + _max.Y) / (double)2, (_min.Z + _max.Z) / (double)2);

	/// <summary>The internal volume of the area.</summary>
	public readonly ulong Volume => (ulong)((_max.X - _min.X) * (_max.Y - _min.Y) * (_max.Z - _min.Z));
	/// <summary>If the region has zero internal volume.</summary>
	public readonly bool IsEmpty => _min.X == _max.X || _min.Y == _max.Y || _min.Z == _max.Z;

	private Point3L _min;
	private Point3L _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3L(long x1, long y1, long z1, long x2, long y2, long z2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1, swapZ = z2 < z1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1, swapZ ? z2 : z1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2, swapZ ? z1 : z2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box3L(Point3L c1, Point3L c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public readonly bool Equals(Box3L o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box3L b && Equals(b);

	public readonly override int GetHashCode() =>
		HashCode.Combine(_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y},{_min.Z}];[{_max.X},{_max.Y},{_max.Z}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)},{_min.Z.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)},{_max.Z.ToString(fmt)}]]";

	public readonly void Deconstruct(
		out long minX, out long minY, out long minZ, out long maxX, out long maxY, out long maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly void Deconstruct(out Point3L min, out Point3L max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box3L Translated(Point3L delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Point3L)"/>
	public readonly void Translated(Point3L delta, out Box3L o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box3L Inflated(Point3L delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Point3L)"/>
	public readonly void Inflated(Point3L delta, out Box3L o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box3L Scaled(Point3L scale, Point3L anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Point3L, Point3L)"/>
	public readonly void Scaled(Point3L scale, Point3L anchor, out Box3L o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3L l, Box3L r) => l.Equals(r);
	public static bool operator != (Box3L l, Box3L r) => !l.Equals(r);

	public static implicit operator Box3L (Box3 o) => new(o.Min.X, o.Min.Y, o.Min.Z, o.Max.X, o.Max.Y, o.Max.Z);
	public static explicit operator Box3L (Box3F o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Min.Z, (long)o.Max.X, (long)o.Max.Y, (long)o.Max.Z);
	public static explicit operator Box3L (Box3D o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Min.Z, (long)o.Max.X, (long)o.Max.Y, (long)o.Max.Z);
	#endregion // Operators
}


/// <summary>Describes a rectangular axis-aligned region in 3D space (also commonly known as an "AABB").</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public struct Box3F : IEquatable<Box3F>, IEqualityOperators<Box3F, Box3F, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box3F Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0,0)</c> and <see cref="Max"/> at <c>(1,1,1)</c>.
	/// </summary>
	public static readonly Box3F Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Vec3 Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
			(_min.Z, _max.Z) = (value.Z < _max.Z) ? (value.Z, _max.Z) : (_max.Z, value.Z);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Vec3 Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
			(_min.Z, _max.Z) = (value.Z > _min.Z) ? (_min.Z, value.Z) : (value.Z, _min.Z);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Vec3 Size =>
		new((float)(_max.X - _min.X), (float)(_max.Y - _min.Y), (float)(_max.Z - _min.Z));
	/// <summary>The width of the region.</summary>
	public readonly float Width => (float)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly float Height => (float)(_max.Y - _min.Y);
	/// <summary>The depth of the region.</summary>
	public readonly float Depth => (float)(_max.Z - _min.Z);

	/// <summary>The center point of the region.</summary>
	public readonly Vec3 Center =>
		new((_min.X + _max.X) / (float)2, (_min.Y + _max.Y) / (float)2, (_min.Z + _max.Z) / (float)2);

	/// <summary>The internal volume of the area.</summary>
	public readonly float Volume => (float)((_max.X - _min.X) * (_max.Y - _min.Y) * (_max.Z - _min.Z));
	/// <summary>If the region has zero internal volume.</summary>
	public readonly bool IsEmpty => _min.X.ApproxEqual(_max.X) || _min.Y.ApproxEqual(_max.Y) || _min.Z.ApproxEqual(_max.Z);

	private Vec3 _min;
	private Vec3 _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3F(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1, swapZ = z2 < z1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1, swapZ ? z2 : z1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2, swapZ ? z1 : z2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box3F(Vec3 c1, Vec3 c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public readonly bool Equals(Box3F o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box3F b && Equals(b);

	public readonly override int GetHashCode() =>
		HashCode.Combine(_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y},{_min.Z}];[{_max.X},{_max.Y},{_max.Z}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)},{_min.Z.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)},{_max.Z.ToString(fmt)}]]";

	public readonly void Deconstruct(
		out float minX, out float minY, out float minZ, out float maxX, out float maxY, out float maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly void Deconstruct(out Vec3 min, out Vec3 max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box3F Translated(Vec3 delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Vec3)"/>
	public readonly void Translated(Vec3 delta, out Box3F o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box3F Inflated(Vec3 delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Vec3)"/>
	public readonly void Inflated(Vec3 delta, out Box3F o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box3F Scaled(Vec3 scale, Vec3 anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Vec3, Vec3)"/>
	public readonly void Scaled(Vec3 scale, Vec3 anchor, out Box3F o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3F l, Box3F r) => l.Equals(r);
	public static bool operator != (Box3F l, Box3F r) => !l.Equals(r);

	public static explicit operator Box3F (Box3 o) => new(o.Min.X, o.Min.Y, o.Min.Z, o.Max.X, o.Max.Y, o.Max.Z);
	public static explicit operator Box3F (Box3L o) => new(o.Min.X, o.Min.Y, o.Min.Z, o.Max.X, o.Max.Y, o.Max.Z);
	public static explicit operator Box3F (Box3D o) => new((float)o.Min.X, (float)o.Min.Y, (float)o.Min.Z, (float)o.Max.X, (float)o.Max.Y, (float)o.Max.Z);
	#endregion // Operators
}


/// <summary>Describes a rectangular axis-aligned region in 3D space (also commonly known as an "AABB").</summary>
[StructLayout(LayoutKind.Sequential, Size = 48)]
public struct Box3D : IEquatable<Box3D>, IEqualityOperators<Box3D, Box3D, bool>
{
	/// <summary>Empty region constant.</summary>
	public static readonly Box3D Empty = new();
	/// <summary>
	/// Unit-sized region with <see cref="Min"/> at <c>(0,0,0)</c> and <see cref="Max"/> at <c>(1,1,1)</c>.
	/// </summary>
	public static readonly Box3D Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public Vec3D Min {
		readonly get => _min;
		set {
			(_min.X, _max.X) = (value.X < _max.X) ? (value.X, _max.X) : (_max.X, value.X);
			(_min.Y, _max.Y) = (value.Y < _max.Y) ? (value.Y, _max.Y) : (_max.Y, value.Y);
			(_min.Z, _max.Z) = (value.Z < _max.Z) ? (value.Z, _max.Z) : (_max.Z, value.Z);
		}
	}
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public Vec3D Max {
		readonly get => _max;
		set {
			(_min.X, _max.X) = (value.X > _min.X) ? (_min.X, value.X) : (value.X, _min.X);
			(_min.Y, _max.Y) = (value.Y > _min.Y) ? (_min.Y, value.Y) : (value.Y, _min.Y);
			(_min.Z, _max.Z) = (value.Z > _min.Z) ? (_min.Z, value.Z) : (value.Z, _min.Z);
		}
	}

	/// <summary>The dimensions of the region.</summary>
	public readonly Vec3D Size =>
		new((double)(_max.X - _min.X), (double)(_max.Y - _min.Y), (double)(_max.Z - _min.Z));
	/// <summary>The width of the region.</summary>
	public readonly double Width => (double)(_max.X - _min.X);
	/// <summary>The height of the region.</summary>
	public readonly double Height => (double)(_max.Y - _min.Y);
	/// <summary>The depth of the region.</summary>
	public readonly double Depth => (double)(_max.Z - _min.Z);

	/// <summary>The center point of the region.</summary>
	public readonly Vec3D Center =>
		new((_min.X + _max.X) / (double)2, (_min.Y + _max.Y) / (double)2, (_min.Z + _max.Z) / (double)2);

	/// <summary>The internal volume of the area.</summary>
	public readonly double Volume => (double)((_max.X - _min.X) * (_max.Y - _min.Y) * (_max.Z - _min.Z));
	/// <summary>If the region has zero internal volume.</summary>
	public readonly bool IsEmpty => _min.X.ApproxEqual(_max.X) || _min.Y.ApproxEqual(_max.Y) || _min.Z.ApproxEqual(_max.Z);

	private Vec3D _min;
	private Vec3D _max;
	#endregion // Fields

	/// <summary>Create a region from explicit corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3D(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		bool swapX = x2 < x1, swapY = y2 < y1, swapZ = z2 < z1;
		_min = new(swapX ? x2 : x1, swapY ? y2 : y1, swapZ ? z2 : z1);
		_max = new(swapX ? x1 : x2, swapY ? y1 : y2, swapZ ? z1 : z2);
	}

	/// <summary>Create a region from explicit corners.</summary>
	public Box3D(Vec3D c1, Vec3D c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public readonly bool Equals(Box3D o) => _min == o._min && _max == o._max;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Box3D b && Equals(b);

	public readonly override int GetHashCode() =>
		HashCode.Combine(_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly override string ToString() => $"[[{_min.X},{_min.Y},{_min.Z}];[{_max.X},{_max.Y},{_max.Z}]]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[[{_min.X.ToString(fmt)},{_min.Y.ToString(fmt)},{_min.Z.ToString(fmt)}];[{_max.X.ToString(fmt)},{_max.Y.ToString(fmt)},{_max.Z.ToString(fmt)}]]";

	public readonly void Deconstruct(
		out double minX, out double minY, out double minZ, out double maxX, out double maxY, out double maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (_min.X, _min.Y, _min.Z, _max.X, _max.Y, _max.Z);

	public readonly void Deconstruct(out Vec3D min, out Vec3D max) => (min, max) = (_min, _max);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets the region translated by the given amount.</summary>
	public readonly Box3D Translated(Vec3D delta) => new(_min + delta, _max + delta);

	/// <inheritdoc cref="Translated(Vec3D)"/>
	public readonly void Translated(Vec3D delta, out Box3D o) => o = new(_min + delta, _max + delta);

	/// <summary>Gets the region expanded or shrunk around the center by the given amount.</summary>
	public readonly Box3D Inflated(Vec3D delta) => new(_min - delta / 2, _max + delta / 2);

	/// <inheritdoc cref="Inflated(Vec3D)"/>
	public readonly void Inflated(Vec3D delta, out Box3D o) => o = new(_min - delta / 2, _max + delta / 2);

	/// <summary>Gets the region scaled by the given amount relative to the anchor.</summary>
	public readonly Box3D Scaled(Vec3D scale, Vec3D anchor) =>
		new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);

	/// <inheritdoc cref="Scaled(Vec3D, Vec3D)"/>
	public readonly void Scaled(Vec3D scale, Vec3D anchor, out Box3D o) =>
		o = new((scale * (_min - anchor)) + anchor, (scale * (_max - anchor)) + anchor);
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3D l, Box3D r) => l.Equals(r);
	public static bool operator != (Box3D l, Box3D r) => !l.Equals(r);

	public static explicit operator Box3D (Box3 o) => new(o.Min.X, o.Min.Y, o.Min.Z, o.Max.X, o.Max.Y, o.Max.Z);
	public static explicit operator Box3D (Box3L o) => new(o.Min.X, o.Min.Y, o.Min.Z, o.Max.X, o.Max.Y, o.Max.Z);
	public static implicit operator Box3D (Box3F o) => new(o.Min.X, o.Min.Y, o.Min.Z, o.Max.X, o.Max.Y, o.Max.Z);
	#endregion // Operators
}


// Space3D methods for Box3
public static partial class Space3D
{

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box3 Union(this Box3 l, Box3 r) => new(Point3.Min(l.Min, r.Min), Point3.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box3,Box3)"/>
	public static void Union(this Box3 l, Box3 r, out Box3 o) =>
		o = new(Point3.Min(l.Min, r.Min), Point3.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box3 Intersect(this Box3 l, Box3 r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box3,Box3)"/>
	public static void Intersect(this Box3 l, Box3 r, out Box3 o)
	{
		Point3 min = Point3.Max(l.Min, r.Min), max = Point3.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Box3.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Point3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Point3L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Point3U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Point3UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y) &&
		((long)point.Z >= box.Min.Z && (long)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Vec3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Vec3H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y) &&
		((float)point.Z >= box.Min.Z && (float)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Vec3D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box3L Union(this Box3L l, Box3L r) => new(Point3L.Min(l.Min, r.Min), Point3L.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box3L,Box3L)"/>
	public static void Union(this Box3L l, Box3L r, out Box3L o) =>
		o = new(Point3L.Min(l.Min, r.Min), Point3L.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box3L Intersect(this Box3L l, Box3L r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box3L,Box3L)"/>
	public static void Intersect(this Box3L l, Box3L r, out Box3L o)
	{
		Point3L min = Point3L.Max(l.Min, r.Min), max = Point3L.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Box3L.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Point3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Point3L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Point3U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Point3UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y) &&
		((long)point.Z >= box.Min.Z && (long)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Vec3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Vec3H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y) &&
		((float)point.Z >= box.Min.Z && (float)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Vec3D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box3F Union(this Box3F l, Box3F r) => new(Vec3.Min(l.Min, r.Min), Vec3.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box3F,Box3F)"/>
	public static void Union(this Box3F l, Box3F r, out Box3F o) =>
		o = new(Vec3.Min(l.Min, r.Min), Vec3.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box3F Intersect(this Box3F l, Box3F r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box3F,Box3F)"/>
	public static void Intersect(this Box3F l, Box3F r, out Box3F o)
	{
		Vec3 min = Vec3.Max(l.Min, r.Min), max = Vec3.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Box3F.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Point3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Point3L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Point3U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Point3UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y) &&
		((long)point.Z >= box.Min.Z && (long)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Vec3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Vec3H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y) &&
		((float)point.Z >= box.Min.Z && (float)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Vec3D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);

	/// <summary>Calculates the minimum region that contains both input regions.</summary>
	public static Box3D Union(this Box3D l, Box3D r) => new(Vec3D.Min(l.Min, r.Min), Vec3D.Max(l.Max, r.Max));
	/// <inheritdoc cref="Union(Box3D,Box3D)"/>
	public static void Union(this Box3D l, Box3D r, out Box3D o) =>
		o = new(Vec3D.Min(l.Min, r.Min), Vec3D.Max(l.Max, r.Max));
	/// <summary>Calculates the overlap between the regions, if any.</summary>
	public static Box3D Intersect(this Box3D l, Box3D r) { Intersect(l, r, out var o); return o; }
	/// <inheritdoc cref="Intersect(Box3D,Box3D)"/>
	public static void Intersect(this Box3D l, Box3D r, out Box3D o)
	{
		Vec3D min = Vec3D.Max(l.Min, r.Min), max = Vec3D.Min(l.Max, r.Max);
		o = (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Box3D.Empty : new(min, max);
	}
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Point3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Point3L point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Point3U point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Point3UL point) =>
		((long)point.X >= box.Min.X && (long)point.X <= box.Max.X) && ((long)point.Y >= box.Min.Y && (long)point.Y <= box.Max.Y) &&
		((long)point.Z >= box.Min.Z && (long)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Vec3 point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Vec3H point) =>
		((float)point.X >= box.Min.X && (float)point.X <= box.Max.X) && ((float)point.Y >= box.Min.Y && (float)point.Y <= box.Max.Y) &&
		((float)point.Z >= box.Min.Z && (float)point.Z <= box.Max.Z);
	/// <summary>Gets if the box contains the vector.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Vec3D point) =>
		(point.X >= box.Min.X && point.X <= box.Max.X) && (point.Y >= box.Min.Y && point.Y <= box.Max.Y) &&
		(point.Z >= box.Min.Z && point.Z <= box.Max.Z);

}
