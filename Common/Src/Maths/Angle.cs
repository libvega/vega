﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

/* This file was generated from a template. Do not edit by hand. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>An angle measurement backed by a float value.</summary>
[StructLayout(LayoutKind.Sequential, Size = sizeof(float))]
public readonly struct Angle :
	IEquatable<Angle>, IComparable<Angle>,
	IComparisonOperators<Angle, Angle, bool>,
	IAdditionOperators<Angle, Angle, Angle>, ISubtractionOperators<Angle, Angle, Angle>,
	IMultiplyOperators<Angle, float, Angle>, IDivisionOperators<Angle, float, Angle>,
	IModulusOperators<Angle, Angle, Angle>, IUnaryNegationOperators<Angle, Angle>
{
	// Conversion constants
	private const float R2D = 180 / MathF.PI;
	private const float D2R = MathF.PI / 180;


	#region Constants
	/// <summary>Zero angle.</summary>
	public static readonly Angle Zero = new(0);

	/// <summary>Angle of 10 degrees.</summary>
	public static readonly Angle D10 = new(10 * D2R);
	/// <summary>Angle of 15 degrees.</summary>
	public static readonly Angle D15 = new(15 * D2R);
	/// <summary>Angle of 20 degrees.</summary>
	public static readonly Angle D20 = new(20 * D2R);
	/// <summary>Angle of 30 degrees.</summary>
	public static readonly Angle D30 = new(30 * D2R);
	/// <summary>Angle of 45 degrees.</summary>
	public static readonly Angle D45 = new(45 * D2R);
	/// <summary>Angle of 60 degrees.</summary>
	public static readonly Angle D60 = new(60 * D2R);
	/// <summary>Angle of 90 degrees.</summary>
	public static readonly Angle D90 = new(90 * D2R);
	/// <summary>Angle of 180 degrees.</summary>
	public static readonly Angle D180 = new(180 * D2R);
	/// <summary>Angle of 360 degrees.</summary>
	public static readonly Angle D360 = new(360 * D2R);

	/// <summary>Angle of π/6 degrees.</summary>
	public static readonly Angle PiO6 = new(MathF.PI / 6);
	/// <summary>Angle of π/4 degrees.</summary>
	public static readonly Angle PiO4 = new(MathF.PI / 4);
	/// <summary>Angle of π/3 degrees.</summary>
	public static readonly Angle PiO3 = new(MathF.PI / 3);
	/// <summary>Angle of π/2 degrees.</summary>
	public static readonly Angle PiO2 = new(MathF.PI / 2);
	/// <summary>Angle of π degrees.</summary>
	public static readonly Angle Pi = new(MathF.PI);
	/// <summary>Angle of τ (2π) degrees.</summary>
	public static readonly Angle Tau = new(MathF.Tau);
	#endregion // Constants


	#region Fields
	/// <summary>The angle measurement in radians.</summary>
	public readonly float Radians;
	/// <summary>The angle measurement in degrees.</summary>
	public float Degrees => Radians * R2D;

	/// <summary>The sine of the angle.</summary>
	public float Sin => MathF.Sin(Radians);
	/// <summary>The cosine of the angle.</summary>
	public float Cos => MathF.Cos(Radians);
	/// <summary>The tangent of the angle.</summary>
	public float Tan => MathF.Tan(Radians);
	/// <summary>The combined sine and cosine of the angle.</summary>
	public (float Sin, float Cos) SinCos => MathF.SinCos(Radians);

	/// <summary>The angle value normalized to <c>(-2π,2π)</c> (without changing the sign).</summary>
	public Angle Normalized => new(Radians % MathF.Tau);
	#endregion // Fields

	// Construct from radians value
	private Angle(float rad) => Radians = rad;

	/// <summary>Create an angle from a radians measurement.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Rad(float rad) => new(rad);
	/// <summary>Create an angle from a degrees measurement.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Deg(float deg) => new(deg * D2R);


	#region Base
	public override string ToString() => $"[{Radians:G} rad]";
	public string ToString([StringSyntax("NumericFormat")] string? fmt) => $"[{Radians.ToString(fmt)} rad]";

	public override bool Equals([NotNullWhen(true)] object? o) => o is Angle a && Radians.ApproxEqual(a.Radians);
	public bool Equals(Angle o) => Radians.ApproxEqual(o.Radians);

	public override int GetHashCode() => Radians.GetHashCode();

	public int CompareTo(Angle o) => Radians.CompareTo(o.Radians);
	#endregion // Base


	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Clamp(Angle v, Angle min, Angle max) => new(Math.Clamp(v.Radians, min.Radians, max.Radians));

	/// <summary>Returns the minimum of the angles.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Min(Angle l, Angle r) => new(Math.Min(l.Radians, r.Radians));

	/// <summary>Returns the maximum of the angles.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Max(Angle l, Angle r) => new(Math.Max(l.Radians, r.Radians));

	/// <inheritdoc cref="MathUtils.ApproxEqual(float,float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxEqual(Angle o, float eps) => Radians.ApproxEqual(o.Radians, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(float,float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxEqual(Angle o) => Radians.ApproxEqual(o.Radians);

	/// <inheritdoc cref="MathUtils.ApproxZero(float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxZero(float eps) => Radians.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxZero() => Radians.ApproxZero();


	#region Operators
	public static bool operator == (Angle l, Angle r) => l.Radians.ApproxEqual(r.Radians);
	public static bool operator != (Angle l, Angle r) => !l.Radians.ApproxEqual(r.Radians);
	public static bool operator <= (Angle l, Angle r) => l.Radians <= r.Radians;
	public static bool operator <  (Angle l, Angle r) => l.Radians < r.Radians;
	public static bool operator >= (Angle l, Angle r) => l.Radians >= r.Radians;
	public static bool operator >  (Angle l, Angle r) => l.Radians > r.Radians;

	public static Angle operator + (Angle l, Angle r) => new(l.Radians + r.Radians);
	public static Angle operator - (Angle l, Angle r) => new(l.Radians - r.Radians);
	public static Angle operator * (Angle l, float r) => new(l.Radians * r);
	public static Angle operator / (Angle l, float r) => new(l.Radians / r);
	public static Angle operator % (Angle l, Angle r) => new(l.Radians % r.Radians);

	public static Angle operator - (Angle r) => new(-r.Radians);

	public static explicit operator Angle (AngleD a) => new((float)a.Radians);
	#endregion // Operators
}


/// <summary>An angle measurement backed by a double value.</summary>
[StructLayout(LayoutKind.Sequential, Size = sizeof(double))]
public readonly struct AngleD :
	IEquatable<AngleD>, IComparable<AngleD>,
	IComparisonOperators<AngleD, AngleD, bool>,
	IAdditionOperators<AngleD, AngleD, AngleD>, ISubtractionOperators<AngleD, AngleD, AngleD>,
	IMultiplyOperators<AngleD, double, AngleD>, IDivisionOperators<AngleD, double, AngleD>,
	IModulusOperators<AngleD, AngleD, AngleD>, IUnaryNegationOperators<AngleD, AngleD>
{
	// Conversion constants
	private const double R2D = 180 / Math.PI;
	private const double D2R = Math.PI / 180;


	#region Constants
	/// <summary>Zero angle.</summary>
	public static readonly AngleD Zero = new(0);

	/// <summary>Angle of 10 degrees.</summary>
	public static readonly AngleD D10 = new(10 * D2R);
	/// <summary>Angle of 15 degrees.</summary>
	public static readonly AngleD D15 = new(15 * D2R);
	/// <summary>Angle of 20 degrees.</summary>
	public static readonly AngleD D20 = new(20 * D2R);
	/// <summary>Angle of 30 degrees.</summary>
	public static readonly AngleD D30 = new(30 * D2R);
	/// <summary>Angle of 45 degrees.</summary>
	public static readonly AngleD D45 = new(45 * D2R);
	/// <summary>Angle of 60 degrees.</summary>
	public static readonly AngleD D60 = new(60 * D2R);
	/// <summary>Angle of 90 degrees.</summary>
	public static readonly AngleD D90 = new(90 * D2R);
	/// <summary>Angle of 180 degrees.</summary>
	public static readonly AngleD D180 = new(180 * D2R);
	/// <summary>Angle of 360 degrees.</summary>
	public static readonly AngleD D360 = new(360 * D2R);

	/// <summary>Angle of π/6 degrees.</summary>
	public static readonly AngleD PiO6 = new(Math.PI / 6);
	/// <summary>Angle of π/4 degrees.</summary>
	public static readonly AngleD PiO4 = new(Math.PI / 4);
	/// <summary>Angle of π/3 degrees.</summary>
	public static readonly AngleD PiO3 = new(Math.PI / 3);
	/// <summary>Angle of π/2 degrees.</summary>
	public static readonly AngleD PiO2 = new(Math.PI / 2);
	/// <summary>Angle of π degrees.</summary>
	public static readonly AngleD Pi = new(Math.PI);
	/// <summary>Angle of τ (2π) degrees.</summary>
	public static readonly AngleD Tau = new(Math.Tau);
	#endregion // Constants


	#region Fields
	/// <summary>The angle measurement in radians.</summary>
	public readonly double Radians;
	/// <summary>The angle measurement in degrees.</summary>
	public double Degrees => Radians * R2D;

	/// <summary>The sine of the angle.</summary>
	public double Sin => Math.Sin(Radians);
	/// <summary>The cosine of the angle.</summary>
	public double Cos => Math.Cos(Radians);
	/// <summary>The tangent of the angle.</summary>
	public double Tan => Math.Tan(Radians);
	/// <summary>The combined sine and cosine of the angle.</summary>
	public (double Sin, double Cos) SinCos => Math.SinCos(Radians);

	/// <summary>The angle value normalized to <c>(-2π,2π)</c> (without changing the sign).</summary>
	public AngleD Normalized => new(Radians % Math.Tau);
	#endregion // Fields

	// Construct from radians value
	private AngleD(double rad) => Radians = rad;

	/// <summary>Create an angle from a radians measurement.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Rad(double rad) => new(rad);
	/// <summary>Create an angle from a degrees measurement.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Deg(double deg) => new(deg * D2R);


	#region Base
	public override string ToString() => $"[{Radians:G} rad]";
	public string ToString([StringSyntax("NumericFormat")] string? fmt) => $"[{Radians.ToString(fmt)} rad]";

	public override bool Equals([NotNullWhen(true)] object? o) => o is AngleD a && Radians.ApproxEqual(a.Radians);
	public bool Equals(AngleD o) => Radians.ApproxEqual(o.Radians);

	public override int GetHashCode() => Radians.GetHashCode();

	public int CompareTo(AngleD o) => Radians.CompareTo(o.Radians);
	#endregion // Base


	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Clamp(AngleD v, AngleD min, AngleD max) => new(Math.Clamp(v.Radians, min.Radians, max.Radians));

	/// <summary>Returns the minimum of the angles.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Min(AngleD l, AngleD r) => new(Math.Min(l.Radians, r.Radians));

	/// <summary>Returns the maximum of the angles.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Max(AngleD l, AngleD r) => new(Math.Max(l.Radians, r.Radians));

	/// <inheritdoc cref="MathUtils.ApproxEqual(double,double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxEqual(AngleD o, double eps) => Radians.ApproxEqual(o.Radians, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(double,double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxEqual(AngleD o) => Radians.ApproxEqual(o.Radians);

	/// <inheritdoc cref="MathUtils.ApproxZero(double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxZero(double eps) => Radians.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxZero() => Radians.ApproxZero();


	#region Operators
	public static bool operator == (AngleD l, AngleD r) => l.Radians.ApproxEqual(r.Radians);
	public static bool operator != (AngleD l, AngleD r) => !l.Radians.ApproxEqual(r.Radians);
	public static bool operator <= (AngleD l, AngleD r) => l.Radians <= r.Radians;
	public static bool operator <  (AngleD l, AngleD r) => l.Radians < r.Radians;
	public static bool operator >= (AngleD l, AngleD r) => l.Radians >= r.Radians;
	public static bool operator >  (AngleD l, AngleD r) => l.Radians > r.Radians;

	public static AngleD operator + (AngleD l, AngleD r) => new(l.Radians + r.Radians);
	public static AngleD operator - (AngleD l, AngleD r) => new(l.Radians - r.Radians);
	public static AngleD operator * (AngleD l, double r) => new(l.Radians * r);
	public static AngleD operator / (AngleD l, double r) => new(l.Radians / r);
	public static AngleD operator % (AngleD l, AngleD r) => new(l.Radians % r.Radians);

	public static AngleD operator - (AngleD r) => new(-r.Radians);

	public static implicit operator AngleD (Angle a) => new(a.Radians);
	#endregion // Operators
}

