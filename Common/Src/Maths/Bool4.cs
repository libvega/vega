﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>Four-component vector of booleans.</summary>
[StructLayout(LayoutKind.Sequential, Size = 4)]
public struct Bool4 :
	IEquatable<Bool4>,
	IEqualityOperators<Bool4, Bool4, bool>, IBitwiseOperators<Bool4, Bool4, Bool4>
{
	#region Fields
	/// <summary>The X component.</summary>
	public bool X;
	/// <summary>The Y component.</summary>
	public bool Y;
	/// <summary>The Z component.</summary>
	public bool Z;
	/// <summary>The W component.</summary>
	public bool W;

	/// <summary>If any components are true.</summary>
	public readonly bool Any => X || Y || Z || W;
	/// <summary>If all components are true.</summary>
	public readonly bool All => X && Y && Z && W;
	/// <summary>If no components are true.</summary>
	public readonly bool None => !(X || Y || Z || W);

	/// <summary>The number of components that are true.</summary>
	public readonly uint Count => (X ? 1u : 0) + (Y ? 1u : 0) + (Z ? 1u : 0) + (W ? 1u : 0);
	#endregion // Fields

	/// <summary>Construct a vector with the given value for all components.</summary>
	public Bool4(bool b) => X = Y = Z = W = b;

	/// <summary>Construct a vector from an X,Y vector and explicit Z,W.</summary>
	public Bool4(Bool2 xy, bool z, bool w) => (X, Y, Z, W) = (xy.X, xy.Y, z, w);

	/// <summary>Construct a vector from an X,Y vector and Z,W vector.</summary>
	public Bool4(Bool2 xy, Bool2 zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct a vector with explicit component values.</summary>
	public Bool4(bool x, bool y, bool z, bool w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	public readonly bool Equals(Bool4 o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? obj) =>
		obj is Bool4 o && X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override int GetHashCode() => (X, Y, Z, W) switch {
		(false, false, false, false) => 0xE7DF.GetHashCode(), (false, false, false, true) => 0x9E4D.GetHashCode(),
		(false, false, true, false)  => 0x7691.GetHashCode(), (false, false, true, true)  => 0xC28F.GetHashCode(),
		(false, true, false, false)  => 0x95A5.GetHashCode(), (false, true, false, true)  => 0x69EF.GetHashCode(),
		(false, true, true, false)   => 0xAB97.GetHashCode(), (false, true, true, true)   => 0x7E15.GetHashCode(),
		(true, false, false, false)  => 0xDBE3.GetHashCode(), (true, false, false, true)  => 0xA8B7.GetHashCode(),
		(true, false, true, false)   => 0xEF11.GetHashCode(), (true, false, true, true)   => 0x8847.GetHashCode(),
		(true, true, false, false)   => 0xC7FD.GetHashCode(), (true, true, false, true)   => 0x5951.GetHashCode(),
		(true, true, true, false)    => 0xB46D.GetHashCode(), (true, true, true, true)    => 0xD5FF.GetHashCode()
	};

	public readonly override string ToString() => (X, Y, Z, W) switch {
		(false, false, false, false) => "[F,F,F,F]", (false, false, false, true) => "[F,F,F,T]",
		(false, false, true, false)  => "[F,F,T,F]", (false, false, true, true)  => "[F,F,T,T]",
		(false, true, false, false)  => "[F,T,F,F]", (false, true, false, true)  => "[F,T,T,T]",
		(false, true, true, false)   => "[F,T,T,F]", (false, true, true, true)   => "[F,T,F,T]",
		(true, false, false, false)  => "[T,F,T,F]", (true, false, false, true)  => "[T,F,T,T]",
		(true, false, true, false)   => "[T,F,F,F]", (true, false, true, true)   => "[T,F,T,T]",
		(true, true, false, false)   => "[T,T,T,F]", (true, true, false, true)   => "[T,T,F,T]",
		(true, true, true, false)    => "[T,T,T,F]", (true, true, true, true)    => "[T,T,T,T]"
	};

	public readonly void Deconstruct(out bool x, out bool y, out bool z, out bool w) => (x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Operators
	public static bool operator == (Bool4 l, Bool4 r) => l.X == r.X && l.Y == r.Y && l.Z == r.Z && l.W == r.W;
	public static bool operator != (Bool4 l, Bool4 r) => l.X != r.X || l.Y != r.Y || l.Z != r.Z || l.W != r.W;

	public static Bool4 operator & (Bool4 l, Bool4 r) => new(l.X && r.X, l.Y && r.Y, l.Z && r.Z, l.W && r.W);
	public static Bool4 operator | (Bool4 l, Bool4 r) => new(l.X || r.X, l.Y || r.Y, l.Z || r.Z, l.W || r.W);
	public static Bool4 operator ^ (Bool4 l, Bool4 r) => new(l.X != r.X, l.Y != r.Y, l.Z != r.Z, l.W != r.W);
	public static Bool4 operator ~ (Bool4 r) => new(!r.X, !r.Y, !r.Z, !r.W);
	public static Bool4 operator ! (Bool4 r) => new(!r.X, !r.Y, !r.Z, !r.W);
	#endregion // Operators
}
