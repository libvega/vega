﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
// ReSharper disable InconsistentNaming

namespace Astrum.Maths;


/// <summary>Provides additional utilities for mathematics.</summary>
public static class MathUtils
{
	// Max optimization
	internal const MethodImplOptions MAX_OPT =
		MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization;

	// Default epsilons
	internal static readonly Half REL_EPS_16 = (Half)1e-4f;
	internal static readonly Half ZERO_EPS_16 = (Half)1e-7f;
	internal const float REL_EPS_32 = 1e-7f;
	internal const float ZERO_EPS_32 = 1e-10f;
	internal const double REL_EPS_64 = 1e-7;
	internal const double ZERO_EPS_64 = 1e-10;


	#region FastApprox
	/// <summary>Performs a fast approximate floating point equality check with an absolute difference.</summary>
	/// <remarks>Generally accurate except for very large values, and does not support non-normal values.</remarks>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this float l, float r, float eps) => MathF.Abs(r - l) <= eps;

	/// <inheritdoc cref="ApproxEqual(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this float l, float r) => ApproxEqual(l, r, REL_EPS_32);

	/// <inheritdoc cref="ApproxEqual(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this Half l, Half r, Half eps) => Half.Abs(r - l) <= eps;

	/// <inheritdoc cref="ApproxEqual(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this Half l, Half r) => ApproxEqual(l, r, REL_EPS_16);

	/// <inheritdoc cref="ApproxEqual(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this double l, double r, double eps) => Math.Abs(r - l) <= eps;

	/// <inheritdoc cref="ApproxEqual(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this double l, double r) => ApproxEqual(l, r, REL_EPS_64);
	#endregion // FastApprox


	#region ApproxZero
	/// <summary>Calculates if the value is approximately zero within an absolute tolerance.</summary>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this float l, float eps) => MathF.Abs(l) <= eps;

	/// <inheritdoc cref="ApproxZero(float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this float l) => ApproxZero(l, ZERO_EPS_32);

	/// <inheritdoc cref="ApproxZero(float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this Half l, Half eps) => Half.Abs(l) <= eps;

	/// <inheritdoc cref="ApproxZero(float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this Half l) => ApproxZero(l, ZERO_EPS_16);

	/// <inheritdoc cref="ApproxZero(float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this double l, double eps) => Math.Abs(l) <= eps;

	/// <inheritdoc cref="ApproxZero(float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this double l) => ApproxZero(l, ZERO_EPS_64);
	#endregion // ApproxZero
}
