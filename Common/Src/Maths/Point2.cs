﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

/* This file was generated from a template. Do not edit by hand. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
// ReSharper disable InconsistentNaming RedundantCast

namespace Astrum.Maths;


/// <summary>A two-component vector of <c>int</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 8)]
public struct Point2 :
	IEquatable<Point2>, IEqualityOperators<Point2, Point2, bool>,
	IAdditionOperators<Point2, Point2, Point2>, IAdditionOperators<Point2, int, Point2>,
	ISubtractionOperators<Point2, Point2, Point2>, ISubtractionOperators<Point2, int, Point2>,
	IMultiplyOperators<Point2, Point2, Point2>, IMultiplyOperators<Point2, int, Point2>,
	IDivisionOperators<Point2, Point2, Point2>, IDivisionOperators<Point2, int, Point2>,
	IModulusOperators<Point2, Point2, Point2>, IModulusOperators<Point2, int, Point2>
	, IUnaryNegationOperators<Point2, Point2>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point2 Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point2 UnitX = new(1, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point2 UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public int X;
	/// <summary>The Y-component.</summary>
	public int Y;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length => Single.Sqrt(X*X + Y*Y);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq => X*X + Y*Y;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point2(int v) => X = Y = v;

	/// <summary>Construct the point from explicit components.</summary>
	public Point2(int x, int y) => (X, Y) = (x, y);


	#region Base
	public readonly bool Equals(Point2 o) => X == o.X && Y == o.Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point2 p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"[{X},{Y}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)}]";

	public readonly void Deconstruct(out int x, out int y) => (x, y) = (X, Y);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly int Dot(Point2 o) => X*o.X + Y*o.Y;

	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point2 o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2 Clamp(Point2 v, Point2 min, Point2 max) =>
		new(Int32.Clamp(v.X, min.X, max.X), Int32.Clamp(v.Y, min.Y, max.Y));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2 Min(Point2 l, Point2 r) => new(Int32.Min(l.X, r.X), Int32.Min(l.Y, r.Y));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2 Max(Point2 l, Point2 r) => new(Int32.Max(l.X, r.X), Int32.Max(l.Y, r.Y));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point2 l, Point2 r) => l.Equals(r);
	public static bool operator != (Point2 l, Point2 r) => !l.Equals(r);

	public static Bool2 operator <= (Point2 l, Point2 r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Point2 l, Point2 r) => new(l.X < r.X, l.Y < r.Y);
	public static Bool2 operator >= (Point2 l, Point2 r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Point2 l, Point2 r) => new(l.X > r.X, l.Y > r.Y);

	public static Point2 operator + (Point2 l, Point2 r) => new(l.X + r.X, l.Y + r.Y);
	public static Point2 operator + (Point2 l, int r) => new(l.X + r, l.Y + r);
	public static Point2 operator - (Point2 l, Point2 r) => new(l.X - r.X, l.Y - r.Y);
	public static Point2 operator - (Point2 l, int r) => new(l.X - r, l.Y - r);
	public static Point2 operator * (Point2 l, Point2 r) => new(l.X * r.X, l.Y * r.Y);
	public static Point2 operator * (Point2 l, int r) => new(l.X * r, l.Y * r);
	public static Point2 operator / (Point2 l, Point2 r) => new(l.X / r.X, l.Y / r.Y);
	public static Point2 operator / (Point2 l, int r) => new(l.X / r, l.Y / r);
	public static Point2 operator % (Point2 l, Point2 r) => new(l.X % r.X, l.Y % r.Y);
	public static Point2 operator % (Point2 l, int r) => new(l.X % r, l.Y % r);

	public static Point2 operator - (Point2 r) => new(-r.X, -r.Y);

	public static explicit operator Point2 (Vec2H o) => new((int)o.X, (int)o.Y);
	public static explicit operator Point2 (Vec2 o) => new((int)o.X, (int)o.Y);
	public static explicit operator Point2 (Vec2D o) => new((int)o.X, (int)o.Y);

	public static explicit operator Point2 (Point2L o) => new((int)o.X, (int)o.Y);
	public static explicit operator Point2 (Point2U o) => new((int)o.X, (int)o.Y);
	public static explicit operator Point2 (Point2UL o) => new((int)o.X, (int)o.Y);
	#endregion // Operators
}


/// <summary>A two-component vector of <c>long</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Point2L :
	IEquatable<Point2L>, IEqualityOperators<Point2L, Point2L, bool>,
	IAdditionOperators<Point2L, Point2L, Point2L>, IAdditionOperators<Point2L, long, Point2L>,
	ISubtractionOperators<Point2L, Point2L, Point2L>, ISubtractionOperators<Point2L, long, Point2L>,
	IMultiplyOperators<Point2L, Point2L, Point2L>, IMultiplyOperators<Point2L, long, Point2L>,
	IDivisionOperators<Point2L, Point2L, Point2L>, IDivisionOperators<Point2L, long, Point2L>,
	IModulusOperators<Point2L, Point2L, Point2L>, IModulusOperators<Point2L, long, Point2L>
	, IUnaryNegationOperators<Point2L, Point2L>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point2L Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point2L UnitX = new(1, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point2L UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public long X;
	/// <summary>The Y-component.</summary>
	public long Y;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length => Double.Sqrt(X*X + Y*Y);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq => X*X + Y*Y;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point2L(long v) => X = Y = v;

	/// <summary>Construct the point from explicit components.</summary>
	public Point2L(long x, long y) => (X, Y) = (x, y);


	#region Base
	public readonly bool Equals(Point2L o) => X == o.X && Y == o.Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point2L p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"[{X},{Y}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)}]";

	public readonly void Deconstruct(out long x, out long y) => (x, y) = (X, Y);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly long Dot(Point2L o) => X*o.X + Y*o.Y;

	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point2L o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2L Clamp(Point2L v, Point2L min, Point2L max) =>
		new(Int64.Clamp(v.X, min.X, max.X), Int64.Clamp(v.Y, min.Y, max.Y));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2L Min(Point2L l, Point2L r) => new(Int64.Min(l.X, r.X), Int64.Min(l.Y, r.Y));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2L Max(Point2L l, Point2L r) => new(Int64.Max(l.X, r.X), Int64.Max(l.Y, r.Y));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point2L l, Point2L r) => l.Equals(r);
	public static bool operator != (Point2L l, Point2L r) => !l.Equals(r);

	public static Bool2 operator <= (Point2L l, Point2L r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Point2L l, Point2L r) => new(l.X < r.X, l.Y < r.Y);
	public static Bool2 operator >= (Point2L l, Point2L r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Point2L l, Point2L r) => new(l.X > r.X, l.Y > r.Y);

	public static Point2L operator + (Point2L l, Point2L r) => new(l.X + r.X, l.Y + r.Y);
	public static Point2L operator + (Point2L l, long r) => new(l.X + r, l.Y + r);
	public static Point2L operator - (Point2L l, Point2L r) => new(l.X - r.X, l.Y - r.Y);
	public static Point2L operator - (Point2L l, long r) => new(l.X - r, l.Y - r);
	public static Point2L operator * (Point2L l, Point2L r) => new(l.X * r.X, l.Y * r.Y);
	public static Point2L operator * (Point2L l, long r) => new(l.X * r, l.Y * r);
	public static Point2L operator / (Point2L l, Point2L r) => new(l.X / r.X, l.Y / r.Y);
	public static Point2L operator / (Point2L l, long r) => new(l.X / r, l.Y / r);
	public static Point2L operator % (Point2L l, Point2L r) => new(l.X % r.X, l.Y % r.Y);
	public static Point2L operator % (Point2L l, long r) => new(l.X % r, l.Y % r);

	public static Point2L operator - (Point2L r) => new(-r.X, -r.Y);

	public static explicit operator Point2L (Vec2H o) => new((long)o.X, (long)o.Y);
	public static explicit operator Point2L (Vec2 o) => new((long)o.X, (long)o.Y);
	public static explicit operator Point2L (Vec2D o) => new((long)o.X, (long)o.Y);

	public static implicit operator Point2L (Point2 o) => new(o.X, o.Y);
	public static implicit operator Point2L (Point2U o) => new(o.X, o.Y);
	public static explicit operator Point2L (Point2UL o) => new((long)o.X, (long)o.Y);
	#endregion // Operators
}


/// <summary>A two-component vector of <c>uint</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 8)]
public struct Point2U :
	IEquatable<Point2U>, IEqualityOperators<Point2U, Point2U, bool>,
	IAdditionOperators<Point2U, Point2U, Point2U>, IAdditionOperators<Point2U, uint, Point2U>,
	ISubtractionOperators<Point2U, Point2U, Point2U>, ISubtractionOperators<Point2U, uint, Point2U>,
	IMultiplyOperators<Point2U, Point2U, Point2U>, IMultiplyOperators<Point2U, uint, Point2U>,
	IDivisionOperators<Point2U, Point2U, Point2U>, IDivisionOperators<Point2U, uint, Point2U>,
	IModulusOperators<Point2U, Point2U, Point2U>, IModulusOperators<Point2U, uint, Point2U>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point2U Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point2U UnitX = new(1, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point2U UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public uint X;
	/// <summary>The Y-component.</summary>
	public uint Y;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length => Single.Sqrt(X*X + Y*Y);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq => X*X + Y*Y;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point2U(uint v) => X = Y = v;

	/// <summary>Construct the point from explicit components.</summary>
	public Point2U(uint x, uint y) => (X, Y) = (x, y);


	#region Base
	public readonly bool Equals(Point2U o) => X == o.X && Y == o.Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point2U p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"[{X},{Y}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)}]";

	public readonly void Deconstruct(out uint x, out uint y) => (x, y) = (X, Y);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly uint Dot(Point2U o) => X*o.X + Y*o.Y;

	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point2U o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2U Clamp(Point2U v, Point2U min, Point2U max) =>
		new(UInt32.Clamp(v.X, min.X, max.X), UInt32.Clamp(v.Y, min.Y, max.Y));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2U Min(Point2U l, Point2U r) => new(UInt32.Min(l.X, r.X), UInt32.Min(l.Y, r.Y));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2U Max(Point2U l, Point2U r) => new(UInt32.Max(l.X, r.X), UInt32.Max(l.Y, r.Y));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point2U l, Point2U r) => l.Equals(r);
	public static bool operator != (Point2U l, Point2U r) => !l.Equals(r);

	public static Bool2 operator <= (Point2U l, Point2U r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Point2U l, Point2U r) => new(l.X < r.X, l.Y < r.Y);
	public static Bool2 operator >= (Point2U l, Point2U r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Point2U l, Point2U r) => new(l.X > r.X, l.Y > r.Y);

	public static Point2U operator + (Point2U l, Point2U r) => new(l.X + r.X, l.Y + r.Y);
	public static Point2U operator + (Point2U l, uint r) => new(l.X + r, l.Y + r);
	public static Point2U operator - (Point2U l, Point2U r) => new(l.X - r.X, l.Y - r.Y);
	public static Point2U operator - (Point2U l, uint r) => new(l.X - r, l.Y - r);
	public static Point2U operator * (Point2U l, Point2U r) => new(l.X * r.X, l.Y * r.Y);
	public static Point2U operator * (Point2U l, uint r) => new(l.X * r, l.Y * r);
	public static Point2U operator / (Point2U l, Point2U r) => new(l.X / r.X, l.Y / r.Y);
	public static Point2U operator / (Point2U l, uint r) => new(l.X / r, l.Y / r);
	public static Point2U operator % (Point2U l, Point2U r) => new(l.X % r.X, l.Y % r.Y);
	public static Point2U operator % (Point2U l, uint r) => new(l.X % r, l.Y % r);

	public static explicit operator Point2U (Vec2H o) => new((uint)o.X, (uint)o.Y);
	public static explicit operator Point2U (Vec2 o) => new((uint)o.X, (uint)o.Y);
	public static explicit operator Point2U (Vec2D o) => new((uint)o.X, (uint)o.Y);

	public static explicit operator Point2U (Point2 o) => new((uint)o.X, (uint)o.Y);
	public static explicit operator Point2U (Point2L o) => new((uint)o.X, (uint)o.Y);
	public static explicit operator Point2U (Point2UL o) => new((uint)o.X, (uint)o.Y);
	#endregion // Operators
}


/// <summary>A two-component vector of <c>ulong</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Point2UL :
	IEquatable<Point2UL>, IEqualityOperators<Point2UL, Point2UL, bool>,
	IAdditionOperators<Point2UL, Point2UL, Point2UL>, IAdditionOperators<Point2UL, ulong, Point2UL>,
	ISubtractionOperators<Point2UL, Point2UL, Point2UL>, ISubtractionOperators<Point2UL, ulong, Point2UL>,
	IMultiplyOperators<Point2UL, Point2UL, Point2UL>, IMultiplyOperators<Point2UL, ulong, Point2UL>,
	IDivisionOperators<Point2UL, Point2UL, Point2UL>, IDivisionOperators<Point2UL, ulong, Point2UL>,
	IModulusOperators<Point2UL, Point2UL, Point2UL>, IModulusOperators<Point2UL, ulong, Point2UL>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point2UL Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point2UL UnitX = new(1, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point2UL UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public ulong X;
	/// <summary>The Y-component.</summary>
	public ulong Y;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length => Double.Sqrt(X*X + Y*Y);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq => X*X + Y*Y;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point2UL(ulong v) => X = Y = v;

	/// <summary>Construct the point from explicit components.</summary>
	public Point2UL(ulong x, ulong y) => (X, Y) = (x, y);


	#region Base
	public readonly bool Equals(Point2UL o) => X == o.X && Y == o.Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point2UL p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"[{X},{Y}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)}]";

	public readonly void Deconstruct(out ulong x, out ulong y) => (x, y) = (X, Y);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly ulong Dot(Point2UL o) => X*o.X + Y*o.Y;

	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point2UL o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2UL Clamp(Point2UL v, Point2UL min, Point2UL max) =>
		new(UInt64.Clamp(v.X, min.X, max.X), UInt64.Clamp(v.Y, min.Y, max.Y));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2UL Min(Point2UL l, Point2UL r) => new(UInt64.Min(l.X, r.X), UInt64.Min(l.Y, r.Y));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2UL Max(Point2UL l, Point2UL r) => new(UInt64.Max(l.X, r.X), UInt64.Max(l.Y, r.Y));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point2UL l, Point2UL r) => l.Equals(r);
	public static bool operator != (Point2UL l, Point2UL r) => !l.Equals(r);

	public static Bool2 operator <= (Point2UL l, Point2UL r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Point2UL l, Point2UL r) => new(l.X < r.X, l.Y < r.Y);
	public static Bool2 operator >= (Point2UL l, Point2UL r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Point2UL l, Point2UL r) => new(l.X > r.X, l.Y > r.Y);

	public static Point2UL operator + (Point2UL l, Point2UL r) => new(l.X + r.X, l.Y + r.Y);
	public static Point2UL operator + (Point2UL l, ulong r) => new(l.X + r, l.Y + r);
	public static Point2UL operator - (Point2UL l, Point2UL r) => new(l.X - r.X, l.Y - r.Y);
	public static Point2UL operator - (Point2UL l, ulong r) => new(l.X - r, l.Y - r);
	public static Point2UL operator * (Point2UL l, Point2UL r) => new(l.X * r.X, l.Y * r.Y);
	public static Point2UL operator * (Point2UL l, ulong r) => new(l.X * r, l.Y * r);
	public static Point2UL operator / (Point2UL l, Point2UL r) => new(l.X / r.X, l.Y / r.Y);
	public static Point2UL operator / (Point2UL l, ulong r) => new(l.X / r, l.Y / r);
	public static Point2UL operator % (Point2UL l, Point2UL r) => new(l.X % r.X, l.Y % r.Y);
	public static Point2UL operator % (Point2UL l, ulong r) => new(l.X % r, l.Y % r);

	public static explicit operator Point2UL (Vec2H o) => new((ulong)o.X, (ulong)o.Y);
	public static explicit operator Point2UL (Vec2 o) => new((ulong)o.X, (ulong)o.Y);
	public static explicit operator Point2UL (Vec2D o) => new((ulong)o.X, (ulong)o.Y);

	public static explicit operator Point2UL (Point2 o) => new((ulong)o.X, (ulong)o.Y);
	public static implicit operator Point2UL (Point2U o) => new(o.X, o.Y);
	public static explicit operator Point2UL (Point2L o) => new((ulong)o.X, (ulong)o.Y);
	#endregion // Operators
}

