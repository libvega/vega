﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

/* This file was generated from a template. Do not edit by hand. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
// ReSharper disable InconsistentNaming RedundantCast

namespace Astrum.Maths;


/// <summary>A three-component vector of <c>int</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct Point3 :
	IEquatable<Point3>, IEqualityOperators<Point3, Point3, bool>,
	IAdditionOperators<Point3, Point3, Point3>, IAdditionOperators<Point3, int, Point3>,
	ISubtractionOperators<Point3, Point3, Point3>, ISubtractionOperators<Point3, int, Point3>,
	IMultiplyOperators<Point3, Point3, Point3>, IMultiplyOperators<Point3, int, Point3>,
	IDivisionOperators<Point3, Point3, Point3>, IDivisionOperators<Point3, int, Point3>,
	IModulusOperators<Point3, Point3, Point3>, IModulusOperators<Point3, int, Point3>
	, IUnaryNegationOperators<Point3, Point3>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point3 Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point3 UnitX = new(1, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point3 UnitY = new(0, 1, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point3 UnitZ = new(0, 0, 1);
	/// <summary>Right-pointing point in right-handed space, along +X.</summary>
	public static readonly Point3 Right = new(1, 0, 0);
	/// <summary>Up-pointing point in right-handed space, along +Y.</summary>
	public static readonly Point3 Up = new(0, 1, 0);
	/// <summary>Backward-pointing point in right-handed space, along +Z.</summary>
	public static readonly Point3 Backward = new(0, 0, 1);
	/// <summary>Left-pointing point in right-handed space, along -X.</summary>
	public static readonly Point3 Left = new(-1, 0, 0);
	/// <summary>Down-pointing point in right-handed space, along -Y.</summary>
	public static readonly Point3 Down = new(0, -1, 0);
	/// <summary>Forward-pointing point in right-handed space, along -Z.</summary>
	public static readonly Point3 Forward = new(0, 0, -1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public int X;
	/// <summary>The Y-component.</summary>
	public int Y;
	/// <summary>The Z-component.</summary>
	public int Z;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length => Single.Sqrt(X*X + Y*Y + Z*Z);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq => X*X + Y*Y + Z*Z;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point3(int v) => X = Y = Z = v;

	/// <summary>Construct the point from an existing X,Y point and Z component.</summary>
	public Point3(Point2 xy, int z) => (X, Y, Z) = (xy.X, xy.Y, z);

	/// <summary>Construct the point from explicit components.</summary>
	public Point3(int x, int y, int z) => (X, Y, Z) = (x, y, z);


	#region Base
	public readonly bool Equals(Point3 o) => X == o.X && Y == o.Y && Z == o.Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point3 p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"[{X},{Y},{Z}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}]";

	public readonly void Deconstruct(out int x, out int y, out int z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly int Dot(Point3 o) => X*o.X + Y*o.Y + Z*o.Z;

	/// <summary>Calculates the cartesian cross-product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Point3 Cross(Point3 o) => new(Y * o.Z - Z * o.Y, Z * o.X - X * o.Z, X * o.Y - Y * o.X);

	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point3 o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3 Clamp(Point3 v, Point3 min, Point3 max) =>
		new(Int32.Clamp(v.X, min.X, max.X), Int32.Clamp(v.Y, min.Y, max.Y), Int32.Clamp(v.Z, min.Z, max.Z));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3 Min(Point3 l, Point3 r) =>
		new(Int32.Min(l.X, r.X), Int32.Min(l.Y, r.Y), Int32.Min(l.Z, r.Z));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3 Max(Point3 l, Point3 r) =>
		new(Int32.Max(l.X, r.X), Int32.Max(l.Y, r.Y), Int32.Max(l.Z, r.Z));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point3 l, Point3 r) => l.Equals(r);
	public static bool operator != (Point3 l, Point3 r) => !l.Equals(r);

	public static Bool3 operator <= (Point3 l, Point3 r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Point3 l, Point3 r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Point3 l, Point3 r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Point3 l, Point3 r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Point3 operator + (Point3 l, Point3 r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Point3 operator + (Point3 l, int r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Point3 operator - (Point3 l, Point3 r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Point3 operator - (Point3 l, int r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Point3 operator * (Point3 l, Point3 r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Point3 operator * (Point3 l, int r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Point3 operator / (Point3 l, Point3 r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Point3 operator / (Point3 l, int r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Point3 operator % (Point3 l, Point3 r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Point3 operator % (Point3 l, int r) => new(l.X % r, l.Y % r, l.Z % r);

	public static Point3 operator - (Point3 r) => new(-r.X, -r.Y, -r.Z);

	public static explicit operator Point3 (Vec3H o) => new((int)o.X, (int)o.Y, (int)o.Z);
	public static explicit operator Point3 (Vec3 o) => new((int)o.X, (int)o.Y, (int)o.Z);
	public static explicit operator Point3 (Vec3D o) => new((int)o.X, (int)o.Y, (int)o.Z);

	public static explicit operator Point3 (Point3L o) => new((int)o.X, (int)o.Y, (int)o.Z);
	public static explicit operator Point3 (Point3U o) => new((int)o.X, (int)o.Y, (int)o.Z);
	public static explicit operator Point3 (Point3UL o) => new((int)o.X, (int)o.Y, (int)o.Z);
	#endregion // Operators
}


/// <summary>A three-component vector of <c>long</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public struct Point3L :
	IEquatable<Point3L>, IEqualityOperators<Point3L, Point3L, bool>,
	IAdditionOperators<Point3L, Point3L, Point3L>, IAdditionOperators<Point3L, long, Point3L>,
	ISubtractionOperators<Point3L, Point3L, Point3L>, ISubtractionOperators<Point3L, long, Point3L>,
	IMultiplyOperators<Point3L, Point3L, Point3L>, IMultiplyOperators<Point3L, long, Point3L>,
	IDivisionOperators<Point3L, Point3L, Point3L>, IDivisionOperators<Point3L, long, Point3L>,
	IModulusOperators<Point3L, Point3L, Point3L>, IModulusOperators<Point3L, long, Point3L>
	, IUnaryNegationOperators<Point3L, Point3L>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point3L Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point3L UnitX = new(1, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point3L UnitY = new(0, 1, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point3L UnitZ = new(0, 0, 1);
	/// <summary>Right-pointing point in right-handed space, along +X.</summary>
	public static readonly Point3L Right = new(1, 0, 0);
	/// <summary>Up-pointing point in right-handed space, along +Y.</summary>
	public static readonly Point3L Up = new(0, 1, 0);
	/// <summary>Backward-pointing point in right-handed space, along +Z.</summary>
	public static readonly Point3L Backward = new(0, 0, 1);
	/// <summary>Left-pointing point in right-handed space, along -X.</summary>
	public static readonly Point3L Left = new(-1, 0, 0);
	/// <summary>Down-pointing point in right-handed space, along -Y.</summary>
	public static readonly Point3L Down = new(0, -1, 0);
	/// <summary>Forward-pointing point in right-handed space, along -Z.</summary>
	public static readonly Point3L Forward = new(0, 0, -1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public long X;
	/// <summary>The Y-component.</summary>
	public long Y;
	/// <summary>The Z-component.</summary>
	public long Z;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length => Double.Sqrt(X*X + Y*Y + Z*Z);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq => X*X + Y*Y + Z*Z;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point3L(long v) => X = Y = Z = v;

	/// <summary>Construct the point from an existing X,Y point and Z component.</summary>
	public Point3L(Point2L xy, long z) => (X, Y, Z) = (xy.X, xy.Y, z);

	/// <summary>Construct the point from explicit components.</summary>
	public Point3L(long x, long y, long z) => (X, Y, Z) = (x, y, z);


	#region Base
	public readonly bool Equals(Point3L o) => X == o.X && Y == o.Y && Z == o.Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point3L p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"[{X},{Y},{Z}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}]";

	public readonly void Deconstruct(out long x, out long y, out long z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly long Dot(Point3L o) => X*o.X + Y*o.Y + Z*o.Z;

	/// <summary>Calculates the cartesian cross-product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Point3L Cross(Point3L o) => new(Y * o.Z - Z * o.Y, Z * o.X - X * o.Z, X * o.Y - Y * o.X);

	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point3L o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3L Clamp(Point3L v, Point3L min, Point3L max) =>
		new(Int64.Clamp(v.X, min.X, max.X), Int64.Clamp(v.Y, min.Y, max.Y), Int64.Clamp(v.Z, min.Z, max.Z));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3L Min(Point3L l, Point3L r) =>
		new(Int64.Min(l.X, r.X), Int64.Min(l.Y, r.Y), Int64.Min(l.Z, r.Z));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3L Max(Point3L l, Point3L r) =>
		new(Int64.Max(l.X, r.X), Int64.Max(l.Y, r.Y), Int64.Max(l.Z, r.Z));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point3L l, Point3L r) => l.Equals(r);
	public static bool operator != (Point3L l, Point3L r) => !l.Equals(r);

	public static Bool3 operator <= (Point3L l, Point3L r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Point3L l, Point3L r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Point3L l, Point3L r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Point3L l, Point3L r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Point3L operator + (Point3L l, Point3L r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Point3L operator + (Point3L l, long r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Point3L operator - (Point3L l, Point3L r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Point3L operator - (Point3L l, long r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Point3L operator * (Point3L l, Point3L r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Point3L operator * (Point3L l, long r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Point3L operator / (Point3L l, Point3L r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Point3L operator / (Point3L l, long r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Point3L operator % (Point3L l, Point3L r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Point3L operator % (Point3L l, long r) => new(l.X % r, l.Y % r, l.Z % r);

	public static Point3L operator - (Point3L r) => new(-r.X, -r.Y, -r.Z);

	public static explicit operator Point3L (Vec3H o) => new((long)o.X, (long)o.Y, (long)o.Z);
	public static explicit operator Point3L (Vec3 o) => new((long)o.X, (long)o.Y, (long)o.Z);
	public static explicit operator Point3L (Vec3D o) => new((long)o.X, (long)o.Y, (long)o.Z);

	public static implicit operator Point3L (Point3 o) => new(o.X, o.Y, o.Z);
	public static implicit operator Point3L (Point3U o) => new(o.X, o.Y, o.Z);
	public static explicit operator Point3L (Point3UL o) => new((long)o.X, (long)o.Y, (long)o.Z);
	#endregion // Operators
}


/// <summary>A three-component vector of <c>uint</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct Point3U :
	IEquatable<Point3U>, IEqualityOperators<Point3U, Point3U, bool>,
	IAdditionOperators<Point3U, Point3U, Point3U>, IAdditionOperators<Point3U, uint, Point3U>,
	ISubtractionOperators<Point3U, Point3U, Point3U>, ISubtractionOperators<Point3U, uint, Point3U>,
	IMultiplyOperators<Point3U, Point3U, Point3U>, IMultiplyOperators<Point3U, uint, Point3U>,
	IDivisionOperators<Point3U, Point3U, Point3U>, IDivisionOperators<Point3U, uint, Point3U>,
	IModulusOperators<Point3U, Point3U, Point3U>, IModulusOperators<Point3U, uint, Point3U>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point3U Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point3U UnitX = new(1, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point3U UnitY = new(0, 1, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point3U UnitZ = new(0, 0, 1);
	/// <summary>Right-pointing point in right-handed space, along +X.</summary>
	public static readonly Point3U Right = new(1, 0, 0);
	/// <summary>Up-pointing point in right-handed space, along +Y.</summary>
	public static readonly Point3U Up = new(0, 1, 0);
	/// <summary>Backward-pointing point in right-handed space, along +Z.</summary>
	public static readonly Point3U Backward = new(0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public uint X;
	/// <summary>The Y-component.</summary>
	public uint Y;
	/// <summary>The Z-component.</summary>
	public uint Z;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length => Single.Sqrt(X*X + Y*Y + Z*Z);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq => X*X + Y*Y + Z*Z;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point3U(uint v) => X = Y = Z = v;

	/// <summary>Construct the point from an existing X,Y point and Z component.</summary>
	public Point3U(Point2U xy, uint z) => (X, Y, Z) = (xy.X, xy.Y, z);

	/// <summary>Construct the point from explicit components.</summary>
	public Point3U(uint x, uint y, uint z) => (X, Y, Z) = (x, y, z);


	#region Base
	public readonly bool Equals(Point3U o) => X == o.X && Y == o.Y && Z == o.Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point3U p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"[{X},{Y},{Z}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}]";

	public readonly void Deconstruct(out uint x, out uint y, out uint z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly uint Dot(Point3U o) => X*o.X + Y*o.Y + Z*o.Z;


	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point3U o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3U Clamp(Point3U v, Point3U min, Point3U max) =>
		new(UInt32.Clamp(v.X, min.X, max.X), UInt32.Clamp(v.Y, min.Y, max.Y), UInt32.Clamp(v.Z, min.Z, max.Z));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3U Min(Point3U l, Point3U r) =>
		new(UInt32.Min(l.X, r.X), UInt32.Min(l.Y, r.Y), UInt32.Min(l.Z, r.Z));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3U Max(Point3U l, Point3U r) =>
		new(UInt32.Max(l.X, r.X), UInt32.Max(l.Y, r.Y), UInt32.Max(l.Z, r.Z));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point3U l, Point3U r) => l.Equals(r);
	public static bool operator != (Point3U l, Point3U r) => !l.Equals(r);

	public static Bool3 operator <= (Point3U l, Point3U r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Point3U l, Point3U r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Point3U l, Point3U r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Point3U l, Point3U r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Point3U operator + (Point3U l, Point3U r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Point3U operator + (Point3U l, uint r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Point3U operator - (Point3U l, Point3U r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Point3U operator - (Point3U l, uint r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Point3U operator * (Point3U l, Point3U r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Point3U operator * (Point3U l, uint r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Point3U operator / (Point3U l, Point3U r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Point3U operator / (Point3U l, uint r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Point3U operator % (Point3U l, Point3U r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Point3U operator % (Point3U l, uint r) => new(l.X % r, l.Y % r, l.Z % r);

	public static explicit operator Point3U (Vec3H o) => new((uint)o.X, (uint)o.Y, (uint)o.Z);
	public static explicit operator Point3U (Vec3 o) => new((uint)o.X, (uint)o.Y, (uint)o.Z);
	public static explicit operator Point3U (Vec3D o) => new((uint)o.X, (uint)o.Y, (uint)o.Z);

	public static explicit operator Point3U (Point3 o) => new((uint)o.X, (uint)o.Y, (uint)o.Z);
	public static explicit operator Point3U (Point3L o) => new((uint)o.X, (uint)o.Y, (uint)o.Z);
	public static explicit operator Point3U (Point3UL o) => new((uint)o.X, (uint)o.Y, (uint)o.Z);
	#endregion // Operators
}


/// <summary>A three-component vector of <c>ulong</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public struct Point3UL :
	IEquatable<Point3UL>, IEqualityOperators<Point3UL, Point3UL, bool>,
	IAdditionOperators<Point3UL, Point3UL, Point3UL>, IAdditionOperators<Point3UL, ulong, Point3UL>,
	ISubtractionOperators<Point3UL, Point3UL, Point3UL>, ISubtractionOperators<Point3UL, ulong, Point3UL>,
	IMultiplyOperators<Point3UL, Point3UL, Point3UL>, IMultiplyOperators<Point3UL, ulong, Point3UL>,
	IDivisionOperators<Point3UL, Point3UL, Point3UL>, IDivisionOperators<Point3UL, ulong, Point3UL>,
	IModulusOperators<Point3UL, Point3UL, Point3UL>, IModulusOperators<Point3UL, ulong, Point3UL>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point3UL Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point3UL UnitX = new(1, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point3UL UnitY = new(0, 1, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point3UL UnitZ = new(0, 0, 1);
	/// <summary>Right-pointing point in right-handed space, along +X.</summary>
	public static readonly Point3UL Right = new(1, 0, 0);
	/// <summary>Up-pointing point in right-handed space, along +Y.</summary>
	public static readonly Point3UL Up = new(0, 1, 0);
	/// <summary>Backward-pointing point in right-handed space, along +Z.</summary>
	public static readonly Point3UL Backward = new(0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public ulong X;
	/// <summary>The Y-component.</summary>
	public ulong Y;
	/// <summary>The Z-component.</summary>
	public ulong Z;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length => Double.Sqrt(X*X + Y*Y + Z*Z);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq => X*X + Y*Y + Z*Z;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point3UL(ulong v) => X = Y = Z = v;

	/// <summary>Construct the point from an existing X,Y point and Z component.</summary>
	public Point3UL(Point2UL xy, ulong z) => (X, Y, Z) = (xy.X, xy.Y, z);

	/// <summary>Construct the point from explicit components.</summary>
	public Point3UL(ulong x, ulong y, ulong z) => (X, Y, Z) = (x, y, z);


	#region Base
	public readonly bool Equals(Point3UL o) => X == o.X && Y == o.Y && Z == o.Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point3UL p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"[{X},{Y},{Z}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}]";

	public readonly void Deconstruct(out ulong x, out ulong y, out ulong z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly ulong Dot(Point3UL o) => X*o.X + Y*o.Y + Z*o.Z;


	/// <summary>Calculates the angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point3UL o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3UL Clamp(Point3UL v, Point3UL min, Point3UL max) =>
		new(UInt64.Clamp(v.X, min.X, max.X), UInt64.Clamp(v.Y, min.Y, max.Y), UInt64.Clamp(v.Z, min.Z, max.Z));

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3UL Min(Point3UL l, Point3UL r) =>
		new(UInt64.Min(l.X, r.X), UInt64.Min(l.Y, r.Y), UInt64.Min(l.Z, r.Z));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3UL Max(Point3UL l, Point3UL r) =>
		new(UInt64.Max(l.X, r.X), UInt64.Max(l.Y, r.Y), UInt64.Max(l.Z, r.Z));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point3UL l, Point3UL r) => l.Equals(r);
	public static bool operator != (Point3UL l, Point3UL r) => !l.Equals(r);

	public static Bool3 operator <= (Point3UL l, Point3UL r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Point3UL l, Point3UL r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Point3UL l, Point3UL r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Point3UL l, Point3UL r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Point3UL operator + (Point3UL l, Point3UL r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Point3UL operator + (Point3UL l, ulong r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Point3UL operator - (Point3UL l, Point3UL r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Point3UL operator - (Point3UL l, ulong r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Point3UL operator * (Point3UL l, Point3UL r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Point3UL operator * (Point3UL l, ulong r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Point3UL operator / (Point3UL l, Point3UL r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Point3UL operator / (Point3UL l, ulong r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Point3UL operator % (Point3UL l, Point3UL r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Point3UL operator % (Point3UL l, ulong r) => new(l.X % r, l.Y % r, l.Z % r);

	public static explicit operator Point3UL (Vec3H o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z);
	public static explicit operator Point3UL (Vec3 o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z);
	public static explicit operator Point3UL (Vec3D o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z);

	public static explicit operator Point3UL (Point3 o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z);
	public static implicit operator Point3UL (Point3U o) => new(o.X, o.Y, o.Z);
	public static explicit operator Point3UL (Point3L o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z);
	#endregion // Operators
}

