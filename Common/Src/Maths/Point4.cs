﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

/* This file was generated from a template. Do not edit by hand. */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
// ReSharper disable InconsistentNaming RedundantCast

namespace Astrum.Maths;


/// <summary>A four-component vector of <c>int</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Point4 :
	IEquatable<Point4>, IEqualityOperators<Point4, Point4, bool>,
	IAdditionOperators<Point4, Point4, Point4>, IAdditionOperators<Point4, int, Point4>,
	ISubtractionOperators<Point4, Point4, Point4>, ISubtractionOperators<Point4, int, Point4>,
	IMultiplyOperators<Point4, Point4, Point4>, IMultiplyOperators<Point4, int, Point4>,
	IDivisionOperators<Point4, Point4, Point4>, IDivisionOperators<Point4, int, Point4>,
	IModulusOperators<Point4, Point4, Point4>, IModulusOperators<Point4, int, Point4>
	, IUnaryNegationOperators<Point4, Point4>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point4 Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point4 UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point4 UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point4 UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Point4 UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public int X;
	/// <summary>The Y-component.</summary>
	public int Y;
	/// <summary>The Z-component.</summary>
	public int Z;
	/// <summary>The W-component.</summary>
	public int W;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length => Single.Sqrt(X*X + Y*Y + Z*Z + W*W);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq => X*X + Y*Y + Z*Z + W*W;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point4(int v) => X = Y = Z = W = v;

	/// <summary>Construct the point from an existing X,Y,Z point and w component.</summary>
	public Point4(Point3 xyz, int w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the point from an existing X,Y and Z,W points.</summary>
	public Point4(Point2 xy, Point2 zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the point from explicit components.</summary>
	public Point4(int x, int y, int z, int w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	public readonly bool Equals(Point4 o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point4 p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"[{X},{Y},{Z},{W}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}]";

	public readonly void Deconstruct(out int x, out int y, out int z, out int w) => (x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly int Dot(Point4 o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4 Clamp(Point4 v, Point4 min, Point4 max) => new(
		Int32.Clamp(v.X, min.X, max.X), Int32.Clamp(v.Y, min.Y, max.Y),
		Int32.Clamp(v.Z, min.Z, max.Z), Int32.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4 Min(Point4 l, Point4 r) =>
		new(Int32.Min(l.X, r.X), Int32.Min(l.Y, r.Y), Int32.Min(l.Z, r.Z), Int32.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4 Max(Point4 l, Point4 r) =>
		new(Int32.Max(l.X, r.X), Int32.Max(l.Y, r.Y), Int32.Max(l.Z, r.Z), Int32.Max(l.W, r.W));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point4 l, Point4 r) => l.Equals(r);
	public static bool operator != (Point4 l, Point4 r) => !l.Equals(r);

	public static Bool4 operator <= (Point4 l, Point4 r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Point4 l, Point4 r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Point4 l, Point4 r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Point4 l, Point4 r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Point4 operator + (Point4 l, Point4 r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Point4 operator + (Point4 l, int r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Point4 operator - (Point4 l, Point4 r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Point4 operator - (Point4 l, int r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Point4 operator * (Point4 l, Point4 r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Point4 operator * (Point4 l, int r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Point4 operator / (Point4 l, Point4 r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Point4 operator / (Point4 l, int r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Point4 operator % (Point4 l, Point4 r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Point4 operator % (Point4 l, int r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static Point4 operator - (Point4 r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator Point4 (Vec4H o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);
	public static explicit operator Point4 (Vec4 o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);
	public static explicit operator Point4 (Vec4D o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);

	public static explicit operator Point4 (Point4L o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);
	public static explicit operator Point4 (Point4U o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);
	public static explicit operator Point4 (Point4UL o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);
	#endregion // Operators
}


/// <summary>A four-component vector of <c>long</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct Point4L :
	IEquatable<Point4L>, IEqualityOperators<Point4L, Point4L, bool>,
	IAdditionOperators<Point4L, Point4L, Point4L>, IAdditionOperators<Point4L, long, Point4L>,
	ISubtractionOperators<Point4L, Point4L, Point4L>, ISubtractionOperators<Point4L, long, Point4L>,
	IMultiplyOperators<Point4L, Point4L, Point4L>, IMultiplyOperators<Point4L, long, Point4L>,
	IDivisionOperators<Point4L, Point4L, Point4L>, IDivisionOperators<Point4L, long, Point4L>,
	IModulusOperators<Point4L, Point4L, Point4L>, IModulusOperators<Point4L, long, Point4L>
	, IUnaryNegationOperators<Point4L, Point4L>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point4L Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point4L UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point4L UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point4L UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Point4L UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public long X;
	/// <summary>The Y-component.</summary>
	public long Y;
	/// <summary>The Z-component.</summary>
	public long Z;
	/// <summary>The W-component.</summary>
	public long W;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length => Double.Sqrt(X*X + Y*Y + Z*Z + W*W);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq => X*X + Y*Y + Z*Z + W*W;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point4L(long v) => X = Y = Z = W = v;

	/// <summary>Construct the point from an existing X,Y,Z point and w component.</summary>
	public Point4L(Point3L xyz, long w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the point from an existing X,Y and Z,W points.</summary>
	public Point4L(Point2L xy, Point2L zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the point from explicit components.</summary>
	public Point4L(long x, long y, long z, long w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	public readonly bool Equals(Point4L o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point4L p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"[{X},{Y},{Z},{W}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}]";

	public readonly void Deconstruct(out long x, out long y, out long z, out long w) => (x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly long Dot(Point4L o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4L Clamp(Point4L v, Point4L min, Point4L max) => new(
		Int64.Clamp(v.X, min.X, max.X), Int64.Clamp(v.Y, min.Y, max.Y),
		Int64.Clamp(v.Z, min.Z, max.Z), Int64.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4L Min(Point4L l, Point4L r) =>
		new(Int64.Min(l.X, r.X), Int64.Min(l.Y, r.Y), Int64.Min(l.Z, r.Z), Int64.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4L Max(Point4L l, Point4L r) =>
		new(Int64.Max(l.X, r.X), Int64.Max(l.Y, r.Y), Int64.Max(l.Z, r.Z), Int64.Max(l.W, r.W));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point4L l, Point4L r) => l.Equals(r);
	public static bool operator != (Point4L l, Point4L r) => !l.Equals(r);

	public static Bool4 operator <= (Point4L l, Point4L r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Point4L l, Point4L r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Point4L l, Point4L r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Point4L l, Point4L r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Point4L operator + (Point4L l, Point4L r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Point4L operator + (Point4L l, long r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Point4L operator - (Point4L l, Point4L r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Point4L operator - (Point4L l, long r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Point4L operator * (Point4L l, Point4L r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Point4L operator * (Point4L l, long r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Point4L operator / (Point4L l, Point4L r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Point4L operator / (Point4L l, long r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Point4L operator % (Point4L l, Point4L r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Point4L operator % (Point4L l, long r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static Point4L operator - (Point4L r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator Point4L (Vec4H o) => new((long)o.X, (long)o.Y, (long)o.Z, (long)o.W);
	public static explicit operator Point4L (Vec4 o) => new((long)o.X, (long)o.Y, (long)o.Z, (long)o.W);
	public static explicit operator Point4L (Vec4D o) => new((long)o.X, (long)o.Y, (long)o.Z, (long)o.W);

	public static implicit operator Point4L (Point4 o) => new(o.X, o.Y, o.Z, o.W);
	public static implicit operator Point4L (Point4U o) => new(o.X, o.Y, o.Z, o.W);
	public static explicit operator Point4L (Point4UL o) => new((long)o.X, (long)o.Y, (long)o.Z, (long)o.W);
	#endregion // Operators
}


/// <summary>A four-component vector of <c>uint</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Point4U :
	IEquatable<Point4U>, IEqualityOperators<Point4U, Point4U, bool>,
	IAdditionOperators<Point4U, Point4U, Point4U>, IAdditionOperators<Point4U, uint, Point4U>,
	ISubtractionOperators<Point4U, Point4U, Point4U>, ISubtractionOperators<Point4U, uint, Point4U>,
	IMultiplyOperators<Point4U, Point4U, Point4U>, IMultiplyOperators<Point4U, uint, Point4U>,
	IDivisionOperators<Point4U, Point4U, Point4U>, IDivisionOperators<Point4U, uint, Point4U>,
	IModulusOperators<Point4U, Point4U, Point4U>, IModulusOperators<Point4U, uint, Point4U>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point4U Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point4U UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point4U UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point4U UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Point4U UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public uint X;
	/// <summary>The Y-component.</summary>
	public uint Y;
	/// <summary>The Z-component.</summary>
	public uint Z;
	/// <summary>The W-component.</summary>
	public uint W;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length => Single.Sqrt(X*X + Y*Y + Z*Z + W*W);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq => X*X + Y*Y + Z*Z + W*W;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point4U(uint v) => X = Y = Z = W = v;

	/// <summary>Construct the point from an existing X,Y,Z point and w component.</summary>
	public Point4U(Point3U xyz, uint w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the point from an existing X,Y and Z,W points.</summary>
	public Point4U(Point2U xy, Point2U zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the point from explicit components.</summary>
	public Point4U(uint x, uint y, uint z, uint w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	public readonly bool Equals(Point4U o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point4U p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"[{X},{Y},{Z},{W}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}]";

	public readonly void Deconstruct(out uint x, out uint y, out uint z, out uint w) => (x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly uint Dot(Point4U o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4U Clamp(Point4U v, Point4U min, Point4U max) => new(
		UInt32.Clamp(v.X, min.X, max.X), UInt32.Clamp(v.Y, min.Y, max.Y),
		UInt32.Clamp(v.Z, min.Z, max.Z), UInt32.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4U Min(Point4U l, Point4U r) =>
		new(UInt32.Min(l.X, r.X), UInt32.Min(l.Y, r.Y), UInt32.Min(l.Z, r.Z), UInt32.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4U Max(Point4U l, Point4U r) =>
		new(UInt32.Max(l.X, r.X), UInt32.Max(l.Y, r.Y), UInt32.Max(l.Z, r.Z), UInt32.Max(l.W, r.W));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point4U l, Point4U r) => l.Equals(r);
	public static bool operator != (Point4U l, Point4U r) => !l.Equals(r);

	public static Bool4 operator <= (Point4U l, Point4U r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Point4U l, Point4U r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Point4U l, Point4U r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Point4U l, Point4U r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Point4U operator + (Point4U l, Point4U r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Point4U operator + (Point4U l, uint r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Point4U operator - (Point4U l, Point4U r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Point4U operator - (Point4U l, uint r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Point4U operator * (Point4U l, Point4U r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Point4U operator * (Point4U l, uint r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Point4U operator / (Point4U l, Point4U r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Point4U operator / (Point4U l, uint r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Point4U operator % (Point4U l, Point4U r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Point4U operator % (Point4U l, uint r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static explicit operator Point4U (Vec4H o) => new((uint)o.X, (uint)o.Y, (uint)o.Z, (uint)o.W);
	public static explicit operator Point4U (Vec4 o) => new((uint)o.X, (uint)o.Y, (uint)o.Z, (uint)o.W);
	public static explicit operator Point4U (Vec4D o) => new((uint)o.X, (uint)o.Y, (uint)o.Z, (uint)o.W);

	public static explicit operator Point4U (Point4 o) => new((uint)o.X, (uint)o.Y, (uint)o.Z, (uint)o.W);
	public static explicit operator Point4U (Point4L o) => new((uint)o.X, (uint)o.Y, (uint)o.Z, (uint)o.W);
	public static explicit operator Point4U (Point4UL o) => new((uint)o.X, (uint)o.Y, (uint)o.Z, (uint)o.W);
	#endregion // Operators
}


/// <summary>A four-component vector of <c>ulong</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct Point4UL :
	IEquatable<Point4UL>, IEqualityOperators<Point4UL, Point4UL, bool>,
	IAdditionOperators<Point4UL, Point4UL, Point4UL>, IAdditionOperators<Point4UL, ulong, Point4UL>,
	ISubtractionOperators<Point4UL, Point4UL, Point4UL>, ISubtractionOperators<Point4UL, ulong, Point4UL>,
	IMultiplyOperators<Point4UL, Point4UL, Point4UL>, IMultiplyOperators<Point4UL, ulong, Point4UL>,
	IDivisionOperators<Point4UL, Point4UL, Point4UL>, IDivisionOperators<Point4UL, ulong, Point4UL>,
	IModulusOperators<Point4UL, Point4UL, Point4UL>, IModulusOperators<Point4UL, ulong, Point4UL>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point4UL Zero = new();
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point4UL UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point4UL UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point4UL UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Point4UL UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public ulong X;
	/// <summary>The Y-component.</summary>
	public ulong Y;
	/// <summary>The Z-component.</summary>
	public ulong Z;
	/// <summary>The W-component.</summary>
	public ulong W;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length => Double.Sqrt(X*X + Y*Y + Z*Z + W*W);

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq => X*X + Y*Y + Z*Z + W*W;
	#endregion // Fields

	/// <summary>Construct the point with the given value for all components.</summary>
	public Point4UL(ulong v) => X = Y = Z = W = v;

	/// <summary>Construct the point from an existing X,Y,Z point and w component.</summary>
	public Point4UL(Point3UL xyz, ulong w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the point from an existing X,Y and Z,W points.</summary>
	public Point4UL(Point2UL xy, Point2UL zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the point from explicit components.</summary>
	public Point4UL(ulong x, ulong y, ulong z, ulong w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	public readonly bool Equals(Point4UL o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point4UL p && Equals(p);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"[{X},{Y},{Z},{W}]";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"[{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}]";

	public readonly void Deconstruct(out ulong x, out ulong y, out ulong z, out ulong w) => (x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly ulong Dot(Point4UL o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the point components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4UL Clamp(Point4UL v, Point4UL min, Point4UL max) => new(
		UInt64.Clamp(v.X, min.X, max.X), UInt64.Clamp(v.Y, min.Y, max.Y),
		UInt64.Clamp(v.Z, min.Z, max.Z), UInt64.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4UL Min(Point4UL l, Point4UL r) =>
		new(UInt64.Min(l.X, r.X), UInt64.Min(l.Y, r.Y), UInt64.Min(l.Z, r.Z), UInt64.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4UL Max(Point4UL l, Point4UL r) =>
		new(UInt64.Max(l.X, r.X), UInt64.Max(l.Y, r.Y), UInt64.Max(l.Z, r.Z), UInt64.Max(l.W, r.W));
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point4UL l, Point4UL r) => l.Equals(r);
	public static bool operator != (Point4UL l, Point4UL r) => !l.Equals(r);

	public static Bool4 operator <= (Point4UL l, Point4UL r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Point4UL l, Point4UL r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Point4UL l, Point4UL r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Point4UL l, Point4UL r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Point4UL operator + (Point4UL l, Point4UL r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Point4UL operator + (Point4UL l, ulong r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Point4UL operator - (Point4UL l, Point4UL r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Point4UL operator - (Point4UL l, ulong r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Point4UL operator * (Point4UL l, Point4UL r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Point4UL operator * (Point4UL l, ulong r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Point4UL operator / (Point4UL l, Point4UL r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Point4UL operator / (Point4UL l, ulong r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Point4UL operator % (Point4UL l, Point4UL r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Point4UL operator % (Point4UL l, ulong r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static explicit operator Point4UL (Vec4H o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z, (ulong)o.W);
	public static explicit operator Point4UL (Vec4 o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z, (ulong)o.W);
	public static explicit operator Point4UL (Vec4D o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z, (ulong)o.W);

	public static explicit operator Point4UL (Point4 o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z, (ulong)o.W);
	public static implicit operator Point4UL (Point4U o) => new(o.X, o.Y, o.Z, o.W);
	public static explicit operator Point4UL (Point4L o) => new((ulong)o.X, (ulong)o.Y, (ulong)o.Z, (ulong)o.W);
	#endregion // Operators
}

