﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Astrum;


/// <summary>Manages a compact collection of bit flags of arbitrary size.</summary>
/// <remarks>For bitmasks of size &lt;= 64, this type is free of additional allocations.</remarks>
public sealed class Bitmask
{
	private const uint WORD_SIZE  = sizeof(ulong) * 8;
	private const ulong WORD_NONE = 0UL;
	private const ulong WORD_ALL  = 0xFFFF_FFFF_FFFF_FFFFUL;


	#region Fields
	/// <summary>The total number of bits in the mask.</summary>
	public readonly uint Size;
	private readonly uint _words; // The number of words used by the mask
	
	/// <summary>Get or set the bit at the given index.</summary>
	public bool this[uint index] {
		get => Get(index);
		set { if (value) Set(index); else Clear(index); }
	}

	/// <summary>The number of bits set to one/true in the set.</summary>
	public uint Count {
		get {
			if (_words <= 1) return (uint)BitOperations.PopCount(_smallBits);

			uint size = 0;
			for (uint wi = 0; wi < _bits!.Length; ++wi) {
				size += (uint)BitOperations.PopCount(_bits[wi]);
			}
			return size;
		}
	}

	/// <summary>If no bits are set.</summary>
	public bool None => Count == 0;
	/// <summary>If at least one bit is set.</summary>
	public bool Any => Count > 0;
	/// <summary>If all bits are set.</summary>
	public bool All => Count == Size;
	
	// The array of data
	private readonly ulong[]? _bits;
	// For small bitsets (<= WORD_SIZE), use an inline value instead of a separate array
	private ulong _smallBits;
	#endregion // Fields
	
	
	/// <summary>Create a new mask with the given number of bits, all set to zero.</summary>
	/// <param name="count">The number of bits to include in the set.</param>
	public Bitmask(uint count)
	{
		Size = count;
		_words = CountToWords(count);

		if (_words <= 1) {
			_bits = null;
			_smallBits = 0;
		}
		else {
			_bits = new ulong[_words]; // CLR guarantees we have all-zeros here
		}
	}
	
	
	/// <summary>Gets the bit at the given index.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool Get(uint index) => (index >= Size)
		? throw new ArgumentOutOfRangeException(nameof(index), "Bit index out of range")
		: (_words <= 1)
			? (_smallBits & (1ul << (int)index)) != 0
			: (_bits![index / WORD_SIZE] & (1ul << (int)(index % WORD_SIZE))) != 0;

	/// <summary>Enables the bit (set to one/true) at the given index.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Set(uint index)
	{
		if (index >= Size) {
			throw new ArgumentOutOfRangeException(nameof(index), "Bit index out of range");
		}
		if (_words <= 1) _smallBits |= (1ul << (int)index);
		else _bits![index / WORD_SIZE] |= (1ul << (int)(index % WORD_SIZE));
	}

	/// <summary>Disables the bit (set to zero/false) at the given index.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void Clear(uint index)
	{
		if (index >= Size) {
			throw new ArgumentOutOfRangeException(nameof(index), "Bit index out of range");
		}
		if (_words <= 1) _smallBits &= ~(1ul << (int)index);
		else _bits![index / WORD_SIZE] &= ~(1ul << (int)(index % WORD_SIZE));
	}

	/// <summary>Sets all bits to one/true.</summary>
	public void SetAll()
	{
		if (_words > 1) {
			for (uint wi = 0; wi < _bits!.Length; ++wi) _bits[wi] = WORD_ALL;
		}

		if (_words <= 1 || (Size % WORD_SIZE) != 0) {
			var toFill = Size % WORD_SIZE;
			var mask = (toFill != 0) ? (1ul << (int)toFill) - 1 : WORD_ALL;
			if (_words <= 1) _smallBits = mask;
			else _bits![^1] = mask; // Set the bits while keeping (> Size) clear
		}
	}

	/// <summary>Sets all bits to zero/false.</summary>
	public void ClearAll()
	{
		if (_words <= 1) {
			_smallBits = WORD_NONE;
		}
		else {
			for (uint wi = 0; wi < _bits!.Length; ++wi) _bits[wi] = WORD_NONE;
		}
	}
	
	
	/// <summary>Gets the index of the first clear (zero/false) bit, or <c>null</c> if none are clear.</summary>
	public uint? FirstClear()
	{
		if (_words <= 1) {
			var zc = (uint)BitOperations.TrailingZeroCount(~_smallBits);
			return (zc < Size) ? zc : null;
		}

		uint index = 0;
		for (uint wi = 0; wi < _bits!.Length; ++wi) {
			if (_bits[wi] == WORD_ALL) {
				index += WORD_SIZE;
				continue;
			}
			index += (uint)BitOperations.TrailingZeroCount(~_bits[wi]);
			break; // Still valid for the last word, since we break anyway
		}
		return (index < Size) ? index : null;
	}

	/// <summary>Gets the index of the first set (one/true) bit, or <c>null</c> if none are set.</summary>
	public uint? FirstSet()
	{
		if (_words <= 1) {
			var zc = (uint)BitOperations.TrailingZeroCount(_smallBits);
			return (zc < Size) ? zc : null;
		}

		uint index = 0;
		for (uint wi = 0; wi < _bits!.Length; ++wi) {
			if (_bits[wi] == WORD_NONE) {
				index += WORD_SIZE;
				continue;
			}
			index += (uint)BitOperations.TrailingZeroCount(_bits[wi]);
			break; // Still valid for the last word, since we break anyway
		}
		return (index < Size) ? index : null;
	}
	
	
	// Convert a count to a number of words
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private static uint CountToWords(uint count) => (count == 0) ? 0 : ((count - 1) / WORD_SIZE) + 1;
}
