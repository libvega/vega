﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum;


/// <summary>Space-efficient, value type bitmask of 64 flags.</summary>
[StructLayout(LayoutKind.Explicit, Size = 8)]
public struct Bitmask64 : IEquatable<Bitmask64>, IComparable<Bitmask64>
{
	private const ulong NO_BITS = 0;
	private const ulong ALL_BITS = 0xFFFF_FFFF_FFFF_FFFFUL;
	
	/// <summary>Constant mask with no bits set.</summary>
	public static readonly Bitmask64 NONE = new(NO_BITS);
	/// <summary>Constant mask with all bits set.</summary>
	public static readonly Bitmask64 ALL = new(ALL_BITS);
	
	
	#region Fields
	/// <summary>The underlying value of the mask.</summary>
	[FieldOffset(0)] public ulong Value;

	/// <summary>If all bits are set in the mask.</summary>
	public readonly bool All {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value == ALL_BITS;
	}
	/// <summary>If no bits are set in the mask.</summary>
	public readonly bool None {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value == 0;
	}
	/// <summary>If any bits are set in the mask.</summary>
	public readonly bool Any {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value != 0;
	}

	/// <summary>The number of set bits (<c>1</c>) in the mask.</summary>
	public readonly uint SetCount {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => (uint)BitOperations.PopCount(Value);
	}
	/// <summary>The number of unset bits (<c>0</c>) in the mask.</summary>
	public readonly uint ClearCount {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => (uint)BitOperations.PopCount(~Value);
	}

	/// <summary>The zero-based index of the first set bit (LSB), or <c>null</c> if no bits are set.</summary>
	public readonly uint? FirstSet {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value == 0 ? null : (uint)BitOperations.TrailingZeroCount(Value);
	}
	/// <summary>The zero-based index of the last set bit (MSB), or <c>null</c> if no bits are set.</summary>
	public readonly uint? LastSet {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value == 0 ? null : 63 - (uint)BitOperations.LeadingZeroCount(Value);
	}

	/// <summary>The zero-based index of the first unset bit (LSB), or <c>null</c> if all bits are set.</summary>
	public readonly uint? FirstClear {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value == ALL_BITS ? null : (uint)BitOperations.TrailingZeroCount(~Value);
	}
	/// <summary>The zero-based index of the last unset bit (MSB), or <c>null</c> if all bits are set.</summary>
	public readonly uint? LastClear {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => Value == ALL_BITS ? null : 63 - (uint)BitOperations.LeadingZeroCount(~Value);
	}

	/// <summary>Gets or sets the bit at the given index.</summary>
	/// <param name="index">The zero-based bit index.</param>
	public bool this [uint index] {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		readonly get => (index <= 63)
			? (Value & (1ul << (int)index)) != 0
			: throw new IndexOutOfRangeException();
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => Value = (index > 63) ? throw new IndexOutOfRangeException() : value
			? (Value | (1ul << (int)index))
			: (Value & ~(1ul << (int)index));
	}
	#endregion // Fields
	
	
	/// <summary>Construct a mask from the bits of an existing <c>ulong</c>.</summary>
	public Bitmask64(ulong mask) => Value = mask;

	/// <summary>Construct a mask from the bits of an existing <c>long</c>.</summary>
	public unsafe Bitmask64(long mask) => Value = *(ulong*)&mask;
	
	
	#region Object
	public readonly override string ToString() => $"{Value:X16}";

	public readonly override int GetHashCode() => Value.GetHashCode();

	public readonly override bool Equals(object? obj) => obj is Bitmask64 mask && mask.Value == Value;

	public readonly bool Equals(Bitmask64 other) => other.Value == Value;

	public readonly int CompareTo(Bitmask64 other) => (Value == other.Value) ? 0 : (Value < other.Value) ? -1 : 1;
	#endregion // Object

	
	public static bool operator == (Bitmask64 l, Bitmask64 r) => l.Value == r.Value;
	public static bool operator != (Bitmask64 l, Bitmask64 r) => l.Value != r.Value;

	public static Bitmask64 operator | (Bitmask64 l, Bitmask64 r) => new(l.Value | r.Value);
	public static Bitmask64 operator & (Bitmask64 l, Bitmask64 r) => new(l.Value & r.Value);
	public static Bitmask64 operator ~ (Bitmask64 r) => new(~r.Value);
}
