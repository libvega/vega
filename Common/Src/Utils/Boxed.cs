﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;

namespace Astrum;


/// <summary>Simple typesafe boxing of a value type on the heap.</summary>
/// <typeparam name="T">The value type in the box.</typeparam>
public sealed class Boxed<T> : IEquatable<Boxed<T>>, IEquatable<T>
	where T : struct
{
	/// <summary>The value in the box.</summary>
	public T Value;

	/// <summary>Construct a box initialized with the default value.</summary>
	public Boxed() => Value = default!;

	/// <summary>Construct a box initialized with a specific value.</summary>
	public Boxed(T value) => Value = value;

	/// <inheritdoc/>
	public override string ToString() => Value.ToString() ?? String.Empty;

	// ReSharper disable once NonReadonlyMemberInGetHashCode
	#pragma warning disable CS0809
	/// <inheritdoc/>
	[Obsolete("GetHashCode() should not be called on Boxed<T> as the boxed value is not read-only")]
	public override int GetHashCode() => Value.GetHashCode();
	#pragma warning restore CS0809

	/// <inheritdoc/>
	public override bool Equals(object? obj) => obj switch {
		Boxed<T> box => Equals(box),
		T value      => Equals(value),
		_            => false
	};

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool Equals(Boxed<T>? box) => box is not null && Value.Equals(box.Value);

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool Equals(T value) => Value.Equals(value);
	
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool operator == (Boxed<T>? l, Boxed<T>? r) => l?.Equals(r) ?? r is null;
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool operator != (Boxed<T>? l, Boxed<T>? r) => !(l?.Equals(r) ?? r is null);
	
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool operator == (Boxed<T>? l, in T r) => l is not null && l.Value.Equals(r);
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool operator != (Boxed<T>? l, in T r) => l is null || !l.Value.Equals(r);
	
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool operator == (in T l, Boxed<T>? r) => r is not null && l.Equals(r.Value);
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool operator != (in T l, Boxed<T>? r) => r is null || !l.Equals(r.Value);

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static implicit operator T (Boxed<T>? box) => box?.Value ?? default!;
}
