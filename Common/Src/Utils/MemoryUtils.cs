﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;

namespace Astrum;


/// <summary>Utility functionality for working directly with memory.</summary>
public static unsafe class MemoryUtils
{
	// Vector sizes (bytes)
	private const uint VEC128_SIZE = 128 / 8;
	private const uint VEC256_SIZE = 256 / 8;
	
	
	/// <summary>Quickly zeros all bits in the given memory block.</summary>
	/// <param name="memory">The start of the memory block to zero.</param>
	/// <param name="length">The length of the memory block to zero, in bytes.</param>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	[SkipLocalsInit]
	public static void ZeroMemory(void* memory, ulong length)
	{
		if (memory == null) throw new ArgumentNullException(nameof(memory));
		
		// Loop over memory, zeroing with the largest type available
		var memStart = (byte*)memory;
		if (length >= VEC256_SIZE && Vector256.IsHardwareAccelerated) {
			var zeroVec = Vector256<byte>.Zero;
			while (length >= VEC256_SIZE) {
				Unsafe.WriteUnaligned(memStart, zeroVec);
				length -= VEC256_SIZE;
				memStart += VEC256_SIZE;
			}
		}
		if (length >= VEC128_SIZE && Vector128.IsHardwareAccelerated) {
			var zeroVec = Vector128<byte>.Zero;
			while (length >= VEC128_SIZE) {
				Unsafe.WriteUnaligned(memStart, zeroVec);
				length -= VEC128_SIZE;
				memStart += VEC128_SIZE;
			}
		}
		while (length >= 8) {
			Unsafe.WriteUnaligned(memStart, 0UL);
			length -= 8;
			memStart += 8;
		}
		while (length-- >= 1) {
			*memStart++ = 0;
		}
	}
}
