﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Astrum;


/// <summary>
/// Used to decorate <c>delegate* unmanaged</c> instance fields for reflection-driven loading of native library function
/// symbols.
/// </summary>
/// <remarks>
/// Note that no validation is done to ensure that the unmanaged function pointer signatures match the signature of the
/// imported native functions.
/// </remarks>
[AttributeUsage(AttributeTargets.Field)]
public sealed class NativeFunctionAttribute : Attribute
{
	/// <summary>The name of the function symbol as exported by the native library.</summary>
	public readonly string Name;
	/// <summary>If <c>true</c>, then failing to load the function will not generate an exception.</summary>
	public bool Optional { get; init; } = false;

	/// <summary>Decorates a field for reflection-based loading.</summary>
	/// <param name="name">The name of the exported native function symbol.</param>
	public NativeFunctionAttribute(string name) => Name = name;


	/// <summary>
	/// Resolves all fields in <paramref name="target"/> decorated with <see cref="NativeFunctionAttribute"/>.
	/// </summary>
	/// <param name="handle">The loaded native library handle from which to resolve function symbols.</param>
	/// <param name="target">The target object with fields to resolve.</param>
	/// <exception cref="TypeLoadException">
	/// One of the non-optional functions in <paramref name="target"/> could not be loaded.
	/// </exception>
	public static void Resolve<T>(nint handle, T target)
		where T : class
	{
		// Get decorated fields
		var fields = typeof(T)
			.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
			.Where(static f => f.FieldType.IsUnmanagedFunctionPointer)
			.Select(static f => (Field: f, Attr: f.GetCustomAttribute<NativeFunctionAttribute>()))
			.Where(static f => f.Attr is not null);

		// Load and assign the reflected symbols
		foreach (var (field, attr) in fields) {
			if (NativeLibrary.TryGetExport(handle, attr!.Name, out var funcPtr)) {
				field.SetValue(target, funcPtr);
				continue;
			}
			if (!attr.Optional) {
				throw new TypeLoadException(
					$"Could not load native function '{attr.Name}' for field {typeof(T).Name}.{field.Name}");
			}
			field.SetValue(target, IntPtr.Zero);
		}
	}
}
