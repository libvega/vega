﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Buffers;

namespace Astrum;


/// <summary>
/// Disposable, stack-only container for safely releasing arrays rented with <see cref="ArrayPool{T}.Rent"/>.
/// </summary>
/// <remarks>Should be wrapped in <c>using</c> statements or blocks for proper cleanup.</remarks>
/// <typeparam name="T">The element type in the rented array.</typeparam>
public ref struct RentedArray<T>
{
	/// <summary>The underlying array pool.</summary>
	public readonly ArrayPool<T> Pool;
	/// <summary>The underlying rented array. Will be <c>null</c> once released.</summary>
	public T[] Array { get; private set; }
	/// <summary>The length of the rented array.</summary>
	public uint Length => (uint)Array.Length;
	/// <summary>If the rented array is valid (not yet returned to the pool).</summary>
	public bool IsValid => Array is not null;


	/// <summary>Rents a new array of the given length from the given pool.</summary>
	/// <param name="length">The length of the array to rent.</param>
	/// <param name="pool">The pool to rent from. Defaults to <see cref="ArrayPool{T}.Shared"/> if <c>null</c>.</param>
	public RentedArray(int length, ArrayPool<T>? pool = null) 
		: this(length < 0 ? throw new ArgumentOutOfRangeException(nameof(length)) : (uint)length, pool) 
	{ }

	/// <inheritdoc cref="RentedArray{T}(int,ArrayPool{T}?)"/>
	public RentedArray(uint length, ArrayPool<T>? pool = null)
	{
		Pool = pool ?? ArrayPool<T>.Shared;
		Array = length > 0 ? Pool.Rent((int)length) : System.Array.Empty<T>();
	}

	/// <summary>Returns the rented array to the pool. The array must not be changed after this is called.</summary>
	public void Dispose()
	{
		if (IsValid && Array.Length > 0) Pool.Return(Array);
		Array = null!;
	}


	/// <summary>Gets or sets the element at the given index in the underlying array.</summary>
	public ref T this [int index] => ref Array[index];
	/// <summary>Gets or sets the element at the given index in the underlying array.</summary>
	public ref T this [uint index] => ref Array[index];
}
