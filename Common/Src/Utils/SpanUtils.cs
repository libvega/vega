﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Extension and utility functionality for <c>Span</c> and <c>ReadOnlySpan</c>.</summary>
public static class SpanUtils
{
	/// <summary>A mutating <c>ToArray</c> for spans.</summary>
	/// <remarks>Functionally identical to a LINQ <c>Select</c> followed by <c>ToArray</c>.</remarks>
	/// <param name="span">The span to populate a new array with.</param>
	/// <param name="selector">The mutating selector to convert the span values to the array values.</param>
	/// <typeparam name="TFrom">The source span type.</typeparam>
	/// <typeparam name="TTo">The destination array type.</typeparam>
	/// <returns>A new array with the mutated span values.</returns>
	public static TTo[] ToArray<TFrom, TTo>(this ReadOnlySpan<TFrom> span, Func<TFrom, TTo> selector)
	{
		if (span.Length == 0) return Array.Empty<TTo>();
		var toArray = new TTo[span.Length];
		for (var i = 0; i < span.Length; ++i) toArray[i] = selector(span[i]);
		return toArray;
	}

	/// <inheritdoc cref="ToArray{TFrom,TTo}(System.ReadOnlySpan{TFrom},System.Func{TFrom,TTo})"/>
	public static TTo[] ToArray<TFrom, TTo>(this Span<TFrom> span, Func<TFrom, TTo> selector) =>
		ToArray((ReadOnlySpan<TFrom>)span, selector);

	/// <summary>A mutating <c>ToArray</c> for spans.</summary>
	/// <remarks>Functionally identical to a LINQ <c>Select</c> followed by <c>ToArray</c>.</remarks>
	/// <param name="span">The span to populate a new array with.</param>
	/// <param name="selector">The mutating selector (with index) to convert the span values to the array values.</param>
	/// <typeparam name="TFrom">The source span type.</typeparam>
	/// <typeparam name="TTo">The destination array type.</typeparam>
	/// <returns>A new array with the mutated span values.</returns>
	public static TTo[] ToArray<TFrom, TTo>(this ReadOnlySpan<TFrom> span, Func<TFrom, int, TTo> selector)
	{
		if (span.Length == 0) return Array.Empty<TTo>();
		var toArray = new TTo[span.Length];
		for (var i = 0; i < span.Length; ++i) toArray[i] = selector(span[i], i);
		return toArray;
	}

	/// <inheritdoc cref="ToArray{TFrom,TTo}(System.ReadOnlySpan{TFrom},System.Func{TFrom,int,TTo})"/>
	public static TTo[] ToArray<TFrom, TTo>(this Span<TFrom> span, Func<TFrom, int, TTo> selector) =>
		ToArray((ReadOnlySpan<TFrom>)span, selector);
}
