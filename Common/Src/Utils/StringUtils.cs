﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Extension and utility functionality for <c>string</c> values.</summary>
public static class StringUtils
{
	/// <summary>
	/// Converts the string to have exactly the requested length if needed, through trimming or left padding.
	/// </summary>
	/// <param name="str">The string to convert.</param>
	/// <param name="length">The final total length of the string.</param>
	/// <param name="padChar">The character to use if padding is required.</param>
	public static string TrimOrPadLeft(this string str, uint length, char padChar = ' ') =>
		str.Length < length ? str.PadLeft((int)length, padChar) :
		str.Length > length ? str[..(int)length] :
		str;

	/// <summary>
	/// Converts the string to have exactly the requested length if needed, through trimming or right padding.
	/// </summary>
	/// <param name="str">The string to convert.</param>
	/// <param name="length">The final total length of the string.</param>
	/// <param name="padChar">The character to use if padding is required.</param>
	public static string TrimOrPadRight(this string str, uint length, char padChar = ' ') =>
		str.Length < length ? str.PadRight((int)length, padChar) :
		str.Length > length ? str[..(int)length] :
		str;
}
