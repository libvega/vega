﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;

namespace Astrum.Tests;


// Tests for overall logging functionality
internal class LoggingTests
{
	[Test]
	public void Logger_MultipleUniqueInstances()
	{
		var inst1 = Logger.GetOrCreateLogger("Unique1");
		var inst2 = Logger.GetOrCreateLogger("Unique2");
		var inst3 = Logger.GetOrCreateLogger("Unique3");
		Assert.That(inst1, Is.Not.EqualTo(inst2));
		Assert.That(inst1, Is.Not.EqualTo(inst3));
		Assert.That(inst2, Is.Not.EqualTo(inst3));
	}

	[Test]
	public void Logger_LoggersAreCached()
	{
		var inst1 = Logger.GetOrCreateLogger("Cached1");
		var inst2 = Logger.GetOrCreateLogger("Cached1");
		var inst3 = Logger.GetLogger("Cached1");
		Assert.That(inst1, Is.EqualTo(inst2));
		Assert.That(inst1, Is.EqualTo(inst3));
	}

	[Test]
	public void Logger_GetAndCacheClassLogger()
	{
		var cLogger = Logger.GetClassLogger();
		Assert.That(cLogger.ClassType, Is.EqualTo(typeof(LoggingTests)));
		Assert.That(cLogger, Is.EqualTo(Logger.GetClassLogger()));
	}

	[Test]
	public void Logger_DefaultAndTrimmedTags()
	{
		var l1 = Logger.GetOrCreateLogger("Tag1");
		var l2 = Logger.GetOrCreateLogger("Tag2", "TAG2!!");
		var l3 = Logger.GetOrCreateLogger("ReallyLongNameThatIsTooBigForTheTag");
		var l4 = Logger.GetOrCreateLogger("Tag4", "ReallyLongExplicitTag");
		var l5 = Logger.GetOrCreateLogger("Tag5", "12CharTagWow");
		Assert.That(l1.Tag.ToString(), Is.EqualTo("Tag1        "));
		Assert.That(l2.Tag.ToString(), Is.EqualTo("TAG2!!      "));
		Assert.That(l3.Tag.ToString(), Is.EqualTo("ReallyLongNa"));
		Assert.That(l4.Tag.ToString(), Is.EqualTo("ReallyLongEx"));
		Assert.That(l5.Tag.ToString(), Is.EqualTo("12CharTagWow"));
	}

	[Test]
	[NonParallelizable]
	public void Logger_LogArgumentForwarding()
	{
		var logger = Logger.GetOrCreateLogger("ArgChecker");
		logger.LevelMask = LogLevel.AllMask;
		var checker = new ArgCheckerHandler();
		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Add(checker);

		dispatch(LogLevel.Trace, "Trace1", null);
		dispatch(LogLevel.Trace, "Trace2", new Exception());
		dispatch(LogLevel.Debug, "Debug1", null);
		dispatch(LogLevel.Debug, "Debug2", new Exception());
		dispatch(LogLevel.Info, "Info1", null);
		dispatch(LogLevel.Info, "Info2", new Exception());
		dispatch(LogLevel.Warn, "Warn1", null);
		dispatch(LogLevel.Warn, "Warn2", new Exception());
		dispatch(LogLevel.Error, "Error1", null);
		dispatch(LogLevel.Error, "Error2", new Exception());
		dispatch(LogLevel.Fatal, "Fatal1", null);
		dispatch(LogLevel.Fatal, "Fatal2", new Exception());

		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Remove(checker);
		return;

		void dispatch(LogLevel level, string msg, Exception? ex)
		{
			var count = checker.RecvCount;
			checker.NextLogger = logger;
			checker.NextLevel = level;
			checker.NextMessage = msg;
			checker.NextException = ex;
			switch (level) {
			case LogLevel.Trace: logger.Trace(msg, ex); break;
			case LogLevel.Debug: logger.Debug(msg, ex); break;
			case LogLevel.Info: logger.Info(msg, ex); break;
			case LogLevel.Warn: logger.Warn(msg, ex); break;
			case LogLevel.Error: logger.Error(msg, ex); break;
			case LogLevel.Fatal: logger.Fatal(msg, ex); break;
			default: Assert.Fail("Unknown log level"); break;
			}
			Assert.That(checker.RecvCount, Is.EqualTo(count + 1));
		}
	}

	[Test]
	[NonParallelizable]
	public void Logger_LogLevelMasking()
	{
		var logger = Logger.GetOrCreateLogger("MaskingChecker");
		logger.LevelMask = LogLevel.AllMask;
		var counter = new CounterHandler();
		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Add(counter);

		Assert.That(counter.RecvCount, Is.EqualTo(0));
		checkLevelMasking(LogLevel.Trace, () => logger.Trace(""), 0);
		checkLevelMasking(LogLevel.Debug, () => logger.Debug(""), 2);
		checkLevelMasking(LogLevel.Info, () => logger.Info(""), 4);
		checkLevelMasking(LogLevel.Warn, () => logger.Warn(""), 6);
		checkLevelMasking(LogLevel.Error, () => logger.Error(""), 8);
		checkLevelMasking(LogLevel.Fatal, () => logger.Fatal(""), 10);

		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Remove(counter);

		void checkLevelMasking(LogLevel level, Action logFunc, uint startCount)
		{
			logFunc();
			Assert.That(counter.RecvCount, Is.EqualTo(startCount + 1));
			logger.LevelMask &= ~level;
			logFunc();
			Assert.That(counter.RecvCount, Is.EqualTo(startCount + 1));
			logger.LevelMask |= level;
			counter.SetLevelMask(counter.LevelMask & ~level);
			logFunc();
			Assert.That(counter.RecvCount, Is.EqualTo(startCount + 1));
			counter.SetLevelMask(counter.LevelMask | level);
			logFunc();
			Assert.That(counter.RecvCount, Is.EqualTo(startCount + 2));
		}
	}

	[Test]
	[NonParallelizable]
	public void Logger_CustomStringInterpolatorsAreCorrect()
	{
		var logger = Logger.GetOrCreateLogger("InterpStringChecker");
		logger.LevelMask = LogLevel.AllMask;
		var checker = new ArgCheckerHandler();
		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Add(checker);

		dispatch(LogLevel.Trace, null);
		dispatch(LogLevel.Trace, new Exception());
		dispatch(LogLevel.Debug, null);
		dispatch(LogLevel.Debug, new Exception());
		dispatch(LogLevel.Info,  null);
		dispatch(LogLevel.Info,  new Exception());
		dispatch(LogLevel.Warn,  null);
		dispatch(LogLevel.Warn,  new Exception());
		dispatch(LogLevel.Error, null);
		dispatch(LogLevel.Error, new Exception());
		dispatch(LogLevel.Fatal, null);
		dispatch(LogLevel.Fatal, new Exception());

		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Remove(checker);
		return;

		void dispatch(LogLevel level, Exception? ex)
		{
			var count = checker.RecvCount;
			checker.NextLogger = logger;
			checker.NextLevel = level;
			checker.NextMessage = $"Msg: {level} {ex}";
			checker.NextException = ex;
			switch (level)
			{
			case LogLevel.Trace: logger.Trace($"Msg: {level} {ex}", ex); break;
			case LogLevel.Debug: logger.Debug($"Msg: {level} {ex}", ex); break;
			case LogLevel.Info:  logger.Info($"Msg: {level} {ex}", ex);  break;
			case LogLevel.Warn:  logger.Warn($"Msg: {level} {ex}", ex);  break;
			case LogLevel.Error: logger.Error($"Msg: {level} {ex}", ex); break;
			case LogLevel.Fatal: logger.Fatal($"Msg: {level} {ex}", ex); break;
			default:             Assert.Fail("Unknown log level"); break;
			}
			Assert.That(checker.RecvCount, Is.EqualTo(count + 1));
		}
	}


	[Test]
	[NonParallelizable]
	public void Log_UsesDefaultApplicationLogger()
	{
		Logger.Default.LevelMask = LogLevel.AllMask;
		var checker = new ArgCheckerHandler();
		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Add(checker);

		dispatch(LogLevel.Trace, "Trace1", null);
		dispatch(LogLevel.Trace, "Trace2", new Exception());
		dispatch(LogLevel.Debug, "Debug1", null);
		dispatch(LogLevel.Debug, "Debug2", new Exception());
		dispatch(LogLevel.Info, "Info1", null);
		dispatch(LogLevel.Info, "Info2", new Exception());
		dispatch(LogLevel.Warn, "Warn1", null);
		dispatch(LogLevel.Warn, "Warn2", new Exception());
		dispatch(LogLevel.Error, "Error1", null);
		dispatch(LogLevel.Error, "Error2", new Exception());
		dispatch(LogLevel.Fatal, "Fatal1", null);
		dispatch(LogLevel.Fatal, "Fatal2", new Exception());

		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Remove(checker);
		return;

		void dispatch(LogLevel level, string msg, Exception? ex)
		{
			var count = checker.RecvCount;
			checker.NextLogger = Logger.Default;
			checker.NextLevel = level;
			checker.NextMessage = msg;
			checker.NextException = ex;
			switch (level) {
			case LogLevel.Trace: Log.LogTrace(msg, ex); break;
			case LogLevel.Debug: Log.LogDebug(msg, ex); break;
			case LogLevel.Info: Log.LogInfo(msg, ex); break;
			case LogLevel.Warn: Log.LogWarn(msg, ex); break;
			case LogLevel.Error: Log.LogError(msg, ex); break;
			case LogLevel.Fatal: Log.LogFatal(msg, ex); break;
			default: Assert.Fail("Unknown log level"); break;
			}
			Assert.That(checker.RecvCount, Is.EqualTo(count + 1));
		}
	}


	[Test]
	[NonParallelizable]
	public void EngineLogger_LogArgumentForwarding()
	{
		var checker = new ArgCheckerHandler();
		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Add(checker);

		dispatch(LogLevel.Trace, "Trace1", null);
		dispatch(LogLevel.Trace, "Trace2", new Exception());
		dispatch(LogLevel.Debug, "Debug1", null);
		dispatch(LogLevel.Debug, "Debug2", new Exception());
		dispatch(LogLevel.Info, "Info1", null);
		dispatch(LogLevel.Info, "Info2", new Exception());
		dispatch(LogLevel.Warn, "Warn1", null);
		dispatch(LogLevel.Warn, "Warn2", new Exception());
		dispatch(LogLevel.Error, "Error1", null);
		dispatch(LogLevel.Error, "Error2", new Exception());
		dispatch(LogLevel.Fatal, "Fatal1", null);
		dispatch(LogLevel.Fatal, "Fatal2", new Exception());

		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Remove(checker);
		return;

		void dispatch(LogLevel level, string msg, Exception? ex)
		{
			var count = checker.EngineRecvCount;
			checker.NextLogger = null;
			checker.NextLevel = level;
			checker.NextMessage = msg;
			checker.NextException = ex;
			switch (level) {
			case LogLevel.Trace: EngineLogger.EngineTrace(msg, ex); break;
			case LogLevel.Debug: EngineLogger.EngineDebug(msg, ex); break;
			case LogLevel.Info: EngineLogger.EngineInfo(msg, ex); break;
			case LogLevel.Warn: EngineLogger.EngineWarn(msg, ex); break;
			case LogLevel.Error: EngineLogger.EngineError(msg, ex); break;
			case LogLevel.Fatal: EngineLogger.EngineFatal(msg, ex); break;
			default:
				Assert.Fail("Unknown log level");
				break;
			}
			Assert.That(checker.EngineRecvCount, Is.EqualTo(count + 1));
		}
	}

	[Test]
	[NonParallelizable]
	public void EngineLogger_LogLevelMasking()
	{
		var counter = new CounterHandler();
		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Add(counter);

		Assert.That(counter.EngineRecvCount, Is.EqualTo(0));
		checkLevelMasking(LogLevel.Trace, () => EngineLogger.EngineTrace(""), 0);
		checkLevelMasking(LogLevel.Debug, () => EngineLogger.EngineDebug(""), 2);
		checkLevelMasking(LogLevel.Info, () => EngineLogger.EngineInfo(""), 4);
		checkLevelMasking(LogLevel.Warn, () => EngineLogger.EngineWarn(""), 6);
		checkLevelMasking(LogLevel.Error, () => EngineLogger.EngineError(""), 8);
		checkLevelMasking(LogLevel.Fatal, () => EngineLogger.EngineFatal(""), 10);

		((LogHandler.DispatchHandler)LogHandler.Current).Handlers.Remove(counter);

		void checkLevelMasking(LogLevel level, Action logFunc, uint startCount)
		{
			logFunc();
			Assert.That(counter.EngineRecvCount, Is.EqualTo(startCount + 1));
			counter.SetEngineLevelMask(counter.LevelMask & ~level);
			logFunc();
			Assert.That(counter.EngineRecvCount, Is.EqualTo(startCount + 1));
			counter.SetEngineLevelMask(counter.LevelMask | level);
			logFunc();
			Assert.That(counter.EngineRecvCount, Is.EqualTo(startCount + 2));
		}
	}


	[Test]
	public void LoggerStringHandler_SkipsBasedOnLevelMask()
	{
		var logger = Logger.GetOrCreateLogger("InterpMaskingChecker");

		logger.LevelMask = LogLevel.Trace;
		checkTrace(logger, false, $"Test {logger}");
		logger.LevelMask = LogLevel.None;
		checkTrace(logger, true, $"Test {logger}");

		logger.LevelMask = LogLevel.Debug;
		checkDebug(logger, false, $"Test {logger}");
		logger.LevelMask = LogLevel.None;
		checkDebug(logger, true, $"Test {logger}");

		logger.LevelMask = LogLevel.Info;
		checkInfo(logger, false, $"Test {logger}");
		logger.LevelMask = LogLevel.None;
		checkInfo(logger, true, $"Test {logger}");

		logger.LevelMask = LogLevel.Warn;
		checkWarn(logger, false, $"Test {logger}");
		logger.LevelMask = LogLevel.None;
		checkWarn(logger, true, $"Test {logger}");

		logger.LevelMask = LogLevel.Error;
		checkError(logger, false, $"Test {logger}");
		logger.LevelMask = LogLevel.None;
		checkError(logger, true, $"Test {logger}");

		logger.LevelMask = LogLevel.Fatal;
		checkFatal(logger, false, $"Test {logger}");
		logger.LevelMask = LogLevel.None;
		checkFatal(logger, true, $"Test {logger}");

		static void checkTrace(Logger logger, bool empty,
			[InterpolatedStringHandlerArgument("logger")] ref LoggerStringHandler.Trace msg) =>
			Assert.That(msg.ToStringAndClear().Length, empty ? Is.EqualTo(0) : Is.GreaterThan(0));
		static void checkDebug(Logger logger, bool empty,
			[InterpolatedStringHandlerArgument("logger")] ref LoggerStringHandler.Debug msg) =>
			Assert.That(msg.ToStringAndClear().Length, empty ? Is.EqualTo(0) : Is.GreaterThan(0));
		static void checkInfo(Logger logger, bool empty,
			[InterpolatedStringHandlerArgument("logger")] ref LoggerStringHandler.Info msg) =>
			Assert.That(msg.ToStringAndClear().Length, empty ? Is.EqualTo(0) : Is.GreaterThan(0));
		static void checkWarn(Logger logger, bool empty,
			[InterpolatedStringHandlerArgument("logger")] ref LoggerStringHandler.Warn msg) =>
			Assert.That(msg.ToStringAndClear().Length, empty ? Is.EqualTo(0) : Is.GreaterThan(0));
		static void checkError(Logger logger, bool empty,
			[InterpolatedStringHandlerArgument("logger")] ref LoggerStringHandler.Error msg) =>
			Assert.That(msg.ToStringAndClear().Length, empty ? Is.EqualTo(0) : Is.GreaterThan(0));
		static void checkFatal(Logger logger, bool empty,
			[InterpolatedStringHandlerArgument("logger")] ref LoggerStringHandler.Fatal msg) =>
			Assert.That(msg.ToStringAndClear().Length, empty ? Is.EqualTo(0) : Is.GreaterThan(0));
	}



	// Checks log arguments for validity
	private sealed class ArgCheckerHandler : CounterHandler
	{
		public LogLevel NextLevel;
		public Logger? NextLogger; // null for engine message
		public string NextMessage = null!;
		public Exception? NextException;

		public ArgCheckerHandler() => LevelMask = EngineLevelMask = LogLevel.AllMask;

		protected override void OnOpen() { }
		protected override void OnFlush() { }
		protected override void OnClose() { }

		protected override void HandleMessage(Logger logger, TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message, Exception? ex)
		{
			Assert.That(logger, Is.EqualTo(NextLogger));
			Assert.That(level, Is.EqualTo(NextLevel));
			Assert.That(message.ToString(), Is.EqualTo(NextMessage));
			Assert.That(ex, Is.EqualTo(NextException));
			base.HandleMessage(logger, timeStamp, level, message, ex);
		}

		protected override void HandleEngineMessage(TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message, Exception? ex)
		{
			Assert.IsNull(NextLogger);
			Assert.That(level, Is.EqualTo(NextLevel));
			Assert.That(message.ToString(), Is.EqualTo(NextMessage));
			Assert.That(ex, Is.EqualTo(NextException));
			base.HandleEngineMessage(timeStamp, level, message, ex);
		}
	}

	// Counts the number of messages received
	private class CounterHandler : LogHandler
	{
		public uint RecvCount { get; private set; }
		public uint EngineRecvCount { get; private set; }
		public CounterHandler() => LevelMask = EngineLevelMask = LogLevel.AllMask;
		public void SetLevelMask(LogLevel level) => LevelMask = level;
		public void SetEngineLevelMask(LogLevel level) => EngineLevelMask = level;
		protected override void OnOpen() { }
		protected override void OnFlush() { }
		protected override void OnClose() { }
		protected override void HandleMessage(Logger logger, TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message, Exception? ex) => RecvCount += 1;
		protected override void HandleEngineMessage(TimeSpan timeStamp, LogLevel level, ReadOnlySpan<char> message, Exception? ex) => EngineRecvCount += 1;
	}
}
