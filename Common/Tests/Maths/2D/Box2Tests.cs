﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Box2 and related Space2D methods  (only test Box2 and Box2F for identical generated functionality)
internal class Box2Tests
{
	[Test]
	public void Box2_Constants()
	{
		Assert.That(Box2.Empty.Min, Is.EqualTo(Point2.Zero));
		Assert.That(Box2.Empty.Max, Is.EqualTo(Point2.Zero));
		Assert.That(Box2.Unit.Min, Is.EqualTo(Point2.Zero));
		Assert.That(Box2.Unit.Max, Is.EqualTo(new Point2(1)));

		Assert.That(Box2F.Empty.Min, Is.EqualTo(Vec2.Zero));
		Assert.That(Box2F.Empty.Max, Is.EqualTo(Vec2.Zero));
		Assert.That(Box2F.Unit.Min, Is.EqualTo(Vec2.Zero));
		Assert.That(Box2F.Unit.Max, Is.EqualTo(new Vec2(1)));
	}

	[Test]
	public void Box2_Constructors()
	{
		Box2 b = new();
		Assert.That(b.Min, Is.EqualTo(Point2.Zero));
		Assert.That(b.Max, Is.EqualTo(Point2.Zero));
		b = new(new(1, 2), new(3, 4));
		Assert.That(b.Min, Is.EqualTo(new Point2(1, 2)));
		Assert.That(b.Max, Is.EqualTo(new Point2(3, 4)));
		b = new(5, 6, 7, 8);
		Assert.That(b.Min, Is.EqualTo(new Point2(5, 6)));
		Assert.That(b.Max, Is.EqualTo(new Point2(7, 8)));
		b = new(5, 8, 6, 7);
		Assert.That(b.Min, Is.EqualTo(new Point2(5, 7)));
		Assert.That(b.Max, Is.EqualTo(new Point2(6, 8)));

		Box2F bf = new();
		Assert.That(bf.Min, Is.EqualTo(Vec2.Zero));
		Assert.That(bf.Max, Is.EqualTo(Vec2.Zero));
		bf = new(new(1, 2), new(3, 4));
		Assert.That(bf.Min, Is.EqualTo(new Vec2(1, 2)));
		Assert.That(bf.Max, Is.EqualTo(new Vec2(3, 4)));
		bf = new(5, 6, 7, 8);
		Assert.That(bf.Min, Is.EqualTo(new Vec2(5, 6)));
		Assert.That(bf.Max, Is.EqualTo(new Vec2(7, 8)));
		bf = new(5, 8, 6, 7);
		Assert.That(bf.Min, Is.EqualTo(new Vec2(5, 7)));
		Assert.That(bf.Max, Is.EqualTo(new Vec2(6, 8)));
	}

	[Test]
	public void Box2_MinMax()
	{
		Box2 b = new(1, 2, 3, 4);
		Assert.That(b.Min, Is.EqualTo(new Point2(1, 2)));
		Assert.That(b.Max, Is.EqualTo(new Point2(3, 4)));
		b.Min = new(0, 1);
		Assert.That(b.Min, Is.EqualTo(new Point2(0, 1)));
		Assert.That(b.Max, Is.EqualTo(new Point2(3, 4)));
		b.Max = new(5, 6);
		Assert.That(b.Min, Is.EqualTo(new Point2(0, 1)));
		Assert.That(b.Max, Is.EqualTo(new Point2(5, 6)));
		b.Min = new(8, 9);
		Assert.That(b.Min, Is.EqualTo(new Point2(5, 6)));
		Assert.That(b.Max, Is.EqualTo(new Point2(8, 9))); // Swapped min/max
		b.Max = new(-1, -2);
		Assert.That(b.Min, Is.EqualTo(new Point2(-1, -2))); // Swapped min/max
		Assert.That(b.Max, Is.EqualTo(new Point2(5, 6)));

		Box2F b2 = new(1, 2, 3, 4);
		Assert.That(b2.Min, Is.EqualTo(new Vec2(1, 2)));
		Assert.That(b2.Max, Is.EqualTo(new Vec2(3, 4)));
		b2.Min = new(0, 1);
		Assert.That(b2.Min, Is.EqualTo(new Vec2(0, 1)));
		Assert.That(b2.Max, Is.EqualTo(new Vec2(3, 4)));
		b2.Max = new(5, 6);
		Assert.That(b2.Min, Is.EqualTo(new Vec2(0, 1)));
		Assert.That(b2.Max, Is.EqualTo(new Vec2(5, 6)));
		b2.Min = new(8, 9);
		Assert.That(b2.Min, Is.EqualTo(new Vec2(5, 6)));
		Assert.That(b2.Max, Is.EqualTo(new Vec2(8, 9))); // Swapped min/max
		b2.Max = new(-1, -2);
		Assert.That(b2.Min, Is.EqualTo(new Vec2(-1, -2))); // Swapped min/max
		Assert.That(b2.Max, Is.EqualTo(new Vec2(5, 6)));
	}

	[Test]
	public void Box2_SizeFields()
	{
		Box2 b = new(0, 0, 10, 5);
		Assert.That(b.Size, Is.EqualTo(new Point2U(10, 5)));
		Assert.That(b.Width, Is.EqualTo(10));
		Assert.That(b.Height, Is.EqualTo(5));
		Assert.That(b.Area, Is.EqualTo(50));
		Assert.False(b.IsEmpty);
		b = new(-10, -5, 10, 5);
		Assert.That(b.Size, Is.EqualTo(new Point2U(20, 10)));
		Assert.That(b.Width, Is.EqualTo(20));
		Assert.That(b.Height, Is.EqualTo(10));
		Assert.That(b.Area, Is.EqualTo(200));
		Assert.False(b.IsEmpty);
		Assert.True(Box2.Empty.IsEmpty);

		Box2F bf = new(0, 0, 10, 5);
		Assert.That(bf.Size, Is.EqualTo(new Vec2(10, 5)));
		Assert.That(bf.Width, Is.EqualTo(10));
		Assert.That(bf.Height, Is.EqualTo(5));
		Assert.That(bf.Area, Is.EqualTo(50));
		Assert.False(bf.IsEmpty);
		bf = new(-10, -5, 10, 5);
		Assert.That(bf.Size, Is.EqualTo(new Vec2(20, 10)));
		Assert.That(bf.Width, Is.EqualTo(20));
		Assert.That(bf.Height, Is.EqualTo(10));
		Assert.That(bf.Area, Is.EqualTo(200));
		Assert.False(bf.IsEmpty);
		Assert.True(Box2F.Empty.IsEmpty);
	}

	[Test]
	public void Box2_Center()
	{
		Assert.That(new Box2(0, 0, 10, 5).Center, Is.EqualTo(new Vec2(5, 2.5f)));
		Assert.That(new Box2(-10, -5, 10, 5).Center, Is.EqualTo(Vec2.Zero));
		Assert.That(Box2.Empty.Center, Is.EqualTo(Vec2.Zero));

		Assert.That(new Box2F(0, 0, 10, 5).Center, Is.EqualTo(new Vec2(5, 2.5f)));
		Assert.That(new Box2F(-10, -5, 10, 5).Center, Is.EqualTo(Vec2.Zero));
		Assert.That(Box2F.Empty.Center, Is.EqualTo(Vec2.Zero));
	}

	[Test]
	public void Box2_Equality()
	{
		ReadOnlySpan<Box2> boxList = stackalloc Box2[] {
			new(0, 0, 0, 0), new(1, 0, 0, 1), new(0, 1, 1, 0), new(0, 0, 1, 1), new(0, 0, 2, 2), new(0, 0, 10, 5)
		};
		foreach (var b0 in boxList) {
			foreach (var b1 in boxList) {
				Assert.True(b0 == b0);
				Assert.False(b0 != b0);
				Assert.True(b0.Equals(b0));
				Assert.True(b0.Equals((object)b0));
				Assert.That(b0 == b1, Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0 != b1, Is.EqualTo(b0.Min != b1.Min || b0.Max != b1.Max));
				Assert.That(b0.Equals(b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0.Equals((object)b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
			}
		}

		ReadOnlySpan<Box2F> boxFList = stackalloc Box2F[] {
			new(0, 0, 0, 0), new(1, 0, 0, 1), new(0, 1, 1, 0), new(0, 0, 1, 1), new(0, 0, 2, 2), new(0, 0, 10, 5)
		};
		foreach (var b0 in boxFList) {
			foreach (var b1 in boxFList) {
				Assert.True(b0 == b0);
				Assert.False(b0 != b0);
				Assert.True(b0.Equals(b0));
				Assert.True(b0.Equals((object)b0));
				Assert.That(b0 == b1, Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0 != b1, Is.EqualTo(b0.Min != b1.Min || b0.Max != b1.Max));
				Assert.That(b0.Equals(b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0.Equals((object)b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
			}
		}
	}

	[Test]
	public void Box2_Deconstruct()
	{
		Assert.That((_, _) = new Box2(1, 2, 3, 4), Is.EqualTo((new Point2(1, 2), new Point2(3, 4))));
		Assert.That((_, _, _, _) = new Box2(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));

		Assert.That((_, _) = new Box2F(1, 2, 3, 4), Is.EqualTo((new Vec2(1, 2), new Vec2(3, 4))));
		Assert.That((_, _, _, _) = new Box2F(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
	}

	[Test]
	public void Box2_TransformMethods()
	{
		Assert.That(Box2.Unit.Translated(new(1, 2)), Is.EqualTo(new Box2(1, 2, 2, 3)));
		Assert.That(Box2.Unit.Translated(new(-5, 5)), Is.EqualTo(new Box2(-5, 5, -4, 6)));
		Assert.That(Box2.Unit.Inflated(new(2, 4)), Is.EqualTo(new Box2(-1, -2, 2, 3)));
		Assert.That(Box2.Unit.Inflated(new(-6, 8)), Is.EqualTo(new Box2(-2, -4, 3, 5))); // Shrink larger than box
		Assert.That(Box2.Unit.Scaled(new(2, 2), new(0)), Is.EqualTo(new Box2(0, 0, 2, 2)));
		Assert.That(Box2.Unit.Scaled(new(2, 2), new(1)), Is.EqualTo(new Box2(-1, -1, 1, 1)));
		Assert.That(new Box2(0, 0, 4, 4).Scaled(new(2, 2), new(1, 1)), Is.EqualTo(new Box2(-1, -1, 7, 7)));

		Assert.That(Box2F.Unit.Translated(new(1, 2)), Is.EqualTo(new Box2F(1, 2, 2, 3)));
		Assert.That(Box2F.Unit.Translated(new(-5, 5)), Is.EqualTo(new Box2F(-5, 5, -4, 6)));
		Assert.That(Box2F.Unit.Inflated(new(2, 4)), Is.EqualTo(new Box2F(-1, -2, 2, 3)));
		Assert.That(Box2F.Unit.Inflated(new(-6, 8)), Is.EqualTo(new Box2F(-2, -4, 3, 5))); // Shrink larger than box
		Assert.That(Box2F.Unit.Scaled(new(2, 2), new(0)), Is.EqualTo(new Box2F(0, 0, 2, 2)));
		Assert.That(Box2F.Unit.Scaled(new(2, 2), new(1)), Is.EqualTo(new Box2F(-1, -1, 1, 1)));
		Assert.That(new Box2F(0, 0, 4, 4).Scaled(new(2, 2), new(1, 1)), Is.EqualTo(new Box2F(-1, -1, 7, 7)));
	}

	[Test]
	public void Box2_CombineMethods()
	{
		Assert.That(Box2.Unit.Union(Box2.Unit), Is.EqualTo(Box2.Unit));
		Assert.That(Box2.Unit.Intersect(Box2.Unit), Is.EqualTo(Box2.Unit));
		Assert.That(new Box2(0, 0, 1, 1).Union(new(2, 2, 3, 3)), Is.EqualTo(new Box2(0, 0, 3, 3)));
		Assert.That(new Box2(0, 0, 1, 1).Intersect(new(2, 2, 3, 3)), Is.EqualTo(Box2.Empty));
		Assert.That(new Box2(-2, -2, 1, 1).Union(new(-1, -1, 2, 2)), Is.EqualTo(new Box2(-2, -2, 2, 2)));
		Assert.That(new Box2(-2, -2, 1, 1).Intersect(new(-1, -1, 2, 2)), Is.EqualTo(new Box2(-1, -1, 1, 1)));

		Assert.That(Box2F.Unit.Union(Box2F.Unit), Is.EqualTo(Box2F.Unit));
		Assert.That(Box2F.Unit.Intersect(Box2F.Unit), Is.EqualTo(Box2F.Unit));
		Assert.That(new Box2F(0, 0, 1, 1).Union(new(2, 2, 3, 3)), Is.EqualTo(new Box2F(0, 0, 3, 3)));
		Assert.That(new Box2F(0, 0, 1, 1).Intersect(new(2, 2, 3, 3)), Is.EqualTo(Box2F.Empty));
		Assert.That(new Box2F(-2, -2, 1, 1).Union(new(-1, -1, 2, 2)), Is.EqualTo(new Box2F(-2, -2, 2, 2)));
		Assert.That(new Box2F(-2, -2, 1, 1).Intersect(new(-1, -1, 2, 2)), Is.EqualTo(new Box2F(-1, -1, 1, 1)));
	}

	[Test]
	public void Box2_Box2Casting()
	{
		Box2 b = new(1, 2, 3, 4);
		Box2L bl = new(-5, -6, 7, 8);
		Box2F bf = new(9.5f, 10.5f, -11.5f, -12.5f);
		Box2D bd = new(-13.5, -14.5, 15.5, 16.5);

		Assert.That((Box2)bl,  Is.EqualTo(new Box2(-5, -6, 7, 8)));
		Assert.That((Box2)bf,  Is.EqualTo(new Box2(9, 10, -11, -12)));
		Assert.That((Box2)bd,  Is.EqualTo(new Box2(-13, -14, 15, 16)));
		Assert.That((Box2L)b,  Is.EqualTo(new Box2L(1, 2, 3, 4)));
		Assert.That((Box2L)bf, Is.EqualTo(new Box2L(9, 10, -11, -12)));
		Assert.That((Box2L)bd, Is.EqualTo(new Box2L(-13, -14, 15, 16)));
		Assert.That((Box2F)b,  Is.EqualTo(new Box2F(1, 2, 3, 4)));
		Assert.That((Box2F)bl, Is.EqualTo(new Box2F(-5, -6, 7, 8)));
		Assert.That((Box2F)bd, Is.EqualTo(new Box2F(-13.5f, -14.5f, 15.5f, 16.5f)));
		Assert.That((Box2D)b,  Is.EqualTo(new Box2D(1, 2, 3, 4)));
		Assert.That((Box2D)bl, Is.EqualTo(new Box2D(-5, -6, 7, 8)));
		Assert.That((Box2D)bf, Is.EqualTo(new Box2D(9.5, 10.5, -11.5, -12.5)));
	}


	[Test]
	public void Space2D_Box2DContains()
	{
		Box2 b = new(-2, -2, 2, 2);
		for (int x = -3; x <= 3; ++x) { for (int y = -3; y <= 3; ++y) {
			var goodX = x >= b.Min.X && x <= b.Max.X;
			var goodY = y >= b.Min.Y && y <= b.Max.Y;
			Assert.That(b.Contains(new Point2(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(b.Contains(new Point2L(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(b.Contains(new Vec2(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(b.Contains(new Vec2H((Half)x, (Half)y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(b.Contains(new Vec2D(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			if (x >= 0 && y >= 0) {
				Assert.That(b.Contains(new Point2U((uint)x, (uint)y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
				Assert.That(b.Contains(new Point2UL((uint)x, (uint)y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			}
		} }

		Box2F bf = new(-2, -2, 2, 2);
		for (int x = -3; x <= 3; ++x) { for (int y = -3; y <= 3; ++y) {
			var goodX = x >= bf.Min.X && x <= bf.Max.X;
			var goodY = y >= bf.Min.Y && y <= bf.Max.Y;
			Assert.That(bf.Contains(new Point2(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(bf.Contains(new Point2L(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(bf.Contains(new Vec2(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(bf.Contains(new Vec2H((Half)x, (Half)y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			Assert.That(bf.Contains(new Vec2D(x, y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			if (x >= 0 && y >= 0) {
				Assert.That(bf.Contains(new Point2U((uint)x, (uint)y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
				Assert.That(bf.Contains(new Point2UL((uint)x, (uint)y)), Is.EqualTo(goodX && goodY), $"{x},{y}");
			}
		} }
	}
}
