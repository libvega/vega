﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Box3 and related Space3D methods  (only test Box3 and Box3F for identical generated functionality)
internal class Box3Tests
{
	[Test]
	public void Box3_Constants()
	{
		Assert.That(Box3.Empty.Min, Is.EqualTo(Point3.Zero));
		Assert.That(Box3.Empty.Max, Is.EqualTo(Point3.Zero));
		Assert.That(Box3.Unit.Min, Is.EqualTo(Point3.Zero));
		Assert.That(Box3.Unit.Max, Is.EqualTo(new Point3(1)));

		Assert.That(Box3F.Empty.Min, Is.EqualTo(Vec3.Zero));
		Assert.That(Box3F.Empty.Max, Is.EqualTo(Vec3.Zero));
		Assert.That(Box3F.Unit.Min, Is.EqualTo(Vec3.Zero));
		Assert.That(Box3F.Unit.Max, Is.EqualTo(new Vec3(1)));
	}

	[Test]
	public void Box3_Constructors()
	{
		Box3 b = new();
		Assert.That(b.Min, Is.EqualTo(Point3.Zero));
		Assert.That(b.Max, Is.EqualTo(Point3.Zero));
		b = new(new(1, 2, 5), new(3, 4, 6));
		Assert.That(b.Min, Is.EqualTo(new Point3(1, 2, 5)));
		Assert.That(b.Max, Is.EqualTo(new Point3(3, 4, 6)));
		b = new(5, 6, 7, 8, 9, 10);
		Assert.That(b.Min, Is.EqualTo(new Point3(5, 6, 7)));
		Assert.That(b.Max, Is.EqualTo(new Point3(8, 9, 10)));
		b = new(5, 6, 10, 8, 9, 7);
		Assert.That(b.Min, Is.EqualTo(new Point3(5, 6, 7)));
		Assert.That(b.Max, Is.EqualTo(new Point3(8, 9, 10)));

		Box3F bf = new();
		Assert.That(bf.Min, Is.EqualTo(Vec3.Zero));
		Assert.That(bf.Max, Is.EqualTo(Vec3.Zero));
		bf = new(new(1, 2, 5), new(3, 4, 6));
		Assert.That(bf.Min, Is.EqualTo(new Vec3(1, 2, 5)));
		Assert.That(bf.Max, Is.EqualTo(new Vec3(3, 4, 6)));
		bf = new(5, 6, 7, 8, 9, 10);
		Assert.That(bf.Min, Is.EqualTo(new Vec3(5, 6, 7)));
		Assert.That(bf.Max, Is.EqualTo(new Vec3(8, 9, 10)));
		bf = new(5, 6, 10, 8, 9, 7);
		Assert.That(bf.Min, Is.EqualTo(new Vec3(5, 6, 7)));
		Assert.That(bf.Max, Is.EqualTo(new Vec3(8, 9, 10)));
	}

	[Test]
	public void Box3_MinMax()
	{
		Box3 b = new(1, 2, 3, 4, 5, 6);
		Assert.That(b.Min, Is.EqualTo(new Point3(1, 2, 3)));
		Assert.That(b.Max, Is.EqualTo(new Point3(4, 5, 6)));
		b.Min = new(0, 1, 2);
		Assert.That(b.Min, Is.EqualTo(new Point3(0, 1, 2)));
		Assert.That(b.Max, Is.EqualTo(new Point3(4, 5, 6)));
		b.Max = new(5, 6, 7);
		Assert.That(b.Min, Is.EqualTo(new Point3(0, 1, 2)));
		Assert.That(b.Max, Is.EqualTo(new Point3(5, 6, 7)));
		b.Min = new(8, 9, 10);
		Assert.That(b.Min, Is.EqualTo(new Point3(5, 6, 7)));
		Assert.That(b.Max, Is.EqualTo(new Point3(8, 9, 10))); // Swapped min/max
		b.Max = new(-1, -2, -3);
		Assert.That(b.Min, Is.EqualTo(new Point3(-1, -2, -3))); // Swapped min/max
		Assert.That(b.Max, Is.EqualTo(new Point3(5, 6, 7)));

		Box3F b2 = new(1, 2, 3, 4, 5, 6);
		Assert.That(b2.Min, Is.EqualTo(new Vec3(1, 2, 3)));
		Assert.That(b2.Max, Is.EqualTo(new Vec3(4, 5, 6)));
		b2.Min = new(0, 1, 2);
		Assert.That(b2.Min, Is.EqualTo(new Vec3(0, 1, 2)));
		Assert.That(b2.Max, Is.EqualTo(new Vec3(4, 5, 6)));
		b2.Max = new(5, 6, 7);
		Assert.That(b2.Min, Is.EqualTo(new Vec3(0, 1, 2)));
		Assert.That(b2.Max, Is.EqualTo(new Vec3(5, 6, 7)));
		b2.Min = new(8, 9, 10);
		Assert.That(b2.Min, Is.EqualTo(new Vec3(5, 6, 7)));
		Assert.That(b2.Max, Is.EqualTo(new Vec3(8, 9, 10))); // Swapped min/max
		b2.Max = new(-1, -2, -3);
		Assert.That(b2.Min, Is.EqualTo(new Vec3(-1, -2, -3))); // Swapped min/max
		Assert.That(b2.Max, Is.EqualTo(new Vec3(5, 6, 7)));
	}

	[Test]
	public void Box3_SizeFields()
	{
		Box3 b = new(0, 0, 0, 10, 5, 2);
		Assert.That(b.Size, Is.EqualTo(new Point3U(10, 5, 2)));
		Assert.That(b.Width, Is.EqualTo(10));
		Assert.That(b.Height, Is.EqualTo(5));
		Assert.That(b.Depth, Is.EqualTo(2));
		Assert.That(b.Volume, Is.EqualTo(100));
		Assert.False(b.IsEmpty);
		b = new(-10, -5, -2, 10, 5, 2);
		Assert.That(b.Size, Is.EqualTo(new Point3U(20, 10, 4)));
		Assert.That(b.Width, Is.EqualTo(20));
		Assert.That(b.Height, Is.EqualTo(10));
		Assert.That(b.Depth, Is.EqualTo(4));
		Assert.That(b.Volume, Is.EqualTo(800));
		Assert.False(b.IsEmpty);
		Assert.True(Box3.Empty.IsEmpty);

		Box3F bf = new(0, 0, 0, 10, 5, 2);
		Assert.That(bf.Size, Is.EqualTo(new Vec3(10, 5, 2)));
		Assert.That(bf.Width, Is.EqualTo(10));
		Assert.That(bf.Height, Is.EqualTo(5));
		Assert.That(bf.Depth, Is.EqualTo(2));
		Assert.That(bf.Volume, Is.EqualTo(100));
		Assert.False(bf.IsEmpty);
		bf = new(-10, -5, -2, 10, 5, 2);
		Assert.That(bf.Size, Is.EqualTo(new Vec3(20, 10, 4)));
		Assert.That(bf.Width, Is.EqualTo(20));
		Assert.That(bf.Height, Is.EqualTo(10));
		Assert.That(bf.Depth, Is.EqualTo(4));
		Assert.That(bf.Volume, Is.EqualTo(800));
		Assert.False(bf.IsEmpty);
		Assert.True(Box3F.Empty.IsEmpty);
	}

	[Test]
	public void Box3_Center()
	{
		Assert.That(new Box3(0, 0, 0, 10, 5, 2).Center, Is.EqualTo(new Vec3(5, 2.5f, 1)));
		Assert.That(new Box3(-10, -5, -2, 10, 5, 2).Center, Is.EqualTo(Vec3.Zero));
		Assert.That(Box3.Empty.Center, Is.EqualTo(Vec3.Zero));

		Assert.That(new Box3F(0, 0, 0, 10, 5, 2).Center, Is.EqualTo(new Vec3(5, 2.5f, 1)));
		Assert.That(new Box3F(-10, -5, -2, 10, 5, 2).Center, Is.EqualTo(Vec3.Zero));
		Assert.That(Box3F.Empty.Center, Is.EqualTo(Vec3.Zero));
	}

	[Test]
	public void Box3_Equality()
	{
		ReadOnlySpan<Box3> boxList = stackalloc Box3[] {
			new(0, 0, 0, 0, 0, 0), new(1, 0, 0, 0, 0, 1), new(0, 0, 1, 1, 0, 0), new(0, 0, 0, 1, 1, 1),
			new(0, 0, 0, 2, 2, 2), new(0, 0, 0, 10, 5, 2)
		};
		foreach (var b0 in boxList) {
			foreach (var b1 in boxList) {
				Assert.True(b0 == b0);
				Assert.False(b0 != b0);
				Assert.True(b0.Equals(b0));
				Assert.True(b0.Equals((object)b0));
				Assert.That(b0 == b1, Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0 != b1, Is.EqualTo(b0.Min != b1.Min || b0.Max != b1.Max));
				Assert.That(b0.Equals(b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0.Equals((object)b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
			}
		}

		ReadOnlySpan<Box3F> boxFList = stackalloc Box3F[] {
			new(0, 0, 0, 0, 0, 0), new(1, 0, 0, 0, 0, 1), new(0, 0, 1, 1, 0, 0), new(0, 0, 0, 1, 1, 1),
			new(0, 0, 0, 2, 2, 2), new(0, 0, 0, 10, 5, 2)
		};
		foreach (var b0 in boxFList) {
			foreach (var b1 in boxFList) {
				Assert.True(b0 == b0);
				Assert.False(b0 != b0);
				Assert.True(b0.Equals(b0));
				Assert.True(b0.Equals((object)b0));
				Assert.That(b0 == b1, Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0 != b1, Is.EqualTo(b0.Min != b1.Min || b0.Max != b1.Max));
				Assert.That(b0.Equals(b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
				Assert.That(b0.Equals((object)b1), Is.EqualTo(b0.Min == b1.Min && b0.Max == b1.Max));
			}
		}
	}

	[Test]
	public void Box3_Deconstruct()
	{
		Assert.That((_, _) = new Box3(1, 2, 3, 4, 5, 6), Is.EqualTo((new Point3(1, 2, 3), new Point3(4, 5, 6))));
		Assert.That((_, _, _, _, _, _) = new Box3(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));

		Assert.That((_, _) = new Box3F(1, 2, 3, 4, 5, 6), Is.EqualTo((new Vec3(1, 2, 3), new Vec3(4, 5, 6))));
		Assert.That((_, _, _, _, _, _) = new Box3F(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
	}

	[Test]
	public void Box3_TransformMethods()
	{
		Assert.That(Box3.Unit.Translated(new(1, 2, 3)), Is.EqualTo(new Box3(1, 2, 3, 2, 3, 4)));
		Assert.That(Box3.Unit.Translated(new(-5, 5, 6)), Is.EqualTo(new Box3(-5, 5, 6, -4, 6, 7)));
		Assert.That(Box3.Unit.Inflated(new(2, 4, 6)), Is.EqualTo(new Box3(-1, -2, -3, 2, 3, 4)));
		Assert.That(Box3.Unit.Inflated(new(-6, 8, 10)), Is.EqualTo(new Box3(-2, -4, -5, 3, 5, 6))); // Shrink larger than box
		Assert.That(Box3.Unit.Scaled(new(2, 2, 2), new(0)), Is.EqualTo(new Box3(0, 0, 0, 2, 2, 2)));
		Assert.That(Box3.Unit.Scaled(new(2, 2, 2), new(1)), Is.EqualTo(new Box3(-1, -1, -1, 1, 1, 1)));
		Assert.That(new Box3(0, 0, 0, 4, 4, 4).Scaled(new(2, 2, 2), new(1, 1, 1)), Is.EqualTo(new Box3(-1, -1, -1, 7, 7, 7)));

		Assert.That(Box3F.Unit.Translated(new(1, 2, 3)), Is.EqualTo(new Box3F(1, 2, 3, 2, 3, 4)));
		Assert.That(Box3F.Unit.Translated(new(-5, 5, 6)), Is.EqualTo(new Box3F(-5, 5, 6, -4, 6, 7)));
		Assert.That(Box3F.Unit.Inflated(new(2, 4, 6)), Is.EqualTo(new Box3F(-1, -2, -3, 2, 3, 4)));
		Assert.That(Box3F.Unit.Inflated(new(-6, 8, 10)), Is.EqualTo(new Box3F(-2, -4, -5, 3, 5, 6))); // Shrink larger than box
		Assert.That(Box3F.Unit.Scaled(new(2, 2, 2), new(0)), Is.EqualTo(new Box3F(0, 0, 0, 2, 2, 2)));
		Assert.That(Box3F.Unit.Scaled(new(2, 2, 2), new(1)), Is.EqualTo(new Box3F(-1, -1, -1, 1, 1, 1)));
		Assert.That(new Box3F(0, 0, 0, 4, 4, 4).Scaled(new(2, 2, 2), new(1, 1, 1)), Is.EqualTo(new Box3F(-1, -1, -1, 7, 7, 7)));
	}

	[Test]
	public void Box3_CombineMethods()
	{
		Assert.That(Box3.Unit.Union(Box3.Unit), Is.EqualTo(Box3.Unit));
		Assert.That(Box3.Unit.Intersect(Box3.Unit), Is.EqualTo(Box3.Unit));
		Assert.That(new Box3(0, 0, 0, 1, 1, 1).Union(new(2, 2, 2, 3, 3, 3)), Is.EqualTo(new Box3(0, 0, 0, 3, 3, 3)));
		Assert.That(new Box3(0, 0, 0, 1, 1, 1).Intersect(new(2, 2, 2, 3, 3, 3)), Is.EqualTo(Box3.Empty));
		Assert.That(new Box3(-2, -2, -2, 1, 1, 1).Union(new(-1, -1, -1, 2, 2, 2)), Is.EqualTo(new Box3(-2, -2, -2, 2, 2, 2)));
		Assert.That(new Box3(-2, -2, -2, 1, 1, 1).Intersect(new(-1, -1, -1, 2, 2, 2)), Is.EqualTo(new Box3(-1, -1, -1, 1, 1, 1)));

		Assert.That(Box3F.Unit.Union(Box3F.Unit), Is.EqualTo(Box3F.Unit));
		Assert.That(Box3F.Unit.Intersect(Box3F.Unit), Is.EqualTo(Box3F.Unit));
		Assert.That(new Box3F(0, 0, 0, 1, 1, 1).Union(new(2, 2, 2, 3, 3, 3)), Is.EqualTo(new Box3F(0, 0, 0, 3, 3, 3)));
		Assert.That(new Box3F(0, 0, 0, 1, 1, 1).Intersect(new(2, 2, 2, 3, 3, 3)), Is.EqualTo(Box3F.Empty));
		Assert.That(new Box3F(-2, -2, -2, 1, 1, 1).Union(new(-1, -1, -1, 2, 2, 2)), Is.EqualTo(new Box3F(-2, -2, -2, 2, 2, 2)));
		Assert.That(new Box3F(-2, -2, -2, 1, 1, 1).Intersect(new(-1, -1, -1, 2, 2, 2)), Is.EqualTo(new Box3F(-1, -1, -1, 1, 1, 1)));
	}

	[Test]
	public void Box3_Box3Casting()
	{
		Box3 b = new(1, 2, 3, 4, 5, 6);
		Box3L bl = new(-5, -6, -7, 8, 9, 10);
		Box3F bf = new(9.5f, 10.5f, 11.5f, -12.5f, -13.5f, -14.5f);
		Box3D bd = new(-13.5, -14.5, -15.5, 16.5, 17.5, 18.5);

		Assert.That((Box3)bl,  Is.EqualTo(new Box3(-5, -6, -7, 8, 9, 10)));
		Assert.That((Box3)bf,  Is.EqualTo(new Box3(9, 10, 11, -12, -13, -14)));
		Assert.That((Box3)bd,  Is.EqualTo(new Box3(-13, -14, -15, 16, 17, 18)));
		Assert.That((Box3L)b,  Is.EqualTo(new Box3L(1, 2, 3, 4, 5, 6)));
		Assert.That((Box3L)bf, Is.EqualTo(new Box3L(9, 10, 11, -12, -13, -14)));
		Assert.That((Box3L)bd, Is.EqualTo(new Box3L(-13, -14, -15, 16, 17, 18)));
		Assert.That((Box3F)b,  Is.EqualTo(new Box3F(1, 2, 3, 4, 5, 6)));
		Assert.That((Box3F)bl, Is.EqualTo(new Box3F(-5, -6, -7, 8, 9, 10)));
		Assert.That((Box3F)bd, Is.EqualTo(new Box3F(-13.5f, -14.5f, -15.5f, 16.5f, 17.5f, 18.5f)));
		Assert.That((Box3D)b,  Is.EqualTo(new Box3D(1, 2, 3, 4, 5, 6)));
		Assert.That((Box3D)bl, Is.EqualTo(new Box3D(-5, -6, -7, 8, 9, 10)));
		Assert.That((Box3D)bf, Is.EqualTo(new Box3D(9.5, 10.5, 11.5, -12.5, -13.5, -14.5)));
	}


	[Test]
	public void Space3D_Box3DContains()
	{
		Box3 b = new(-2, -2, -2, 2, 2, 2);
		for (int x = -3; x <= 3; ++x) { for (int y = -3; y <= 3; ++y) { for (int z = -3; z <= 3; ++z) {
			var goodX = x >= b.Min.X && x <= b.Max.X;
			var goodY = y >= b.Min.Y && y <= b.Max.Y;
			var goodZ = z >= b.Min.Z && z <= b.Max.Z;
			Assert.That(b.Contains(new Point3(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(b.Contains(new Point3L(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(b.Contains(new Vec3(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(b.Contains(new Vec3H((Half)x, (Half)y, (Half)z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(b.Contains(new Vec3D(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			if (x >= 0 && y >= 0 && z >= 0) {
				Assert.That(b.Contains(new Point3U((uint)x, (uint)y, (uint)z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
				Assert.That(b.Contains(new Point3UL((uint)x, (uint)y, (uint)z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			}
		} } }

		Box3F bf = new(-2, -2, -2, 2, 2, 2);
		for (int x = -3; x <= 3; ++x) { for (int y = -3; y <= 3; ++y) { for (int z = -3; z <= 3; ++z) {
			var goodX = x >= bf.Min.X && x <= bf.Max.X;
			var goodY = y >= bf.Min.Y && y <= bf.Max.Y;
			var goodZ = z >= bf.Min.Z && z <= bf.Max.Z;
			Assert.That(bf.Contains(new Point3(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(bf.Contains(new Point3L(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(bf.Contains(new Vec3(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(bf.Contains(new Vec3H((Half)x, (Half)y, (Half)z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			Assert.That(bf.Contains(new Vec3D(x, y, z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			if (x >= 0 && y >= 0 && z >= 0) {
				Assert.That(bf.Contains(new Point3U((uint)x, (uint)y, (uint)z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
				Assert.That(bf.Contains(new Point3UL((uint)x, (uint)y, (uint)z)), Is.EqualTo(goodX && goodY && goodZ), $"{x},{y},{z}");
			}
		} } }
	}
}
