﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Maths;

namespace Astrum.Tests.Maths;


// Tests for Angle and AngleD  (only test Angle for functionality that is generated identical for both)
internal class AngleTests
{
	[Test]
	public void Angle_Constants_And_RadDegConversion()
	{
		const float DEG_TOLERANCE = 1e-5f;
		const float RAD_TOLERANCE = 1e-6f;

		Assert.That(Angle.Zero.Degrees, Is.EqualTo(0).Within(RAD_TOLERANCE));

		Assert.That(Angle.D10.Degrees,  Is.EqualTo(10).Within(DEG_TOLERANCE));
		Assert.That(Angle.D15.Degrees,  Is.EqualTo(15).Within(DEG_TOLERANCE));
		Assert.That(Angle.D20.Degrees,  Is.EqualTo(20).Within(DEG_TOLERANCE));
		Assert.That(Angle.D30.Degrees,  Is.EqualTo(30).Within(DEG_TOLERANCE));
		Assert.That(Angle.D45.Degrees,  Is.EqualTo(45).Within(DEG_TOLERANCE));
		Assert.That(Angle.D60.Degrees,  Is.EqualTo(60).Within(DEG_TOLERANCE));
		Assert.That(Angle.D90.Degrees,  Is.EqualTo(90).Within(DEG_TOLERANCE));
		Assert.That(Angle.D180.Degrees, Is.EqualTo(180).Within(DEG_TOLERANCE));
		Assert.That(Angle.D360.Degrees, Is.EqualTo(360).Within(DEG_TOLERANCE));

		Assert.That(Angle.PiO6.Radians,  Is.EqualTo(Math.PI / 6).Within(RAD_TOLERANCE));
		Assert.That(Angle.PiO4.Radians,  Is.EqualTo(Math.PI / 4).Within(RAD_TOLERANCE));
		Assert.That(Angle.PiO3.Radians,  Is.EqualTo(Math.PI / 3).Within(RAD_TOLERANCE));
		Assert.That(Angle.PiO2.Radians,  Is.EqualTo(Math.PI / 2).Within(RAD_TOLERANCE));
		Assert.That(Angle.Pi.Radians,    Is.EqualTo(Math.PI).Within(RAD_TOLERANCE));
		Assert.That(Angle.Tau.Radians,   Is.EqualTo(Math.Tau).Within(RAD_TOLERANCE));
	}

	[Test]
	public void Angle_PrecisionConversion()
	{
		const float TOLERANCE = 1e-5f;

		AngleD a = Angle.D90;
		Assert.That(a.Degrees, Is.EqualTo(90).Within(TOLERANCE));
		Assert.That(((Angle)a).Degrees, Is.EqualTo(90).Within(TOLERANCE));
	}

	[Test]
	public void Angle_TrigFuncFields()
	{
		const float TOLERANCE = 1e-8f;

		Assert.That(Angle.Zero.Sin, Is.EqualTo(MathF.Sin(0)).Within(TOLERANCE));
		Assert.That(Angle.D30.Sin,  Is.EqualTo(MathF.Sin(MathF.PI / 6)).Within(TOLERANCE));
		Assert.That(Angle.D45.Sin,  Is.EqualTo(MathF.Sin(MathF.PI / 4)).Within(TOLERANCE));
		Assert.That(Angle.D60.Sin,  Is.EqualTo(MathF.Sin(MathF.PI / 3)).Within(TOLERANCE));
		Assert.That(Angle.D90.Sin,  Is.EqualTo(MathF.Sin(MathF.PI / 2)).Within(TOLERANCE));
		Assert.That(Angle.D180.Sin, Is.EqualTo(MathF.Sin(MathF.PI)).Within(TOLERANCE));

		Assert.That(Angle.Zero.Cos, Is.EqualTo(MathF.Cos(0)).Within(TOLERANCE));
		Assert.That(Angle.D30.Cos,  Is.EqualTo(MathF.Cos(MathF.PI / 6)).Within(TOLERANCE));
		Assert.That(Angle.D45.Cos,  Is.EqualTo(MathF.Cos(MathF.PI / 4)).Within(TOLERANCE));
		Assert.That(Angle.D60.Cos,  Is.EqualTo(MathF.Cos(MathF.PI / 3)).Within(TOLERANCE));
		Assert.That(Angle.D90.Cos,  Is.EqualTo(MathF.Cos(MathF.PI / 2)).Within(TOLERANCE));
		Assert.That(Angle.D180.Cos, Is.EqualTo(MathF.Cos(MathF.PI)).Within(TOLERANCE));

		Assert.That(Angle.Zero.Tan, Is.EqualTo(MathF.Tan(0)).Within(TOLERANCE));
		Assert.That(Angle.D30.Tan,  Is.EqualTo(MathF.Tan(MathF.PI / 6)).Within(TOLERANCE));
		Assert.That(Angle.D45.Tan,  Is.EqualTo(MathF.Tan(MathF.PI / 4)).Within(TOLERANCE));
		Assert.That(Angle.D60.Tan,  Is.EqualTo(MathF.Tan(MathF.PI / 3)).Within(TOLERANCE));
		Assert.That(Angle.D180.Tan, Is.EqualTo(MathF.Tan(MathF.PI)).Within(TOLERANCE));

		Assert.That(Angle.D45.SinCos.Sin, Is.EqualTo(MathF.Sin(MathF.PI / 4)).Within(TOLERANCE));
		Assert.That(Angle.D45.SinCos.Cos, Is.EqualTo(MathF.Cos(MathF.PI / 4)).Within(TOLERANCE));
		Assert.That(Angle.PiO4.SinCos.Sin, Is.EqualTo(MathF.Sin(MathF.PI / 4)).Within(TOLERANCE));
		Assert.That(Angle.PiO4.SinCos.Cos, Is.EqualTo(MathF.Cos(MathF.PI / 4)).Within(TOLERANCE));
	}

	[Test]
	public void Angle_EqualityAndComparisons()
	{
		Span<Angle> angles = stackalloc Angle[8] {
			Angle.Rad(-10), Angle.Rad(-8), Angle.Rad(-1), Angle.Zero, Angle.PiO6, Angle.PiO2, Angle.Pi, Angle.Rad(100)
		};

		Assert.True(Angle.Zero.ApproxZero());
		Assert.False(Angle.Pi.ApproxZero());
		Assert.False(Angle.D45.ApproxZero());
		Assert.False(Angle.D360.ApproxZero());

		for (int i = 0; i < angles.Length - 1; ++i) {
			Assert.True(angles[i] <  angles[i + 1]);
			Assert.True(angles[i] <= angles[i + 1]);

			Assert.True(angles[i + 1] >  angles[i]);
			Assert.True(angles[i + 1] >= angles[i]);

			Assert.True(angles[i] == angles[i]);
			Assert.True(angles[i].Equals(angles[i]));
			Assert.True(angles[i].Equals((object)angles[i]));
			Assert.True(angles[i].ApproxEqual(angles[i]));
			Assert.True(angles[i] <= angles[i]);
			Assert.True(angles[i] >= angles[i]);

			Assert.True(angles[i] != angles[i + 1]);
			Assert.False(angles[i].Equals(angles[i + 1]));
			Assert.False(angles[i].Equals((object)angles[i + 1]));
			Assert.False(angles[i].ApproxEqual(angles[i + 1]));
		}
	}

	[Test]
	public void Angle_ArithmeticOperators()
	{
		const float TOLERANCE = 1e-8f;

		Assert.That((-Angle.D45).Degrees, Is.EqualTo(-45f).Within(TOLERANCE));
		Assert.That((-Angle.PiO4).Radians, Is.EqualTo(-MathF.PI / 4).Within(TOLERANCE));

		Assert.That(Angle.PiO4 + Angle.PiO4, Is.EqualTo(Angle.PiO2));

		Assert.That(Angle.PiO4 - Angle.PiO4, Is.EqualTo(Angle.Zero));

		Assert.That(Angle.PiO4 + -Angle.PiO4, Is.EqualTo(Angle.Zero));

		Assert.That(Angle.PiO4 - -Angle.PiO4, Is.EqualTo(Angle.PiO2));

		Assert.That(Angle.PiO6 * 2, Is.EqualTo(Angle.PiO3));
		Assert.That(-Angle.PiO6 * 2, Is.EqualTo(-Angle.PiO3));

		Assert.That(Angle.PiO3 / 2, Is.EqualTo(Angle.PiO6));
		Assert.That(-Angle.PiO3 / 2, Is.EqualTo(-Angle.PiO6));

		Assert.That(Angle.Zero % Angle.PiO6, Is.EqualTo(Angle.Zero));
		Assert.That(Angle.PiO6 % Angle.PiO6, Is.EqualTo(Angle.Zero));
		Assert.That(Angle.PiO4 % Angle.PiO6, Is.EqualTo(Angle.Rad(MathF.PI / 12)));
		Assert.That(Angle.PiO3 % Angle.PiO6, Is.EqualTo(Angle.Zero));
	}

	[Test]
	public void Angle_Normalized()
	{
		const float TOLERANCE = 1e-4f;

		Assert.That(Angle.Zero.Normalized, Is.EqualTo(Angle.Zero));

		Assert.That( Angle.PiO6.Normalized, Is.EqualTo(Angle.PiO6));
		Assert.That(-Angle.PiO6.Normalized, Is.EqualTo(-Angle.PiO6));

		Assert.That(Angle.Deg(370).Normalized.Degrees, Is.EqualTo(10).Within(TOLERANCE));
		Assert.That(Angle.Deg(-370).Normalized.Degrees, Is.EqualTo(-10).Within(TOLERANCE));
	}

	[Test]
	public void Angle_Clamp()
	{
		const float TOLERANCE = 1e-5f;

		var step = Angle.PiO6;
		Span<Angle> angles = stackalloc Angle[5] {
			-Angle.PiO4, -Angle.PiO6, Angle.Zero, Angle.PiO4, Angle.PiO2
		};

		foreach (var angle in angles) {
			var min = angle - step;
			var max = angle + step;

			Assert.That(Angle.Clamp(angle - (step * 2), min, max).Radians,
				Is.EqualTo(min.Radians).Within(TOLERANCE));
			Assert.That(Angle.Clamp(angle + (step * 2), min, max).Radians,
				Is.EqualTo(max.Radians).Within(TOLERANCE));
			Assert.That(Angle.Clamp(angle, min, max).Radians,
				Is.EqualTo(angle.Radians).Within(TOLERANCE));
		}
	}

	[Test]
	public void Angle_MinMax()
	{
		Span<Angle> angles = stackalloc Angle[7] {
			-Angle.D360, -Angle.PiO4, -Angle.PiO6, Angle.Zero, Angle.PiO4, Angle.PiO2, Angle.Tau
		};

		for (int ai = 0; ai < angles.Length - 1; ++ai) {
			Assert.That(Angle.Min(angles[ai], angles[ai + 1]), Is.EqualTo(angles[ai]));
			Assert.That(Angle.Max(angles[ai], angles[ai + 1]), Is.EqualTo(angles[ai + 1]));
		}
	}
}
