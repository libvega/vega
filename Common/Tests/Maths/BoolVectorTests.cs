﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Numerics;
using Astrum.Maths;

namespace Astrum.Tests.Maths;


// Tests for boolean vectors (Bool2/3/4)
internal class BoolVectorTests
{
	static readonly Bool2[] _AllVec2;
	static readonly Bool3[] _AllVec3;
	static readonly Bool4[] _AllVec4;

	[Test]
	public void BoolVec_Constructors()
	{
		Bool2 b2;
		b2 = new();
		Assert.That((b2.X, b2.Y), Is.EqualTo((false, false)));
		b2 = new(true);
		Assert.That((b2.X, b2.Y), Is.EqualTo((true, true)));
		b2 = new(true, false);
		Assert.That((b2.X, b2.Y), Is.EqualTo((true, false)));

		Bool3 b3;
		b3 = new();
		Assert.That((b3.X, b3.Y, b3.Z), Is.EqualTo((false, false, false)));
		b3 = new(true);
		Assert.That((b3.X, b3.Y, b3.Z), Is.EqualTo((true, true, true)));
		b3 = new(true, false, true);
		Assert.That((b3.X, b3.Y, b3.Z), Is.EqualTo((true, false, true)));

		Bool4 b4;
		b4 = new();
		Assert.That((b4.X, b4.Y, b4.Z, b4.W), Is.EqualTo((false, false, false, false)));
		b4 = new(true);
		Assert.That((b4.X, b4.Y, b4.Z, b4.W), Is.EqualTo((true, true, true, true)));
		b4 = new(true, false, true, false);
		Assert.That((b4.X, b4.Y, b4.Z, b4.W), Is.EqualTo((true, false, true, false)));
	}

	[Test]
	public void BoolVec_AnyAllNone()
	{
		Bool2 b2 = new();
		Assert.True(b2.None); Assert.False(b2.Any); Assert.False(b2.All);
		Bool3 b3 = new();
		Assert.True(b3.None); Assert.False(b3.Any); Assert.False(b3.All);
		Bool4 b4 = new();
		Assert.True(b4.None); Assert.False(b4.Any); Assert.False(b4.All);

		foreach (var vec in _AllVec2.AsSpan(1, _AllVec2.Length - 2)) {
			Assert.False(vec.None);
			Assert.True(vec.Any);
			Assert.False(vec.All);
		}
		foreach (var vec in _AllVec3.AsSpan(1, _AllVec3.Length - 2)) {
			Assert.False(vec.None);
			Assert.True(vec.Any);
			Assert.False(vec.All);
		}
		foreach (var vec in _AllVec4.AsSpan(1, _AllVec4.Length - 2)) {
			Assert.False(vec.None);
			Assert.True(vec.Any);
			Assert.False(vec.All);
		}

		b2 = new(true, true);
		Assert.False(b2.None); Assert.True(b2.Any); Assert.True(b2.All);
		b3 = new(true, true, true);
		Assert.False(b3.None); Assert.True(b3.Any); Assert.True(b3.All);
		b4 = new(true, true, true, true);
		Assert.False(b4.None); Assert.True(b4.Any); Assert.True(b4.All);
	}

	[Test]
	public void BoolVec_Count()
	{
		for (uint i = 0; i < _AllVec2.Length; ++i) {
			var exp = (uint)BitOperations.PopCount(i);
			Assert.That(_AllVec2[i].Count, Is.EqualTo(exp));
		}
		for (uint i = 0; i < _AllVec3.Length; ++i) {
			var exp = (uint)BitOperations.PopCount(i);
			Assert.That(_AllVec3[i].Count, Is.EqualTo(exp));
		}
		for (uint i = 0; i < _AllVec4.Length; ++i) {
			var exp = (uint)BitOperations.PopCount(i);
			Assert.That(_AllVec4[i].Count, Is.EqualTo(exp));
		}
	}

	[Test]
	public void BoolVec_EqualityOperatorsAndMethods()
	{
		for (int i = 0; i < _AllVec2.Length - 1; ++i) {
			Assert.True(_AllVec2[i] == _AllVec2[i]);
			Assert.True(_AllVec2[i] != _AllVec2[i + 1]);
			Assert.True(_AllVec2[i].Equals(_AllVec2[i]));
			Assert.True(_AllVec2[i].Equals((object)_AllVec2[i]));
			Assert.False(_AllVec2[i].Equals(_AllVec2[i + 1]));
			Assert.False(_AllVec2[i].Equals((object)_AllVec2[i + 1]));
		}
		for (int i = 0; i < _AllVec3.Length - 1; ++i) {
			Assert.True(_AllVec3[i] == _AllVec3[i]);
			Assert.True(_AllVec3[i] != _AllVec3[i + 1]);
			Assert.True(_AllVec3[i].Equals(_AllVec3[i]));
			Assert.True(_AllVec3[i].Equals((object)_AllVec3[i]));
			Assert.False(_AllVec3[i].Equals(_AllVec3[i + 1]));
			Assert.False(_AllVec3[i].Equals((object)_AllVec3[i + 1]));
		}
		for (int i = 0; i < _AllVec4.Length - 1; ++i) {
			Assert.True(_AllVec4[i] == _AllVec4[i]);
			Assert.True(_AllVec4[i] != _AllVec4[i + 1]);
			Assert.True(_AllVec4[i].Equals(_AllVec4[i]));
			Assert.True(_AllVec4[i].Equals((object)_AllVec4[i]));
			Assert.False(_AllVec4[i].Equals(_AllVec4[i + 1]));
			Assert.False(_AllVec4[i].Equals((object)_AllVec4[i + 1]));
		}
	}

	[Test]
	public void BoolVec_Deconstruct()
	{
		for (uint i = 0; i < _AllVec2.Length; ++i) {
			var exp = ((i & 0x1) > 0, (i & 0x2) > 0);
			var (x, y) = _AllVec2[i];
			Assert.That((x, y), Is.EqualTo(exp));
		}
		for (uint i = 0; i < _AllVec3.Length; ++i) {
			var exp = ((i & 0x1) > 0, (i & 0x2) > 0, (i & 0x4) > 0);
			var (x, y, z) = _AllVec3[i];
			Assert.That((x, y, z), Is.EqualTo(exp));
		}
		for (uint i = 0; i < _AllVec4.Length; ++i) {
			var exp = ((i & 0x1) > 0, (i & 0x2) > 0, (i & 0x4) > 0, (i & 0x8) > 0);
			var (x, y, z, w) = _AllVec4[i];
			Assert.That((x, y, z, w), Is.EqualTo(exp));
		}
	}

	[Test]
	public void BoolVec_BitwiseOperators()
	{
		Bool2 b21 = new(true, false);
		Bool2 b22 = new(true, true);
		Assert.That(b21 & b22, Is.EqualTo(new Bool2(true, false)));
		Assert.That(b21 | b22, Is.EqualTo(new Bool2(true, true)));
		Assert.That(b21 ^ b22, Is.EqualTo(new Bool2(false, true)));
		Assert.That(!b21, Is.EqualTo(new Bool2(false, true)));
		Assert.That(~b21, Is.EqualTo(new Bool2(false, true)));

		Bool3 b31 = new(true, false, false);
		Bool3 b32 = new(true, true, false);
		Assert.That(b31 & b32, Is.EqualTo(new Bool3(true, false, false)));
		Assert.That(b31 | b32, Is.EqualTo(new Bool3(true, true, false)));
		Assert.That(b31 ^ b32, Is.EqualTo(new Bool3(false, true, false)));
		Assert.That(!b31, Is.EqualTo(new Bool3(false, true, true)));
		Assert.That(~b31, Is.EqualTo(new Bool3(false, true, true)));

		Bool4 b41 = new(true, false, true, false);
		Bool4 b42 = new(true, true, false, false);
		Assert.That(b41 & b42, Is.EqualTo(new Bool4(true, false, false, false)));
		Assert.That(b41 | b42, Is.EqualTo(new Bool4(true, true, true, false)));
		Assert.That(b41 ^ b42, Is.EqualTo(new Bool4(false, true, true, false)));
		Assert.That(!b41, Is.EqualTo(new Bool4(false, true, false, true)));
		Assert.That(~b41, Is.EqualTo(new Bool4(false, true, false, true)));
	}


	static BoolVectorTests()
	{
		_AllVec2 = new Bool2[4];
		_AllVec3 = new Bool3[8];
		_AllVec4 = new Bool4[16];

		for (uint i = 0; i < _AllVec2.Length; ++i) {
			_AllVec2[i] = new((i & 0x1) > 0, (i & 0x2) > 0);
		}
		for (uint i = 0; i < _AllVec3.Length; ++i) {
			_AllVec3[i] = new((i & 0x1) > 0, (i & 0x2) > 0, (i & 0x4) > 0);
		}
		for (uint i = 0; i < _AllVec4.Length; ++i) {
			_AllVec4[i] = new((i & 0x1) > 0, (i & 0x2) > 0, (i & 0x4) > 0, (i & 0x8) > 0);
		}
	}
}
