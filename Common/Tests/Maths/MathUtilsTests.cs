﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045


// Tests for MathUtils
public class MathUtilsTests
{
	private static readonly Half[] _Range16 = {
		(Half)0, Half.Epsilon, Half.Epsilon * (Half)10_000, (Half)1,
		(Half)1.333333f, (Half)333f, Half.MaxValue / (Half)2, Half.MaxValue
	};
	private static readonly float[] _Range32 = {
		0, Single.Epsilon, Single.Epsilon * 10_000, 1,
		1.3333333f, 333f, Single.MaxValue / 2, Single.MaxValue
	};
	private static readonly double[] _Range64 = {
		0, Double.Epsilon, Double.Epsilon * 10_000, 1,
		1.3333333, 333, Double.MaxValue / 2, Double.MaxValue
	};


	[Test]
	public void ApproxEqual_When_ExactlyEqual()
	{
		foreach (var value in _Range16) {
			Assert.That(value.ApproxEqual(value), Is.True);
			Assert.That((-value).ApproxEqual(-value), Is.True);
			Assert.That(value.ApproxEqual(value, (Half)0f), Is.True);
		}
		foreach (var value in _Range32) {
			Assert.That(value.ApproxEqual(value), Is.True);
			Assert.That((-value).ApproxEqual(-value), Is.True);
			Assert.That(value.ApproxEqual(value, 0f), Is.True);
		}
		foreach (var value in _Range64) {
			Assert.That(value.ApproxEqual(value), Is.True);
			Assert.That((-value).ApproxEqual(-value), Is.True);
			Assert.That(value.ApproxEqual(value, 0d), Is.True);
		}
	}

	[Test]
	public void ApproxZero_When_ExactlyZero()
	{
		Assert.That(((Half)0).ApproxZero(), Is.True);
		Assert.That(0f.ApproxZero(), Is.True);
		Assert.That(0d.ApproxZero(), Is.True);
		Assert.That(((Half)(-0)).ApproxZero(), Is.True);
		Assert.That((-0f).ApproxZero(), Is.True);
		Assert.That((-0d).ApproxZero(), Is.True);
	}

	[Test]
	public void ApproxEqual_With_DifferentEps()
	{
		Assert.That(((Half)10).ApproxEqual((Half)9, (Half)0.01f), Is.False);
		Assert.That(((Half)10).ApproxEqual((Half)9, (Half)0.099f), Is.False);
		Assert.That(((Half)10).ApproxEqual((Half)9, (Half)1f), Is.True);
		Assert.That(10f.ApproxEqual(9f, 0.01f), Is.False);
		Assert.That(10f.ApproxEqual(9f, 0.099f), Is.False);
		Assert.That(10f.ApproxEqual(9f, 1f), Is.True);
		Assert.That(10d.ApproxEqual(9d, 0.01), Is.False);
		Assert.That(10d.ApproxEqual(9d, 0.099), Is.False);
		Assert.That(10d.ApproxEqual(9d, 1), Is.True);
	}

	[Test]
	public void ApproxZero_With_DifferentEps()
	{
		Assert.That(((Half)0).ApproxZero((Half)0f), Is.True);
		Assert.That(0f.ApproxZero(0f), Is.True);
		Assert.That(0d.ApproxZero(0d), Is.True);
		Assert.That(Half.Epsilon.ApproxZero((Half)0f), Is.False);
		Assert.That(Single.Epsilon.ApproxZero(0f), Is.False);
		Assert.That(Double.Epsilon.ApproxZero(0d), Is.False);
		Assert.That(Half.Epsilon.ApproxZero(Half.Epsilon), Is.True);
		Assert.That(Single.Epsilon.ApproxZero(Single.Epsilon), Is.True);
		Assert.That(Double.Epsilon.ApproxZero(Double.Epsilon), Is.True);
		Assert.That(((Half)(-5f)).ApproxZero((Half)10f), Is.True);
		Assert.That((-5f).ApproxZero(10f), Is.True);
		Assert.That((-5d).ApproxZero(10d), Is.True);
	}

	[Test]
	public void ApproxEqual_With_NonNormalValues()
	{
		Assert.That(Half.NaN.ApproxEqual(Half.NaN), Is.False);
		Assert.That(Half.PositiveInfinity.ApproxEqual(Half.PositiveInfinity), Is.False);
		Assert.That(Half.NegativeInfinity.ApproxEqual(Half.NegativeInfinity), Is.False);
		Assert.That(Single.NaN.ApproxEqual(Single.NaN), Is.False);
		Assert.That(Single.PositiveInfinity.ApproxEqual(Single.PositiveInfinity), Is.False);
		Assert.That(Single.NegativeInfinity.ApproxEqual(Single.NegativeInfinity), Is.False);
		Assert.That(Double.NaN.ApproxEqual(Double.NaN), Is.False);
		Assert.That(Double.PositiveInfinity.ApproxEqual(Double.PositiveInfinity), Is.False);
		Assert.That(Double.NegativeInfinity.ApproxEqual(Double.NegativeInfinity), Is.False);
	}

	[Test]
	public void ApproxZero_With_NonNormalValues()
	{
		Assert.That(Half.NaN.ApproxZero(), Is.False);
		Assert.That(Half.PositiveInfinity.ApproxZero(), Is.False);
		Assert.That(Half.NegativeInfinity.ApproxZero(), Is.False);
		Assert.That(Single.NaN.ApproxZero(), Is.False);
		Assert.That(Single.PositiveInfinity.ApproxZero(), Is.False);
		Assert.That(Single.NegativeInfinity.ApproxZero(), Is.False);
		Assert.That(Double.NaN.ApproxZero(), Is.False);
		Assert.That(Double.PositiveInfinity.ApproxZero(), Is.False);
		Assert.That(Double.NegativeInfinity.ApproxZero(), Is.False);
	}
}
