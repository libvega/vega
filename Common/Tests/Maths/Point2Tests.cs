﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Point2  (for identical generated functionality, just test Point2, Point2U, or Point2L)
internal class Point2Tests
{
	private static readonly Point2[] _Points = {
		new(-5, -5), new(-3, -1), new(0, 0), new(2, 2), new(5, 5), new(-1, 1)
	};

	[Test]
	public unsafe void Point2_StructLayout()
	{
		Assert.That(sizeof(Point2), Is.EqualTo(8));
		Assert.That(sizeof(Point2U), Is.EqualTo(8));
		Assert.That(sizeof(Point2L), Is.EqualTo(16));
		Assert.That(sizeof(Point2UL), Is.EqualTo(16));

		Assert.That(Marshal.OffsetOf<Point2>("X").ToInt32(),   Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point2>("Y").ToInt32(),   Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Point2U>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point2U>("Y").ToInt32(),  Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Point2L>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point2L>("Y").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point2UL>("X").ToInt32(), Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point2UL>("Y").ToInt32(), Is.EqualTo(8));
	}

	[Test]
	public void Point2_Constants()
	{
		Assert.That((Point2.Zero.X, Point2.Zero.Y), Is.EqualTo((0, 0)));
		Assert.That((Point2.UnitX.X, Point2.UnitX.Y), Is.EqualTo((1, 0)));
		Assert.That((Point2.UnitY.X, Point2.UnitY.Y), Is.EqualTo((0, 1)));
	}

	[Test]
	public void Point2_Constructors()
	{
		Point2 p2;
		p2 = new();     Assert.That((p2.X, p2.Y), Is.EqualTo((0, 0)));
		p2 = new(-1);   Assert.That((p2.X, p2.Y), Is.EqualTo((-1, -1)));
		p2 = new(2, 5); Assert.That((p2.X, p2.Y), Is.EqualTo((2, 5)));

		Point2U p2u;
		p2u = new();     Assert.That((p2u.X, p2u.Y), Is.EqualTo((0, 0)));
		p2u = new(1);    Assert.That((p2u.X, p2u.Y), Is.EqualTo((1, 1)));
		p2u = new(2, 5); Assert.That((p2u.X, p2u.Y), Is.EqualTo((2, 5)));
	}

	[Test]
	public void Point2_Length()
	{
		Assert.That(Point2.Zero.Length,          Is.EqualTo(0));
		Assert.That(Point2.Zero.LengthSq,        Is.EqualTo(0));
		Assert.That(Point2.UnitX.Length,         Is.EqualTo(1));
		Assert.That(Point2.UnitX.LengthSq,       Is.EqualTo(1));
		Assert.That(Point2.UnitY.Length,         Is.EqualTo(1));
		Assert.That(Point2.UnitY.LengthSq,       Is.EqualTo(1));
		Assert.That(new Point2(3, 4).Length,     Is.EqualTo(5));
		Assert.That(new Point2(3, 4).LengthSq,   Is.EqualTo(25));
		Assert.That(new Point2(-3, -4).Length,   Is.EqualTo(5));
		Assert.That(new Point2(-3, -4).LengthSq, Is.EqualTo(25));
	}

	[Test]
	public void Point2_Deconstruct()
	{
		Assert.That((_, _) = new Point2(2, 3), Is.EqualTo((2, 3)));
	}

	[Test]
	public void Point2_Equality()
	{
		for (int pi = 0; pi < _Points.Length - 1; ++pi) {
			var l = _Points[pi];
			var r = _Points[pi + 1];

			Assert.True(l == l);
			Assert.True(l != r);
			Assert.True(l.Equals(l));
			Assert.True(l.Equals((object)l));
			Assert.False(l.Equals(r));
			Assert.False(l.Equals((object)r));

			Bool2
				lt  = new(l.X <  r.X, l.Y <  r.Y),
				lte = new(l.X <= r.X, l.Y <= r.Y),
				gt  = new(l.X >  r.X, l.Y >  r.Y),
				gte = new(l.X >= r.X, l.Y >= r.Y);
			Assert.That(l < r,  Is.EqualTo(lt));
			Assert.That(l <= r, Is.EqualTo(lte));
			Assert.That(l > r,  Is.EqualTo(gt));
			Assert.That(l >= r, Is.EqualTo(gte));
		}
	}

	[Test]
	public void Point2_ArithmeticOperators()
	{
		Point2 i1 = new(0, 5), i2 = new(-1, -3);
		Assert.That(i1 + i2, Is.EqualTo(new Point2(-1, 2)));
		Assert.That(i1 - i2, Is.EqualTo(new Point2(1, 8)));
		Assert.That(i1 * i2, Is.EqualTo(new Point2(0, -15)));
		Assert.That(i1 * 2,  Is.EqualTo(new Point2(0, 10)));
		Assert.That(-i2,     Is.EqualTo(new Point2(1, 3)));
	}

	[Test]
	public void Point2_Vec2Casting()
	{
		Assert.That((Point2)new Vec2(-5.5f, 2.5f), Is.EqualTo(new Point2(-5, 2)));
		Assert.That((Point2)new Vec2H((Half)(-5.5f), (Half)2.5f), Is.EqualTo(new Point2(-5, 2)));
		Assert.That((Point2)new Vec2D(-5.5, 2.5), Is.EqualTo(new Point2(-5, 2)));
	}

	[Test]
	public void Point2_Dot()
	{
		for (int pi = 0; pi < _Points.Length - 1; ++pi) {
			var l = _Points[pi];
			var r = _Points[pi + 1];

			Assert.That(l.Dot(l), Is.EqualTo(l.LengthSq));
			var exp = l.X * r.X + l.Y * r.Y;
			Assert.That(l.Dot(r), Is.EqualTo(exp));
		}
	}

	[Test]
	public void Point2_AngleWith()
	{
		const float TOLERANCE = 1e-5f;

		Assert.That(Point2.UnitX.AngleWith(Point2.UnitX),  Is.EqualTo(Angle.Zero));
		Assert.That(Point2.UnitX.AngleWith(new(1, 1)),     Is.EqualTo(Angle.PiO4));
		Assert.That(Point2.UnitX.AngleWith(Point2.UnitY),  Is.EqualTo(Angle.PiO2));
		Assert.That(Point2.UnitY.AngleWith(Point2.UnitX),  Is.EqualTo(Angle.PiO2));
		Assert.That(Point2.UnitX.AngleWith(new(-1, 1)),    Is.EqualTo(Angle.PiO2 + Angle.PiO4));
		Assert.That(Point2.UnitX.AngleWith(-Point2.UnitX), Is.EqualTo(Angle.Pi));
		Assert.That(Point2.UnitX.AngleWith(-Point2.UnitY), Is.EqualTo(Angle.PiO2));

		for (Angle angle = Angle.Zero; angle < Angle.D180; angle += Angle.Deg(10)) {
			var sc = angle.SinCos;
			Vec2 vec = new(sc.Cos, sc.Sin);
			Assert.True(Vec2.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
			vec = new(sc.Cos, -sc.Sin);
			Assert.True(Vec2.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
		}
	}

	[Test]
	public void Point2_Clamp()
	{
		Point2 i1 = new(-5, 4);
		Assert.That(Point2.Clamp(i1, new(-1, -2), new(1, 2)),  Is.EqualTo(new Point2(-1, 2))); // all change
		Assert.That(Point2.Clamp(i1, new(-8, -8), new(8, 8)),  Is.EqualTo(new Point2(-5, 4))); // none change
		Assert.That(Point2.Clamp(i1, new(-4, -8), new(8, 8)),  Is.EqualTo(new Point2(-4, 4))); // x min
		Assert.That(Point2.Clamp(i1, new(-8, -8), new(-7, 8)), Is.EqualTo(new Point2(-7, 4))); // x max
		Assert.That(Point2.Clamp(i1, new(-8, 5),  new(8, 8)),  Is.EqualTo(new Point2(-5, 5))); // y min
		Assert.That(Point2.Clamp(i1, new(-8, -8), new(8, 0)),  Is.EqualTo(new Point2(-5, 0))); // y max
	}

	[Test]
	public void Point2_MinMax()
	{
		Point2 i1 = new(-5, 4), i2 = new(3, -2);
		Assert.That(Point2.Min(i1, i2), Is.EqualTo(new Point2(-5, -2)));
		Assert.That(Point2.Max(i1, i2), Is.EqualTo(new Point2(3, 4)));
	}
}
