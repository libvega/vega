﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Point3  (for identical generated functionality, just test Point3, Point3U, or Point3L)
internal class Point3Tests
{
	private static readonly Point3[] _Points = {
		new(-5, -5, -5), new(-3, -1, 0), new(0, 0, 0), new(2, 2, 2), new(5, 5, 5), new(-1, 1, -1)
	};

	[Test]
	public unsafe void Point3_StructLayout()
	{
		Assert.That(sizeof(Point3), Is.EqualTo(12));
		Assert.That(sizeof(Point3U), Is.EqualTo(12));
		Assert.That(sizeof(Point3L), Is.EqualTo(24));
		Assert.That(sizeof(Point3UL), Is.EqualTo(24));

		Assert.That(Marshal.OffsetOf<Point3>("X").ToInt32(),   Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point3>("Y").ToInt32(),   Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Point3>("Z").ToInt32(),   Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point3U>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point3U>("Y").ToInt32(),  Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Point3U>("Z").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point3L>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point3L>("Y").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point3L>("Z").ToInt32(),  Is.EqualTo(16));
		Assert.That(Marshal.OffsetOf<Point3UL>("X").ToInt32(), Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point3UL>("Y").ToInt32(), Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point3UL>("Z").ToInt32(), Is.EqualTo(16));
	}

	[Test]
	public void Point3_Constants()
	{
		Assert.That((Point3.Zero.X, Point3.Zero.Y, Point3.Zero.Z), Is.EqualTo((0, 0, 0)));
		Assert.That((Point3.UnitX.X, Point3.UnitX.Y, Point3.UnitX.Z), Is.EqualTo((1, 0, 0)));
		Assert.That((Point3.UnitY.X, Point3.UnitY.Y, Point3.UnitY.Z), Is.EqualTo((0, 1, 0)));
		Assert.That((Point3.UnitZ.X, Point3.UnitZ.Y, Point3.UnitZ.Z), Is.EqualTo((0, 0, 1)));
		Assert.That((Point3.Forward.X, Point3.Forward.Y, Point3.Forward.Z), Is.EqualTo((0, 0, -1)));
		Assert.That((Point3.Backward.X, Point3.Backward.Y, Point3.Backward.Z), Is.EqualTo((0, 0, 1)));
		Assert.That((Point3.Up.X, Point3.Up.Y, Point3.Up.Z), Is.EqualTo((0, 1, 0)));
		Assert.That((Point3.Down.X, Point3.Down.Y, Point3.Down.Z), Is.EqualTo((0, -1, 0)));
		Assert.That((Point3.Left.X, Point3.Left.Y, Point3.Left.Z), Is.EqualTo((-1, 0, 0)));
		Assert.That((Point3.Right.X, Point3.Right.Y, Point3.Right.Z), Is.EqualTo((1, 0, 0)));
	}

	[Test]
	public void Point3_Constructors()
	{
		Point3 p2;
		p2 = new();               Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((0, 0, 0)));
		p2 = new(-1);             Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((-1, -1, -1)));
		p2 = new(2, 5, 9);        Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((2, 5, 9)));
		p2 = new(new(-1, -2), 9); Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((-1, -2, 9)));

		Point3U p2u;
		p2u = new();             Assert.That((p2u.X, p2u.Y, p2u.Z), Is.EqualTo((0, 0, 0)));
		p2u = new(1);            Assert.That((p2u.X, p2u.Y, p2u.Z), Is.EqualTo((1, 1, 1)));
		p2u = new(2, 5, 9);      Assert.That((p2u.X, p2u.Y, p2u.Z), Is.EqualTo((2, 5, 9)));
		p2u = new(new(4, 6), 9); Assert.That((p2u.X, p2u.Y, p2u.Z), Is.EqualTo((4, 6, 9)));
	}

	[Test]
	public void Point3_Length()
	{
		const float TOLERANCE = 1e-6f;

		Assert.That(Point3.Zero.Length,    Is.EqualTo(0));
		Assert.That(Point3.Zero.LengthSq,  Is.EqualTo(0));
		Assert.That(Point3.UnitX.Length,   Is.EqualTo(1));
		Assert.That(Point3.UnitX.LengthSq, Is.EqualTo(1));
		Assert.That(Point3.UnitY.Length,   Is.EqualTo(1));
		Assert.That(Point3.UnitY.LengthSq, Is.EqualTo(1));
		Assert.That(Point3.UnitZ.Length,   Is.EqualTo(1));
		Assert.That(Point3.UnitZ.LengthSq, Is.EqualTo(1));
		Assert.That(new Point3(3, 4, 1).Length,      Is.EqualTo(Math.Sqrt(26)).Within(TOLERANCE));
		Assert.That(new Point3(3, 4, 1).LengthSq,    Is.EqualTo(26));
		Assert.That(new Point3(-3, -4, -1).Length,   Is.EqualTo(Math.Sqrt(26)).Within(TOLERANCE));
		Assert.That(new Point3(-3, -4, -1).LengthSq, Is.EqualTo(26));
	}

	[Test]
	public void Point3_Deconstruct()
	{
		Assert.That((_, _, _) = new Point3(2, 3, 4), Is.EqualTo((2, 3, 4)));
	}

	[Test]
	public void Point3_Equality()
	{
		for (int pi = 0; pi < _Points.Length - 1; ++pi) {
			var l = _Points[pi];
			var r = _Points[pi + 1];

			Assert.True(l == l);
			Assert.True(l != r);
			Assert.True(l.Equals(l));
			Assert.True(l.Equals((object)l));
			Assert.False(l.Equals(r));
			Assert.False(l.Equals((object)r));

			Bool3
				lt  = new(l.X <  r.X, l.Y <  r.Y, l.Z <  r.Z),
				lte = new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z),
				gt  = new(l.X >  r.X, l.Y >  r.Y, l.Z >  r.Z),
				gte = new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
			Assert.That(l < r,  Is.EqualTo(lt));
			Assert.That(l <= r, Is.EqualTo(lte));
			Assert.That(l > r,  Is.EqualTo(gt));
			Assert.That(l >= r, Is.EqualTo(gte));
		}
	}

	[Test]
	public void Point3_ArithmeticOperators()
	{
		Point3 i1 = new(0, 5, 3), i2 = new(-1, -3, 2);
		Assert.That(i1 + i2, Is.EqualTo(new Point3(-1, 2, 5)));
		Assert.That(i1 - i2, Is.EqualTo(new Point3(1, 8, 1)));
		Assert.That(i1 * i2, Is.EqualTo(new Point3(0, -15, 6)));
		Assert.That(i1 * 2,  Is.EqualTo(new Point3(0, 10, 6)));
		Assert.That(-i2,     Is.EqualTo(new Point3(1, 3, -2)));
	}

	[Test]
	public void Point3_Vec3Casting()
	{
		Assert.That((Point3)new Vec3(-5.5f, 2.5f, 10.99f), Is.EqualTo(new Point3(-5, 2, 10)));
		Assert.That((Point3)new Vec3H((Half)(-5.5f), (Half)2.5f, (Half)10.99f), Is.EqualTo(new Point3(-5, 2, 10)));
		Assert.That((Point3)new Vec3D(-5.5, 2.5, 10.99), Is.EqualTo(new Point3(-5, 2, 10)));
	}

	[Test]
	public void Point3_Dot()
	{
		for (int pi = 0; pi < _Points.Length - 1; ++pi) {
			var l = _Points[pi];
			var r = _Points[pi + 1];

			Assert.That(l.Dot(l), Is.EqualTo(l.LengthSq));
			var exp = l.X * r.X + l.Y * r.Y + l.Z * r.Z;
			Assert.That(l.Dot(r), Is.EqualTo(exp));
		}
	}

	[Test]
	public void Point3_Cross()
	{
		Assert.That(Point3.UnitX.Cross(Point3.UnitY),  Is.EqualTo(Point3.UnitZ));
		Assert.That(Point3.UnitY.Cross(Point3.UnitZ),  Is.EqualTo(Point3.UnitX));
		Assert.That(Point3.UnitZ.Cross(Point3.UnitX),  Is.EqualTo(Point3.UnitY));
		Assert.That(Point3.UnitX.Cross(-Point3.UnitY), Is.EqualTo(-Point3.UnitZ));
		Assert.That(Point3.UnitY.Cross(-Point3.UnitZ), Is.EqualTo(-Point3.UnitX));
		Assert.That(Point3.UnitZ.Cross(-Point3.UnitX), Is.EqualTo(-Point3.UnitY));
	}

	[Test]
	public void Point3_AngleWith()
	{
		const float TOLERANCE = 1e-5f;

		Assert.That(Point3.UnitX.AngleWith(Point3.UnitX),  Is.EqualTo(Angle.Zero));
		Assert.That(Point3.UnitX.AngleWith(new(1, 1, 0)),  Is.EqualTo(Angle.PiO4));
		Assert.That(Point3.UnitX.AngleWith(new(1, 0, 1)),  Is.EqualTo(Angle.PiO4));
		Assert.That(Point3.UnitX.AngleWith(Point3.UnitY),  Is.EqualTo(Angle.PiO2));
		Assert.That(Point3.UnitY.AngleWith(Point3.UnitX),  Is.EqualTo(Angle.PiO2));
		Assert.That(Point3.UnitX.AngleWith(new(-1, 1, 0)), Is.EqualTo(Angle.PiO2 + Angle.PiO4));
		Assert.That(Point3.UnitX.AngleWith(-Point3.UnitX), Is.EqualTo(Angle.Pi));
		Assert.That(Point3.UnitX.AngleWith(-Point3.UnitY), Is.EqualTo(Angle.PiO2));

		for (Angle angle = Angle.Zero; angle < Angle.D180; angle += Angle.Deg(10)) {
			var sc = angle.SinCos;
			Vec3 vec = new(sc.Cos, sc.Sin, 0);
			Assert.True(Vec3.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
			vec = new(sc.Cos, 0, sc.Sin);
			Assert.True(Vec3.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
			vec = new(sc.Cos, -sc.Sin, 0);
			Assert.True(Vec3.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
		}
	}

	[Test]
	public void Point3_Clamp()
	{
		Point3 i1 = new(-5, 4, 6);
		Assert.That(Point3.Clamp(i1, new(-1, -2, 0),  new(1, 2, 0)),  Is.EqualTo(new Point3(-1, 2, 0))); // all change
		Assert.That(Point3.Clamp(i1, new(-8, -8, -8), new(8, 8, 8)),  Is.EqualTo(new Point3(-5, 4, 6))); // none change
		Assert.That(Point3.Clamp(i1, new(-4, -8, -8), new(8, 8, 8)),  Is.EqualTo(new Point3(-4, 4, 6))); // x min
		Assert.That(Point3.Clamp(i1, new(-8, -8, -8), new(-7, 8, 8)), Is.EqualTo(new Point3(-7, 4, 6))); // x max
		Assert.That(Point3.Clamp(i1, new(-8, 5, -8),  new(8, 8, 8)),  Is.EqualTo(new Point3(-5, 5, 6))); // y min
		Assert.That(Point3.Clamp(i1, new(-8, -8, -8), new(8, 0, 8)),  Is.EqualTo(new Point3(-5, 0, 6))); // y max
		Assert.That(Point3.Clamp(i1, new(-8, -8, 7),  new(8, 8, 8)),  Is.EqualTo(new Point3(-5, 4, 7))); // z min
		Assert.That(Point3.Clamp(i1, new(-8, -8, -8), new(8, 8, 4)),  Is.EqualTo(new Point3(-5, 4, 4))); // z max
	}

	[Test]
	public void Point3_MinMax()
	{
		Point3 i1 = new(-5, 4, 6), i2 = new(3, -2, -1);
		Assert.That(Point3.Min(i1, i2), Is.EqualTo(new Point3(-5, -2, -1)));
		Assert.That(Point3.Max(i1, i2), Is.EqualTo(new Point3(3, 4, 6)));
	}
}
