﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Point4  (for identical generated functionality, just test Point4, Point4U, or Point4L)
internal class Point4Tests
{
	private static readonly Point4[] _Points = {
		new(-5, -5, -5, -5), new(-3, -1, 0, 1), new(0, 0, 0, 0), new(2, 2, 2, 2), new(5, 5, 5, 5), new(-1, 1, -1, 1)
	};

	[Test]
	public unsafe void Point4_StructLayout()
	{
		Assert.That(sizeof(Point4), Is.EqualTo(16));
		Assert.That(sizeof(Point4U), Is.EqualTo(16));
		Assert.That(sizeof(Point4L), Is.EqualTo(32));
		Assert.That(sizeof(Point4UL), Is.EqualTo(32));

		Assert.That(Marshal.OffsetOf<Point4>("X").ToInt32(),   Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point4>("Y").ToInt32(),   Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Point4>("Z").ToInt32(),   Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point4>("W").ToInt32(),   Is.EqualTo(12));
		Assert.That(Marshal.OffsetOf<Point4U>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point4U>("Y").ToInt32(),  Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Point4U>("Z").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point4U>("W").ToInt32(),  Is.EqualTo(12));
		Assert.That(Marshal.OffsetOf<Point4L>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point4L>("Y").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point4L>("Z").ToInt32(),  Is.EqualTo(16));
		Assert.That(Marshal.OffsetOf<Point4L>("W").ToInt32(),  Is.EqualTo(24));
		Assert.That(Marshal.OffsetOf<Point4UL>("X").ToInt32(), Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Point4UL>("Y").ToInt32(), Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Point4UL>("Z").ToInt32(), Is.EqualTo(16));
		Assert.That(Marshal.OffsetOf<Point4UL>("W").ToInt32(), Is.EqualTo(24));
	}

	[Test]
	public void Point4_Constants()
	{
		Assert.That((Point4.Zero.X, Point4.Zero.Y, Point4.Zero.Z, Point4.Zero.W), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((Point4.UnitX.X, Point4.UnitX.Y, Point4.UnitX.Z, Point4.UnitX.W), Is.EqualTo((1, 0, 0, 0)));
		Assert.That((Point4.UnitY.X, Point4.UnitY.Y, Point4.UnitY.Z, Point4.UnitY.W), Is.EqualTo((0, 1, 0, 0)));
		Assert.That((Point4.UnitZ.X, Point4.UnitZ.Y, Point4.UnitZ.Z, Point4.UnitZ.W), Is.EqualTo((0, 0, 1, 0)));
		Assert.That((Point4.UnitW.X, Point4.UnitW.Y, Point4.UnitW.Z, Point4.UnitW.W), Is.EqualTo((0, 0, 0, 1)));
	}

	[Test]
	public void Point4_Constructors()
	{
		Point4 p2;
		p2 = new();                              Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((0, 0, 0, 0)));
		p2 = new(-1);                            Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((-1, -1, -1, -1)));
		p2 = new(2, 5, 7, 9);                    Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((2, 5, 7, 9)));
		p2 = new(new Point2(-2, -3), new(6, 8)); Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((-2, -3, 6, 8)));
		p2 = new(new(-5, -6, -7), 4);            Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((-5, -6, -7, 4)));

		Point4U p2u;
		p2u = new();                             Assert.That((p2u.X, p2u.Y, p2u.Z, p2u.W), Is.EqualTo((0, 0, 0, 0)));
		p2u = new(1);                            Assert.That((p2u.X, p2u.Y, p2u.Z, p2u.W), Is.EqualTo((1, 1, 1, 1)));
		p2u = new(2, 5, 7, 9);                   Assert.That((p2u.X, p2u.Y, p2u.Z, p2u.W), Is.EqualTo((2, 5, 7, 9)));
		p2u = new(new Point2U(2, 3), new(6, 8)); Assert.That((p2u.X, p2u.Y, p2u.Z, p2u.W), Is.EqualTo((2, 3, 6, 8)));
		p2u = new(new(5, 6, 7), 4);              Assert.That((p2u.X, p2u.Y, p2u.Z, p2u.W), Is.EqualTo((5, 6, 7, 4)));
	}

	[Test]
	public void Point4_Length()
	{
		const float TOLERANCE = 1e-6f;

		Assert.That(Point4.Zero.Length,    Is.EqualTo(0));
		Assert.That(Point4.Zero.LengthSq,  Is.EqualTo(0));
		Assert.That(Point4.UnitX.Length,   Is.EqualTo(1));
		Assert.That(Point4.UnitX.LengthSq, Is.EqualTo(1));
		Assert.That(Point4.UnitY.Length,   Is.EqualTo(1));
		Assert.That(Point4.UnitY.LengthSq, Is.EqualTo(1));
		Assert.That(Point4.UnitZ.Length,   Is.EqualTo(1));
		Assert.That(Point4.UnitZ.LengthSq, Is.EqualTo(1));
		Assert.That(Point4.UnitW.Length,   Is.EqualTo(1));
		Assert.That(Point4.UnitW.LengthSq, Is.EqualTo(1));
		Assert.That(new Point4(3, 4, 1, 2).Length,      Is.EqualTo(Math.Sqrt(30)).Within(TOLERANCE));
		Assert.That(new Point4(3, 4, 1, 2).LengthSq,    Is.EqualTo(30));
		Assert.That(new Point4(-3, -4, -1, 2).Length,   Is.EqualTo(Math.Sqrt(30)).Within(TOLERANCE));
		Assert.That(new Point4(-3, -4, -1, 2).LengthSq, Is.EqualTo(30));
	}

	[Test]
	public void Point4_Deconstruct()
	{
		Assert.That((_, _, _, _) = new Point4(2, 3, 4, 5), Is.EqualTo((2, 3, 4, 5)));
	}

	[Test]
	public void Point4_Equality()
	{
		for (int pi = 0; pi < _Points.Length - 1; ++pi) {
			var l = _Points[pi];
			var r = _Points[pi + 1];

			Assert.True(l == l);
			Assert.True(l != r);
			Assert.True(l.Equals(l));
			Assert.True(l.Equals((object)l));
			Assert.False(l.Equals(r));
			Assert.False(l.Equals((object)r));

			Bool4
				lt  = new(l.X <  r.X, l.Y <  r.Y, l.Z <  r.Z, l.W <  r.W),
				lte = new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W),
				gt  = new(l.X >  r.X, l.Y >  r.Y, l.Z >  r.Z, l.W >  r.W),
				gte = new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
			Assert.That(l < r,  Is.EqualTo(lt));
			Assert.That(l <= r, Is.EqualTo(lte));
			Assert.That(l > r,  Is.EqualTo(gt));
			Assert.That(l >= r, Is.EqualTo(gte));
		}
	}

	[Test]
	public void Point4_ArithmeticOperators()
	{
		Point4 i1 = new(0, 5, 3, 4), i2 = new(-1, -3, 2, -5);
		Assert.That(i1 + i2, Is.EqualTo(new Point4(-1, 2, 5, -1)));
		Assert.That(i1 - i2, Is.EqualTo(new Point4(1, 8, 1, 9)));
		Assert.That(i1 * i2, Is.EqualTo(new Point4(0, -15, 6, -20)));
		Assert.That(i1 * 2,  Is.EqualTo(new Point4(0, 10, 6, 8)));
		Assert.That(-i2,     Is.EqualTo(new Point4(1, 3, -2, 5)));
	}

	[Test]
	public void Point4_Vec4Casting()
	{
		Assert.That((Point4)new Vec4(-5.5f, 2.5f, 10.99f, -Single.Epsilon), Is.EqualTo(new Point4(-5, 2, 10, 0)));
		Assert.That((Point4)new Vec4H((Half)(-5.5f), (Half)2.5f, (Half)10.99f, -Half.Epsilon), Is.EqualTo(new Point4(-5, 2, 10, 0)));
		Assert.That((Point4)new Vec4D(-5.5, 2.5, 10.99, -Double.Epsilon), Is.EqualTo(new Point4(-5, 2, 10, 0)));
	}

	[Test]
	public void Point4_Dot()
	{
		for (int pi = 0; pi < _Points.Length - 1; ++pi) {
			var l = _Points[pi];
			var r = _Points[pi + 1];

			Assert.That(l.Dot(l), Is.EqualTo(l.LengthSq));
			var exp = l.X * r.X + l.Y * r.Y + l.Z * r.Z + l.W * r.W;
			Assert.That(l.Dot(r), Is.EqualTo(exp));
		}
	}

	[Test]
	public void Point4_Clamp()
	{
		Point4 i1 = new(-5, 4, 6, -7);
		Assert.That(Point4.Clamp(i1, new(-1, -2, 0, -2),  new(1, 2, 0, -1)),  Is.EqualTo(new Point4(-1, 2, 0, -2))); // all change
		Assert.That(Point4.Clamp(i1, new(-8, -8, -8, -8), new(8, 8, 8, 8)),  Is.EqualTo(new Point4(-5, 4, 6, -7))); // none change
		Assert.That(Point4.Clamp(i1, new(-4, -8, -8, -8), new(8, 8, 8, 8)),  Is.EqualTo(new Point4(-4, 4, 6, -7))); // x min
		Assert.That(Point4.Clamp(i1, new(-8, -8, -8, -8), new(-7, 8, 8, 8)), Is.EqualTo(new Point4(-7, 4, 6, -7))); // x max
		Assert.That(Point4.Clamp(i1, new(-8, 5, -8, -8),  new(8, 8, 8, 8)),  Is.EqualTo(new Point4(-5, 5, 6, -7))); // y min
		Assert.That(Point4.Clamp(i1, new(-8, -8, -8, -8), new(8, 0, 8, 8)),  Is.EqualTo(new Point4(-5, 0, 6, -7))); // y max
		Assert.That(Point4.Clamp(i1, new(-8, -8, 7, -8),  new(8, 8, 8, 8)),  Is.EqualTo(new Point4(-5, 4, 7, -7))); // z min
		Assert.That(Point4.Clamp(i1, new(-8, -8, -8, -8), new(8, 8, 4, 8)),  Is.EqualTo(new Point4(-5, 4, 4, -7))); // z max
		Assert.That(Point4.Clamp(i1, new(-8, -8, -8, -4), new(8, 8, 8, 0)),  Is.EqualTo(new Point4(-5, 4, 6, -4))); // w min
		Assert.That(Point4.Clamp(i1, new(-8, -8, -8, -9), new(8, 8, 8, -8)), Is.EqualTo(new Point4(-5, 4, 6, -8))); // w max
	}

	[Test]
	public void Point4_MinMax()
	{
		Point4 i1 = new(-5, 4, 6, -7), i2 = new(3, -2, -1, 0);
		Assert.That(Point4.Min(i1, i2), Is.EqualTo(new Point4(-5, -2, -1, -7)));
		Assert.That(Point4.Max(i1, i2), Is.EqualTo(new Point4(3, 4, 6, 0)));
	}
}
