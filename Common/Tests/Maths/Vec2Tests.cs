﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Vec2
internal class Vec2Tests
{
	private static readonly Vec2[] _Vecs = {
		new(-5, -5), new(-3, -1), new(0, 0), new(2, 2), new(5, 5), new(-1, 1)
	};

	[Test]
	public unsafe void Vec2_StructLayout()
	{
		Assert.That(sizeof(Vec2), Is.EqualTo(8));
		Assert.That(sizeof(Vec2H), Is.EqualTo(4));
		Assert.That(sizeof(Vec2D), Is.EqualTo(16));

		Assert.That(Marshal.OffsetOf<Vec2>("X").ToInt32(),   Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec2>("Y").ToInt32(),   Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Vec2H>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec2H>("Y").ToInt32(),  Is.EqualTo(2));
		Assert.That(Marshal.OffsetOf<Vec2D>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec2D>("Y").ToInt32(),  Is.EqualTo(8));
	}

	[Test]
	public void Vec2_Constants()
	{
		Assert.That((Vec2.Zero.X, Vec2.Zero.Y), Is.EqualTo((0, 0)));
		Assert.That((Vec2.UnitX.X, Vec2.UnitX.Y), Is.EqualTo((1, 0)));
		Assert.That((Vec2.UnitY.X, Vec2.UnitY.Y), Is.EqualTo((0, 1)));
	}

	[Test]
	public void Vec2_Constructors()
	{
		Vec2 p2;
		p2 = new();           Assert.That((p2.X, p2.Y), Is.EqualTo((0, 0)));
		p2 = new(-1);         Assert.That((p2.X, p2.Y), Is.EqualTo((-1, -1)));
		p2 = new(2.5f, 5.5f); Assert.That((p2.X, p2.Y), Is.EqualTo((2.5f, 5.5f)));
	}

	[Test]
	public void Vec2_Length()
	{
		Assert.That(Vec2.Zero.Length,          Is.EqualTo(0));
		Assert.That(Vec2.Zero.LengthSq,        Is.EqualTo(0));
		Assert.That(Vec2.UnitX.Length,         Is.EqualTo(1));
		Assert.That(Vec2.UnitX.LengthSq,       Is.EqualTo(1));
		Assert.That(Vec2.UnitY.Length,         Is.EqualTo(1));
		Assert.That(Vec2.UnitY.LengthSq,       Is.EqualTo(1));
		Assert.That(new Vec2(3, 4).Length,     Is.EqualTo(5));
		Assert.That(new Vec2(3, 4).LengthSq,   Is.EqualTo(25));
		Assert.That(new Vec2(-3, -4).Length,   Is.EqualTo(5));
		Assert.That(new Vec2(-3, -4).LengthSq, Is.EqualTo(25));
	}

	[Test]
	public void Vec2_Deconstruct()
	{
		Assert.That((_, _) = new Vec2(2, 3), Is.EqualTo((2, 3)));
	}

	[Test]
	public void Vec2_Equality()
	{
		for (int pi = 0; pi < _Vecs.Length - 1; ++pi) {
			var l = _Vecs[pi];
			var r = _Vecs[pi + 1];

			Assert.True(l == l);
			Assert.True(l != r);
			Assert.True(l.Equals(l));
			Assert.True(l.Equals((object)l));
			Assert.False(l.Equals(r));
			Assert.False(l.Equals((object)r));

			Bool2
				lt  = new(l.X <  r.X, l.Y <  r.Y),
				lte = new(l.X <= r.X, l.Y <= r.Y),
				gt  = new(l.X >  r.X, l.Y >  r.Y),
				gte = new(l.X >= r.X, l.Y >= r.Y);
			Assert.That(l < r,  Is.EqualTo(lt));
			Assert.That(l <= r, Is.EqualTo(lte));
			Assert.That(l > r,  Is.EqualTo(gt));
			Assert.That(l >= r, Is.EqualTo(gte));
		}
	}

	[Test]
	public void Vec2_ArithmeticOperators()
	{
		Vec2 i1 = new(0, 5), i2 = new(-1, -3);
		Assert.That(i1 + i2, Is.EqualTo(new Vec2(-1, 2)));
		Assert.That(i1 - i2, Is.EqualTo(new Vec2(1, 8)));
		Assert.That(i1 * i2, Is.EqualTo(new Vec2(0, -15)));
		Assert.That(i1 * 2,  Is.EqualTo(new Vec2(0, 10)));
		Assert.That(-i2,     Is.EqualTo(new Vec2(1, 3)));
	}

	[Test]
	public void Vec2_Point2Casting()
	{
		Assert.That((Vec2)new Point2(-1, 2), Is.EqualTo(new Vec2(-1, 2)));
		Assert.That((Vec2)new Point2L(-1, 2), Is.EqualTo(new Vec2(-1, 2)));
		Assert.That((Vec2)new Point2U(3, 4), Is.EqualTo(new Vec2(3, 4)));
		Assert.That((Vec2)new Point2UL(3, 4), Is.EqualTo(new Vec2(3, 4)));
	}

	[Test]
	public void Vec2_Dot()
	{
		for (int pi = 0; pi < _Vecs.Length - 1; ++pi) {
			var l = _Vecs[pi];
			var r = _Vecs[pi + 1];

			Assert.That(l.Dot(l), Is.EqualTo(l.LengthSq));
			var exp = l.X * r.X + l.Y * r.Y;
			Assert.That(l.Dot(r), Is.EqualTo(exp));
		}
	}

	[Test]
	public void Vec2_AngleWith()
	{
		const float TOLERANCE = 1e-5f;

		Assert.That(Vec2.UnitX.AngleWith(Vec2.UnitX),  Is.EqualTo(Angle.Zero));
		Assert.That(Vec2.UnitX.AngleWith(new(1, 1)),     Is.EqualTo(Angle.PiO4));
		Assert.That(Vec2.UnitX.AngleWith(Vec2.UnitY),  Is.EqualTo(Angle.PiO2));
		Assert.That(Vec2.UnitY.AngleWith(Vec2.UnitX),  Is.EqualTo(Angle.PiO2));
		Assert.That(Vec2.UnitX.AngleWith(new(-1, 1)),    Is.EqualTo(Angle.PiO2 + Angle.PiO4));
		Assert.That(Vec2.UnitX.AngleWith(-Vec2.UnitX), Is.EqualTo(Angle.Pi));
		Assert.That(Vec2.UnitX.AngleWith(-Vec2.UnitY), Is.EqualTo(Angle.PiO2));

		for (Angle angle = Angle.Zero; angle < Angle.D180; angle += Angle.Deg(10)) {
			var sc = angle.SinCos;
			Vec2 vec = new(sc.Cos, sc.Sin);
			Assert.True(Vec2.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
			vec = new(sc.Cos, -sc.Sin);
			Assert.True(Vec2.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
		}
	}

	[Test]
	public void Vec2_Project()
	{
		Assert.That(Vec2.UnitX.Project(Vec2.UnitX),      Is.EqualTo(Vec2.UnitX));
		Assert.That(Vec2.UnitX.Project(Vec2.UnitY),      Is.EqualTo(Vec2.Zero));
		Assert.That(new Vec2(1, 1).Project(Vec2.UnitX),  Is.EqualTo(Vec2.UnitX));
		Assert.That(new Vec2(-1, 1).Project(Vec2.UnitX), Is.EqualTo(-Vec2.UnitX));
		Assert.That(new Vec2(1, 1).Project(Vec2.UnitY),  Is.EqualTo(Vec2.UnitY));
		Assert.That(new Vec2(5, 1).Project(Vec2.UnitX),  Is.EqualTo(Vec2.UnitX * 5));
		Assert.That(new Vec2(1, 5).Project(Vec2.UnitY),  Is.EqualTo(Vec2.UnitY * 5));
	}

	[Test]
	public void Vec2_Reflect()
	{
		const float TOLERANCE = 1e-6f;

		var xy = new Vec2(1).Normalized;
		var xnY = new Vec2(1, -1).Normalized;

		Assert.True((-Vec2.UnitX).Reflect(xy).ApproxEqual(Vec2.UnitY, TOLERANCE));
		Assert.True((-Vec2.UnitX).Reflect(xnY).ApproxEqual(-Vec2.UnitY, TOLERANCE));
		Assert.True(new Vec2(-2, -1).Reflect(xy).ApproxEqual(new(1, 2), TOLERANCE));
		Assert.True(new Vec2(-2, -1).Reflect(xnY).ApproxEqual(new(-1, -2), TOLERANCE));
	}

	[Test]
	public void Vec2_Clamp()
	{
		Vec2 vec = new(-5, 4);
		Assert.That(Vec2.Clamp(vec, new(-1, -2), new(1, 2)),  Is.EqualTo(new Vec2(-1, 2))); // all change
		Assert.That(Vec2.Clamp(vec, new(-8, -8), new(8, 8)),  Is.EqualTo(new Vec2(-5, 4))); // none change
		Assert.That(Vec2.Clamp(vec, new(-4, -8), new(8, 8)),  Is.EqualTo(new Vec2(-4, 4))); // x min
		Assert.That(Vec2.Clamp(vec, new(-8, -8), new(-7, 8)), Is.EqualTo(new Vec2(-7, 4))); // x max
		Assert.That(Vec2.Clamp(vec, new(-8, 5),  new(8, 8)),  Is.EqualTo(new Vec2(-5, 5))); // y min
		Assert.That(Vec2.Clamp(vec, new(-8, -8), new(8, 0)),  Is.EqualTo(new Vec2(-5, 0))); // y max
	}

	[Test]
	public void Vec2_MinMax()
	{
		Vec2 v1 = new(-5, 4), v2 = new(3, -2);
		Assert.That(Vec2.Min(v1, v2), Is.EqualTo(new Vec2(-5, -2)));
		Assert.That(Vec2.Max(v1, v2), Is.EqualTo(new Vec2(3, 4)));
	}

	[Test]
	public void Vec2_ApproxEqual()
	{
		Vec2 vec = new(-3, 4);
		Assert.True(vec.ApproxEqual(vec));
		Assert.False(vec.ApproxEqual(vec + new Vec2(0.1f, -0.1f)));
		Assert.True(vec.ApproxEqual(vec + new Vec2(0.1f, -0.1f), 0.15f));
	}

	[Test]
	public void Vec2_ApproxZero()
	{
		Assert.True(Vec2.Zero.ApproxZero());
		Assert.False(new Vec2(1f).ApproxZero());
		Assert.True(new Vec2(1f).ApproxZero(1f));
	}
}
