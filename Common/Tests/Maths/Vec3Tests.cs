﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Vec3
internal class Vec3Tests
{
	private static readonly Vec3[] _Vecs = {
		new(-5, -5, -5), new(-3, -1, 0), new(0, 0, 0), new(2, 2, 2), new(5, 5, 5), new(-1, 1, -1)
	};

	[Test]
	public unsafe void Vec3_StructLayout()
	{
		Assert.That(sizeof(Vec3), Is.EqualTo(12));
		Assert.That(sizeof(Vec3H), Is.EqualTo(6));
		Assert.That(sizeof(Vec3D), Is.EqualTo(24));

		Assert.That(Marshal.OffsetOf<Vec3>("X").ToInt32(),   Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec3>("Y").ToInt32(),   Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Vec3>("Z").ToInt32(),   Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Vec3H>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec3H>("Y").ToInt32(),  Is.EqualTo(2));
		Assert.That(Marshal.OffsetOf<Vec3H>("Z").ToInt32(),  Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Vec3D>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec3D>("Y").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Vec3D>("Z").ToInt32(),  Is.EqualTo(16));
	}

	[Test]
	public void Vec3_Constants()
	{
		Assert.That((Vec3.Zero.X, Vec3.Zero.Y, Vec3.Zero.Z), Is.EqualTo((0, 0, 0)));
		Assert.That((Vec3.UnitX.X, Vec3.UnitX.Y, Vec3.UnitX.Z), Is.EqualTo((1, 0, 0)));
		Assert.That((Vec3.UnitY.X, Vec3.UnitY.Y, Vec3.UnitY.Z), Is.EqualTo((0, 1, 0)));
		Assert.That((Vec3.UnitZ.X, Vec3.UnitZ.Y, Vec3.UnitZ.Z), Is.EqualTo((0, 0, 1)));
		Assert.That((Vec3.Forward.X, Vec3.Forward.Y, Vec3.Forward.Z), Is.EqualTo((0, 0, -1)));
		Assert.That((Vec3.Backward.X, Vec3.Backward.Y, Vec3.Backward.Z), Is.EqualTo((0, 0, 1)));
		Assert.That((Vec3.Up.X, Vec3.Up.Y, Vec3.Up.Z), Is.EqualTo((0, 1, 0)));
		Assert.That((Vec3.Down.X, Vec3.Down.Y, Vec3.Down.Z), Is.EqualTo((0, -1, 0)));
		Assert.That((Vec3.Left.X, Vec3.Left.Y, Vec3.Left.Z), Is.EqualTo((-1, 0, 0)));
		Assert.That((Vec3.Right.X, Vec3.Right.Y, Vec3.Right.Z), Is.EqualTo((1, 0, 0)));
	}

	[Test]
	public void Vec3_Constructors()
	{
		Vec3 p2;
		p2 = new();                 Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((0, 0, 0)));
		p2 = new(-1);               Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((-1, -1, -1)));
		p2 = new(2.5f, 5.5f, 9.5f); Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((2.5f, 5.5f, 9.5f)));
		p2 = new(new(-1, -2), 9);   Assert.That((p2.X, p2.Y, p2.Z), Is.EqualTo((-1, -2, 9)));
	}

	[Test]
	public void Vec3_Length()
	{
		const float TOLERANCE = 1e-6f;

		Assert.That(Vec3.Zero.Length,    Is.EqualTo(0));
		Assert.That(Vec3.Zero.LengthSq,  Is.EqualTo(0));
		Assert.That(Vec3.UnitX.Length,   Is.EqualTo(1));
		Assert.That(Vec3.UnitX.LengthSq, Is.EqualTo(1));
		Assert.That(Vec3.UnitY.Length,   Is.EqualTo(1));
		Assert.That(Vec3.UnitY.LengthSq, Is.EqualTo(1));
		Assert.That(Vec3.UnitZ.Length,   Is.EqualTo(1));
		Assert.That(Vec3.UnitZ.LengthSq, Is.EqualTo(1));
		Assert.That(new Vec3(3, 4, 1).Length,      Is.EqualTo(Math.Sqrt(26)).Within(TOLERANCE));
		Assert.That(new Vec3(3, 4, 1).LengthSq,    Is.EqualTo(26));
		Assert.That(new Vec3(-3, -4, -1).Length,   Is.EqualTo(Math.Sqrt(26)).Within(TOLERANCE));
		Assert.That(new Vec3(-3, -4, -1).LengthSq, Is.EqualTo(26));
	}

	[Test]
	public void Vec3_Deconstruct()
	{
		Assert.That((_, _, _) = new Vec3(2, 3, 4), Is.EqualTo((2, 3, 4)));
	}

	[Test]
	public void Vec3_Equality()
	{
		for (int pi = 0; pi < _Vecs.Length - 1; ++pi) {
			var l = _Vecs[pi];
			var r = _Vecs[pi + 1];

			Assert.True(l == l);
			Assert.True(l != r);
			Assert.True(l.Equals(l));
			Assert.True(l.Equals((object)l));
			Assert.False(l.Equals(r));
			Assert.False(l.Equals((object)r));

			Bool3
				lt  = new(l.X <  r.X, l.Y <  r.Y, l.Z <  r.Z),
				lte = new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z),
				gt  = new(l.X >  r.X, l.Y >  r.Y, l.Z >  r.Z),
				gte = new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
			Assert.That(l < r,  Is.EqualTo(lt));
			Assert.That(l <= r, Is.EqualTo(lte));
			Assert.That(l > r,  Is.EqualTo(gt));
			Assert.That(l >= r, Is.EqualTo(gte));
		}
	}

	[Test]
	public void Vec3_ArithmeticOperators()
	{
		Vec3 i1 = new(0, 5, 3), i2 = new(-1, -3, 2);
		Assert.That(i1 + i2, Is.EqualTo(new Vec3(-1, 2, 5)));
		Assert.That(i1 - i2, Is.EqualTo(new Vec3(1, 8, 1)));
		Assert.That(i1 * i2, Is.EqualTo(new Vec3(0, -15, 6)));
		Assert.That(i1 * 2,  Is.EqualTo(new Vec3(0, 10, 6)));
		Assert.That(-i2,     Is.EqualTo(new Vec3(1, 3, -2)));
	}

	[Test]
	public void Vec3_Point3Casting()
	{
		Assert.That((Vec3)new Point3(-1, 2, 5), Is.EqualTo(new Vec3(-1, 2, 5)));
		Assert.That((Vec3)new Point3L(-1, 2, 5), Is.EqualTo(new Vec3(-1, 2, 5)));
		Assert.That((Vec3)new Point3U(3, 4, 6), Is.EqualTo(new Vec3(3, 4, 6)));
		Assert.That((Vec3)new Point3UL(3, 4, 6), Is.EqualTo(new Vec3(3, 4, 6)));
	}

	[Test]
	public void Vec3_Dot()
	{
		for (int pi = 0; pi < _Vecs.Length - 1; ++pi) {
			var l = _Vecs[pi];
			var r = _Vecs[pi + 1];

			Assert.That(l.Dot(l), Is.EqualTo(l.LengthSq));
			var exp = l.X * r.X + l.Y * r.Y + l.Z * r.Z;
			Assert.That(l.Dot(r), Is.EqualTo(exp));
		}
	}
	
	[Test]
	public void Vec3_Cross()
	{
		Assert.That(Vec3.UnitX.Cross(Vec3.UnitY),  Is.EqualTo(Vec3.UnitZ));
		Assert.That(Vec3.UnitY.Cross(Vec3.UnitZ),  Is.EqualTo(Vec3.UnitX));
		Assert.That(Vec3.UnitZ.Cross(Vec3.UnitX),  Is.EqualTo(Vec3.UnitY));
		Assert.That(Vec3.UnitX.Cross(-Vec3.UnitY), Is.EqualTo(-Vec3.UnitZ));
		Assert.That(Vec3.UnitY.Cross(-Vec3.UnitZ), Is.EqualTo(-Vec3.UnitX));
		Assert.That(Vec3.UnitZ.Cross(-Vec3.UnitX), Is.EqualTo(-Vec3.UnitY));
	}

	[Test]
	public void Vec3_AngleWith()
	{
		const float TOLERANCE = 1e-5f;

		Assert.That(Vec3.UnitX.AngleWith(Vec3.UnitX),  Is.EqualTo(Angle.Zero));
		Assert.That(Vec3.UnitX.AngleWith(new(1, 1, 0)),  Is.EqualTo(Angle.PiO4));
		Assert.That(Vec3.UnitX.AngleWith(new(1, 0, 1)),  Is.EqualTo(Angle.PiO4));
		Assert.That(Vec3.UnitX.AngleWith(Vec3.UnitY),  Is.EqualTo(Angle.PiO2));
		Assert.That(Vec3.UnitY.AngleWith(Vec3.UnitX),  Is.EqualTo(Angle.PiO2));
		Assert.That(Vec3.UnitX.AngleWith(new(-1, 1, 0)), Is.EqualTo(Angle.PiO2 + Angle.PiO4));
		Assert.That(Vec3.UnitX.AngleWith(-Vec3.UnitX), Is.EqualTo(Angle.Pi));
		Assert.That(Vec3.UnitX.AngleWith(-Vec3.UnitY), Is.EqualTo(Angle.PiO2));

		for (Angle angle = Angle.Zero; angle < Angle.D180; angle += Angle.Deg(10)) {
			var sc = angle.SinCos;
			Vec3 vec = new(sc.Cos, sc.Sin, 0);
			Assert.True(Vec3.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
			vec = new(sc.Cos, 0, sc.Sin);
			Assert.True(Vec3.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
			vec = new(sc.Cos, -sc.Sin, 0);
			Assert.True(Vec3.UnitX.AngleWith(vec).ApproxEqual(angle, TOLERANCE));
		}
	}

	[Test]
	public void Vec3_Project()
	{
		Assert.That(Vec3.UnitX.Project(Vec3.UnitX),           Is.EqualTo(Vec3.UnitX));
		Assert.That(Vec3.UnitX.Project(Vec3.UnitY),           Is.EqualTo(Vec3.Zero));
		Assert.That(Vec3.UnitX.Project(Vec3.UnitZ),           Is.EqualTo(Vec3.Zero));
		Assert.That(new Vec3(1, 1, 1).Project(Vec3.UnitX),    Is.EqualTo(Vec3.UnitX));
		Assert.That(new Vec3(1, 1, 1).Project(Vec3.UnitY),    Is.EqualTo(Vec3.UnitY));
		Assert.That(new Vec3(1, 1, 1).Project(Vec3.UnitZ),    Is.EqualTo(Vec3.UnitZ));
		Assert.That(new Vec3(-1, -1, -1).Project(Vec3.UnitX), Is.EqualTo(-Vec3.UnitX));
		Assert.That(new Vec3(-1, -1, -1).Project(Vec3.UnitY), Is.EqualTo(-Vec3.UnitY));
		Assert.That(new Vec3(-1, -1, -1).Project(Vec3.UnitZ), Is.EqualTo(-Vec3.UnitZ));
		Assert.That(new Vec3(5, 1, 1).Project(Vec3.UnitX),    Is.EqualTo(Vec3.UnitX * 5));
		Assert.That(new Vec3(1, 5, 1).Project(Vec3.UnitY),    Is.EqualTo(Vec3.UnitY * 5));
		Assert.That(new Vec3(1, 1, 5).Project(Vec3.UnitZ),    Is.EqualTo(Vec3.UnitZ * 5));
	}

	[Test]
	public void Vec3_Reflect()
	{
		const float TOLERANCE = 1e-6f;

		Vec3
			xy = new Vec3(1, 1, 0).Normalized,
			yz = new Vec3(0, 1, 1).Normalized,
			xz = new Vec3(1, 0, 1).Normalized;

		Assert.True((-Vec3.UnitX).Reflect(xy).ApproxEqual(Vec3.UnitY, TOLERANCE));
		Assert.True((-Vec3.UnitX).Reflect(xz).ApproxEqual(Vec3.UnitZ, TOLERANCE));
		Assert.True((-Vec3.UnitY).Reflect(xy).ApproxEqual(Vec3.UnitX, TOLERANCE));
		Assert.True((-Vec3.UnitY).Reflect(yz).ApproxEqual(Vec3.UnitZ, TOLERANCE));
		Assert.True((-Vec3.UnitZ).Reflect(xz).ApproxEqual(Vec3.UnitX, TOLERANCE));
		Assert.True((-Vec3.UnitZ).Reflect(yz).ApproxEqual(Vec3.UnitY, TOLERANCE));
	}

	[Test]
	public void Vec3_Clamp()
	{
		Vec3 i1 = new(-5, 4, 6);
		Assert.That(Vec3.Clamp(i1, new(-1, -2, 0),  new(1, 2, 0)),  Is.EqualTo(new Vec3(-1, 2, 0))); // all change
		Assert.That(Vec3.Clamp(i1, new(-8, -8, -8), new(8, 8, 8)),  Is.EqualTo(new Vec3(-5, 4, 6))); // none change
		Assert.That(Vec3.Clamp(i1, new(-4, -8, -8), new(8, 8, 8)),  Is.EqualTo(new Vec3(-4, 4, 6))); // x min
		Assert.That(Vec3.Clamp(i1, new(-8, -8, -8), new(-7, 8, 8)), Is.EqualTo(new Vec3(-7, 4, 6))); // x max
		Assert.That(Vec3.Clamp(i1, new(-8, 5, -8),  new(8, 8, 8)),  Is.EqualTo(new Vec3(-5, 5, 6))); // y min
		Assert.That(Vec3.Clamp(i1, new(-8, -8, -8), new(8, 0, 8)),  Is.EqualTo(new Vec3(-5, 0, 6))); // y max
		Assert.That(Vec3.Clamp(i1, new(-8, -8, 7),  new(8, 8, 8)),  Is.EqualTo(new Vec3(-5, 4, 7))); // z min
		Assert.That(Vec3.Clamp(i1, new(-8, -8, -8), new(8, 8, 4)),  Is.EqualTo(new Vec3(-5, 4, 4))); // z max
	}

	[Test]
	public void Vec3_MinMax()
	{
		Vec3 i1 = new(-5, 4, 6), i2 = new(3, -2, -1);
		Assert.That(Vec3.Min(i1, i2), Is.EqualTo(new Vec3(-5, -2, -1)));
		Assert.That(Vec3.Max(i1, i2), Is.EqualTo(new Vec3(3, 4, 6)));
	}

	[Test]
	public void Vec3_ApproxEqual()
	{
		Vec3 vec = new(-3, 4, 5);
		Assert.True(vec.ApproxEqual(vec));
		Assert.False(vec.ApproxEqual(vec + new Vec3(0.1f, -0.1f, -0.1f)));
		Assert.True(vec.ApproxEqual(vec + new Vec3(0.1f, -0.1f, -0.1f), 0.15f));
	}

	[Test]
	public void Vec3_ApproxZero()
	{
		Assert.True(Vec3.Zero.ApproxZero());
		Assert.False(new Vec3(1f).ApproxZero());
		Assert.True(new Vec3(1f).ApproxZero(1f));
	}
}
