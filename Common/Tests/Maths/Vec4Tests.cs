﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using Astrum.Maths;

namespace Astrum.Tests.Maths;

#pragma warning disable NUnit2045
#pragma warning disable CS1718


// Tests for Vec4
internal class Vec4Tests
{
	private static readonly Vec4[] _Vecs = {
		new(-5, -5, -5, -5), new(-3, -1, 0, 1), new(0, 0, 0, 0), new(2, 2, 2, 2), new(5, 5, 5, 5), new(-1, 1, -1, 1)
	};

	[Test]
	public unsafe void Vec4_StructLayout()
	{
		Assert.That(sizeof(Vec4), Is.EqualTo(16));
		Assert.That(sizeof(Vec4H), Is.EqualTo(8));
		Assert.That(sizeof(Vec4D), Is.EqualTo(32));

		Assert.That(Marshal.OffsetOf<Vec4>("X").ToInt32(),   Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec4>("Y").ToInt32(),   Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Vec4>("Z").ToInt32(),   Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Vec4>("W").ToInt32(),   Is.EqualTo(12));
		Assert.That(Marshal.OffsetOf<Vec4H>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec4H>("Y").ToInt32(),  Is.EqualTo(2));
		Assert.That(Marshal.OffsetOf<Vec4H>("Z").ToInt32(),  Is.EqualTo(4));
		Assert.That(Marshal.OffsetOf<Vec4H>("W").ToInt32(),  Is.EqualTo(6));
		Assert.That(Marshal.OffsetOf<Vec4D>("X").ToInt32(),  Is.EqualTo(0));
		Assert.That(Marshal.OffsetOf<Vec4D>("Y").ToInt32(),  Is.EqualTo(8));
		Assert.That(Marshal.OffsetOf<Vec4D>("Z").ToInt32(),  Is.EqualTo(16));
		Assert.That(Marshal.OffsetOf<Vec4D>("W").ToInt32(),  Is.EqualTo(24));
	}

	[Test]
	public void Vec4_Constants()
	{
		Assert.That((Vec4.Zero.X, Vec4.Zero.Y, Vec4.Zero.Z, Vec4.Zero.W), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((Vec4.UnitX.X, Vec4.UnitX.Y, Vec4.UnitX.Z, Vec4.UnitX.W), Is.EqualTo((1, 0, 0, 0)));
		Assert.That((Vec4.UnitY.X, Vec4.UnitY.Y, Vec4.UnitY.Z, Vec4.UnitY.W), Is.EqualTo((0, 1, 0, 0)));
		Assert.That((Vec4.UnitZ.X, Vec4.UnitZ.Y, Vec4.UnitZ.Z, Vec4.UnitZ.W), Is.EqualTo((0, 0, 1, 0)));
		Assert.That((Vec4.UnitW.X, Vec4.UnitW.Y, Vec4.UnitW.Z, Vec4.UnitW.W), Is.EqualTo((0, 0, 0, 1)));
	}

	[Test]
	public void Vec4_Constructors()
	{
		Vec4 p2;
		p2 = new();                            Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((0, 0, 0, 0)));
		p2 = new(-1);                          Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((-1, -1, -1, -1)));
		p2 = new(2.5f, 5.5f, 9.5f, 10.5f);     Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((2.5f, 5.5f, 9.5f, 10.5f)));
		p2 = new(new Vec2(-2, -3), new(6, 8)); Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((-2, -3, 6, 8)));
		p2 = new(new(-5, -6, -7), 4);          Assert.That((p2.X, p2.Y, p2.Z, p2.W), Is.EqualTo((-5, -6, -7, 4)));
	}

	[Test]
	public void Vec4_Length()
	{
		const float TOLERANCE = 1e-6f;

		Assert.That(Vec4.Zero.Length,    Is.EqualTo(0));
		Assert.That(Vec4.Zero.LengthSq,  Is.EqualTo(0));
		Assert.That(Vec4.UnitX.Length,   Is.EqualTo(1));
		Assert.That(Vec4.UnitX.LengthSq, Is.EqualTo(1));
		Assert.That(Vec4.UnitY.Length,   Is.EqualTo(1));
		Assert.That(Vec4.UnitY.LengthSq, Is.EqualTo(1));
		Assert.That(Vec4.UnitZ.Length,   Is.EqualTo(1));
		Assert.That(Vec4.UnitZ.LengthSq, Is.EqualTo(1));
		Assert.That(Vec4.UnitW.Length,   Is.EqualTo(1));
		Assert.That(Vec4.UnitW.LengthSq, Is.EqualTo(1));
		Assert.That(new Vec4(3, 4, 1, 2).Length,       Is.EqualTo(Math.Sqrt(30)).Within(TOLERANCE));
		Assert.That(new Vec4(3, 4, 1, 2).LengthSq,     Is.EqualTo(30));
		Assert.That(new Vec4(-3, -4, -1, -2).Length,   Is.EqualTo(Math.Sqrt(30)).Within(TOLERANCE));
		Assert.That(new Vec4(-3, -4, -1, -2).LengthSq, Is.EqualTo(30));
	}

	[Test]
	public void Vec4_Deconstruct()
	{
		Assert.That((_, _, _, _) = new Vec4(2, 3, 4, 5), Is.EqualTo((2, 3, 4, 5)));
	}

	[Test]
	public void Vec4_Equality()
	{
		for (int pi = 0; pi < _Vecs.Length - 1; ++pi) {
			var l = _Vecs[pi];
			var r = _Vecs[pi + 1];

			Assert.True(l == l);
			Assert.True(l != r);
			Assert.True(l.Equals(l));
			Assert.True(l.Equals((object)l));
			Assert.False(l.Equals(r));
			Assert.False(l.Equals((object)r));

			Bool4
				lt  = new(l.X <  r.X, l.Y <  r.Y, l.Z <  r.Z, l.W <  r.W),
				lte = new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W),
				gt  = new(l.X >  r.X, l.Y >  r.Y, l.Z >  r.Z, l.W >  r.W),
				gte = new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
			Assert.That(l < r,  Is.EqualTo(lt));
			Assert.That(l <= r, Is.EqualTo(lte));
			Assert.That(l > r,  Is.EqualTo(gt));
			Assert.That(l >= r, Is.EqualTo(gte));
		}
	}

	[Test]
	public void Vec4_ArithmeticOperators()
	{
		Vec4 i1 = new(0, 5, 3, 4), i2 = new(-1, -3, 2, -5);
		Assert.That(i1 + i2, Is.EqualTo(new Vec4(-1, 2, 5, -1)));
		Assert.That(i1 - i2, Is.EqualTo(new Vec4(1, 8, 1, 9)));
		Assert.That(i1 * i2, Is.EqualTo(new Vec4(0, -15, 6, -20)));
		Assert.That(i1 * 2,  Is.EqualTo(new Vec4(0, 10, 6, 8)));
		Assert.That(-i2,     Is.EqualTo(new Vec4(1, 3, -2, 5)));
	}

	[Test]
	public void Vec4_Point4Casting()
	{
		Assert.That((Vec4)new Point4(-1, 2, 5, -7), Is.EqualTo(new Vec4(-1, 2, 5, -7)));
		Assert.That((Vec4)new Point4L(-1, 2, 5, -7), Is.EqualTo(new Vec4(-1, 2, 5, -7)));
		Assert.That((Vec4)new Point4U(3, 4, 6, 8), Is.EqualTo(new Vec4(3, 4, 6, 8)));
		Assert.That((Vec4)new Point4UL(3, 4, 6, 8), Is.EqualTo(new Vec4(3, 4, 6, 8)));
	}

	[Test]
	public void Vec4_Dot()
	{
		for (int pi = 0; pi < _Vecs.Length - 1; ++pi) {
			var l = _Vecs[pi];
			var r = _Vecs[pi + 1];

			Assert.That(l.Dot(l), Is.EqualTo(l.LengthSq));
			var exp = l.X * r.X + l.Y * r.Y + l.Z * r.Z + l.W * r.W;
			Assert.That(l.Dot(r), Is.EqualTo(exp));
		}
	}
	
	[Test]
	public void Vec4_Clamp()
	{
		Vec4 i1 = new(-5, 4, 6, -7);
		Assert.That(Vec4.Clamp(i1, new(-1, -2, 0, -2),  new(1, 2, 0, -1)),  Is.EqualTo(new Vec4(-1, 2, 0, -2))); // all change
		Assert.That(Vec4.Clamp(i1, new(-8, -8, -8, -8), new(8, 8, 8, 8)),  Is.EqualTo(new Vec4(-5, 4, 6, -7))); // none change
		Assert.That(Vec4.Clamp(i1, new(-4, -8, -8, -8), new(8, 8, 8, 8)),  Is.EqualTo(new Vec4(-4, 4, 6, -7))); // x min
		Assert.That(Vec4.Clamp(i1, new(-8, -8, -8, -8), new(-7, 8, 8, 8)), Is.EqualTo(new Vec4(-7, 4, 6, -7))); // x max
		Assert.That(Vec4.Clamp(i1, new(-8, 5, -8, -8),  new(8, 8, 8, 8)),  Is.EqualTo(new Vec4(-5, 5, 6, -7))); // y min
		Assert.That(Vec4.Clamp(i1, new(-8, -8, -8, -8), new(8, 0, 8, 8)),  Is.EqualTo(new Vec4(-5, 0, 6, -7))); // y max
		Assert.That(Vec4.Clamp(i1, new(-8, -8, 7, -8),  new(8, 8, 8, 8)),  Is.EqualTo(new Vec4(-5, 4, 7, -7))); // z min
		Assert.That(Vec4.Clamp(i1, new(-8, -8, -8, -8), new(8, 8, 4, 8)),  Is.EqualTo(new Vec4(-5, 4, 4, -7))); // z max
		Assert.That(Vec4.Clamp(i1, new(-8, -8, -8, -4), new(8, 8, 8, 0)),  Is.EqualTo(new Vec4(-5, 4, 6, -4))); // w min
		Assert.That(Vec4.Clamp(i1, new(-8, -8, -8, -9), new(8, 8, 8, -8)), Is.EqualTo(new Vec4(-5, 4, 6, -8))); // w max
	}

	[Test]
	public void Vec4_MinMax()
	{
		Vec4 i1 = new(-5, 4, 6, -7), i2 = new(3, -2, -1, 0);
		Assert.That(Vec4.Min(i1, i2), Is.EqualTo(new Vec4(-5, -2, -1, -7)));
		Assert.That(Vec4.Max(i1, i2), Is.EqualTo(new Vec4(3, 4, 6, 0)));
	}

	[Test]
	public void Vec4_ApproxEqual()
	{
		Vec4 vec = new(-3, 4, 5, -6);
		Assert.True(vec.ApproxEqual(vec));
		Assert.False(vec.ApproxEqual(vec + new Vec4(0.1f, -0.1f, -0.1f, 0.1f)));
		Assert.True(vec.ApproxEqual(vec + new Vec4(0.1f, -0.1f, -0.1f, 0.1f), 0.15f));
	}

	[Test]
	public void Vec4_ApproxZero()
	{
		Assert.True(Vec4.Zero.ApproxZero());
		Assert.False(new Vec4(1f).ApproxZero());
		Assert.True(new Vec4(1f).ApproxZero(1f));
	}
}
