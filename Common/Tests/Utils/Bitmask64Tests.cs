﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Tests;


// Tests for Bitmask64
internal class Bitmask64Tests
{
	static readonly ulong[] Val64 = [0, ~0u, 0xDEADBEEFDEADBEEF, 0xF00FF00FF00FF00F, 0x8086BAD08086BAD0, 0];
	
	[Test]
	public void Bitmask64_Constants()
	{
		Assert.That(Bitmask64.NONE.Value, Is.EqualTo(0ul));
		Assert.That(Bitmask64.ALL.Value, Is.EqualTo(~0ul));
	}

	[Test]
	public void Bitmask64_AllAnyNone()
	{
		Assert.False(Bitmask64.NONE.All);
		Assert.False(Bitmask64.NONE.Any);
		Assert.True (Bitmask64.NONE.None);

		Assert.True (Bitmask64.ALL.All);
		Assert.True (Bitmask64.ALL.Any);
		Assert.False(Bitmask64.ALL.None);

		Assert.False(new Bitmask64(1).All);
		Assert.True (new Bitmask64(1).Any);
		Assert.False(new Bitmask64(1).None);
	}

	[Test]
	public void Bitmask64_Count()
	{
		Assert.That(Bitmask64.NONE.SetCount,   Is.EqualTo(0));
		Assert.That(Bitmask64.NONE.ClearCount, Is.EqualTo(64));

		Assert.That(Bitmask64.ALL.SetCount,   Is.EqualTo(64));
		Assert.That(Bitmask64.ALL.ClearCount, Is.EqualTo(0));

		Assert.That(new Bitmask64(0xFFF0_0FFF_FFF0_0FFF).SetCount,   Is.EqualTo(48));
		Assert.That(new Bitmask64(0xFFF0_0FFF_FFF0_0FFF).ClearCount, Is.EqualTo(16));
	}

	[Test]
	public void Bitmask64_FirstLastSet()
	{
		Assert.That(Bitmask64.NONE.FirstSet, Is.EqualTo(null));
		Assert.That(Bitmask64.NONE.LastSet,  Is.EqualTo(null));

		for (int bi = 0; bi < 63; ++bi) {
			Assert.That(new Bitmask64(3ul << bi).FirstSet, Is.EqualTo(bi));
			Assert.That(new Bitmask64(3ul << bi).LastSet,  Is.EqualTo(bi + 1));
		}

		Assert.That(Bitmask64.ALL.FirstSet, Is.EqualTo(0));
		Assert.That(Bitmask64.ALL.LastSet,  Is.EqualTo(63));
	}

	[Test]
	public void Bitmask64_FirstLastClear()
	{
		Assert.That(Bitmask64.NONE.FirstClear, Is.EqualTo(0));
		Assert.That(Bitmask64.NONE.LastClear,  Is.EqualTo(63));

		for (int bi = 0; bi < 63; ++bi) {
			Assert.That(new Bitmask64(~(3ul << bi)).FirstClear, Is.EqualTo(bi));
			Assert.That(new Bitmask64(~(3ul << bi)).LastClear,  Is.EqualTo(bi + 1));
		}

		Assert.That(Bitmask64.ALL.FirstClear, Is.EqualTo(null));
		Assert.That(Bitmask64.ALL.LastClear,  Is.EqualTo(null));
	}

	[Test]
	public void Bitmask64_Indexing()
	{
		Assert.Throws<IndexOutOfRangeException>(() => _ = new Bitmask64()[64]);

		for (uint bi = 0; bi < 64; ++bi) {
			Bitmask64 mask = new(1ul << (int)bi);
			Assert.True(mask[bi]);
			mask[bi] = false;
			Assert.False(mask[bi]);
		}
	}

	[Test]
	public void Bitmask64_ConstructorAndCasting()
	{
		Assert.That(new Bitmask64().Value, Is.EqualTo(0));

		for (int bi = 0; bi < 64; ++bi) {
			Assert.That(new Bitmask64(1ul << bi).Value, Is.EqualTo(1ul << bi));
			Assert.That(new Bitmask64(1L << bi).Value, Is.EqualTo(1uL << bi));
		}
	}

	[Test]
	public void Bitmask64_Equality()
	{
		for (uint li = 0; li < Val64.Length; ++li) {
			for (uint ri = 0; ri < Val64.Length; ++ri) {
				ulong lv = Val64[li], rv = Val64[ri];
				Bitmask64 l = new(lv), r = new(rv);
				Assert.That(l.Equals(r), Is.EqualTo(lv == rv));
				Assert.That(l.Equals((object)r), Is.EqualTo(lv == rv));
				Assert.That(l == r, Is.EqualTo(lv == rv));
				Assert.That(l != r, Is.EqualTo(lv != rv));
			}
		}
	}

	[Test]
	public void Bitmask64_BitwiseOperators()
	{
		for (uint li = 0; li < Val64.Length; ++li) {
			for (uint ri = 0; ri < Val64.Length; ++ri) {
				ulong lv = Val64[li], rv = Val64[ri];
				Bitmask64 l = new(lv), r = new(rv);
				Assert.That((l | r).Value, Is.EqualTo(lv | rv));
				Assert.That((l & r).Value, Is.EqualTo(lv & rv));
				Assert.That((~l).Value, Is.EqualTo(~lv));
			}
		}
	}
}
