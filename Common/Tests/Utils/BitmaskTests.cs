﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Tests;


// Tests for Bitmask
internal class BitmaskTests
{
	// Test a full set of bitset sizes:
	//   42:  non-word aligned, small set optimization
	//   64:  word aligned, small set optimization
	//   100: non-word aligned, normal array impl
	//   128: word aligned, normal array impl
	//   999: large, normal array impl
	private static readonly uint[] _Sizes = [42, 64, 100, 128, 999];

	[Test]
	public void Bitmask_DefaultEmpty()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);
			Assert.That(set.Count, Is.EqualTo(0));
			Assert.True(set.None);
		}
	}

	[Test]
	public void Bitmask_ZeroSize()
	{
		var set = new Bitmask(0);
		Assert.That(set.Size, Is.EqualTo(0));
	}

	[Test]
	public void Bitmask_IndexerSetClear()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);
			
			set[0] = true;
			Assert.True(set[0]);
			Assert.True(set.Get(0));

			set.Set(size / 2);
			Assert.True(set[size / 2]);
			Assert.True(set.Get(size / 2));

			set[0] = false;
			Assert.False(set[0]);
			Assert.False(set.Get(0));

			set.Clear(size / 2);
			Assert.False(set[size / 2]);
			Assert.False(set.Get(size /2));
		}
	}

	[Test]
	public void Bitmask_Count()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);

			set[0] = true;
			Assert.That(set.Count, Is.EqualTo(1));

			set[1] = true;
			Assert.That(set.Count, Is.EqualTo(2));

			set.Set(2);
			Assert.That(set.Count, Is.EqualTo(3));

			set.Clear(2);
			Assert.That(set.Count, Is.EqualTo(2));

			set[0] = false;
			Assert.That(set.Count, Is.EqualTo(1));
		}
	}

	[Test]
	public void Bitmask_DuplicateSetsAndClears()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);

			set[0] = true;
			Assert.That(set.Count, Is.EqualTo(1));

			set[0] = true;
			Assert.That(set.Count, Is.EqualTo(1));
			set.Set(0);
			Assert.That(set.Count, Is.EqualTo(1));

			set.Clear(0);
			Assert.That(set.Count, Is.EqualTo(0));

			set.Clear(0);
			Assert.That(set.Count, Is.EqualTo(0));
			set[0] = false;
			Assert.That(set.Count, Is.EqualTo(0));
		}
	}

	[Test]
	public void Bitmask_AllAnyNone()
	{
		var set = new Bitmask(2);

		Assert.False(set.All);
		Assert.False(set.Any);
		Assert.True (set.None);

		set.Set(0);
		Assert.False(set.All);
		Assert.True (set.Any);
		Assert.False(set.None);

		set.Set(1);
		Assert.True (set.All);
		Assert.True (set.Any);
		Assert.False(set.None);
	}

	[Test]
	public void Bitmask_OutOfRangeIndexer()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);

			Assert.Throws<ArgumentOutOfRangeException>(() => {
				set[size * 2] = true;
			});

			Assert.Throws<ArgumentOutOfRangeException>(() => {
				_ = set[size * 2];
			});

			Assert.Throws<ArgumentOutOfRangeException>(() => {
				set.Get(size);
			});

			Assert.Throws<ArgumentOutOfRangeException>(() => {
				set.Set(size);
			});

			Assert.Throws<ArgumentOutOfRangeException>(() => {
				set.Clear(size);
			});
		}
	}

	[Test]
	public void Bitmask_FirstSetFirstClear()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);

			Assert.That(set.FirstSet(), Is.Null);
			Assert.That(set.FirstClear(), Is.EqualTo(0));

			set.Set(0);
			Assert.That(set.FirstSet(), Is.EqualTo(0));
			Assert.That(set.FirstClear(), Is.EqualTo(1));

			set.Set(size - 1);
			Assert.That(set.FirstSet(), Is.EqualTo(0));
			Assert.That(set.FirstClear(), Is.EqualTo(1));

			set.Clear(0);
			Assert.That(set.FirstSet(), Is.EqualTo(size - 1));
			Assert.That(set.FirstClear(), Is.EqualTo(0));

			set.SetAll();
			Assert.That(set.FirstSet(), Is.EqualTo(0));
			Assert.That(set.FirstClear(), Is.Null);
		}
	}

	[Test]
	public void Bitmask_SetAllClearAll()
	{
		foreach (var size in _Sizes) {
			var set = new Bitmask(size);

			set.SetAll();
			Assert.That(set.Count, Is.EqualTo(size));
			Assert.That(set.FirstSet(), Is.EqualTo(0));
			Assert.That(set.FirstClear(), Is.Null);

			set.ClearAll();
			Assert.That(set.Count, Is.EqualTo(0));
			Assert.That(set.FirstSet(), Is.Null);
			Assert.That(set.FirstClear(), Is.EqualTo(0));
		}
	}
}
