﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Tests;


// Tests for Boxed<T>
internal class BoxedTests
{
	private record struct CustomStruct(int Field1, float Field2);
	
	
	[Test]
	public void Boxed_Ctor()
	{
		Assert.That(new Boxed<int>().Value, Is.EqualTo(0));
		Assert.That(new Boxed<int>(10).Value, Is.EqualTo(10));
		
		Assert.That(new Boxed<CustomStruct>().Value, Is.EqualTo(new CustomStruct()));
		Assert.That(new Boxed<CustomStruct>(new(0, 1)).Value, Is.EqualTo(new CustomStruct(0, 1)));
	}

	[Test]
	public void Boxed_ToString()
	{
		Assert.That(new Boxed<int>(10).ToString(), Is.EqualTo(10.ToString()));
	}

	[Test]
	public void Boxed_Equals()
	{
		Boxed<int> b1 = new(), b2 = new(10), b3 = new(10);
		
		Assert.True(b1.Equals(b1));
		Assert.False(b1.Equals(b2));
		Assert.True(b2.Equals(b2));
		Assert.True(b2.Equals(b3));
		
		Assert.True(b1.Equals((object?)b1));
		Assert.True(b2.Equals(b2.Value));
	}

	[Test]
	public void Boxed_EqualityOperators()
	{
		Boxed<int> b1 = new(), b2 = new(10), b3 = new(10);
		
		#pragma warning disable CS1718
		Assert.True(b1 == b1);
		Assert.False(b1 == b2);
		Assert.True(b2 == b2);
		Assert.True(b2 == b3);
		
		Assert.False(b1 != b1);
		Assert.True(b1 != b2);
		Assert.False(b2 != b2);
		Assert.False(b2 != b3);
		
		Assert.True(b2 == b2.Value);
		Assert.True(b2.Value == b2);
		
		Assert.False(b2 != b2.Value);
		Assert.False(b2.Value != b2);
		#pragma warning restore CS1718
	}

	[Test]
	public void Boxed_ImplicitValueCast()
	{
		Boxed<int> box = new(10);
		assertValueEquals(box, box);
		return;

		static void assertValueEquals(Boxed<int> box, int value) => Assert.That(box.Value, Is.EqualTo(value));
	}
}
