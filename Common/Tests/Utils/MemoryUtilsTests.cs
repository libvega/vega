﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Tests;


// Tests for MemoryUtils
internal unsafe class MemoryUtilsTests
{
	[Test]
	public void MemoryUtils_ZeroMemory()
	{
		var intArr = new int[999];

		fixed (int* intPtr = intArr) {
			foreach (var len in (int[])[1, 3, 8, 15, 32, 100, 999]) {
				var intData = intArr.AsSpan(0, len);
				fillMemory(intData);
			
				Assert.True(hasNonZero(intData));
				MemoryUtils.ZeroMemory(intPtr, (uint)(len * sizeof(int)));
				Assert.False(hasNonZero(intData));
			}
		}

		return;

		static void fillMemory(Span<int> data)
		{
			for (var i = 0; i < data.Length; ++i) data[i] = i * 2 + 1;
		}
		static bool hasNonZero(Span<int> data)
		{
			for (var i = 0; i < data.Length; ++i) {
				if (data[i] != 0) return true;
			}
			return false;
		}
	}
}
