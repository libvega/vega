﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

#pragma warning disable CS0649 // Field is never assigned to, and will always have its default value

namespace Astrum.Tests;


// Tests for NativeFunctionAttribute
internal unsafe class NativeFunctionAttributeTests
{
	private nint _handle = default;
	private NativeFunctions _funcs = new();

	[OneTimeSetUp]
	public void Setup()
	{
		var libName = OperatingSystem.IsWindows() ? "AstrumCommonTests.dll" : "libAstrumCommonTests.so";
		libName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, libName);
		_handle = NativeLibrary.Load(libName);
	}

	[OneTimeTearDown]
	public void TearDown() => NativeLibrary.Free(_handle);


	[Order(0)]
	[Test]
	public void NativeFunctionAttribute_Resolve()
	{
		NativeFunctionAttribute.Resolve(_handle, _funcs);
		Assert.NotZero((nuint)_funcs.AddNumbers);
		Assert.NotZero((nuint)_funcs.IncPtr);
		Assert.Zero((nuint)_funcs.DoesNotExist);
	}

	[Order(1)]
	[Test]
	public void NativeFunctionAttribute_Call()
	{
		Assert.That(_funcs.AddNumbers(3, 5), Is.EqualTo(8));
		ulong value = 10;
		_funcs.IncPtr(&value);
		Assert.That(value, Is.EqualTo(11));
	}

	[Order(2)]
	[Test]
	public void NativeFunctionAttribute_ThrowsForBadRequiredFunction() =>
		Assert.Throws<TypeLoadException>(() => NativeFunctionAttribute.Resolve(_handle, new BadNativeFunction()));


	private sealed class NativeFunctions
	{
		[NativeFunction("add_numbers")]
		public delegate* unmanaged<uint, uint, uint> AddNumbers;

		[NativeFunction("inc_ptr")]
		public delegate* unmanaged<ulong*, void> IncPtr;

		[NativeFunction("does_not_exist", Optional = true)]
		internal delegate* unmanaged<void> DoesNotExist;
	}

	private sealed class BadNativeFunction
	{
		[NativeFunction("does_not_exist", Optional = false)]
		public delegate* unmanaged<void> DoesNotExist;
	}
}
