﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Buffers;

namespace Astrum.Tests;


// Tests for RentedArray<T>
internal class RentedArrayTests
{
	[Test]
	public void RentedArray_Ctor()
	{
		using var array0 = new RentedArray<int>(0);
		Assert.That(array0.Array, Is.Not.Null);
		Assert.That(array0.Length, Is.EqualTo(0));
		Assert.That(array0.IsValid, Is.True);
		
		using var array5 = new RentedArray<int>(5u);
		Assert.That(array5.Array, Is.Not.Null);
		Assert.That(array5.Length, Is.GreaterThanOrEqualTo(5));
		Assert.That(array5.IsValid, Is.True);
		
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = new RentedArray<int>(-1));
	}

	[Test]
	public void RentedArray_CustomPools()
	{
		using var array0 = new RentedArray<int>(1);
		Assert.That(ReferenceEquals(array0.Pool, ArrayPool<int>.Shared), Is.True);

		var pool = ArrayPool<int>.Create();
		using var array1 = new RentedArray<int>(1, pool);
		Assert.That(ReferenceEquals(array1.Pool, pool), Is.True);
	}

	[Test]
	public void RentedArray_LengthField()
	{
		using var array0 = new RentedArray<int>(0);
		Assert.That(array0.Length, Is.EqualTo(array0.Array.Length));
		
		using var array5 = new RentedArray<int>(5u);
		Assert.That(array5.Length, Is.EqualTo(array5.Array.Length));
	}

	[Test]
	public void RentedArray_Dispose()
	{
		var array = new RentedArray<int>(1);
		array.Dispose();
		Assert.That(array.IsValid, Is.False);
		Assert.That(array.Array, Is.Null);
	}

	[Test]
	public void RentedArray_Indexer()
	{
		using var array = new RentedArray<int>(2);
		array[0] = 10;
		array[1u] = 100;
		Assert.That(array[0], Is.EqualTo(10));
		Assert.That(array[1], Is.EqualTo(100));
		Assert.That(array.Array[0], Is.EqualTo(10));
		Assert.That(array.Array[1], Is.EqualTo(100));
	}
}
