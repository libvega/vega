﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Tests;


// Tests for SpanUtils
internal class SpanUtilsTests
{
	[Test]
	public void SpanUtils_ToArray_EmptySpan()
	{
		Assert.That(Span<int>.Empty.ToArray(static i => i), Is.Empty);
		Assert.That(ReadOnlySpan<int>.Empty.ToArray(static i => i), Is.Empty);

		Assert.That(Span<int>.Empty.ToArray(static (i, idx) => i), Is.Empty);
		Assert.That(ReadOnlySpan<int>.Empty.ToArray(static (i, idx) => i), Is.Empty);
	}

	[Test]
	public void SpanUtils_ToArray()
	{
		Span<int> span = stackalloc int[] { 0, 1, 2, 3, 4 };
		ReadOnlySpan<string> roSpan = new[] { "hello", "world" };

		var arr = span.ToArray(static i => i * i);
		var arr2 = roSpan.ToArray(static s => s + s);

		Assert.That(arr, Is.EquivalentTo(new[] { 0, 1, 4, 9, 16 }));
		Assert.That(arr2, Is.EquivalentTo(new[] { "hellohello", "worldworld" }));
	}

	[Test]
	public void SpanUtils_ToArray_Indexer()
	{
		Span<int> span = stackalloc int[] { 0, 1, 2, 3, 4 };
		ReadOnlySpan<string> roSpan = new[] { "hello", "world" };

		var arr = span.ToArray(static (i, idx) => i + idx);
		var arr2 = roSpan.ToArray(static (s, idx) => s + idx);

		Assert.That(arr, Is.EquivalentTo(new[] { 0, 2, 4, 6, 8 }));
		Assert.That(arr2, Is.EquivalentTo(new[] { "hello0", "world1" }));
	}
}
