﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Tests;


// Tests for StringUtils
internal class StringUtilsTests
{
	[Test]
	public void StringUtils_TrimOrPadLeft()
	{
		Assert.That("".TrimOrPadLeft(0), Is.EqualTo(""));
		Assert.That("".TrimOrPadLeft(5), Is.EqualTo("     "));

		Assert.That("Test".TrimOrPadLeft(2), Is.EqualTo("Te"));
		Assert.That("Test".TrimOrPadLeft(4), Is.EqualTo("Test"));
		Assert.That("Test".TrimOrPadLeft(6), Is.EqualTo("  Test"));

		Assert.That("Test".TrimOrPadLeft(2, '*'), Is.EqualTo("Te"));
		Assert.That("Test".TrimOrPadLeft(4, '*'), Is.EqualTo("Test"));
		Assert.That("Test".TrimOrPadLeft(6, '*'), Is.EqualTo("**Test"));
	}

	[Test]
	public void StringUtils_TrimOrPadRight()
	{
		Assert.That("".TrimOrPadRight(0), Is.EqualTo(""));
		Assert.That("".TrimOrPadRight(5), Is.EqualTo("     "));

		Assert.That("Test".TrimOrPadRight(2), Is.EqualTo("Te"));
		Assert.That("Test".TrimOrPadRight(4), Is.EqualTo("Test"));
		Assert.That("Test".TrimOrPadRight(6), Is.EqualTo("Test  "));

		Assert.That("Test".TrimOrPadRight(2, '*'), Is.EqualTo("Te"));
		Assert.That("Test".TrimOrPadRight(4, '*'), Is.EqualTo("Test"));
		Assert.That("Test".TrimOrPadRight(6, '*'), Is.EqualTo("Test**"));
	}
}
