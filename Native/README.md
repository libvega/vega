## Native

This directory contains the native dependencies used by Astrum projects, including pre-compiled binaries, source, and build scripts.

For native projects that need to be built themselves, instructions are given in this document for how to set up and execute the build. If you ever update the native dependencies, make sure to update this document as well.

For dependencies that use CMake, the tables below only show parameters that are different than their defaults. If a parameter is not in the table, leave it as the default value.

### [GLFW](https://www.glfw.org/)

- Use: Windowing and Peripheral Input
- Version: 3.3.8 (`7482de6`)
- CMake Config:
  - Windows: Generator=`Visual Studio 17 2022`, Platform=`x64`
  - Linux: Generator=`Unix Makefiles`, Platform=`Ubuntu 20.04`, Compiler=`GCC 9.4.0`

|Param|Setting|Param|Setting|
|:---:|:-----:|:---:|:-----:|
|BUILD\_SHARED\_LIBS|**On**|\_BUILD\_DOCS|**Off**|
|\_BUILD\_EXAMPLES|**Off**|\_BUILD\_TESTS|**Off**|
|\_INSTALL|**Off**|||
||*Windows*|||
|CMAKE\_CONFIGURATION\_TYPES|`Release`|||
||*Linux*|||
|CMAKE\_BUILD\_TYPE|`Release`|||

### [VMA](https://gpuopen.com/vulkan-memory-allocator/)

- Use: Vulkan Memory Allocations
- Version: 3.0.1 (`a6bfc23`)
- Compiler Config:
  - Windows: `Visual Studio 17 2022`
  - Linux: `Ubuntu 20.04`, `GCC 9.4.0`
- Build:
  - Windows: Run `Src/VMA/build.bat` with a system that has ***64-bit*** `cl.exe` (such as "x64 Native Tools Command Prompt")
  - Linux: Run `Src/VMA/build.sh` with a system that has `g++`

### AstrumCommonTests

- Use: Testing native library functionality in `Astrum.Common.Tests`
- Compiler Config:
  - Windows: `Visual Studio 17 2022`
  - Linux: `Ubuntu 20.04`, `GCC 9.4.0`
- Build:
  - Windows: Run `Src/AstrumCommonTests/build.bat` with a system that has ***64-bit*** `cl.exe` (such as "x64 Native Tools Command Prompt")
  - Linux: Run `Src/AstrumCommonTests/build.sh` with a system that has `g++`
