@rem
@rem Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
@rem This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
@rem the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
@rem

@echo off

cl /LD src.cpp /link /out:../../Lib/win-x64/AstrumCommonTests.dll
del *.lib *.exp *.obj
