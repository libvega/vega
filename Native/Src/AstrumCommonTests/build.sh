#!/bin/sh
#
# Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
# This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
# the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
#

g++ -shared -o ../../Lib/linux-x64/libAstrumCommonTests.so -fPIC -m64 src.cpp
