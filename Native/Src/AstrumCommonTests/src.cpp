/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2023
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#if defined(_MSC_VER)
#   define EXPORT_SYM __declspec(dllexport)
#else
#   define EXPORT_SYM __attribute__((visibility("default")))
#endif

#include <cstdint>

extern "C"
{

EXPORT_SYM uint32_t add_numbers(uint32_t x, uint32_t y) { 
    return x + y; 
}

EXPORT_SYM void inc_ptr(uint64_t* ptr) {
    *ptr += 1;
}

}
