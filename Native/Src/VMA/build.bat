@rem
@rem Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
@rem This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
@rem the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
@rem

@echo off

cl /LD /O2 vma_impl.cpp /link /out:../../Lib/win-x64/vma.dll
del *.lib *.exp *.obj
