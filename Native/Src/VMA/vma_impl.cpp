/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#if defined(_MSC_VER)
#   define EXPORT_SYM __declspec(dllexport)
#else
#   define EXPORT_SYM __attribute__((visibility("default")))
#endif

#include <cstdint>

#define VMA_IMPLEMENTATION
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#define VMA_DEDICATED_ALLOCATION 1
#define VMA_BIND_MEMORY2 1
#define VMA_MEMORY_BUDGET 0
#define VMA_BUFFER_DEVICE_ADDRESS 1
#define VMA_MEMORY_PRIORITY 0
#define VMA_EXTERNAL_MEMORY 0
#define WIN32_LEAN_AND_MEAN
#include "../../../Deps/Vulkan-Headers/include/vulkan/vulkan.h"
#include "../../../Deps/VulkanMemoryAllocator/include/vk_mem_alloc.h"


/// Contains the set of core application memory types
struct MemoryTypes
{
    uint32_t device;
    uint32_t upload;
    uint32_t readback;
    uint32_t dynamic;
    uint32_t lazy; // == UINT32_MAX if not present
};


extern "C"
{

/////
EXPORT_SYM VkResult CreateAllocator(
    VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device, 
    PFN_vkGetInstanceProcAddr _vkGetInstanceProcAddr, PFN_vkGetDeviceProcAddr _vkGetDeviceProcAddr,
    VmaAllocator* pAllocator)
{
    // Create info
    VmaAllocatorCreateInfo createInfo = {};
    createInfo.flags = VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT | VMA_ALLOCATOR_CREATE_KHR_BIND_MEMORY2_BIT | 
        VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    createInfo.physicalDevice = physicalDevice;
    createInfo.device = device;
    createInfo.instance = instance;
    createInfo.vulkanApiVersion = VK_API_VERSION_1_1;

    // Function pointers
    VmaVulkanFunctions functions = {};
    functions.vkGetInstanceProcAddr = _vkGetInstanceProcAddr;
    functions.vkGetDeviceProcAddr = _vkGetDeviceProcAddr;
    createInfo.pVulkanFunctions = &functions;

    // Create
    return vmaCreateAllocator(&createInfo, pAllocator);
}

/////
EXPORT_SYM void DestroyAllocator(VmaAllocator allocator) 
{
    if (!allocator) return;
    vmaDestroyAllocator(allocator);
}

/////
EXPORT_SYM void FindMemoryTypes(VmaAllocator allocator, MemoryTypes* memoryTypes)
{
    VmaAllocationCreateInfo createInfo = {};
    createInfo.usage = VMA_MEMORY_USAGE_UNKNOWN;
    createInfo.memoryTypeBits = UINT32_MAX;

    // Device
    createInfo.flags = 0;
    createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    createInfo.preferredFlags = 0;
    assert(vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &memoryTypes->device) == VK_SUCCESS && "Failed to find DEVICE memory");

    // Upload
    createInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
    createInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    createInfo.preferredFlags = 0;
    assert(vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &memoryTypes->upload) == VK_SUCCESS && "Failed to find UPLOAD memory");

    // Readback
    createInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT;
    createInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    createInfo.preferredFlags = VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
    if (vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &memoryTypes->readback) != VK_SUCCESS) {
        memoryTypes->readback = memoryTypes->upload;
    }

    // Dynamic
    createInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
    createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | 
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    createInfo.preferredFlags = 0;
    if (vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &memoryTypes->dynamic) != VK_SUCCESS) {
        memoryTypes->dynamic = memoryTypes->upload;
    }

    // Lazy
    createInfo.flags = 0;
    createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
    createInfo.preferredFlags = 0;
    if (vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &memoryTypes->lazy) != VK_SUCCESS) {
        memoryTypes->lazy = UINT32_MAX;
    }
}

/////
EXPORT_SYM VkResult CreateImage(VmaAllocator allocator, const VkImageCreateInfo* pCreateInfo, uint32_t memoryIndex, VkBool32 dedicated,
    VkImage* pImage, VmaAllocation* pAllocation)
{
    VmaAllocationCreateInfo createInfo = {};
    createInfo.flags = dedicated ? VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT : VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT;
    createInfo.usage = VMA_MEMORY_USAGE_UNKNOWN; // Allows easy selection of single memoryTypeBits
    createInfo.memoryTypeBits = 1u << memoryIndex;

    return vmaCreateImage(allocator, pCreateInfo, &createInfo, pImage, pAllocation, nullptr);
}

/////
EXPORT_SYM void DestroyImage(VmaAllocator allocator, VkImage image, VmaAllocation allocation)
{
    if (!image || !allocation) return;
    vmaDestroyImage(allocator, image, allocation);
}

/////
EXPORT_SYM VkResult CreateBuffer(VmaAllocator allocator, const VkBufferCreateInfo* pCreateInfo, uint32_t memoryIndex, VkBool32 dedicated,
    VkBool32 mapped, VkBuffer* pBuffer, VmaAllocation* pAllocation, void** ppMapping)
{
    VmaAllocationCreateInfo createInfo = {};
    createInfo.flags = 
        (dedicated ? VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT : VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT) |
        (mapped ? VMA_ALLOCATION_CREATE_MAPPED_BIT : 0);
    createInfo.usage = VMA_MEMORY_USAGE_UNKNOWN; // Allows easy selection of single memoryTypeBits
    createInfo.memoryTypeBits = 1u << memoryIndex;

    VmaAllocationInfo allocInfo;
    const auto res = vmaCreateBuffer(allocator, pCreateInfo, &createInfo, pBuffer, pAllocation, &allocInfo);
    if (res != VK_SUCCESS) return res;
    *ppMapping = mapped ? allocInfo.pMappedData : nullptr;
    return res;
}

/////
EXPORT_SYM void DestroyBuffer(VmaAllocator allocator, VkBuffer buffer, VmaAllocation allocation)
{
    if (!buffer || !allocation) return;
    vmaDestroyBuffer(allocator, buffer, allocation);
}

}
