# [Astrum](https://gitlab.com/libastrum/astrum)

A .NET graphics middleware for games, tools, and visualizations. Powered by Vulkan.

## Overview

Astrum is a middleware platform for games and visualizations, providing access to graphics, compute, audio, input, and windowing. It is not a full-fledged game engine, instead giving the user a set of powerful components and relying on them to tie together the logic for the app to fit their specific needs. This highly-customizable design is influnced by projects like [MonoGame](https://www.monogame.net/) and [Cocos2d-x](https://www.cocos.com/en/).

Astrum is written in C# 12 and is designed for .NET 8, taking advantage of the features available in these versions. While it does rely on some native dependencies, the library itself is written in C#, taking advantage of low-level optimizations and careful design to minimize allocations.

Astrum contains two additional major sub-projects:
- *Spectra* - A custom GPU shader language designed for Astrum.
- *Fusor* - An extensible pipeline and associated tool for processing and optimizing assets.

## Platforms

All platforms that Astrum supports are 64-bit only.

#### Current Platforms
Platforms that are actively supported.

- Desktop Windows
- Desktop Linux (X11/xcb)

#### Planned Platforms
Platforms that we intend to support in the future.

- Steam Deck (Vulkan)
- VR Windows/Linux (Vulkan)
- Android (Vulkan)
- iOS (MoltenVK)
- macOS (MoltenVK)

## Contributors

Note that the Vulkan SDK is required for developers. The SDK is *not* required for end users. The lowest tested version of the Vulkan SDK is 1.2.182.0.

If you want to contribute to Astrum development, please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## Credits

Astrum and the assocated projects utilize the following third-party products:

- [GLFW](https://www.glfw.org) ([License](https://github.com/glfw/glfw/blob/master/LICENSE.md))
- [Antlr](https://www.antlr.org/index.html) ([License](https://github.com/antlr/antlr4/blob/dev/LICENSE.txt))
- [VMA](https://gpuopen.com/vulkan-memory-allocator/) ([License](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/blob/master/LICENSE.txt))

More details on the native dependencies can be found in the `Native/` folder in the repo.

## Legal

Astrum is licensed under the [Microsoft Public License (Ms-PL)](https://choosealicense.com/licenses/ms-pl/). This is a permissive OSS license close to the MIT license, but with an extra clause that modifications to the Astrum source (*if also made open source*) need to be under a license compatible with the Ms-PL.

All third-party libraries and projects are utilized within their original licenses, and, where applicible, are rehosted under the original licenses as well. These licenses are available in at the links above. These third party libraries belong solely to their original authors.
