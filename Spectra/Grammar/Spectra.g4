///
/// Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
/// This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
/// the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
///

parser grammar Spectra;
options {
    tokenVocab = SpectraLexer;
}

@header {
/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */
}


/////
// Top-Level Shader Statements
completeShader
    : shaderHeaderStatement shaderTopLevelStatement* EOF
    ;

// Header shader block
shaderHeaderStatement
    : '@shader' Kind=IDENTIFIER ';'
    ;

// All top-level statements
shaderTopLevelStatement
    : shaderConstantDefinition
    | structDefinition
    | sharedDeclaration
    | uniformOrPushConstantDeclaration
    | bindingDeclaration
    | localDeclaration
    | vertexInput
    | fragmentOutput
    | userFunction
    | stageFunction
    ;

// Shader constant definition
shaderConstantDefinition
    : variableDeclaration ';'
    ;

// Struct type definition
structDefinition
    : '@struct' Name=IDENTIFIER '{' Fields+=structField* '}' ';'
    ;
structField
    : variableDeclaration ';'                                                                     #normalField
    | '@struct' '{' Fields+=structField* '}' Name=IDENTIFIER ('[' ArraySize=expression ']')? ';'  #nestedStructField
    ;

// Shared block declaration
sharedDeclaration
    : '@shared' '{' (Fields+=variableDeclaration ';')* '}' ';'
    ;

// Uniform declaration
uniformOrPushConstantDeclaration
    : ('@uniform'|'@push') (TypeName=IDENTIFIER | ('{' Fields+=structField* '}')) Name=IDENTIFIER ';'
    ;

// Binding declaration
bindingDeclaration
    : 'bind' '(' Index=INTEGER_LITERAL ')' variableDeclaration ';'
    ;

// Local declaration
localDeclaration
    : 'local' 'flat'? variableDeclaration ';'
    ;

// Vertex input
vertexInput
    : 'in' '(' Use=VERTEX_SEMANTIC ')' variableDeclaration ';'
    ;

// Fragment output
fragmentOutput
    : 'out' '(' Index=INTEGER_LITERAL ')' variableDeclaration ';'
    ;

// User-defined functions
userFunction
    : '@func' RetType=typeName Name=IDENTIFIER (('(' ')')|('(' Args+=functionArg (',' Args+=functionArg)* ')')) statementBlock
    ;
functionArg
    : ('ref'|'out')? variableDeclaration
    ;

// Shader stage functions
stageFunction
    : Stage=KW_STAGE statementBlock
    ;


/////
// Statements
statement
    : funcOrCtorCall ';'       #callStatement
    | variableDeclaration ';'  #variableDeclarationStatement
    | assignment ';'           #assignStatement
    | ifBlock                  #ifStatement
    | whileLoop                #whileStatement
    | forLoop                  #forLoopStatement
    | controlFlow ';'          #controlFlowStatement
    ;
statementBlock
    : '{' Statements+=statement* '}'
    ;

// Variable declaration (optionally with initial value)
variableDeclaration
    : Type=typeName Variables+=variableDeclOrDef (',' Variables+=variableDeclOrDef)*
    ;
variableDeclOrDef
    : Name=IDENTIFIER ('=' Value=expression)?
    ;

// Assignment to an lvalue
assignment
    : LValue=lvalue Op=('='|'+='|'&='|'/='|'<<='|'%='|'*='|'|='|'>>='|'-='|'^=') Value=expression
    ;
lvalue
    : '(' lvalue ')'
    | lvalue '.' Member=IDENTIFIER
    | lvalue '[' Index=expression ']'
    | Name=IDENTIFIER
    ;

// if/elif/else statements
ifBlock
    : 'if' '(' Cond=expression ')' statementBlock elifBlock* elseBlock?
    ;
elifBlock
    : 'elif' '(' Cond=expression ')' statementBlock
    ;
elseBlock
    : 'else' statementBlock
    ;

// While and Do-While loops
whileLoop
    : 'while' '(' Cond=expression ')' statementBlock
    ;

// For loop type
forLoop
    : 'for' '(' LoopVar=variableDeclaration ';' Cond=expression ';' Update=assignment ')' statementBlock
    ;

// Control flow statments
controlFlow
    : 'break'
    | 'continue'
    | 'discard'
    | 'return' Expression=expression?
    ;

// Type name identifier
typeName
    : 'const'? ('readwrite'|'writeonly')? BaseType=IDENTIFIER ('<' GenType=IDENTIFIER '>')? ('[' ArraySize=expression ']')?
    ;


/////
// Expressions
expression
    : atom                                                       #atomExpr
    // Unary op
    | Op=('-'|'!'|'~') Right=expression                          #unaryExpr
    // Binary op
    | Left=expression Op=('*'|'/'|'%')       Right=expression    #mulDivModExpr
    | Left=expression Op=('+'|'-')           Right=expression    #addSubExpr
    | Left=expression Op=('<<'|'>>')         Right=expression    #shiftExpr
    | Left=expression Op=('<'|'>'|'<='|'>=') Right=expression    #relationExpr
    | Left=expression Op=('=='|'!=')         Right=expression    #equalityExpr
    | Left=expression Op=('&'|'|'|'^')       Right=expression    #bitwiseExpr
    | Left=expression Op=('&&'|'||'|'^^')    Right=expression    #logicalExpr
    // Ternary Operator
    | Cond=expression '?' Texpr=expression ':' Fexpr=expression  #ternaryExpr
    ;

// Atomic expressions (those with indivisible parts)
atom
    : '(' expression ')'                    #groupAtom
    | Target=atom '[' Index=expression ']'  #indexAtom
    | Target=atom '.' Member=IDENTIFIER     #memberAtom
    | funcOrCtorCall                        #callAtom
    | literal                               #literalAtom
    | IDENTIFIER                            #nameAtom
    ;

// Function or constructor call
funcOrCtorCall
    : Name=IDENTIFIER '(' (Args+=callArgument (',' Args+=callArgument)*)? ')'
    ;
callArgument
    : ('ref'|'out')? expression
    ;

// Literal expression (numeric scalars and bools)
literal
    : INTEGER_LITERAL
    | FLOAT_LITERAL
    | 'true'
    | 'false'
    ;