///
/// Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
/// This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
/// the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
///

lexer grammar SpectraLexer;


/// Keywords
KW_BIND      : 'bind' ;
KW_BREAK     : 'break' ;
KW_CONST     : 'const' ;
KW_CONTINUE  : 'continue' ;
KW_DISCARD   : 'discard' ;
KW_ELIF      : 'elif' ;
KW_ELSE      : 'else' ;
KW_FALSE     : 'false' ;
KW_FLAT      : 'flat' ;
KW_FOR       : 'for' ;
KW_FUNC      : '@func' ;
KW_IF        : 'if' ;
KW_IN        : 'in' ;
KW_LOCAL     : 'local' ;
KW_OUT       : 'out' ;
KW_PUSH      : '@push' ;
KW_READWRITE : 'readwrite' ;
KW_REF       : 'ref' ;
KW_RETURN    : 'return' ;
KW_SHADER    : '@shader' ;
KW_SHARED    : '@shared' ;
KW_STAGE     : '@kernel' | '@frag' | '@vert' ;
KW_STRUCT    : '@struct' ;
KW_TRUE      : 'true' ;
KW_UNIFORM   : '@uniform' ;
KW_WHILE     : 'while' ;
KW_WRITEONLY : 'writeonly' ;

/// Number Literals
INTEGER_LITERAL
    : '-'? DecimalLiteral [uU]?
    | HexLiteral
    ;
FLOAT_LITERAL
    : '-'? DecimalLiteral ExponentPart
    | '-'? DecimalLiteral '.' DecimalLiteral? ExponentPart?
    ;
fragment DecimalLiteral : Digit+ ;
fragment HexLiteral     : '0x' HexDigit+ ;
fragment ExponentPart   : [eE] ('-'|'+')? Digit+ ;

/// Vertex Input Semantics
VERTEX_SEMANTIC
    : '#' (Alpha | Digit)+
    ;

/// Identifiers (type, var, or function names)
IDENTIFIER
    : '$'? IdentifierPart (NamespaceSep IdentifierPart)*
    ;
fragment NamespaceSep   : '::' ;
fragment IdentifierPart : (Alpha | '_') (Alpha | Digit | '_')* ;

/// Punctuation
PUNC_COLON     : ':' ;
PUNC_COMMA     : ',' ;
PUNC_L_BRACE   : '{' ;
PUNC_L_BRACKET : '[' ;
PUNC_L_PAREN   : '(' ;
PUNC_PERIOD    : '.' ;
PUNC_QMARK     : '?' ;
PUNC_R_BRACE   : '}' ;
PUNC_R_BRACKET : ']' ;
PUNC_R_PAREN   : ')' ;
PUNC_SEMICOLON : ';' ;

/// Operators
OP_ADD           : '+' ;
OP_ASSIGN        : '=' ;
OP_ASSIGN_ADD    : '+=' ;
OP_ASSIGN_AND    : '&=' ;
OP_ASSIGN_DIV    : '/=' ;
OP_ASSIGN_LSHIFT : '<<=' ;
OP_ASSIGN_MOD    : '%=' ;
OP_ASSIGN_MUL    : '*=' ;
OP_ASSIGN_OR     : '|=' ;
OP_ASSIGN_RSHIFT : '>>=' ;
OP_ASSIGN_SUB    : '-=' ;
OP_ASSIGN_XOR    : '^=' ;
OP_BIN_AND       : '&' ;
OP_BIN_NOT       : '~' ;
OP_BIN_OR        : '|' ;
OP_BIN_XOR       : '^' ;
OP_DIV           : '/' ;
OP_EQUAL         : '==' ;
OP_GEQUAL        : '>=' ;
OP_GREATER       : '>' ;
OP_LEQUAL        : '<=' ;
OP_LESS          : '<' ;
OP_LOG_AND       : '&&' ;
OP_LOG_NOT       : '!' ;
OP_LOG_OR        : '||' ;
OP_LOG_XOR       : '^^' ;
OP_LSHIFT        : '<<' ;
OP_MOD           : '%' ;
OP_MUL           : '*' ;
OP_NEQUAL        : '!=' ;
OP_RSHIFT        : '>>' ;
OP_SUB           : '-' ;

/// Whitespace and Comments
WS
    : [ \t\r\n\u000C] + -> channel(HIDDEN)
    ;
COMMENT
    : '/*' .*? '*/' -> channel(HIDDEN)
    ;
LINE_COMMENT
    : '//' ~[\r\n]* (('\r'? '\n') | EOF) -> channel(HIDDEN)
    ;

/// Character Fragments
fragment AlphaLower : [a-z] ;
fragment AlphaUpper : [A-Z] ;
fragment Alpha      : AlphaUpper | AlphaLower ;
fragment Digit      : [0-9] ;
fragment HexDigit   : [a-fA-F0-9] ;
