#!/bin/sh
#
# Microsoft Public License (Ms-PL) - Copyright (c) 2024 Vega Authors
# This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
# the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
#

java                                 \
    -jar ./antlr-4.13.1-complete.jar \
    -no-listener                     \
    -visitor                         \
    -o ../Src/Grammar                \
    -package Spectra.Grammar         \
    -Xexact-output-dir               \
    -Dlanguage=CSharp                \
    SpectraLexer.g4

java                                 \
    -jar ./antlr-4.13.1-complete.jar \
    -no-listener                     \
    -visitor                         \
    -o ../Src/Grammar                \
    -package Spectra.Grammar         \
    -Xexact-output-dir               \
    -Dlanguage=CSharp                \
    Spectra.g4
