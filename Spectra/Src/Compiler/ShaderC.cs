﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Spectra.Compiler;


// Runtime interface to the native ShaderC library
internal static class ShaderC
{
	// If the ShaderC binary was found and loaded
	public static bool IsAvailable => _LibraryHandle != 0;
	// The library handle
	private static readonly nint _LibraryHandle;
	
	// static Ctor
	static ShaderC()
	{
		// Check for the SDK
		var sdkPath =
			Environment.GetEnvironmentVariable("VULKAN_SDK") ?? Environment.GetEnvironmentVariable("VK_SDK_PATH");
		if (sdkPath is null || !Directory.Exists(sdkPath = Path.GetFullPath(sdkPath))) {
			_LibraryHandle = 0;
			return;
		}
		
		// Try to load the native library
		foreach (var libPath in getLibraryCheckPaths(sdkPath)) {
			if (!NativeLibrary.TryLoad(libPath, out var libHandle)) continue;
			_LibraryHandle = libHandle;
			break;
		}
		if (_LibraryHandle == 0) return;
		
		// Load the functions
		// TODO

		return;
		
		// Library path enumerator
		static IEnumerable<string> getLibraryCheckPaths(string sdkPath)
		{
			// TODO: make this more exhaustive
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
				yield return Path.Combine(sdkPath, "Bin", "shaderc_shared.dll");
				yield return Path.Combine(sdkPath, "Bin", "shaderc.dll");
			}
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
				yield return Path.Combine(sdkPath, "lib", "libshaderc_shared.so");
				yield return Path.Combine(sdkPath, "lib", "libshaderc.so");
			}
			else {
				// TODO: look into adding macOS support
			}
		}
	}
}
