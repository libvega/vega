﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using Spectra.Parsing;
using Spectra.Reflection;

namespace Spectra.Compiler;


// Performs compilation of a parsed spectra shader (description -> GLSL -> SPIR-V)
internal sealed class ShaderCompiler
{
	// API call for running the compiler on a build shader description
	public static Task<ShaderProgram> CompileAsync(ShaderDescription shader, CancellationToken token)
	{
		return Task.FromResult<ShaderProgram>(null!);
	}
}
