﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Antlr4.Runtime;

namespace Spectra.Parsing;


// Base interface for results that are produced by visiting Spectra parse nodes
internal interface IVisitResult
{
	// The parser context associated with the result, if any
	ParserRuleContext? ParserContext { get; }
	
	
	// Result type for "void" or "unit" results
	public sealed record None : IVisitResult
	{
		ParserRuleContext? IVisitResult.ParserContext => null;
	}
}
