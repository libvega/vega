﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using Antlr4.Runtime;

namespace Spectra.Parsing;


// Error listener and reporter for Spectra parsing (main reporter for grammar errors)
internal sealed class ParserErrorReporter(SpectraParser parser) : BaseErrorListener, IAntlrErrorListener<int>
{
	// Parser using the reporter
	public readonly SpectraParser Parser = parser;

	
	public override void SyntaxError(TextWriter output, IRecognizer recognizer, IToken? offendingSymbol, int line, 
		int charPositionInLine, string msg, RecognitionException? e)
	{
		// Extract additional error information
		RuleContext? ctx = null;
		var badText = offendingSymbol?.Text;
		if (e is not null) {
			ctx = e.Context;
			badText ??= e.OffendingToken?.Text;
		}
		var ruleIdx = ctx?.RuleIndex;

		// Create customized error text (TODO: expand this over time)
		//   This is the central location for reporting lexing/parsing errors
		string errorMsg;
		if (badText == "<EOF>") {
			errorMsg = "unexpected end-of-file encountered";
		}
		else {
			errorMsg = $"(Rule {(ruleIdx.HasValue ? recognizer.RuleNames[ruleIdx.Value] : "none")}) - {msg}";
		}

		// Set the error
		throw new ShaderError(errorMsg, (uint)line, (uint)charPositionInLine, badText);
	}
	
	public void SyntaxError(TextWriter output, IRecognizer recognizer, int offendingSymbol, int line, 
		int charPositionInLine, string msg, RecognitionException? e) =>
		SyntaxError(output, recognizer, e?.OffendingToken, line, charPositionInLine, msg, e);
}
