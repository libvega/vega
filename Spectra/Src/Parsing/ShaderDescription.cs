﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Spectra.Parsing;


// Represents an in-memory description of a shader program metadata, API, and syntax tree
//   Allows progressive construction of a shader with validation
internal sealed class ShaderDescription
{
	
}
