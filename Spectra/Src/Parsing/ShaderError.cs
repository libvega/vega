﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Spectra.Parsing;


// Exception type for reporting errors during shader parsing or generation
internal sealed class ShaderError : Exception
{
	// Error position in the source (if known)
	public readonly (uint Line, uint Character)? Position;
	// VSL source causing the error (if known)
	public readonly string? BadSource;

	public ShaderError(string message) : base(message)
	{
		Position = null;
		BadSource = null;
	}

	public ShaderError(string message, uint line, uint @char, string? badSource = null)
		: base(message)
	{
		Position = (line, @char);
		BadSource = badSource;
	}


	// Converts to a CompileResult.Error instance
	public SpectraCompiler.Result.Failure ToCompileResults()
	{
		string msg;
		if (Position.HasValue) {
			msg = (BadSource is not null)
				? $"[at {Position.Value.Line}:{Position.Value.Character}] {Message} ('{BadSource}')"
				: $"[at {Position.Value.Line}:{Position.Value.Character}] {Message}";
		}
		else {
			msg = (BadSource is not null)
				? $"{Message} ('{BadSource}')"
				: Message;
		}
		return new(msg, this);
	}
}
