﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using Spectra.Grammar;

namespace Spectra.Parsing;


// Implementation of the ANTLR lexer/parser/visitor for Spectrum
internal sealed partial class SpectraParser : SpectraBaseVisitor<IVisitResult>
{
	// "void/unit" visit return type
	private static readonly IVisitResult _None = new IVisitResult.None();

	
	#region Fields
	// The original source
	private readonly string _source;
	
	// ANTLR parse objects
	private readonly CommonTokenStream _tokens;
	private readonly Grammar.Spectra _parser;
	
	// The shader description being constructed by the parser
	private ShaderDescription _shader = null!;
	#endregion // Fields

	private SpectraParser(string source)
	{
		// Setup source and parsing
		_source = source;
		AntlrInputStream inStream = new(source);
		SpectraLexer lexer = new(inStream);
		_tokens = new(lexer);
		_parser = new(_tokens);
		
		// Setup error handling
		ParserErrorReporter errorReport = new(this);
		lexer.RemoveErrorListeners();
		lexer.AddErrorListener(errorReport);
		_parser.RemoveErrorListeners();
		_parser.AddErrorListener(errorReport);
	}

	
	// Performs the parsing and shader construction
	private Task<Result> parse(CancellationToken token)
	{
		try {
			// Perform the parsing and lexing
			var shaderCtx = _parser.completeShader(); // Throws ShaderError
			token.ThrowIfCancellationRequested();

			// Run the visitor on the tree
			_ = VisitCompleteShader(shaderCtx); // Throws ShaderError
			token.ThrowIfCancellationRequested();

			// Report success
			return Task.FromResult<Result>(new Result.Success(_shader));
		}
		catch (ShaderError error) {
			return Task.FromResult<Result>(new Result.Failure(error));
		}
	}
	
	// API call for parsing source
	public static Task<Result> ParseAsync(string source, CancellationToken token)
	{
		SpectraParser parser = new(source);
		return parser.parse(token);
	}


	#region Errors
	public ShaderError Error(string msg) => new(msg);

	public ShaderError Error(IToken token, string msg)
	{
		var src = token.Text.Length <= 16 ? token.Text : $"{token.Text[..13]}...";
		return new(msg, (uint)token.Line, (uint)token.Column, src);
	}

	public ShaderError Error(ITerminalNode node, string msg) => Error(_tokens.Get(node.SourceInterval.a), msg);

	public ShaderError Error(ParserRuleContext ctx, string msg) => Error(ctx.Start, msg);

	public ShaderError Error(IVisitResult result, string msg) =>
		result.ParserContext is { } ctx ? Error(ctx, msg) : Error(msg);
	#endregion // Errors
	
	
	// Parsing results
	public abstract record Result
	{
		private Result() { }
		
		// Failed parse with the associated error
		public sealed record Failure(ShaderError Error) : Result;
		
		// Successful parse with the resulting shader description
		public sealed record Success(ShaderDescription Shader) : Result;
	}
}
