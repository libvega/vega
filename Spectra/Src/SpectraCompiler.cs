﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) The Astrum Authors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Spectra.Compiler;
using Spectra.Parsing;
using Spectra.Reflection;

namespace Spectra;


// Core API for running shader compilation tasks
internal static class SpectraCompiler
{
	// Execute the compilation process on a stream that contains Spectra source to the end
	public static async Task<Result> CompileAsync(Stream source, CancellationToken token)
	{
		// Check for ShaderC
		if (!ShaderC.IsAvailable) {
			return new Result.Failure("ShaderC is not available (ensure the Vulkan SDK is installed)", null);
		}
		
		// Read the source
		string sourceCode;
		using (var reader = new StreamReader(source)) {
			sourceCode = await reader.ReadToEndAsync(token);
		}

		try {
			// Perform parsing on the spectra source
			var result = await SpectraParser.ParseAsync(sourceCode, token);
			if (result is SpectraParser.Result.Failure { Error: var parseError }) {
				return new Result.Failure(parseError.Position is { } errPos
					? $"Error (at {errPos.Line}:{errPos.Character} - '{parseError.BadSource}'): {parseError.Message}"
					: $"Error: {parseError.Message}", 
					null
				);
			}
			
			// Perform compilation on the shader description
			var shader = await ShaderCompiler.CompileAsync(((SpectraParser.Result.Success)result).Shader, token);
			return new Result.Success(shader);
		}
		catch (TaskCanceledException ex) {
			return new Result.Failure("The compilation was cancelled", ex);
		}
		catch (OperationCanceledException ex) {
			return new Result.Failure("The compilation was cancelled", ex);
		}
		catch (Exception ex) {
#if DEBUG
			await Console.Error.WriteLineAsync(ex.StackTrace);
#endif
			return new Result.Failure($"Unhandled compile exception ({ex.GetType()}): {ex.Message}", ex);
		}
	}


	// Describes the result of a shader compilation process
	public abstract record Result
	{
		private Result() { }
		
		// Failed compilation, with a human readable error message and an optional associated exception
		public sealed record Failure(string Message, Exception? Exception) : Result;
		
		// Successful compilation with the compiled shader program object
		public sealed record Success(ShaderProgram Program) : Result;
	}
}
